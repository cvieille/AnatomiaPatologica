﻿<%@ Page Title="Resumen de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ResumenBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_ResumenBiopsia" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:TextBox ID="txt_id" runat="server" Width="51px" Visible="False"></asp:TextBox>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Codificación de Muestra</h2>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-hover table-responsive">

                        <tr>
                            <td>
                                <label>Total 08-01-001</label>
                                <asp:TextBox ID="txt_0801001" runat="server" CssClass="form-control" type="number"
                                    ToolTip="Citodiagnóstico corriente (PAP) " MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-002</label>
                                <asp:TextBox ID="txt_0801002" runat="server" CssClass="form-control"  type="number"
                                    ToolTip="Citología aspirativa (por punción)" MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-003</label>
                                <asp:TextBox ID="txt_0801003" runat="server"  CssClass="form-control"  type="number"
                                    ToolTip="Estudio histopatológico con microscopía electrónica (por cada órgano)" MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-004</label>
                                <asp:TextBox ID="txt_0801004" runat="server"  CssClass="form-control" MaxLength="3"  type="number"
                                    ToolTip="Est. histopatológico c/técnicas de inmunohistoquímica o inmunofluorescencia (por cada órgano)">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-005</label>
                                <asp:TextBox ID="txt_0801005" runat="server"  CssClass="form-control"  type="number"
                                    ToolTip="Estudio histopatológico con técnicas histoquímicas especiales (incluye descalcificación) (por cada órgano)"
                                    MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-006</label>
                                <asp:TextBox ID="txt_0801006" runat="server"  CssClass="form-control" type="number"
                                    ToolTip="Est. histopatol. de biopsia contemporánea (ráp.) a intervenc. quirúrgicas (X cada órgano, no incl. biopsia diferida)"
                                    MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-007</label>
                                <asp:TextBox ID="txt_0801007" runat="server"  CssClass="form-control" type="number"
                                    ToolTip="Estudio histopatológico con tinción corriente de biopsia diferida con estudio seriado (mínimo 10 muestras) 
                                de un órgano o parte de él (no incluye estudio con técnica habitual de otros órganos incluidos en la muestra)"
                                    MaxLength="3">0</asp:TextBox>
                            </td>
                            <td>
                                <label>Total 08-01-008</label>
                                <asp:TextBox ID="txt_0801008" runat="server"  CssClass="form-control"  type="number"
                                    ToolTip="Estudio histopatológico corriente de biopsia diferida (por cada órgano)"
                                    MaxLength="3">0</asp:TextBox>
                            </td>
                            
                            
                        </tr>
                        <tr>
                            <td colspan="8" >
                                <asp:Button ID="cmd_GuardarCodificacion" runat="server" CssClass="btn btn-primary" Text="Guardar" />
                                <asp:Button ID="cmd_RecalcularCodificacion" runat="server" CssClass="btn btn-success" Text="Calcular" />
                            </td>
                        </tr>
                    </table>

                    <br />
                    <div style="text-align: center">
                        <asp:Label ID="lbl_MensajeCodificacion" runat="server" Style="text-align: center" class="alert alert-success" role="alert"
                            Text="Aca Van los Mensajes" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>

            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Listado de Casete</h2>
                </div>
                <br />
                <asp:GridView ID="gdv_cortes" runat="server" AutoGenerateColumns="False" CaptionAlign="Top"
                    DataKeyNames="GEN_loginUsuarios" DataSourceID="dts_cortes" EnableTheming="True"
                    HorizontalAlign="Center" RowHeaderColumn="id" CssClass="table table-bordered table-hover table-responsive">
                    <Columns>
                        <asp:TemplateField HeaderText="Sel.">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSeleccion" runat="server" Checked="true" CssClass="controlSeleccion" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia" ReadOnly="True"
                            SortExpression="ANA_IdBiopsia" Visible="False" />
                        <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Corte" ReadOnly="True" SortExpression="ANA_NomCortes_Muestras" />
                        <asp:BoundField DataField="ANA_FecCortes_Muestras" DataFormatString="{0:dd-MM-yyyy}"
                            HeaderText="Fecha" ReadOnly="True" SortExpression="ANA_FecCortes_Muestras" />
                        <asp:BoundField DataField="nombre_patologo" HeaderText="Patólogo" ReadOnly="True" SortExpression="nombre_patologo" />
                        <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Creado por" ReadOnly="True" SortExpression="GEN_loginUsuarios" />
                        <asp:BoundField DataField="ANA_EstCortes_Muestras" HeaderText="Estado" ReadOnly="True" SortExpression="ANA_EstCortes_Muestras" />
                        <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="Id. Casete" ReadOnly="True" SortExpression="ANA_IdCortes_Muestras" />
                    </Columns>
                </asp:GridView>

                <div style="width: 95%; text-align: center; margin: 0 auto; padding: 5px">
                    <asp:Button ID="cmd_seleccionar_todo" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                    <asp:Button ID="cmd_seleccionar_todo0" runat="server" CssClass="btn btn-default" Text="Quitar Selección" />
                    <br />
                    <asp:Button ID="cmd_especiales" runat="server" Text="Solicitar Especial" CssClass="btn btn-success" />
                    <asp:Button ID="cmd_imprimir" runat="server" Text="Imprimir Selección" CssClass="btn btn-primary" />
                    <asp:Button ID="cmd_solicitar_macro" runat="server" Text="Solicitar Macro" CssClass="btn btn-success" />
                    <asp:Button ID="cmd_solicitar_casete" runat="server" Text="Solicitar Casete" CssClass="btn btn-success" />
                    <asp:Button ID="cmd_inmuno" runat="server" CssClass="btn btn-success" Text="Solicitar Inmuno" />
                    <asp:Button ID="cmd_para_interconsulta" runat="server" Text="Para Interconsulta" CssClass="btn btn-warning" />
                    <asp:Button ID="cmd_enviar_almacena_casete" runat="server" Text="Enviar a Almacenamiento" CssClass="btn btn-primary" />
                </div>
                <asp:Panel ID="Panel1" runat="server" CssClass="alert alert-info" HorizontalAlign="Center" Visible="false">
                    <asp:DropDownList ID="cmb_tecnicas_especiales" runat="server" AutoPostBack="True" Visible="False" Width="380px">
                    </asp:DropDownList>
                    <asp:Button ID="cmd_agregar_especiales" runat="server" CssClass="btn btn-success" Text="Agregar Técnica Especial"
                        Visible="False" />
                </asp:Panel>
                <asp:Panel ID="pnl_inmuno" runat="server" CssClass="alert alert-info"
                    HorizontalAlign="Center" Visible="false">
                    <asp:DropDownList ID="cmb_inmuno_histoquimica" runat="server" Width="380px">
                    </asp:DropDownList>
                    <asp:Button ID="cmd_agregar_inmuno" runat="server" CssClass="btn btn-success"
                        Text="Agregar Inmunohistoquímica" />
                </asp:Panel>

                <br />
                <div style="text-align: center">
                    <asp:Label ID="lbl_mensaje" runat="server" Style="text-align: center" class="alert alert-success" role="alert"
                        Text="Aca Van los Mensajes" Visible="False"></asp:Label>
                </div>
            </div>

            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Listado de Técnicas Especiales</h2>
                </div>
                <asp:GridView ID="gdv_tecnicas" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover table-responsive"
                    DataKeyNames="ANA_UsuCrea" DataSourceID="dts_tecnicas">
                    <Columns>
                        <asp:BoundField DataField="ANA_FecTecnica" DataFormatString="{0:dd-MM-yyyy}"
                            HeaderText="Fecha Técnica" SortExpression="ANA_FecTecnica" />
                        <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" SortExpression="ANA_NomCortes_Muestras" />
                        <asp:BoundField DataField="ANA_NomTecnica" HeaderText="Técnica" SortExpression="ANA_NomTecnica" />
                        <asp:BoundField DataField="ANA_EstTecnica" HeaderText="Estado" SortExpression="ANA_EstTecnica" />
                        <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True" SortExpression="ANA_UsuCrea" />
                    </Columns>
                </asp:GridView>
            </div>

            <!-- Panel Principal de Inmunohistoquímica-->
            <div class="panel panel-default">
                <!-- Default panel contents Listado de Inmunohistoquímica-->
                <div class="panel-heading">
                    <h2 class="text-center">Listado de Inmunohistoquímica</h2>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gdv_InmunoHistoquimica" runat="server" AutoGenerateColumns="False"
                        DataKeyNames="ANA_UsuCrea" DataSourceID="dts_InmunoHistoquimica"
                        HorizontalAlign="Center" CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="ANA_FecInmuno_Histoquimica_Biopsia" DataFormatString="{0:dd-MM-yyyy}"
                                HeaderText="Fecha Inmuno" SortExpression="ANA_FecInmuno_Histoquimica_Biopsia" />
                            <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" SortExpression="ANA_NomCortes_Muestras" />
                            <asp:BoundField DataField="ANA_NomInmuno" HeaderText="Inmunohistoquímicas" SortExpression="ANA_NomInmuno" />
                            <asp:BoundField DataField="ANA_EstInmuno_Histoquimica_Biopsia" HeaderText="Estado" ReadOnly="true" />
                            <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True" SortExpression="ANA_UsuCrea" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_InmunoHistoquimica" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </div>
            </div>

            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Listado de Laminas</h2>
                </div>
                <br />
                <asp:GridView ID="gdv_laminas" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" DataSourceID="dts_laminas"
                    CssClass="table table-bordered table-hover table-responsive">
                    <Columns>
                        <asp:TemplateField HeaderText="Sel.">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSeleccion0" runat="server" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ANA_nomLamina" HeaderText="Lamina" SortExpression="ANA_nomLamina" />
                        <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha Lamina" SortExpression="ANA_FecLamina" />
                        <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" SortExpression="ANA_EstLamina" />
                        <asp:BoundField DataField="ANA_idLamina" HeaderText="Id. Lamina" SortExpression="ANA_idLamina" />
                    </Columns>
                </asp:GridView>


                <div style="width: 90%; text-align: center; margin: 0 auto; padding: 5px">
                    <asp:Button ID="cmd_select_all3" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                    <asp:Button ID="cmd_select_all4" runat="server" CssClass="btn btn-default" Text="Quitar Selección" />
                    <br />
                    <asp:Button ID="cmd_solicitar_casete0" runat="server" Text="Solicitar Lamina" CssClass="btn btn-success" />
                    <asp:Button ID="cmd_para_interconsulta1" runat="server" Text="Para Interconsulta" CssClass="btn btn-warning" />
                    <asp:Button ID="cmd_solicitar_casete1" runat="server" Text="Enviar a Almacenamiento" CssClass="btn btn-primary" />
                    <asp:Button ID="cmd_imprime_laminas" runat="server" Text="Imprimir Selección" Visible="False" CssClass="btn btn-primary" />
                </div>
            </div>

            <asp:SqlDataSource ID="dts_cortes" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dts_tecnicas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <asp:SqlDataSource ID="dts_laminas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>