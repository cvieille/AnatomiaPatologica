﻿Imports Newtonsoft.Json
Imports System.Web.Services

Partial Public Class frm_Cortes
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    '<WebMethod>
    'Public Shared Function secuenciaLaminas(s, cantLaminas, ANA_IdBiopsia, id_usuario)
    '    Dim nombre_lamina As String = "", NombreSecuencia As String = "", contador As Byte
    '    Dim l = JsonConvert.DeserializeObject(Of String()())(s)
    '    Dim lReturn As New List(Of String)
    '    'Me.lbl_debe_guardar.Visible = False
    '    'Image1.Visible = False
    '    For i = 0 To l.Count() - 1
    '        Dim cortes As New NegocioCortes
    '        cortes.Cortes(l(i)(0))
    '        If (cortes.Get_GEN_idTipo_Estados_Sistemas = 60 Or cortes.Get_GEN_idTipo_Estados_Sistemas = 62) Then
    '            'Tipo En inclusion o tincion y montaje
    '            '*** GENERA SECUENCIA DE LAMINAS
    '            '==========================================================
    '            For contador = 1 To cantLaminas
    '                nombre_lamina = Trim(l(i)(1)) & "-(" & contador & ")"
    '                NombreSecuencia = NombreSecuencia & nombre_lamina & "; "

    '                Dim lam As New Laminas
    '                lam.Set_ANA_IdBiopsia(ANA_IdBiopsia)
    '                lam.Set_ANA_nomLamina(nombre_lamina)
    '                lam.Set_GEN_IdUsuarios(id_usuario)
    '                lam.Set_ANA_IdCortes_Muestras(l(i)(0))
    '                lam.Set_Crea_lamina()
    '            Next

    '            '*** CREA MOVIMIENTO REALIZADO
    '            '==========================================================
    '            Dim mov As New movimientos
    '            mov.Set_GEN_IdUsuarios(id_usuario)
    '            mov.Set_ANA_IdBiopsia(ANA_IdBiopsia)
    '            mov.Set_GEN_idTipo_Movimientos_Sistemas(96)
    '            mov.Set_ANA_DetalleMovimiento("Genera Secuencia de Laminas:" & NombreSecuencia)
    '            mov.Set_CrearNuevoMoviento()

    '            '*** SE REFRESCAN LAS GRILLAS
    '            '==========================================================
    '            'CargarGrillas()
    '        Else
    '            lReturn.Add(l(i)(0))
    '            'wuc_alert.showModal(Alert._ERROR, "Solo se pueden crear laminas sobre tacos que esten en Inclusión o en Tinción")
    '        End If
    '    Next
    '    Return JsonConvert.SerializeObject(lReturn)
    'End Function

    <WebMethod>
    Public Shared Function laminasSolicitarLaminas(s As Object, id_usuario As Integer, ANA_IdBiopsia As Integer)
        Dim l = JsonConvert.DeserializeObject(Of Integer())(s)
        Dim lReturn As New List(Of String)
        For i = 0 To l.Count() - 1
            Dim NL As New NegocioLaminas
            NL.Set_ANA_idLamina(l(i))
            NL.Set_ANA_IdBiopsia(ANA_IdBiopsia)
            Dim Realizado = NL.Set_SolicitarLamina(id_usuario)
            If Realizado = False Then
                lReturn.Add(l(i))
                'wuc_alert.showModal(Alert.ADVERTENCIA, "¡Selecciono laminas que no estaban almacenadas. Se han solicitado solo las almacenadas!")
            End If
        Next
        Return JsonConvert.SerializeObject(lReturn)
    End Function

End Class