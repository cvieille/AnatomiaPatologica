﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frm_despachar_biopsia
    
    '''<summary>
    '''Control txt_biopsia_n.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_biopsia_n As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txt_biopsia_a.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_biopsia_a As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txt_id.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_id As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txt_estado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_estado As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control chk_dictado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chk_dictado As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control chk_tumoral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chk_tumoral As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control chk_rapida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chk_rapida As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control txt_organo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_organo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txt_patologo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_patologo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cmb_tipo_destino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_tipo_destino As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cmb_servicio_dest.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_servicio_dest As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control lbl_despachar1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbl_despachar1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txt_fecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_fecha As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txt_recibe_informe.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_recibe_informe As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cmd_guardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_guardar As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_guardar0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_guardar0 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control Image1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control lbl_debe_guardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbl_debe_guardar As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control SqlDataSource4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents SqlDataSource4 As Global.System.Web.UI.WebControls.SqlDataSource
End Class
