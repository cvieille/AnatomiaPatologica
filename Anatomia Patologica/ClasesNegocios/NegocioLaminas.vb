﻿Public Class NegocioLaminas
    Inherits Laminas

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================



    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================



    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================

    Public Sub Set_EliminaLaminaPorId(ByVal IdUsuario As Integer)

        Dim consulta As String = "DELETE " _
        & "ANA_Laminas " _
        & "WHERE " _
        & "ANA_idLamina=" & Me.Get_ANA_idLamina()
        ejecuta_sql(consulta)

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_GEN_IdUsuarios(IdUsuario)
        mov.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(102)
        mov.Set_ANA_DetalleMovimiento("Se elimina lamina: " & Get_ANA_NomLamina())
        mov.Set_CrearNuevoMoviento()

        
    End Sub

    Public Function Set_SolicitarLamina(ByVal IdUsuario As Integer)
        Dim L As New Laminas
        L.Laminas(Me.Get_ANA_idLamina())
        Dim Realizado As Boolean = True

        If L.Get_ANA_Almacenada() = "SI" Then
            L.Set_ANA_Solicitada("SI")
            L.Set_Update_Lamina()

            '*** CREA MOVIMIENTO REALIZADO
            '==========================================================
            Dim mov As New movimientos
            mov.Set_ANA_IdBiopsia(L.Get_ANA_IdBiopsia())
            mov.Set_ANA_DetalleMovimiento("Se solicita Lamina:" & L.Get_ANA_NomLamina())
            mov.Set_GEN_IdUsuarios(IdUsuario)
            mov.Set_CrearNuevoMoviento()
        Else
            Realizado = False
        End If

        Return Realizado
    End Function

    Public Sub Set_EnviarLaminaaInterconsulta(ByVal IdUsuario As Integer)
        Dim L As New Laminas
        Dim Realizado As Boolean = True
        Dim GlosaMovimiento As String
        L.Laminas(Me.Get_ANA_idLamina())
        If L.Get_ANA_Almacenada() = "SI" Then
            L.Set_ANA_Solicitada("SI")
            L.Set_ANA_EstLamina("Solicitada Para Interconsulta")
            GlosaMovimiento = "Se solicita Lamina:" & L.Get_ANA_NomLamina() & " para enviar a Interconsulta."
        Else
            GlosaMovimiento = "Se envia a Interconsulta Lamina:" & L.Get_ANA_NomLamina()
            L.Set_ANA_EstLamina("Para Interconsulta")
        End If

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(L.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento(GlosaMovimiento)
        mov.Set_GEN_IdUsuarios(IdUsuario)
        mov.Set_CrearNuevoMoviento()

        L.Set_Update_Lamina()

    End Sub

    Public Sub Set_EnviarLaminaaAlmacenar(ByVal IdUsuario As Integer)
        Dim L As New Laminas
        L.Laminas(Me.Get_ANA_idLamina())
        L.Set_ANA_EstLamina("Para Almacenar")
        L.Set_ANA_Solicitada("NO")
        L.Set_Update_Lamina()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_DetalleMovimiento("Se envia Lamina:" & L.Get_ANA_NomLamina() & " a Para Almacenar")
        mov.Set_ANA_IdBiopsia(L.Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(200)
        mov.Set_GEN_IdUsuarios(IdUsuario)
        mov.Set_CrearNuevoMoviento()

    End Sub

    Public Sub Set_DesecharLaminas()
        Dim l As New Laminas
        l.Laminas(Me.Get_ANA_idLamina())
        l.Set_ANA_EstLamina("Desechada")
        l.Set_ANA_Almacenada("NO")
        l.Set_ANA_Desechada("SI")
        l.Set_ANA_Solicitada("NO")
        l.Set_Update_Lamina()

        '*** AL HACER UPDATE, ASIGNA VALOR A ANA_IDBIOPSIA PARA PODER CREAR EL REGISTRO DE MOVIMIENTO.
        Me.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Se desecha corte con fecha: " & Get_FechaconHora())
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()


    End Sub

    Public Sub Set_RetirarLaminasSolicitadas()
        Dim l As New Laminas
        l.Laminas(Me.Get_ANA_idLamina())
        l.Set_ANA_Almacenada("NO")
        l.Set_ANA_Solicitada("NO")
        l.Set_ANA_EstLamina("Solicitada")
        l.Set_Update_Lamina()

        
        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Se retira lamina " & l.Get_ANA_NomLamina())
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()

    End Sub

    Public Sub Set_EliminarSolicitudesdeLaminas()
        Dim l As New Laminas
        l.Laminas(Me.Get_ANA_idLamina())
        l.Set_ANA_Solicitada("NO")
        l.Set_Update_Lamina()

        '*** AL HACER UPDATE, ASIGNA VALOR A ANA_IDBIOPSIA PARA PODER CREAR EL REGISTRO DE MOVIMIENTO.
        Me.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(198)
        mov.Set_ANA_DetalleMovimiento("Se elimina solicitud de lamina " & l.Get_ANA_NomLamina())
        mov.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()

    End Sub

    Public Sub Set_CreaLaminaIndividual(ByVal NombreLamina As String)
        Dim l As New Laminas
        l.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
        l.Set_ANA_nomLamina(NombreLamina)
        l.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
        l.Set_ANA_IdCortes_Muestras(Get_ANA_IdCortes_Muestras())
        l.Set_Crea_lamina()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos

        mov.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
        mov.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(96)
        mov.Set_ANA_DetalleMovimiento("Lamina: " & l.Get_ANA_NomLamina())
        mov.Set_CrearNuevoMoviento()

    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================



    ' *** FIN DE GET
    '==========================================================
#End Region

End Class
