﻿Imports Newtonsoft.Json

Public Class NegocioRegistroMuestra
    Inherits Registro_Biopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE
    Dim GEN_idTipo_Estados_Sistemas As Integer
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    Public Function Get_DetalleFrascosBiopsia()
        Dim consulta As String = ""

        consulta = "SELECT * FROM ANA_Detalle_Registro_Muestra WHERE ANA_estadoDetalle_Registro_Muestra = 'Activo' and ANA_IdBiopsia=" & Me.Get_ANA_IdBiopsia()

        Dim tablas As DataTable = consulta_sql_datatable(consulta)

        Dim ANA_DescripcionBiopsia As String = ""

        If tablas.Rows.Count > 0 Then
            For Each dtr As DataRow In tablas.Rows
                If ANA_DescripcionBiopsia <> "" Then ANA_DescripcionBiopsia = ANA_DescripcionBiopsia & " + "
                ANA_DescripcionBiopsia = ANA_DescripcionBiopsia & (dtr.Item("ANA_DescripcionBiopsia").ToString())

            Next
        End If
        Return ANA_DescripcionBiopsia
    End Function



    Public Sub Set_Consulta_AnulaBiopsia(ByVal motivo_anula As String)
        Dim consulta = "UPDATE ANA_Registro_Biopsias SET " _
        & "ANA_EstadoRegistro_Biopsias='Anulada', " _
        & "GEN_idTipo_Estados_Sistemas = 48, " _
        & "ANA_MotivoAnulaBiopsia='" & motivo_anula & "', " _
        & "ANA_AnuladaBiopsia='SI' " _
        & "WHERE(ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ")"
        ejecuta_sql(consulta)

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(82)
        mov.Set_ANA_DetalleMovimiento("Se anula Biopsia")
        mov.Set_CrearNuevoMoviento()


    End Sub

    Public Sub Set_Consulta_RecepcionaInterconsulta()
        '*** Cambia el estado de ana_interconsulta a NO. 
        '*** CON ESTO SE ENTIENDE QUE YA FUE RECIBIDA Y CERRADA. ADEMAS SE CREA MOVIMIENTO.
        '==========================================================
        Dim consulta As String = "UPDATE ANA_Registro_Biopsias SET " _
        & "ANA_InterconsultaBiopsia='NO', " _
        & "fec_reg_interconsulta ='" & Get_FechaconHora() & "' " _
        & "WHERE(ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ")"
        ejecuta_sql(consulta)

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Se Cierra interconsulta. Es Recepcionada e ingresada por Patólogo al Sistema")
        mov.Set_CrearNuevoMoviento()

    End Sub










    Public Sub Set_Consulta_IngresaDetalleMuestras(ByVal frascos As String, ByVal cod_detalle As String, ByVal descripcion As String, ByVal muestras As String)

        '& "ANA_FrascoBiopsia, " _
        Dim consulta As String = "INSERT INTO ANA_Detalle_Registro_Muestra (" _
        & "ANA_IdBiopsia, " _
        & "ANA_IdDetalleCatalogo_Muestras, " _
        & "ANA_DescripcionBiopsia, " _
        & "ANA_CantidadMuestras, ANA_estadoDetalle_Registro_Muestra) " _
        & "VALUES(" _
        & Me.Get_ANA_IdBiopsia() & ", " _
        & frascos & ", " _
        & cod_detalle & ", " _
        & "'" & descripcion & "', " _
        & "'" & muestras & "', 'Activo') "

        ejecuta_sql(consulta)
    End Sub

    Public Function Get_NombrePatologoValida()
        Dim consulta As String = "SELECT " _
        & "ISNULL(rtrim(GEN_nombreUsuarios),'') + ' ' + ISNULL(rtrim(GEN_apellido_paternoUsuarios),'') + ' ' + ISNULL(rtrim(GEN_apellido_maternoUsuarios),'') as Patologo " _
        & "FROM " _
        & "GEN_Usuarios " _
        & "WHERE(GEN_IdUsuarios = " & Me.Get_GEN_idUsuarioValida() & ")"
        Dim dato As String = consulta_sql_devuelve_string(consulta)
        Return dato
    End Function

    Public Sub Set_Consulta_EliminarDetalleMuestra()
        Dim dato As String = "DELETE " _
        & "FROM " _
        & "ANA_Detalle_Registro_Muestra " _
        & "WHERE(ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ")"
        ejecuta_sql(dato)
    End Sub

    Public Function Get_Cargar_Detalle_Muestra()
        Dim dato As String = "SELECT adc.ANA_IdDetalleCatalogo_Muestras, " _
        & "adc.ANA_DetalleCatalogo_Muestras, " _
        & "drm.ANA_DescripcionBiopsia, " _
        & "drm.ANA_CantidadMuestras, " _
        & "drm.ANA_IdBiopsia " _
        & "FROM ANA_Detalle_Registro_Muestra AS drm INNER JOIN " _
        & "ANA_Detalle_Catalogo_Muestras AS adc ON " _
        & "drm.ANA_IdDetalleCatalogo_Muestras = adc.ANA_IdDetalleCatalogo_Muestras INNER JOIN " _
        & "ANA_Catalogo_Muestras AS acm ON " _
        & "adc.ANA_IdCatalogo_Muestras = acm.ANA_IdCatalogo_Muestras " _
        & "WHERE(ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ")"
        Return dato
    End Function

    Public Sub Set_GeneraNotificaciondeCritico(ByVal GEN_IdUsuarios_NotificadoBiopsias_Critico As Integer, ByVal Ana_ObservacionCritico As String)
        ' *** INSTANCIA EL CONSTRUCTOR DE BIOPSIA CRITICO POR ID REGISTRO BIOPSIA
        '==========================================================
        Dim bc As New BiopsiaCritico
        bc.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())

        bc.Set_GEN_IdUsuarios_NotificadoBiopsias_Critico(GEN_IdUsuarios_NotificadoBiopsias_Critico)
        bc.Set_Ana_ObservacionCritico(Ana_ObservacionCritico)
        bc.Set_GEN_IdUsuariosCritico(Me.Get_GEN_IdUsuarios())
        bc.Set_Crea_Critico_Biopsia_db()

        Dim negbiopsia As New NegocioRegistroMuestra
        negbiopsia.Registro_Biopsia(Me.Get_ANA_IdBiopsia())
        negbiopsia.Set_ANA_CriticoBiopsia("SI")
        ' negbiopsia.Set_ModificaRegistroBiopsia()

        Dim u As New Usuarios
        u.Usuarios(Int(GEN_IdUsuarios_NotificadoBiopsias_Critico))

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_GEN_idTipo_Movimientos_Sistemas(104)
        mov.Set_ANA_DetalleMovimiento("Se Informa a " & u.Get_GEN_NombreCompleto())
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()
    End Sub



    'Public Sub InsertarRegistroMuestra()
    '    Set_ANA_IdBiopsia(Set_Consulta_CrearRegistroBiopsia())
    '    '*** CREA MOVIMIENTO REALIZADO
    '    '==========================================================
    '    Dim mov As New movimientos
    '    mov.Set_GEN_idTipo_Movimientos_Sistemas(57)
    '    mov.Set_ANA_DetalleMovimiento("Nº " & Me.Get_ANA_NumBiopsia() & "-" & Me.Get_ANA_AñoBiopsia() & " al Sistema")
    '    mov.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
    '    mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
    '    mov.Set_CrearNuevoMoviento()

    'End Sub

    '*** CONSULTA PARA PANTALLA INICIAL
    Public Function Ds_con_Biopsia_Inicio(ByVal FRecepcionInicio As Date, ByVal FRecepcionFinal As Date, ByVal RutPaciente As String, ByVal DigPaciente As String,
                                          ByVal NomPaciente As String, ByVal Ap1Paciente As String, ByVal Ap2Paciente As String, ByVal TieneInter As String,
                                          ByVal BiopsiaCodificada As String, ByVal EstBiopsia As String, ByVal BiopsiaCritico As String, ByVal TipoBiopsia As String)
        Dim campos As String
        Dim consulta As String = ""

        campos = "SELECT arb.ANA_idBiopsia, ANA_idTipo_Biopsia, arb.ANA_dictadaBiopsia,
            RTRIM(CONVERT(char(10), arb.ANA_NumeroRegistro_Biopsias)) + '-' + RTRIM(CONVERT(char(10), arb.ANA_AñoRegistro_Biopsias)) AS n_biopsia, 
            arb.ANA_fec_RecepcionRegistro_Biopsias, 
            arb.ANA_GesRegistro_Biopsias, 
            arb.GEN_idTipo_Estados_Sistemas, 
            s.GEN_nombreServicio AS nombre1, 
            GEN_nombreTipo_Estados_Sistemas, 
            vdb.serv_destino, 
            DATEDIFF(day, arb.ANA_fec_RecepcionRegistro_Biopsias, CONVERT(date, SYSDATETIME())) AS dias, 
            arb.ANA_fec_IngresoRegistro_Biopsias, 
            arb.ANA_NumeroRegistro_Biopsias, 
            arb.ANA_AñoRegistro_Biopsias, 
            arb.ANA_fecValidaBiopsia, 
            STUFF((SELECT ', ' + ANA_DescripcionBiopsia FROM ANA_Detalle_Registro_Muestra WHERE ANA_IdBiopsia=arb.ANA_IdBiopsia FOR XML PATH('')),1,1,'') as NombreOrgano 
            FROM GEN_Servicio AS s INNER JOIN 
            ANA_Registro_Biopsias as arb ON 
            s.GEN_idServicio = arb.GEN_IdServicioOrigen LEFT OUTER JOIN 
            GEN_Paciente AS p ON 
            arb.gen_idpaciente = p.gen_idpaciente  LEFT OUTER JOIN 
            ANA_vi_BiopsiasDestino AS vdb ON 
            arb.ANA_IdBiopsia = vdb.ANA_IdBiopsia
inner join GEN_Tipo_Estados_Sistemas on arb.GEN_idTipo_Estados_Sistemas = GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas
" _
        & "WHERE arb.ANA_fec_RecepcionRegistro_Biopsias BETWEEN '" & FRecepcionInicio & "' AND '" & FRecepcionFinal & "' " _
        '& "arb.ANA_fec_RecepcionRegistro_Biopsias between CONVERT(datetime,'" & FRecepcionInicio.ToString("dd/MM/yyyy") & "') AND CONVERT(datetime,'" & FRecepcionFinal.ToString("dd/MM/yyyy") & "')" _

        If TipoBiopsia <> "" And TipoBiopsia <> "0" Then
            consulta = consulta & "AND arb.ANA_idTipo_Biopsia = " & TipoBiopsia & " "
        End If

        If NomPaciente <> "" Then
            consulta = consulta & "AND p.GEN_NombrePaciente like '%" & NomPaciente & "%' "
        End If
        If Ap1Paciente <> "" Then
            consulta = consulta & "and p.GEN_ape_paternoPaciente like '%" & Ap1Paciente & "%' "
        End If
        If Ap2Paciente <> "" Then
            consulta = consulta & "and p.GEN_ape_maternoPaciente like '%" & Ap2Paciente & "%' "
        End If

        If RutPaciente <> "" Then
            consulta = consulta & "AND p.GEN_numero_documentoPaciente = '" & RutPaciente & "' AND " _
            & "p.GEN_digitopaciente='" & DigPaciente & "' "
        End If

        If Get_ANA_NumBiopsia() > 0 Then
            consulta = consulta & " AND arb.ANA_NumeroRegistro_Biopsias=" & Get_ANA_NumBiopsia() & " "
        End If

        If Get_ANA_AñoBiopsia() > 0 Then
            consulta = consulta & " AND arb.ANA_AñoRegistro_Biopsias=" & Get_ANA_AñoBiopsia() & " "
        End If

        If TieneInter = "SI" Then
            consulta = consulta & " AND arb.ANA_InterconsultaBiopsia='SI' "
        End If

        If Get_GEN_IdUsuarioPatologo() > 0 Then
            consulta = consulta & " AND arb.GEN_IdUsuarioPatologo=" & Get_GEN_IdUsuarioPatologo() & " "
        End If

        If EstBiopsia <> "" Then
            consulta = consulta & " AND (" & EstBiopsia & ") "
        End If

        If BiopsiaCodificada <> "" Then
            consulta = consulta & " AND arb.ANA_CodificadaEstadoRegistro_Biopsias='NO' "
        End If

        If BiopsiaCritico <> "" Then
            consulta = consulta & " AND arb.ANA_CriticoBiopsia='SI' "
        End If

        consulta = consulta & " group by arb.ANA_IdBiopsia, ANA_idTipo_Biopsia, arb.ANA_dictadaBiopsia,
arb.ANA_NumeroRegistro_Biopsias, 
arb.ANA_AñoRegistro_Biopsias, 
arb.GEN_idTipo_Estados_Sistemas,
arb.ANA_fec_RecepcionRegistro_Biopsias, 
p.GEN_numero_documentoPaciente, 
p.GEN_digitoPaciente, 
arb.ANA_GesRegistro_Biopsias, 
arb.GEN_IdServicioOrigen, 
arb.GEN_IdServicioDestino, 
GEN_nombreTipo_Estados_Sistemas, 
s.GEN_nombreServicio, 
arb.ANA_OrganoBiopsia, 
vdb.serv_destino, 
arb.ANA_fec_IngresoRegistro_Biopsias, 
arb.ANA_fecValidaBiopsia
order by arb.ANA_IdBiopsia asc "

        consulta = campos & consulta
        Return consulta

    End Function
End Class
