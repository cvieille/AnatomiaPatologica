﻿Public Class NegocioInmuno
    Inherits InmunoHistoquimicaBiopsia

    Public Sub Set_EntregarInmunoyCreaLamina(ByVal IdUsuario As Long, ByVal ANA_IdInmunoBiopsia As Integer)
        Dim ihb As New InmunoHistoquimicaBiopsia
        ihb.InmunoHistoquimicaBiopsia(ANA_IdInmunoBiopsia)
        ihb.Set_ANA_IdInmunoBiopsia(ANA_IdInmunoBiopsia)
        ihb.Set_GEN_idTipo_Estados_Sistemas(46)
        ihb.Set_ANA_EstadoInmuno("Entregada a Patologo")
        ihb.Set_CambiaEstadoInmuno()

        Dim c As New Cortes
        c.Cortes(ihb.Get_ANA_IdCortes_Muestras())
        ' *** TOMA LOS TRES PRIMEROS CARACTERES DEL NOMBRE DE LA TECNICA PARA CREAR LA LAMINA.
        '==========================================================
        Dim NombreTecnica As String = Mid(ihb.Get_ANA_NombreInmuno(), 1, 3)
        Dim NombreLamina As String = Trim(c.Get_ANA_NomCortes_Muestras()) & "-(" & NombreTecnica & ")"
        '==========================================================

        Dim l As New Laminas
        l.Set_ANA_IdBiopsia(ihb.Get_ANA_IdBiopsia())
        l.Set_ANA_nomLamina(NombreLamina)
        l.Set_GEN_idTipo_Estados_Sistemas(58)
        l.Set_GEN_IdUsuarios(IdUsuario)
        l.Set_ANA_IdCortes_Muestras(c.Get_ANA_IdCortes_Muestras())
        l.Set_Crea_lamina()

        Dim mov1 As New movimientos

        mov1.Set_GEN_IdUsuarios(IdUsuario)
        mov1.Set_ANA_IdBiopsia(l.Get_ANA_IdBiopsia())
        mov1.Set_GEN_idTipo_Movimientos_Sistemas(96)
        mov1.Set_ANA_DetalleMovimiento("Genera Lamina: " & NombreLamina)
        mov1.Set_CrearNuevoMoviento()
    End Sub

End Class
