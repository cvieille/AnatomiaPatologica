﻿Public Class NegocioCortes
    Inherits Cortes

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region


#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================



    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================



    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================

    Public Sub Set_DesecharCasete()
        Set_ANA_Almacenada("NO")
        Set_ANA_Solicitada("NO")
        Set_ANA_Desechada("SI")
        Set_GEN_idTipo_Estados_Sistemas(51)
        Set_ANA_EstCortes_Muestras("Desechado")
        Set_Update_Cortes()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        Dim glosa As String = "Se desecha corte " & Get_ANA_NomCortes_Muestras()
        mov.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(109)
        mov.Set_ANA_DetalleMovimiento(glosa)
        mov.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()

    End Sub


    Public Sub Set_AProcesador()
        '*** INSTANCIA EL CONSTRUCTOR DE CORTES Y CARGA ID DE CORTE
        '==========================================================
        Set_ANA_EstCortes_Muestras("En Procesador")
        Set_GEN_idTipo_Estados_Sistemas(53)
        Set_Update_Cortes()

        ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
        '==========================================================
        Dim nrm As New NegocioRegistroMuestra
        nrm.Registro_Biopsia(Get_ANA_IdBiopsia())
        nrm.Set_ANA_EstadoRegistro_Biopsias("En Proceso")
        nrm.Set_GEN_idTipo_Estados_Sistemas(47)
        nrm.Set_ModificaRegistroBiopsia()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Casete: " & Get_ANA_NomCortes_Muestras())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(88)
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()
    End Sub

    Public Sub Set_AInclusion()

        Set_ANA_EstCortes_Muestras("En Inclusion")
        Set_GEN_idTipo_Estados_Sistemas(62)
        Set_Update_Cortes()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Casete: " & Get_ANA_NomCortes_Muestras())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(85)
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()
    End Sub

    Public Sub Set_ATincionyMontaje()
        Set_ANA_EstCortes_Muestras("En Tincion y Montaje")
        Set_GEN_idTipo_Estados_Sistemas(60)
        Set_Update_Cortes()

        ' *** ASIGNA TECNOLOGO A REGISTRO MUESTRA
        '==========================================================
        Dim Nrm As New NegocioRegistroMuestra
        Nrm.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        Nrm.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        Nrm.Set_AsignaTecnologo()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento("Casete: " & Get_ANA_NomCortes_Muestras())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(86)
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()
    End Sub
    Public Sub Set_AAlmacenarCasete()
        Set_ANA_EstCortes_Muestras("Para Almacenar")
        Set_GEN_idTipo_Estados_Sistemas(55)
        Set_Update_Cortes()

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
        mov.Set_GEN_idTipo_Movimientos_Sistemas(103)
        mov.Set_ANA_DetalleMovimiento("Se Entrega para almacenar casete " & Get_ANA_NomCortes_Muestras())
        mov.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
        mov.Set_CrearNuevoMoviento()
    End Sub

    Public Function Set_SolicitaCaseteAlmacenado(ByVal PerfilUsuario As Integer, ByVal Optional bBien As Boolean = False)
        '*** INSTANCIA EL CONSTRUCTOR DE CORTES Y CARGA ID DE CORTE
        '==========================================================
        Dim c As New Cortes
        Dim idCorte As Integer = Get_ANA_IdCortes_Muestras()
        c.Cortes(idCorte)
        '*** SOLO SE PUEDEN SOLICITAR CASETE QUE NO TENGAN SOLICITUD PENDIENTE
        '==========================================================

        ' *** INSTANCIA EL CLASE PERFIL
        '==========================================================
        Dim per As New Perfil
        Dim GlosaCasete As String
        Dim Mensaje As String = ""
        Dim ArrayMensaje(3) As String

        If c.Get_ANA_Almacenada() = "SI" Then
            If c.Get_ANA_Solicitada = "NO" Then
                c.Set_ANA_Solicitada("SI")

                If PerfilUsuario = 4 Then
                    c.Set_ANA_SolporCortes_Muestras("Tecnólogo")
                    GlosaCasete = "Tecnólogo solicita corte " & c.Get_ANA_NomCortes_Muestras()
                Else
                    c.Set_ANA_SolporCortes_Muestras("Patologo")
                    GlosaCasete = "Patologo solicita corte " & c.Get_ANA_NomCortes_Muestras()
                End If
                c.Set_Update_Cortes()

                '*** CREA MOVIMIENTO REALIZADO
                '==========================================================
                Dim mov As New movimientos
                mov.Set_GEN_IdUsuarios(Get_GEN_IdUsuarios())
                mov.Set_ANA_IdBiopsia(Get_ANA_IdBiopsia())
                mov.Set_ANA_DetalleMovimiento(GlosaCasete)
                mov.Set_GEN_idTipo_Movimientos_Sistemas(111)
                mov.Set_CrearNuevoMoviento()
                Mensaje = "Se ha Solicitado Casete"
                ArrayMensaje = New String() {"Se ha solicitado casete", "t", idCorte.ToString()}
            Else
                Mensaje = "¡Existen Casete que ya estaban solicitados!"
                ArrayMensaje = New String() {"Existen casetes que ya estaban solicitados", "f", idCorte.ToString()}
            End If
        Else
            Mensaje = "¡Solo se pueden solicitar Casete almacenados!"
            ArrayMensaje = New String() {"Solo se pueden solicitar casetes almacenados", "a", idCorte.ToString()}
        End If

        If bBien Then
            Return ArrayMensaje
        Else
            Return Mensaje
        End If
    End Function

    Public Function SolicitaCaseteparaInterconsulta()
        Dim glosa As String
        Dim c As New Cortes
        c.Cortes(Me.Get_ANA_IdCortes_Muestras())
        Dim mensaje As String

        If c.Get_ANA_Almacenada() = "SI" Then
            c.Set_ANA_Solicitada("SI")
            c.Set_ANA_SolporCortes_Muestras("Tecnólogo")
            c.Set_ANA_EstCortes_Muestras("Solicitado para Interconsulta")
            c.Set_Update_Cortes()

            glosa = "Se solicitud de retiro de Casete " & Trim(c.Get_ANA_NomCortes_Muestras()) & " para Interconsulta"
            mensaje = "Solicitado para Interconsulta"

        Else
            c.Set_ANA_EstCortes_Muestras("Para Interconsulta")
            c.Set_ANA_Almacenada("NO")
            c.Set_ANA_Solicitada("NO")
            c.Set_Update_Cortes()

            glosa = "Se envia Casete " & Trim(c.Get_ANA_NomCortes_Muestras()) & " a estado Para Interconsulta"
            mensaje = "Para Interconsulta"

        End If

        '*** CREA MOVIMIENTO REALIZADO
        '==========================================================
        Dim mov As New movimientos
        mov.Set_GEN_IdUsuarios(Me.Get_GEN_IdUsuarios())
        mov.Set_ANA_IdBiopsia(Me.Get_ANA_IdBiopsia())
        mov.Set_ANA_DetalleMovimiento(glosa)
        mov.Set_CrearNuevoMoviento()

        Return mensaje

    End Function

    Public Function Get_PuedeEliminarCasete()
        ' *** CONSULTA SI TIENE TECNICAS ASOCIADAS AL ID DE CORTE
        '==========================================================
        Dim consulta As String
        consulta = "SELECT COUNT(ANA_IdCortes_Muestras) AS cantidad " _
        & "FROM ANA_Tecnica_Biopsia " _
        & "WHERE ANA_IdCortes_Muestras=" & Get_ANA_IdCortes_Muestras() & " " _
        & "GROUP BY ANA_IdCortes_Muestras "
        Dim CantTecnicas As Integer = CInt(consulta_sql_devuelve_string(consulta))

        consulta = "SELECT COUNT(ANA_IdCortes_Muestras) AS cantidad " _
        & "FROM ANA_Inmuno_Histoquimica_Biopsia " _
        & "WHERE ANA_IdCortes_Muestras=" & Get_ANA_IdCortes_Muestras() & " " _
        & "GROUP BY ANA_IdCortes_Muestras "
        Dim CantHinmuno As Integer = CInt(consulta_sql_devuelve_string(consulta))

        Dim Eliminar As Boolean
        If CantHinmuno > 0 Or CantTecnicas > 0 Then
            Eliminar = False
        Else
            Eliminar = True
        End If

        Return Eliminar
    End Function

    ' *** FIN DE SET
    '==========================================================
#End Region
End Class
