﻿Public Class Negocio
#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    '*** CONSULTA CUENTA DESCALSIFICACIONES POR NUMERO DE BIOPSIA
    Public Function Cantidad_Descalcificaciones(ByVal IdBiopsia As Integer)
        Dim consulta As String = "SELECT " _
        & "COUNT(ANA_DescalcificadoCortes_Muestras) as CantDescalcificacion " _
        & "FROM " _
        & "ANA_Cortes_Muestras " _
        & "WHERE " _
        & "ANA_IdBiopsia=" & IdBiopsia & " AND " _
        & "ANA_DescalcificadoCortes_Muestras ='SI' " _
        & "GROUP BY ANA_IdBiopsia"
        If IsNumeric(consulta_sql_devuelve_string(consulta)) Then
            Return Int(consulta_sql_devuelve_string(consulta))
        Else
            Return 0
        End If

    End Function

    '*** CONSULTA CUENTA CANTIDAD DE TECNICAS POR NUMERO DE BIOPSIA
    Public Function Cantidad_Tecnicas(ByVal IdBiopsia As Integer)
        Dim consulta As String = ""
        consulta = "SELECT count(tb.ANA_IdTecnicaBiopsia) as CantTecnicas " _
        & "FROM " _
        & "ANA_Tecnica_Biopsia AS tb INNER JOIN " _
        & "ANA_Tecnicas_Especiales AS ate ON " _
        & "tb.ANA_idTecnica = ate.ANA_idTecnica " _
        & "WHERE " _
        & "not( ate.ANA_nomTecnica='Nivel' or " _
        & "ate.ANA_nomTecnica='Desgaste') AND " _
        & "tb.ANA_IdBiopsia=" & IdBiopsia & " " _
        & "group by tb.ANA_IdBiopsia"
        If IsNumeric(consulta_sql_devuelve_string(consulta)) Then
            Return Int(consulta_sql_devuelve_string(consulta))
        Else
            Return 0
        End If


    End Function



    '*** CONSULTA CUENTA CANTIDAD DE CORTES POR BIOPSIA
    Public Function Cantidad_Cortes(ByVal IdBiopsia As Integer)
        Dim consulta As String = ""
        consulta = "SELECT COUNT(ANA_IdBiopsia) AS cant_cortes " _
        & "FROM " _
        & "ANA_Cortes_Muestras AS c " _
        & "WHERE " _
        & "ANA_IdBiopsia = " & IdBiopsia & " " _
        & "GROUP BY " _
        & "ANA_IdBiopsia"

        If IsNumeric(consulta_sql_devuelve_string(consulta)) Then
            Return Int(consulta_sql_devuelve_string(consulta))
        Else
            Return 0
        End If

    End Function
End Class