﻿Public Class NegocioProfesionales
    Inherits Profesionales
    '-----------------------------------------------------------------------------------------------------------------------------------------
    ' INSERTA LAS ESPECIALIDADES DEL PROFESIONAL. CODIGO EXTRAIDO DE PABELLON PERO OPTIMIZADO 
    '-----------------------------------------------------------------------------------------------------------------------------------------
    Public Sub Set_Agrega_Especialidad(ByVal idespecialidad As String)
        Dim consulta As String = "SELECT " _
        & "COUNT(GEN_idPro_Especialidad) " _
        & "FROM " _
        & "GEN_Pro_Especialidad " _
        & "WHERE " _
        & "GEN_IdEspecialidad=" & idespecialidad & " " _
        & "AND GEN_idProfesional=" & Me.Get_Gen_Idprofesional()

        Dim existe As String = consulta_sql_devuelve_string(consulta)

        If existe = "0" Then
            consulta = "INSERT INTO GEN_Pro_Especialidad (" _
            & "GEN_idEspecialidad, " _
            & "GEN_idProfesional, " _
            & "GEN_estadoPro_Especialidad) " _
            & "VALUES (" & idespecialidad & ", " _
            & Me.Get_Gen_Idprofesional() & ", " _
            & "'Activo')"
            ejecuta_sql(consulta)

        Else
            consulta = "UPDATE GEN_Pro_Especialidad SET " _
            & "GEN_estadoPro_Especialidad = 'Activo' " _
            & "WHERE " _
            & "GEN_idEspecialidad=" & idespecialidad & " " _
            & "AND GEN_IdProfesional=" & Me.Get_Gen_Idprofesional()
            ejecuta_sql(consulta)
        End If
    End Sub

    '-----------------------------------------------------------------------------------------------------------------------------------------
    ' INSERTA LAS PROFESION DEL PROFESIONAL. CODIGO EXTRAIDO DE PABELLON PERO OPTIMIZADO 
    '-----------------------------------------------------------------------------------------------------------------------------------------
    Public Sub Set_Agrega_Profesion(ByVal idprofesion As String)
        Dim consulta As String = "SELECT COUNT(GEN_idPro_Profesional) " _
        & "FROM " _
        & "GEN_Pro_Profesion " _
        & "WHERE GEN_idProfesion=" & idprofesion & " " _
        & " AND GEN_idProfesional=" & Me.Get_Gen_Idprofesional()

        Dim existe As String = consulta_sql_devuelve_string(consulta)

        If existe = "0" Then
            consulta = "INSERT INTO GEN_Pro_Profesion (" _
            & "GEN_idProfesion, " _
            & "GEN_idProfesional, " _
            & "GEN_estadoPro_Profesional) " _
            & "VALUES (" _
            & idprofesion & ", " & Me.Get_Gen_Idprofesional() & ", " _
            & "'Activo')"
            ejecuta_sql(consulta)
        Else
            consulta = "UPDATE GEN_Pro_Profesion SET " _
            & "GEN_estadoPro_Profesional = 'Activo' " _
            & "WHERE GEN_idProfesion=" & idprofesion & " " _
            & "AND GEN_idProfesional=" & Me.Get_Gen_Idprofesional()
            ejecuta_sql(consulta)
        End If
    End Sub

End Class