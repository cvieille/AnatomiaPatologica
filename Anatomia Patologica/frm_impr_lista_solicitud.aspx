﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm_impr_lista_solicitud.aspx.vb" Inherits="Anatomia_Patologica.frm_impr_lista_solicitud" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <div style="text-align:center">
        <form id="form1" runat="server">
         <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="Sqlds_lista_solicitud" 
        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" Width="100%" Height="100%">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:BoundField DataField="OT" HeaderText="OT" SortExpression="OT" />
            <asp:BoundField DataField="AÑO" HeaderText="AÑO" SortExpression="AÑO" />
            <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" DataFormatString="{0:dd-MM-yyyy}" />
            <asp:BoundField DataField="PACIENTE" HeaderText="PACIENTE" SortExpression="PACIENTE" ReadOnly="True" />
            <asp:BoundField DataField="ANA_GesRegistro_Biopsias" HeaderText="GES" SortExpression="ANA_GesRegistro_Biopsias" />
            <asp:BoundField DataField="MEDICO" HeaderText="MEDICO" SortExpression="MEDICO" ReadOnly="True" />
            <asp:BoundField DataField="MUESTRA" HeaderText="MUESTRA" SortExpression="MUESTRA" />
            <asp:BoundField DataField="ORIGEN" HeaderText="ORIGEN" SortExpression="ORIGEN" />
            <asp:BoundField DataField="SERVICIO" HeaderText="SERVICIO" SortExpression="SERVICIO" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="Sqlds_lista_solicitud" runat="server" 
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
        SelectCommand="SELECT * FROM [vi_recibe_solicitud] ORDER BY [FECHA] DESC, [ID] DESC"></asp:SqlDataSource>
    </form>
    </div>
</body>
</html>
