﻿Imports System.Web.Services
Imports Newtonsoft.Json

Partial Public Class frm_PrincipalClinico
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    <WebMethod>
    Public Shared Function informacionNotificacion(idBiopsiaCritico)
        Dim jsonList As New Dictionary(Of String, String)
        Dim bc As New BiopsiaCritico
        bc.BiopsiaCritico_Id_Notificacion(idBiopsiaCritico)

        Dim rm As New Registro_Biopsia
        rm.Registro_Biopsia(bc.Get_ANA_IdBiopsia())

        jsonList.Add("ANA_EstadoRegistro_Biopsias", rm.Get_ANA_EstadoRegistro_Biopsias())
        jsonList.Add("ANA_IdBiopsia", rm.Get_ANA_IdBiopsia())
        jsonList.Add("Ana_IdBiopsiaCritico", bc.Get_Ana_IdBiopsiaCritico()) 'txt_id_notificacion
        jsonList.Add("ANA_NumBiopsia", rm.Get_ANA_NumBiopsia() & "-" & rm.Get_ANA_AñoBiopsia())
        jsonList.Add("ANA_OrganoBiopsia", rm.Get_ANA_OrganoBiopsia())
        jsonList.Add("Nom_Servicio_Solicita", rm.Get_Nom_Servicio_Solicita(rm.Get_GEN_IdServicioOrigen()))
        Dim pro As New Profesionales
        pro.Profesionales(Int(rm.Get_ANA_IdMedSolicitaBiopsia()))
        jsonList.Add("GEN_NombreSolicitante", pro.Get_GEN_Nombre() & " " & pro.Get_GEN_ApePaterno() & " " & pro.Get_GEN_ApeMaterno())

        'Me.txt_estado.Text = rm.Get_ANA_EstadoRegistro_Biopsias()
        'Me.txt_id_biopsia.Text = rm.Get_ANA_IdBiopsia()
        'Me.txt_id_notificacion.Text = bc.Get_Ana_IdBiopsiaCritico()
        'Me.txt_numero.Text = rm.Get_ANA_NumBiopsia() & "-" & rm.Get_ANA_AñoBiopsia()
        'Me.txt_organo.Text = rm.Get_ANA_OrganoBiopsia()
        'Me.txt_servicio.Text = rm.Get_Nom_Servicio_Solicita(rm.Get_GEN_IdServicioOrigen())

        'Me.txt_solicitado_por.Text = pro.Get_GEN_Nombre() & " " & pro.Get_GEN_ApePaterno() & " " & pro.Get_GEN_ApeMaterno()

        ' *** INSTANCIA EL CONSTRUCTOR DE CRITICOS POR ID DE NOTIFICACION
        '==========================================================
        'If bc.Get_ANA_LeidoBiopsias_Critico() = "SI" Then
        'chk_leido.Checked = True
        'Else
        'chk_leido.Checked = False
        ' End If

        jsonList.Add("ANA_LeidoBiopsias_Critico", "SI")
        'updatear si es la primera vez que lo leo, "VISTO"
        If bc.Get_ANA_FechaLeidoBiopsias_Critico() = Nothing Then
            'el update en algún momento será por la api
            ejecuta_sql(String.Format("update ANA_Biopsias_Critico set ANA_FechaLeidoBiopsias_Critico = '{0}', ANA_LeidoBiopsias_Critico = 'SI' WHERE ANA_IdBiopsias_Critico = {1}", DateTime.Now(), idBiopsiaCritico))
            'validar en informe de la doctora, el que tiene las grillas
        End If



        jsonList.Add("Ana_ObservacionCritico", bc.Get_Ana_ObservacionCritico())
        jsonList.Add("ANA_RespuestaBiopsias_Critico", bc.Get_ANA_RespuestaBiopsias_Critico())

        'Me.txt_notificacion.Text = bc.Get_Ana_ObservacionCritico()
        'Me.txt_respuesta_notificacion.Text = bc.Get_ANA_RespuestaBiopsias_Critico()

        ' *** INSTANCIA EL CONSTRUCTOR DE PACIENTES
        '==========================================================
        Dim p As New Pacientes
        p.Pacientes(rm.Get_GEN_Id_Paciente)

        jsonList.Add("GEN_FecNacimientoPaciente", CDate(p.Get_GEN_FecNacimientoPaciente()))
        jsonList.Add("GEN_NuiPaciente", p.Get_GEN_NuiPaciente())
        'Me.txt_fnac.Text = CDate(p.Get_GEN_FecNacimientoPaciente())
        'Me.txt_ficha.Text = p.Get_GEN_NuiPaciente()
        '==========================================================
        '*** cuando es extranjero el valor del digito es z
        '*** en ese caso, se debe ocualtar
        '==========================================================
        If p.Get_GEN_digitoPaciente() = "z" Then
            jsonList.Add("GEN_numero_documentoPaciente", p.Get_GEN_numero_documentoPaciente())
            'Me.txt_rut.Text = p.Get_GEN_numero_documentoPaciente()
        Else
            jsonList.Add("GEN_numero_documentoPaciente", p.Get_GEN_numero_documentoPaciente() & "-" & p.Get_GEN_digitoPaciente())
            'Me.txt_rut.Text = p.Get_GEN_numero_documentoPaciente() & "-" & p.Get_GEN_digitoPaciente()
        End If

        jsonList.Add("GEN_NombreCompletoPaciente", p.Get_GEN_NombreCompleto())

        Dim Meses_Totales = DateDiff("m", CDate(p.Get_GEN_FecNacimientoPaciente()), Date.Today)
        Dim Years = Int(Meses_Totales / 12)
        jsonList.Add("edadPaciente", Years & " años " & Meses_Totales - (Years * 12) & " meses")
        'Me.txt_edad.Text = Years & " años " & Meses_Totales - (Years * 12) & " meses"

        ' *** INSTANCIA USUARIOS PARA OBTENER NOMBRE DE PATOLOGO  QUE VALIDA 
        '==========================================================
        Dim uValida As New Usuarios
        uValida.Usuarios(CInt(rm.Get_GEN_idUsuarioValida()))
        jsonList.Add("GEN_NombreCompletoUsuario", uValida.Get_GEN_NombreCompleto())
        'Me.txt_quien_valida.Text = uValida.Get_GEN_NombreCompleto()

        'jsonList.Add("ANA_EstadoRegistro_Biopsias", rm.Get_ANA_EstadoRegistro_Biopsias())
        'Me.txt_estado.Text = rm.Get_ANA_EstadoRegistro_Biopsias()
        'Me.lbl_mensaje.Visible = False
        'Me.lbl_mensaje.Text = ""
        Return JsonConvert.SerializeObject(jsonList)
    End Function

    <WebMethod>
    Public Shared Function guardarNotificacion(idBiopsiaCritico As Integer, respuestaNotificacion As String)
        Dim bc As New BiopsiaCritico
        bc.BiopsiaCritico_Id_Notificacion(idBiopsiaCritico)
        bc.Set_ANA_RespuestaBiopsias_Critico(respuestaNotificacion)
        'If visto Then
        bc.Set_ANA_LeidoBiopsias_Critico("SI")
        'Else
        '    bc.Set_ANA_LeidoBiopsias_Critico("NO")
        'End If

        bc.Set_update_Critico_Biopsia_db()

        Return True
        'Me.lbl_mensaje.Text = "¡La Información fue Guardada Correctamente!"
        'Me.lbl_mensaje.Visible = True
        'mp1.Show()
    End Function

    <WebMethod>
    Public Shared Function cerrarNotificacion(idBiopsiaCritico, respuestaNotificacion, idUsuario)
        Dim negocio As New BiopsiaCritico
        negocio.BiopsiaCritico(idBiopsiaCritico)

        negocio.Set_CerrarNoficicaciondeCritico(idBiopsiaCritico, respuestaNotificacion, idUsuario)

        Return True
        'Me.lbl_mensaje.Text = "¡Se ha cerrado correctamente la notificación!"
        'Me.lbl_mensaje.Visible = True
        'mp1.Show()
    End Function

    <WebMethod>
    Public Shared Function informacionBiopsia(idBiopsia)
        Dim jsonList As New List(Of Dictionary(Of String, String))

        Dim sb As New StringBuilder()
        sb.AppendLine("SELECT ANA_Registro_Biopsias.ANA_fec_RecepcionRegistro_Biopsias, GEN_Paciente.GEN_numero_documentoPaciente + ' ' + ISNULL(GEN_Paciente.GEN_digitoPaciente,'') GEN_numero_documentoPaciente,")
        sb.AppendLine("GEN_Paciente.GEN_nombrePaciente + ' ' + GEN_Paciente.GEN_ape_paternoPaciente + ' ' + ISNULL(GEN_Paciente.GEN_ape_maternoPaciente,'') GEN_nombrePaciente,")
        sb.AppendLine("ANA_Registro_Biopsias.ANA_OrganoBiopsia,")
        sb.AppendLine("GEN_Usuarios.GEN_nombreUsuarios + ' ' + GEN_Usuarios.GEN_apellido_paternoUsuarios + ' ' + ISNULL(GEN_Usuarios.GEN_apellido_maternoUsuarios,'') GEN_nombreUsuarios,")
        sb.AppendLine("GEN_Profesional.GEN_nombreProfesional + ' ' + GEN_Profesional.GEN_apellidoProfesional + ' ' + ISNULL(GEN_Profesional.GEN_sapellidoProfesional,'') GEN_nombreProfesional,")
        sb.AppendLine("GEN_ServicioOrigen.GEN_nombreServicio GEN_ServicioOrigen, GEN_ServicioDestino.GEN_nombreServicio GEN_ServicioDestino,")
        sb.AppendLine("GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas")
        sb.AppendLine("FROM ANA_Registro_Biopsias")
        sb.AppendLine("INNER JOIN GEN_Paciente ON GEN_Paciente.GEN_idPaciente = ANA_Registro_Biopsias.GEN_idPaciente")
        sb.AppendLine("LEFT JOIN GEN_Usuarios ON GEN_Usuarios.GEN_idUsuarios = ANA_Registro_Biopsias.GEN_IdUsuarioPatologo")
        sb.AppendLine("LEFT JOIN GEN_Profesional ON GEN_Profesional.GEN_idProfesional = ANA_Registro_Biopsias.ANA_IdMedSolicitaBiopsia")
        sb.AppendLine("INNER JOIN GEN_Servicio GEN_ServicioOrigen ON GEN_ServicioOrigen.GEN_idServicio = ANA_Registro_Biopsias.GEN_IdServicioOrigen")
        sb.AppendLine("INNER JOIN GEN_Servicio GEN_ServicioDestino ON GEN_ServicioDestino.GEN_idServicio = ANA_Registro_Biopsias.GEN_IdServicioDestino")
        sb.AppendLine("INNER JOIN GEN_Tipo_Estados_Sistemas ON GEN_Tipo_Estados_Sistemas.GEN_idTipo_Estados_Sistemas = ANA_Registro_Biopsias.GEN_idTipo_Estados_Sistemas")
        sb.AppendLine(String.Format("WHERE ANA_Registro_Biopsias.ANA_IdBiopsia = {0}", idBiopsia))

        Dim d As DataTable = consulta_sql_datatable(sb.ToString())
        If d.Rows.Count > 0 Then
            Dim json As New Dictionary(Of String, String)
            json("ANA_fec_RecepcionRegistro_Biopsias") = d.Rows(0)("ANA_fec_RecepcionRegistro_Biopsias").ToString
            json("GEN_numero_documentoPaciente") = d.Rows(0)("GEN_numero_documentoPaciente").ToString
            json("GEN_nombrePaciente") = d.Rows(0)("GEN_nombrePaciente").ToString
            json("ANA_OrganoBiopsia") = d.Rows(0)("ANA_OrganoBiopsia").ToString
            json("GEN_nombreUsuarios") = d.Rows(0)("GEN_nombreUsuarios").ToString
            json("GEN_nombreProfesional") = d.Rows(0)("GEN_nombreProfesional").ToString
            json("GEN_ServicioOrigen") = d.Rows(0)("GEN_ServicioOrigen").ToString
            json("GEN_ServicioDestino") = d.Rows(0)("GEN_ServicioDestino").ToString
            json("GEN_nombreTipo_Estados_Sistemas") = d.Rows(0)("GEN_nombreTipo_Estados_Sistemas").ToString
            jsonList.Add(json)
        End If

        Return JsonConvert.SerializeObject(jsonList)

    End Function

    <WebMethod>
    Public Shared Function grillaBiopsias(idProfesional, sDesde, sHasta)
        Dim jsonList As New List(Of Dictionary(Of String, String))

        Dim con As New Consultas
        'If Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Enfermera Criticos") Then
        'pnl_bandeja.Visible = False
        'Else
        'pnl_bandeja.Visible = True

        'OJO OJO OJO OJ O OJ OOJO OJO OJO 
        'en el código original la sesión idprofesional siempre es nothing, osea que siempre muestra toda la grilla
        'idProfesional = Nothing

        Dim d As DataTable = consulta_sql_datatable(con.Ds_con_clinicos("", "", "", "", "", idProfesional, sDesde, sHasta))
        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            For i = 0 To dtr.Table.Columns.Count - 1
                json(dtr.Table.Columns(i).ColumnName) = dtr(dtr.Table.Columns(i).ColumnName).ToString
            Next
            jsonList.Add(json)
        Next

        'End If

        Return JsonConvert.SerializeObject(jsonList)
    End Function

    <WebMethod>
    Public Shared Function grillaNotificaciones(idProfesional, sRut, sFicha, sNombre, sApellidoP, sApellidoM)
        Dim jsonList As New List(Of Dictionary(Of String, String))

        Dim con As New Consultas


        Dim d As DataTable = consulta_sql_datatable(con.Ds_con_Notificacionclinicos(idProfesional, sRut, sFicha, sNombre, sApellidoP, sApellidoM))
        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            For i = 0 To dtr.Table.Columns.Count - 1
                json(dtr.Table.Columns(i).ColumnName) = dtr(dtr.Table.Columns(i).ColumnName).ToString
            Next
            jsonList.Add(json)
        Next

        Return JsonConvert.SerializeObject(jsonList)
    End Function

End Class