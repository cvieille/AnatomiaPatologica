﻿Imports System.IO
Imports System.Security.Cryptography
Imports Newtonsoft.Json


'Public Class AnaConf
'    Public Property n_biopsia As String
'    Public Property a_biopsia As String
'    Public Property n_solicitud As String
'    Public Property a_solicitud As String
'    Public Property d_configuracion As String
'    Public Property url_configuracion As String
'End Class

Public Class MetodosGenerales
    Implements System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Select Case context.Request("method")
            Case "DeleteSession"
                DeleteSession(context.Request("key").ToString())
            Case "GetSession"
                GetSession()
            Case "SetSession"
                SetSession(context.Request("key").ToString(), context.Request("value").ToString())
            Case "GuardarArchivo"
                GuardarArchivo(context.Request("idBiopsia").ToString())
            'Case "GetArchivos"
            '    GetArchivos(context.Request("perfilUsuario"), context.Request("idBiopsia"))
            'Case "InsertArchivo"
            '    InsertArchivo(context.Request("fn").ToString(), context.Request("idb").ToString(), context.Request("idu").ToString(), context.Request("f").ToString())
            Case "EliminarArchivo"
                DeleteArchivo(context.Request("nombreArchivo").ToString(), context.Request("idBiopsia").ToString())
            Case "MoverArchivo"
                MoverArchivo(context.Request("nombreArchivo").ToString(), context.Request("idBiopsia").ToString())
            Case "Encriptar"
                aesEncrypt(context.Request("texto").ToString())
            'Case "GetPropiedad"
            '    GetPropiedad(context.Request("p").ToString())
            Case "CargarConf"
                CargarConf(context.Request("c").ToString())
            Case "GuardarConf"
                GuardarConf(context.Request("c").ToString(), context.Request("v").ToString())
                'Case "SetPropiedad"
                '    SetPropiedad(context.Request("p").ToString(), context.Request("v").ToString())
            Case "ObtenerApiPdf"
                ObtenerApiPdf(context.Request("id").ToString())
            Case Else
                Console.WriteLine("")
        End Select
    End Sub

    Private Sub aesEncrypt(txt As String)
        Try
            Dim contexto As HttpContext = HttpContext.Current
            Dim pass As String = "$2b$10$054qoSwWVa9zUAjfVBcViOzEdijcHNkxq2Q.hL8.p6Zrcd8Oi8LQ6"
            Dim sha As SHA256 = SHA256.Create()

            Dim key As Byte() = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(pass))
            Dim iv As Byte() = New Byte() {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Dim txtEncriptado As String = encryptString(txt, key, iv)

            contexto.Response.Write(txtEncriptado)
            contexto.Response.End()
        Catch ex As Exception
            'String fail = ex.Message
        End Try

    End Sub

    Private Function encryptString(txt As String, key As Byte(), iv As Byte())
        Dim Aes As Aes = Aes.Create()
        Aes.Mode = CipherMode.CBC

        Aes.Key = key
        Aes.IV = iv

        Dim MS As MemoryStream = New MemoryStream()
        Dim aesEncryptor As ICryptoTransform = Aes.CreateEncryptor()
        Dim CryptoStream As CryptoStream = New CryptoStream(MS, aesEncryptor, CryptoStreamMode.Write)
        Dim plainBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(txt)
        CryptoStream.Write(plainBytes, 0, plainBytes.Length)
        CryptoStream.FlushFinalBlock()
        Dim cypherBytes As Byte() = MS.ToArray()
        MS.Close()
        CryptoStream.Close()
        Dim cypherText As String = Convert.ToBase64String(cypherBytes, 0, cypherBytes.Length)

        Return cypherText
    End Function

    Public Sub Credenciales(usuario, contraseña)
        Dim impersonator As New UserImpersonation()
        impersonator.impersonateUser(usuario, "", contraseña)
    End Sub

    'Public Sub GetPropiedad(ByVal p As String)
    '    Credenciales("desarrollo", "Hcm2016")
    '    Dim contexto As HttpContext = HttpContext.Current
    '    Dim s As String = WebConfigurationManager.OpenWebConfiguration(contexto.Request.ApplicationPath).AppSettings.Settings(p).Value
    '    contexto.Response.Write(s)
    '    contexto.Response.End()
    'End Sub

    'Public Function GetPropiedad(ByVal p As String, ByVal b As Boolean)
    '    Credenciales("desarrollo", "Hcm2016")
    '    Dim contexto As HttpContext = HttpContext.Current
    '    Dim s As String = WebConfigurationManager.OpenWebConfiguration(contexto.Request.ApplicationPath).AppSettings.Settings(p).Value
    '    Return s
    'End Function

    'Public Sub SetPropiedad(ByVal p As String, ByVal v As String)
    '    Credenciales("desarrollo", "Hcm2016")
    '    Dim contexto As HttpContext = HttpContext.Current
    '    Dim s As System.Configuration.Configuration = WebConfigurationManager.OpenWebConfiguration(contexto.Request.ApplicationPath)
    '    s.AppSettings.Settings(p).Value = v
    '    s.Save(ConfigurationSaveMode.Modified)
    '    's.Save(ConfigurationSaveMode.Modified)
    'End Sub

    Public Sub GuardarConf(c, v)
        Credenciales("desarrollo", "Hcm2016")
        Dim context As HttpContext = HttpContext.Current

        Dim p As String = ConfigurationManager.AppSettings("ANAJSON").ToString()
        'Dim sr As New StreamReader(p)
        Dim s As String
        Using sr As New StreamReader(p)
            s = sr.ReadToEnd()
        End Using
        'sr.Close()
        'por alguna razón no siempre cerraba el sr

        Dim auxJson = New With {
            .n_biopsia = "",
            .a_biopsia = "",
            .n_solicitud = "",
            .a_solicitud = "",
            .d_configuracion = "",
            .url_configuracion = "",
            .urlApiPdf = "",
            .tokenApiPdf = ""
        }
        Dim json = JsonConvert.DeserializeAnonymousType(s, auxJson)

        Select Case c
            Case "n_biopsia"
                json.n_biopsia = v
            Case "a_biopsia"
                json.a_biopsia = v
            Case "n_solicitud"
                json.n_solicitud = v
            Case "a_solicitud"
                json.a_solicitud = v
            Case "d_configuracion"
                json.d_configuracion = v
            Case "url_configuracion"
                json.url_configuracion = v
            Case "urlApiPdf"
                json.urlApiPdf = v
            Case "tokenApiPdf"
                json.tokenApiPdf = v
            Case Else
                Console.WriteLine("so")
        End Select

        Dim n As String = JsonConvert.SerializeObject(json)
        Dim sw As New StreamWriter(p, False)
        sw.Write(n)
        sw.Close()
    End Sub

    Public Sub CargarConf(c)
        Credenciales("desarrollo", "Hcm2016")
        Dim context As HttpContext = HttpContext.Current

        Dim p As String = ConfigurationManager.AppSettings("ANAJSON").ToString()
        Dim sr As New StreamReader(p)
        Dim s As String = sr.ReadToEnd()
        sr.Close()

        Dim auxJson = New With {
            .n_biopsia = "",
            .a_biopsia = "",
            .n_solicitud = "",
            .a_solicitud = "",
            .d_configuracion = "",
            .url_configuracion = "",
            .urlApiPdf = "",
            .tokenApiPdf = "",
            .urlWebApi = "",
            .RutaAdjuntos = ""
        }

        Dim json = JsonConvert.DeserializeAnonymousType(s, auxJson)

        Dim re As String
        Select Case c
            Case "n_biopsia"
                re = json.n_biopsia
            Case "a_biopsia"
                re = json.a_biopsia
            Case "n_solicitud"
                re = json.n_solicitud
            Case "a_solicitud"
                re = json.a_solicitud
            Case "d_configuracion"
                re = json.d_configuracion
            Case "url_configuracion"
                re = json.url_configuracion
            Case "urlApiPdf"
                re = json.urlApiPdf
            Case "tokenApiPdf"
                re = json.tokenApiPdf
            Case "urlWebApi"
                re = json.urlWebApi
            Case "RutaAdjuntos"
                re = json.RutaAdjuntos
            Case Else
                re = ""
        End Select

        context.Response.Write(re)
        context.Response.End()
    End Sub

    Public Function CargarConf(c, aux)

        Credenciales("desarrollo", "Hcm2016")

        Dim p As String = ConfigurationManager.AppSettings("ANAJSON").ToString()
        Dim sr As New StreamReader(p)
        Dim s As String = sr.ReadToEnd()
        sr.Close()

        Dim auxJson = New With {
            .n_biopsia = "",
            .a_biopsia = "",
            .n_solicitud = "",
            .a_solicitud = "",
            .d_configuracion = "",
            .url_configuracion = "",
            .urlApiPdf = "",
            .tokenApiPdf = "",
            .urlWebApi = "",
            .RutaAdjuntos = ""
        }
        Dim json = JsonConvert.DeserializeAnonymousType(s, auxJson)

        Dim re As String
        Select Case c
            Case "n_biopsia"
                re = json.n_biopsia
            Case "a_biopsia"
                re = json.a_biopsia
            Case "n_solicitud"
                re = json.n_solicitud
            Case "a_solicitud"
                re = json.a_solicitud
            Case "d_configuracion"
                re = json.d_configuracion
            Case "url_configuracion"
                re = json.url_configuracion
            Case "urlApiPdf"
                re = json.urlApiPdf
            Case "tokenApiPdf"
                re = json.tokenApiPdf
            Case "urlWebApi"
                re = json.urlWebApi
            Case "RutaAdjuntos"
                re = json.RutaAdjuntos
            Case Else
                re = ""
        End Select

        Return re
    End Function

    Public Sub GuardarArchivo(idBiopsia As Integer)
        Dim contexto As HttpContext = HttpContext.Current
        Dim ColeccionArchivos As HttpFileCollection = contexto.Request.Files
        Dim nombreArchivo As String = ""
        For i = 0 To ColeccionArchivos.Count - 1
            nombreArchivo = ColeccionArchivos(i).FileName
            Dim fi As New IO.FileInfo(ColeccionArchivos(i).FileName)
            Dim DatosArchivo As String = fi.Name
            DatosArchivo = DatosArchivo.Replace("+", " ")
            DatosArchivo = DatosArchivo.Replace("°", " ")
            DatosArchivo = DatosArchivo.Replace("  ", " ")
            DatosArchivo = DatosArchivo.Replace("   ", " ")

            If (DatosArchivo.Length > 100) Then
                DatosArchivo = DatosArchivo.Substring(0, 95)
                DatosArchivo &= ".pdf"
            End If
            ''Dim p As String = ConfigurationManager.AppSettings("RutaAdjunto").ToString()
            Dim p As String = CargarConf("RutaAdjuntos", True)
            Dim CarpetaParaGuardar As String = p + idBiopsia.ToString() + "/"
            Directory.CreateDirectory(CarpetaParaGuardar)
            ColeccionArchivos(i).SaveAs(CarpetaParaGuardar + DatosArchivo)
        Next
    End Sub

    Public Sub DeleteSession(ByVal key As String)
        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session.Remove(key)
        contexto.Response.End()
    End Sub

    'Public Sub GetArchivos(ByVal perfilUsuario As String, ByVal idBiopsia As String)
    '    Dim contexto As HttpContext = HttpContext.Current

    '    Dim jsonList As New List(Of Dictionary(Of String, String))
    '    Dim cons As String = String.Format("select * from ANA_Archivos_Biopsias where ANA_IdBiopsia = {0}", idBiopsia)
    '    Dim d As DataTable = consulta_sql_datatable(cons)
    '    For i = 0 To d.Rows.Count - 1
    '        'DataRow dtr = dt.Rows[i];
    '        'Dictionary<string, string> json = new Dictionary<string, string>();
    '        Dim dtr As DataRow = d.Rows(i)
    '        Dim json As New Dictionary(Of String, String)
    '        json("ANA_IdArchivos_Biopsias") = dtr("ANA_IdArchivos_Biopsias").ToString
    '        json("ANA_NomArchivos_Biopsias") = dtr("ANA_NomArchivos_Biopsias").ToString
    '        json("ANA_IdBiopsia") = dtr("ANA_IdBiopsia").ToString
    '        json("ANA_FecArchivos_Biopsias") = dtr("ANA_FecArchivos_Biopsias").ToString
    '        json("GEN_IdUsuarios") = dtr("GEN_IdUsuarios").ToString
    '        jsonList.Add(json)
    '    Next

    '    contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
    '    contexto.Response.End()
    'End Sub

    'Public Sub TablaMovBiopsia(ByVal idBiopsia As String)
    '    Dim contexto As HttpContext = HttpContext.Current

    '    Dim sb As New StringBuilder
    '    sb.AppendLine("SELECT")
    '    sb.AppendLine("am.ANA_IdMovimientos,")
    '    sb.AppendLine("am.ANA_FechaMovimientos,")
    '    sb.AppendLine("am.ANA_DescMovimientos,")
    '    sb.AppendLine("gu.GEN_loginUsuarios,")
    '    sb.AppendLine("mo.GEN_descripcionTipo_Movimientos_Sistemas")
    '    sb.AppendLine("FROM")
    '    sb.AppendLine("ANA_Movimientos AS am INNER JOIN")
    '    sb.AppendLine("GEN_Usuarios AS gu ON")
    '    sb.AppendLine("am.GEN_IdUsuarios = gu.GEN_idUsuarios LEFT OUTER JOIN")
    '    sb.AppendLine("GEN_Tipo_Movimientos_Sistemas as mo ON")
    '    sb.AppendLine("am.GEN_idTipo_Movimientos_Sistemas = mo.GEN_idTipo_Movimientos_Sistemas")
    '    sb.AppendLine("WHERE")
    '    sb.AppendLine(String.Format("am.ANA_IdBiopsia = {0} ", idBiopsia))
    '    sb.AppendLine("order by ANA_FechaMovimientos desc")

    '    Dim jsonList As New List(Of Dictionary(Of String, String))
    '    Dim d As DataTable = consulta_sql_datatable(sb.ToString())

    '    For Each dtr As DataRow In d.Rows
    '        Dim json As New Dictionary(Of String, String)
    '        json("ANA_IdMovimientos") = dtr("ANA_IdMovimientos").ToString
    '        json("ANA_FechaMovimientos") = dtr("ANA_FechaMovimientos").ToString
    '        json("ANA_DescMovimientos") = dtr("ANA_DescMovimientos").ToString
    '        json("GEN_loginUsuarios") = dtr("GEN_loginUsuarios").ToString
    '        json("GEN_descripcionTipo_Movimientos_Sistemas") = dtr("GEN_descripcionTipo_Movimientos_Sistemas").ToString
    '        jsonList.Add(json)
    '    Next

    '    contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
    '    contexto.Response.End()
    'End Sub
    'Public Sub TablaMuestrasporBiopsia(ByVal idBiopsia As String)
    '    Dim contexto As HttpContext = HttpContext.Current

    '    Dim sb As New StringBuilder
    '    sb.AppendLine("SELECT")
    '    'sb.AppendLine("drm.ANA_FrascoBiopsia,")
    '    sb.AppendLine("drm.ANA_CantidadMuestras,")
    '    sb.AppendLine("dc.ANA_DetalleCatalogo_Muestras,")
    '    sb.AppendLine("drm.ANA_DescripcionBiopsia")
    '    sb.AppendLine("FROM")
    '    sb.AppendLine("ANA_Detalle_Registro_Muestra AS drm INNER JOIN")
    '    sb.AppendLine("ANA_Detalle_Catalogo_Muestras AS dc ON")
    '    sb.AppendLine("drm.ANA_IdDetalleCatalogo_Muestras = dc.ANA_IdDetalleCatalogo_Muestras")
    '    sb.AppendLine("WHERE")
    '    sb.AppendLine("(drm.ANA_IdBiopsia =" & idBiopsia & ")")
    '    sb.AppendLine("order by ANA_IdDetalleBiopsia")


    '    Dim jsonList As New List(Of Dictionary(Of String, String))
    '    Dim d As DataTable = consulta_sql_datatable(sb.ToString())

    '    For Each dtr As DataRow In d.Rows
    '        Dim json As New Dictionary(Of String, String)
    '        'json("ANA_FrascoBiopsia") = dtr("ANA_FrascoBiopsia").ToString
    '        json("ANA_CantidadMuestras") = dtr("ANA_CantidadMuestras").ToString
    '        json("ANA_DetalleCatalogo_Muestras") = dtr("ANA_DetalleCatalogo_Muestras").ToString
    '        json("ANA_DescripcionBiopsia") = dtr("ANA_DescripcionBiopsia").ToString
    '        jsonList.Add(json)
    '    Next

    '    contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
    '    contexto.Response.End()
    'End Sub

    Public Sub GetSession()
        Dim contexto As HttpContext = HttpContext.Current
        Dim json As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

        For index = 0 To contexto.Session.Count - 1
            If contexto.Session(index) IsNot Nothing Then
                Dim key As String = contexto.Session.Keys(index).ToString()
                Dim value As String = contexto.Session(index).ToString()
                json.Add(key, value)
            End If
        Next

        json.Add("FECHA_ACTUAL", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"))
        contexto.Response.Write(JsonConvert.SerializeObject(json))
        contexto.Response.End()
    End Sub

    Public Sub DeleteArchivo(ByVal nombreArchivo As String, idBiopsia As Integer)

        Try

            ''Dim p As String = ConfigurationManager.AppSettings("RutaAdjunto").ToString()
            Dim p As String = CargarConf("RutaAdjuntos", True)
            Dim sPath As String = p + idBiopsia.ToString() + "/" + nombreArchivo

            If File.Exists(sPath) Then
                File.Delete(sPath)
            End If

        Catch ex As Exception
            Log.WriteLog("Error al eliminar archivo: " + ex.Message + " " + ex.StackTrace)
        End Try

    End Sub
    Public Sub MoverArchivo(ByVal nombreArchivo As String, idBiopsia As Integer)

        Try
            ''Dim p As String = ConfigurationManager.AppSettings("RutaAdjunto").ToString()
            Dim p As String = CargarConf("RutaAdjuntos", True)
            Dim sPathAnterior As String = p + nombreArchivo
            Dim sPathNueva As String = p + idBiopsia.ToString + "\" + nombreArchivo

            Dim imp As New UserImpersonation()
            imp.impersonateUser("Desarrollo", "", "Hcm2016")

            If File.Exists(sPathAnterior) Then
                Directory.CreateDirectory(p + idBiopsia.ToString + "\")
                File.Move(sPathAnterior, sPathNueva)
            End If

        Catch ex As Exception
            Log.WriteLog("Error al mover archivo: " + ex.Message + " " + ex.StackTrace)
        End Try

    End Sub

    'Public Sub InsertArchivo(ByVal nArchivo As String, ByVal idBiopsia As String, ByVal idUsuario As String, ByVal fecha As String)
    '    Dim contexto As HttpContext = HttpContext.Current

    '    Dim cons As String = String.Format("select * from ANA_Archivos_Biopsias where ANA_NomArchivos_Biopsias = '{0}' and ANA_IdBiopsia = {1} and GEN_IdUsuarios = {2}", nArchivo, idBiopsia, idUsuario)
    '    Dim d As DataTable = consulta_sql_datatable(cons)
    '    If d.Rows.Count > 0 Then
    '        'ya existe nombre de archivo con el mismo nombre de usuario y biopsia
    '    Else
    '        cons = String.Format("insert into ANA_Archivos_Biopsias(ANA_NomArchivos_Biopsias, ANA_IdBiopsia, ANA_FecArchivos_Biopsias, GEN_IdUsuarios) values('{0}',{1},'{2}',{3})", nArchivo, idBiopsia, fecha, idUsuario)
    '        ejecuta_sql(cons)
    '    End If

    '    contexto.Response.Write("")
    '    contexto.Response.End()
    'End Sub


    Public Sub SetSession(ByVal key As String, ByVal value As String)

        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session(key) = value
        contexto.Response.Write("{'" + key + "':'" + value.ToString() + "'}")
        contexto.Response.End()
    End Sub


#Region "WEB API PDF"

    Public Sub ObtenerApiPdf(id As Integer)

        Try

            Dim contexto As HttpContext = HttpContext.Current
            Dim dominio As String = CargarConf("urlApiPdf", True)
            'Dim dominio As String = funciones.getPropiedad(contexto, "urlApiPdf").ToString()
            Dim url As String = dominio & "api/protocolo/operatorio/" & id & "/pdf"

            contexto.Response.ContentType = "application/pdf"
            contexto.Response.BufferOutput = True
            contexto.Response.AddHeader("Content-Disposition", "inline;filename=Formulario.pdf")
            contexto.Response.BinaryWrite(ObtenerFileWebApi(url))
            contexto.Response.Flush()
            contexto.Response.End()

        Catch ex As Exception
            Dim fail As String = ex.Message & ex.StackTrace
        End Try

    End Sub

    Private Function ObtenerFileWebApi(sUrl As String) As Byte()

reiniciar: Dim contexto As HttpContext = HttpContext.Current
        Try
            Dim token As String = "Bearer " + GetTokenApiPdf()
            Dim WebClient As New System.Net.WebClient()
            WebClient.Headers.Add("Authorization", token)
            WebClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")

            Return WebClient.DownloadData(sUrl)

        Catch ex As System.Net.WebException
            Dim fail As String = ex.Message & ex.StackTrace
            Dim response As System.Net.HttpWebResponse = CType(ex.Response, System.Net.HttpWebResponse)
            If response.StatusCode = System.Net.HttpStatusCode.Unauthorized Then
                'funciones.setPropiedad(contexto, "tokenApiPdf", "")
                GuardarConf("tokenApiPdf", "")
                GoTo reiniciar
            End If
        End Try

        Return Nothing
    End Function

    Private Function GetTokenApiPdf() As String

        Dim contexto As HttpContext = HttpContext.Current

        Try
            'If funciones.getPropiedad(contexto, "tokenApiPdf").ToString() = "" Then
            If CargarConf("tokenApiPdf", True) = "" Then
                Dim dominio As String = CargarConf("urlApiPdf", True)
                'Dim dominio As String = funciones.getPropiedad(contexto, "urlApiPdf").ToString()
                Dim url As String = dominio + "oauth/token"
                Dim webClient As New System.Net.WebClient()
                Dim parametros As New System.Collections.Specialized.NameValueCollection()
                parametros.Add("grant_type", "client_credentials")
                parametros.Add("client_id", "1")
                parametros.Add("client_secret", "Xp2zY4rwPoBZQ3SyBADFD2QO1YmNJaWtTICPff1M")

                Dim responsebytes As Byte() = webClient.UploadValues(url, "POST", parametros)
                Dim responsebody As String = (New System.Text.UTF8Encoding()).GetString(responsebytes)

                Dim json As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(responsebody)
                'funciones.setPropiedad(contexto, "tokenApiPdf", json("access_token").ToString())
                GuardarConf("tokenApiPdf", json("access_token").ToString())

                'Return funciones.getPropiedad(contexto, "tokenApiPdf").ToString()
                Return CargarConf("tokenApiPdf", True)
            End If

            'Return funciones.getPropiedad(contexto, "tokenApiPdf").ToString()
            Return CargarConf("tokenApiPdf", True)
        Catch ex As System.Net.WebException
            Dim fail As String = ex.Message & ex.StackTrace
        End Try

        Return Nothing
    End Function

#End Region



    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class