﻿Imports System.Web.Services
Imports ClosedXML.Excel
Imports Newtonsoft.Json

Partial Public Class frm_BiopsiasCriticas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    <WebMethod>
    Public Shared Function detalleBiopsia(idBiopsiaCritico)
        Dim jsonList As New Dictionary(Of String, String)

        Dim id = idBiopsiaCritico

        Dim bc As New BiopsiaCritico
        bc.BiopsiaCritico(id)

        Dim usuariosNot As New Usuarios
        usuariosNot.Usuarios(CInt(bc.Get_GEN_IdUsuarios_NotificadoBiopsias_Critico()))

        jsonList("usuarioNotificado") = usuariosNot.Get_GEN_NombreCompleto
        jsonList("diagnostico") = bc.Get_Ana_ObservacionCritico()
        jsonList("respuestaNotificacion") = bc.Get_ANA_RespuestaBiopsias_Critico()
        jsonList("fechaNotificacion") = bc.Get_ANA_FechaBiopsias_Critico()

        Dim usuariosPat As New Usuarios
        usuariosPat.Usuarios(CInt(bc.Get_GEN_IdUsuariosCritico()))
        jsonList("nombrePatologo") = usuariosPat.Get_GEN_NombreCompleto

        Return JsonConvert.SerializeObject(jsonList)
    End Function

    <WebMethod>
    Public Shared Function grillaCriticos(fechaInicio, fechaFin, idPatologo, visto)
        Dim jsonList As New List(Of Dictionary(Of String, String))

        Dim con As New Consultas

        If visto = "0" Then
            visto = ""
        End If
        Dim d As DataTable = consulta_sql_datatable(con.Ds_con_impr_biopsia_critico(fechaInicio, fechaFin, idPatologo, visto))
        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            For i = 0 To dtr.Table.Columns.Count - 1
                json(dtr.Table.Columns(i).ColumnName) = dtr(dtr.Table.Columns(i).ColumnName).ToString
            Next
            jsonList.Add(json)
        Next

        Return JsonConvert.SerializeObject(jsonList)
    End Function

    Protected Sub btnExportarH_Click(sender As Object, e As EventArgs)
        Dim sCommand As String = Request.Form("__EVENTARGUMENT")

        Dim settings As New JsonSerializerSettings()
        settings.NullValueHandling = NullValueHandling.Ignore
        settings.MissingMemberHandling = MissingMemberHandling.Ignore

        Dim des As List(Of List(Of Object)) = JsonConvert.DeserializeObject(Of List(Of List(Of Object)))(sCommand, settings)
        Dim dic As New List(Of Dictionary(Of String, String))

        For i = 0 To des.Count - 1
            Dim d As New Dictionary(Of String, String)
            d("Nombre paciente") = des(i)(1).ToString()
            d("Rut") = des(i)(2).ToString()
            d("N° de examen") = des(i)(3).ToString()
            d("Fecha validación") = des(i)(4).ToString()
            d("Fecha notificación") = des(i)(5).ToString()
            d("Descripción diagnóstico") = des(i)(6).ToString()
            d("Días notificación") = des(i)(7).ToString()
            d("Patólogo") = des(i)(8).ToString()
            d("Fecha lectura") = des(i)(9).ToString()
            d("Respuesta") = des(i)(10).ToString()
            d("Usuario notificado") = des(i)(11).ToString()
            dic.Add(d)
        Next

        Dim workbook As New XLWorkbook
        funciones.CrearHojaExcel(workbook, "Biopsias críticas", dic)
        funciones.ExportarTablaClosedXml(Me.Page, "Biopsias críticas", workbook)
    End Sub
End Class