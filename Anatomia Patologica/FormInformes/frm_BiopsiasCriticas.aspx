﻿<%@ Page Title="Inf. Biopsias Criticas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasCriticas.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasCriticas" %>
<%--<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_BiopsiasCriticas.js") %>'}?${new Date().getTime()}` });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblCritico').DataTable();
                __doPostBack("<%= btnExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
    <%--<script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_grilla_critico").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_grilla_critico").find("tr:first"))).dataTable();
        }
    </script>--%>    
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de críticos</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-2">
                        <label>Desde:</label>
                        <input type="date" id="txtDesde" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <input type="date" id="txtHasta" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <label>Patólogo:</label>
                        <select id="selPatologo" class="form-control">
                            <option value="0">-Seleccione-</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Visto:</label>
                        <select id="selVisto" class="form-control">
                            <option value="0">-Seleccione-</option>
                            <option value="SI">SI</option>
                            <option value="NO">NO</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <button id="btnBuscar" class="btn btn-success" style="width:100%;">Buscar</button>
                    </div>
                    <div class="col-md-2">
                        <br />
                        <button id="btnExportar" class="btn btn-success" style="width:100%;">Exportar a excel</button>
                        <asp:Button runat="server" ID="btnExportarH" OnClick="btnExportarH_Click" style="display:none;"/>
                    </div>
                </div>
            </div>
            <hr />
            <div>
                <table id="tblCritico" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mdlDetalle" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de crítico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Usuario notificado:</label>
                            <input id="txtUsuarioNotificado" disabled class="form-control"/>
                        </div>
                        <div class="col-md-6">
                            <label>Fecha notificación:</label>
                            <input type="date" id="txtFechaNotificado" disabled class="form-control"/>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="col-md-12">
                            <label>Nombre del patólogo:</label>
                            <input id="txtNombrePatologo" disabled class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Diagnóstico:</label>
                            <textarea id="txtDiagnostico" disabled rows="3" class="form-control" style="resize:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Respuesta notificación:</label>
                            <textarea id="txtRespuestaNotificacion" disabled rows="3" class="form-control" style="resize:none;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>




    <%--<div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de Criticos</h2>
        </div>
        <div class="panel-body">
            <div class="row text-left">
                <div class="col-md-2">
                    <label for="txt_fec_inicio">Desde:</label>
                    <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required" CssClass="form-control">
                    </asp:TextBox>
                </div>
                <div class="col-md-2">
                    <label>Hasta:</label>
                    <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required" CssClass="form-control">
                    </asp:TextBox>
                </div>
                <div class="col-md-3">
                    <label>Patologo:</label>
                    <div class="form-inline">
                        <div class="row">
                            <div class="col-md-2">
                                <asp:CheckBox ID="chk_patologo" runat="server" />
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="cmb_patologo" runat="server" AppendDataBoundItems="True" CssClass="form-control"
                                    Enabled="False">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Leido:</label>
                    <div class="row">

                        <div class="form-inline">
                            <div class="col-md-2">
                                <asp:CheckBox ID="chk_leido" runat="server" />
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList ID="cmb_leido" runat="server" Enabled="False" CssClass="form-control">
                                    <asp:ListItem>SI</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <br />
                    <div class="form-inline">
                        <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                        <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                    </div>

                </div>

            </div>
            <hr />

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <asp:UpdateProgress ID="upp_general" runat="server">
                            <ProgressTemplate>
                                <asp:Label ID="Label5" runat="server" Text="Espere un momento..." CssClass="alert alert-info"></asp:Label>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:GridView ID="grilla_critico" runat="server" AutoGenerateColumns="False" role="grid"
                            DataSourceID="dts_ListaCriticos" CssClass="table table-bordered  table-hover table-responsive" style="font-size:small;">
                            <Columns>
                                <asp:BoundField DataField="NomPaciente" HeaderText="Nombre Paciente" ReadOnly="True" />

                                <asp:BoundField DataField="RutPaciente" HeaderText="Rut" ReadOnly="True">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="n_biopsia" HeaderText="Nº de Examen" ReadOnly="True" />
                                <asp:BoundField DataField="ANA_fecValidaBiopsia" HeaderText="Fecha Validación" />
                                <asp:BoundField DataField="ANA_FechaBiopsias_Critico" HeaderText="Fecha Notificación" />
                                <asp:BoundField DataField="DiasNotificacion" HeaderText="Días Not." />
                                <asp:BoundField DataField="NomPatologo" HeaderText="Patologo" />
                                <asp:BoundField DataField="ANA_DescDiagnostico" HeaderText="Diagnóstico" Visible="false" />
                                <asp:BoundField DataField="UsuarioNotificado" HeaderText="Nombre de Quien Recibe Notificación" Visible="false"/>
                                <asp:BoundField DataField="ANA_RespuestaBiopsias_Critico" HeaderText="Respuesta" Visible="false"/>
                                <asp:BoundField DataField="ANA_FechaLeidoBiopsias_Critico" HeaderText="Fecha de lectura" NullDisplayText="-"/>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Detalle
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnk_Detalle" runat="server" Text="ver" Style="color: white;" CommandArgument='<%# Eval("ANA_IdBiopsias_Critico") %>' 
                                            onclick="lnk_Detalle_Click" CssClass="btn btn-info">
                                                <span class="glyphicon glyphicon-list"></span>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:ModalPopupExtender ID="mdl_Detalle" runat="server" BackgroundCssClass="modalBackground"
                            DropShadow="true" PopupControlID="pnl_Detalle" TargetControlID="b">
                        </asp:ModalPopupExtender>
                        <asp:Button runat="server" ID="b" style="display:none;" />
                    </div>
                    <br />
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                        <wuc:Alert ID="wuc_alert" runat="server" />



                    <asp:Panel ID="pnl_Detalle" runat="server">
                        <div class="panel panel-primary" style="width: 500px;">
                            <div class="panel-heading">Detalle de crítico</div>
                            <div class="panel-body">                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Usuario notificado:</label>
                                        <asp:TextBox ID="txt_UsuarioNotificado" runat="server" Enabled="false" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Diagnóstico:</label>
                                        <asp:TextBox ID="txt_Diagnostico" runat="server" Enabled="false" TextMode="MultiLine" CssClass="form-control"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Respuesta Notificación: </label>
                                        <asp:TextBox ID="txt_RespuestaNotificacion" runat="server" TextMode="MultiLine" CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" Text="Salir" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
                    <asp:PostBackTrigger ControlID="Btn_Excel" />
                </Triggers>
            </asp:UpdatePanel>


        </div>
    </div>
    <asp:SqlDataSource ID="dts_ListaCriticos" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_Cnt_Principal_chk_patologo").click(function () {
                if ($(this).is(":checked")) {
                    $("#ctl00_Cnt_Principal_cmb_patologo").prop("disabled", false);
                } else {
                    $("#ctl00_Cnt_Principal_cmb_patologo").prop("disabled", true);
                }
            });
            $("#ctl00_Cnt_Principal_chk_leido").click(function () {
                if ($(this).is(":checked")) {
                    $("#ctl00_Cnt_Principal_cmb_leido").prop("disabled", false);
                } else {
                    $("#ctl00_Cnt_Principal_cmb_leido").prop("disabled", true);
                }
            });
        });
    </script>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>--%>
</asp:Content>
