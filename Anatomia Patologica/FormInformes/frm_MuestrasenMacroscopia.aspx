﻿<%@ Page Title="Muestras en Macroscopia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MuestrasenMacroscopia.aspx.vb" Inherits="Anatomia_Patologica.frm_MuestrasenMacroscopia" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormInformes/frm_MuestrasenMacroscopia.js") %>'}?${new Date().getTime()}` });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnExportar').click(function () {
                var tblObj = $('#tblMuestrasenMacroscopia').DataTable();
                __doPostBack("<%= btnExportarH.UniqueID %>", JSON.stringify(tblObj.rows().data().toArray()));
            });
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Informe de muestras en macroscopía</h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2 col-md-offset-10">
                    <button id="btnExportar" class="btn btn-success" style="width: 100%; margin-top: 4px;">Exportar</button>
                    <asp:Button runat="server" ID="btnExportarH" onclick="btnExportarH_Click" style="display:none;"/>
                </div>
            </div>
            <hr />
            <table id="tblMuestrasenMacroscopia" class="table table-bordered table-hover table-responsive"></table>
        </div>
    </div>
</asp:Content>
