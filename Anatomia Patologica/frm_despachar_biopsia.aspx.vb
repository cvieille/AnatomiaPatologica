﻿Public Partial Class frm_despachar_biopsia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then

            Dim id_biopsia As String = Request.QueryString("id")
            Me.txt_id.Text = id_biopsia

            ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
            '==========================================================
            Dim com As New Combobox
            com.Get_Origen_Servicios_combobox(Me.cmb_tipo_destino)

            Try
                ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
                '==========================================================
                Dim rm As New Registro_Biopsia
                rm.Registro_Biopsia(id_biopsia)

                
                Me.txt_biopsia_a.Text = Trim(rm.Get_ANA_AñoBiopsia())
                Me.txt_biopsia_n.Text = Trim(rm.Get_ANA_NumBiopsia())
                Me.txt_organo.Text = Trim(rm.Get_ANA_OrganoBiopsia())
                Me.txt_estado.Text = Trim(rm.Get_ANA_EstadoRegistro_Biopsias())

                Me.txt_patologo.Text = rm.Get_GEN_IdUsuarioPatologo()

                If rm.Get_ANA_DictadaBiopsia() = "SI" Then chk_dictado.Checked = True

                If rm.Get_ANA_TumoralBiopsia() = "SI" Then chk_tumoral.Checked = True

                If rm.Get_ANA_DictadaBiopsia() = "SI" Then chk_rapida.Checked = True

                Me.txt_fecha.Text = Today.Date

            Catch ex As Exception
                Image1.Visible = True
                lbl_debe_guardar.Visible = True
                lbl_debe_guardar.Text = ex.Message

            End Try

        End If

    End Sub

    Protected Sub cmd_guardar0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_guardar0.Click
        Response.Redirect("frm_principal.aspx")
    End Sub

    'Protected Sub cmd_guardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_guardar.Click

    '    ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
    '    '==========================================================
    '    Dim rm As New Registro_Biopsia
    '    rm.Registro_Biopsia(Me.txt_id.Text)

    '    rm.Set_ANA_fecDespBiopsia(Me.txt_fecha.Text)
    '    rm.Set_ANA_EstadoRegistro_Biopsias("Despachado")
    '    rm.Set_ANA_NomRecibeBiopsia(Me.txt_recibe_informe.Text)
    '    rm.Set_GEN_IdUsuarioDespacha(Session("id_usuario"))
    '    rm.Set_ANA_DespachadaBiopsia("SI")
    '    rm.Set_ModificaRegistroBiopsia()

    '    '*** CREA MOVIMIENTO REALIZADO
    '    '==========================================================
    '    Dim mov As New movimientos
    '    mov.Set_ANA_DetalleMovimiento("Se despacha informe a:" & Me.cmb_servicio_dest.Text & ", lo recibe:  " & Me.txt_recibe_informe.Text & ", con fecha:" & Me.txt_fecha.Text)
    '    mov.Set_ANA_IdBiopsia(Me.txt_id.Text)
    '    mov.Set_GEN_IdUsuarios(Session("id_usuario"))
    '    mov.Set_CrearNuevoMoviento()
    'End Sub
End Class