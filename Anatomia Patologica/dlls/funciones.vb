﻿Imports System.Web.UI.WebControls.DropDownList

Module funciones
    'Public cadena_conexion As String = "Data Source=INTRANET\SQLEXPRESS;Initial Catalog=anatomia_patologica;Persist Security Info=True;User ID=sa;Password=hcmpartes"
    Public cadena_conexion As String = ConfigurationManager.ConnectionStrings("anatomia_patologicaConnectionString").ConnectionString

    Public cadena_guardar_dcto_adj = "C:\GIAP\GIAP\dcto\adj\"
    Public connectionStringODBC As String = "Dsn=salud;uid=cvieille;pwd=cv2014"
    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================

    Public Function crea_nueva_nomina(ByVal estado_a, ByVal estado_n, ByVal id_usuario)

        Dim consulta As String
        Dim fecha As String
        Dim ID As Integer

        fecha = Date.Today

        Dim cnnBaseDat As SqlClient.SqlConnection
        Dim comBaseDat As SqlClient.SqlCommand

        cnnBaseDat = New SqlClient.SqlConnection
        cnnBaseDat.ConnectionString = cadena_conexion
        cnnBaseDat.Open()

        consulta = "INSERT INTO nomina_mov(fecha, estado_nuevo, estado_ant, id_usuario)"
        consulta = consulta & "VALUES( '" & fecha & "', '" & estado_n & "', '" & estado_a & "', " & id_usuario & " )"

        Dim query2 As String = "Select @@Identity"

        comBaseDat = New SqlClient.SqlCommand(consulta, cnnBaseDat)
        comBaseDat.ExecuteNonQuery()

        comBaseDat.CommandText = query2
        ID = comBaseDat.ExecuteScalar()

        crea_nueva_nomina = ID
        cnnBaseDat.Close()

    End Function

    Public Sub crea_nueva_detalle_nomina(ByVal id_biopsia, ByVal id_corte, ByVal estado, ByVal id_usuario, ByVal id_nomina)

        Dim consulta As String

        consulta = "INSERT INTO detalle_nomina_mov (id_biopsia, id_corte, id_nomina)"
        consulta = consulta & "VALUES(" & id_biopsia & ", " & id_corte & ", " & id_nomina & ")"

        ejecuta_sql(consulta)

    End Sub

    Public Function RutDigito(ByVal Rut As Integer) As String
        Dim Digito As Integer
        Dim Contador As Integer
        Dim Multiplo As Integer
        Dim Acumulador As Integer

        Contador = 2
        Acumulador = 0
        While Rut <> 0
            Multiplo = (Rut Mod 10) * Contador
            Acumulador = Acumulador + Multiplo
            Rut = Rut \ 10
            Contador = Contador + 1
            If Contador = 8 Then
                Contador = 2
            End If
        End While
        Digito = 11 - (Acumulador Mod 11)
        RutDigito = CStr(Digito)
        If Digito = 10 Then RutDigito = "K"
        If Digito = 11 Then RutDigito = "0"
    End Function

    Public Function Get_FechaconHora()
        Get_FechaconHora = Now
    End Function

    Public Function reemplaza_comillas(ByRef texto)
        reemplaza_comillas = Replace(texto, "'", "#")
    End Function

    Public Function devuelve_dias_habiles(ByVal f_inicio As DateTime, ByVal f_fin As DateTime)
        Dim fecha_inicial As String = f_inicio
        Dim fecha_final As String = f_fin

        Dim d1 As DateTime = f_inicio
        Dim d2 As DateTime = f_fin

        Dim iDays As Integer = DateDiff(DateInterval.Day, d1, d2)

        'Creo una variable de tipo fecha temporal
        Dim dTemp As DateTime = d1
        Dim iCounter As Integer
        'leo los días completos
        For i As Integer = 1 To iDays
            'checo si es sabado o domingo, si no es así le sumo los días..
            If dTemp.DayOfWeek <> DayOfWeek.Saturday And dTemp.DayOfWeek <> DayOfWeek.Sunday Then
                iCounter += 1
            End If
            dTemp = dTemp.AddDays(1)
        Next

        Return iCounter

    End Function

    Public Sub selecciona_todo_grilla(ByVal nombre_grilla As GridView, ByVal nombre_chk As String, ByVal activa As Boolean)
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        For Each row As GridViewRow In nombre_grilla.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl(nombre_chk), CheckBox)

            If activa = True Then
                elcheckbox.Checked = True
            Else
                elcheckbox.Checked = False
            End If

        Next

    End Sub
    '---------------------------------------------------
    ' MUESTRA U OCULTA UNA COLUMNA DE UN GRIDVIEW
    '---------------------------------------------------
    Public Sub OcultarColumna(ByVal gdv As GridView, ByVal bool As Boolean, ByVal columna As Byte)
        On Error Resume Next
        gdv.HeaderRow.Cells(columna).Visible = bool
        For Each row As GridViewRow In gdv.Rows
            row.Cells(columna).Visible = bool
        Next
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' DEVUELVE EL HASH MD5 DE LA CLAVE PASADA POR PARAMETRO
    '----------------------------------------------------------------------------------------------------
    Public Function get_HashClaveMD5(clave() As Char) As String

        ' SE ESPECIFICA EL OBJ QUE ENCRIPTA LA CLAVE A UN HASH MD5
        '====================================================================================================
        Dim md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()

        ' SE ESPECIFICA EL ARREGLO DE BYTES QUE GUARDA LOS CARACTERE DE LA CLAVE ENCRIPTADA
        '====================================================================================================
        Dim array() As Byte = System.Text.Encoding.UTF8.GetBytes(clave)
        array = md5.ComputeHash(array)
        Dim hashDevuelto As New System.Text.StringBuilder

        ' SE ARMA EL STRING PARA ENTREGAR LA CLAVE ENCRIPTADA
        '====================================================================================================
        For Each b As Byte In array
            hashDevuelto.Append(b.ToString("x2").ToLower())
        Next

        Return hashDevuelto.ToString()
    End Function
#Region "PDF"

    Public Sub ExportarPDF(ByVal baseURL As String, ByVal page As Page, ByRef nombreArchivo As String)

        Dim pdf_page_size As String = SelectPdf.PdfPageSize.Letter
        Dim pageSize As SelectPdf.PdfPageSize = DirectCast([Enum].Parse(GetType(SelectPdf.PdfPageSize),
                pdf_page_size, True), SelectPdf.PdfPageSize)
        Dim converter As New SelectPdf.HtmlToPdf()

        Dim sw As New System.IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        page.RenderControl(hw)

        Dim htmlString As String = sw.ToString()

        converter.Options.PdfPageSize = pageSize
        converter.Options.WebPageWidth = 765
        converter.Options.WebPageHeight = 990
        'converter.Options.WebPageWidth = 612
        'converter.Options.WebPageHeight = 792

        converter.Options.MarginBottom = 14
        converter.Options.MarginLeft = 14
        converter.Options.MarginRight = 14
        converter.Options.MarginTop = 14

        Dim doc As SelectPdf.PdfDocument = converter.ConvertHtmlString(htmlString, baseURL)

        'doc.Save(page.Response, False, nombreArchivo & ".pdf")
        'doc.Close()

        Dim ms As New System.IO.MemoryStream()
        doc.Save(ms)
        doc.Close()

        page.Response.ContentType = "application/pdf"
        page.Response.BufferOutput = True
        page.Response.AddHeader("Content-Disposition", "inline;filename=" + nombreArchivo + ".pdf")
        page.Response.BinaryWrite(ms.ToArray())
        page.Response.Flush()

    End Sub

#End Region

#Region "URL"
    Public Function getUrlAbsolutaHost(ByRef page As Page)
        Return page.Request.Url.Scheme & "://" & page.Request.Url.Authority _
                & page.Request.ApplicationPath.TrimEnd("/") & "/"
    End Function

    Public Sub redirigirPagina(ByRef page As Page)

        Select Case page.Session("perfil_del_usuario")
            Case 2, 3, 4, 6, 7, 8
                page.Response.Redirect("FormPrincipales/frm_principal.aspx")
            Case Else
                page.Response.Redirect("frm_login.aspx")
        End Select

        'Case "Administrativo"
        '        page.Response.Redirect("frm_principal_adm.aspx")

        '    Case "Usuario Limitado"
        '        page.Response.Redirect("frm_login.aspx")
        '    Case 6
        '        page.Response.Redirect("FormPrincipales/frm_principal.aspx")
        '    Case 4
        '        page.Response.Redirect("FormPrincipales/frm_principal.aspx")
        '    Case "Supervisor Técnologo"
        '        page.Response.Redirect("FormPrincipales/frm_principal.aspx")
        '    Case "Auxiliar"
        '        page.Response.Redirect("FormPrincipales/frm_principal.aspx")

        '    Case "Clínico"
        '        page.Response.Redirect("FormPrincipales/frm_PrincipalClinico.aspx")
        '    Case "Registro de Tumores"
        '        page.Response.Redirect("FormPrincipales/frm_PrincipalGestionClinica.aspx")
        '    Case "Enfermera Criticos"
        '        page.Response.Redirect("FormPrincipales/frm_PrincipalClinico.aspx")
        '    Case "Recaudación"
        '        page.Response.Redirect("frm_consulta_clinicos.aspx")
        '    
        'End Select
    End Sub

#End Region
End Module