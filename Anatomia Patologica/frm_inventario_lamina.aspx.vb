﻿Public Partial Class frm_inventario_lamina
    Inherits System.Web.UI.Page

    
    Protected Sub cmb_exportar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_exportar.Click

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        grilla_lamina.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(grilla_lamina)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Técnicas Pendientes</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=laminas" & Get_FechaconHora() & ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()

    End Sub

    Private Sub frm_inventario_lamina_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
            '==========================================================
            Dim com As New Combobox
            com.EstadoLamina_ComboBox(Me.cmb_estado)
        End If
    End Sub
End Class