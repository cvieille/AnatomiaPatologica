﻿<%@ Page Title="Anular Registro" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_AnularRegistro.aspx.vb" Inherits="Anatomia_Patologica.frm_AnularRegistro" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_AnularRegistro.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Anular biopsias</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2 col-md-offset-3">
                            <label>N°:</label>
                            <input id="txtNBiopsia" type="text" class="form-control"/>
                        </div>
                        <div class="col-md-2">
                            <label>Año:</label>
                            <input id="txtAñoBiopsia" type="text" class="form-control"/>
                        </div>
                        <div class="col-md-2">
                            <br />
                            <button id="btnBuscarBiopsia" class="btn btn-success" style="margin-top: 4px;">Buscar biopsia</button>
                        </div>
                    </div>
                    
                    <hr />
                    <div>
                        <table id="tblBiopsia" class="table table-bordered table-hover table-responsive"></table>
                    </div>
                    <div id="alertEstado" style="display:none;" class="alert alert-warning text-center">Solamente se pueden anular registros con estado <b>Ingresado</b></div>
                    <hr />
                    

                    <div class="row">
                        <div class="col-md-4">
                            <label>Paciente:</label>
                            <input id="txtPaciente" disabled type="text" class="form-control"/>
                        </div>
                        <div class="col-md-8">
                            <label>Motivo:</label>
                            <textarea id="txtMotivo" disabled maxlength="300" rows="4" style="resize:none;" class="form-control"></textarea>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <button id="btnAnular" class="btn btn-primary pull-right" disabled>Anular</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>