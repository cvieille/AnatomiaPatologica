<%@ Page Title="Plantillas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaPlantillas.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_plantillas" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_ListaPlantillas.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Listado de Plantillas</h2>
        </div>
        <div class="panel-body">
            <div class="pull-right" style="margin-bottom: 10px;">
                <button id="btnNuevoRegistro" class="btn btn-success">Nuevo registro</button>
            </div>
            <hr />
            <div>
                <table id="tblPlantillas" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>
</asp:Content>
