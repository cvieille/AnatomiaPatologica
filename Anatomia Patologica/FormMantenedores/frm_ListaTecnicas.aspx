﻿<%@ Page Title="Lista de Técnicas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaTecnicas.aspx.vb" Inherits="Anatomia_Patologica.frm_Lista_Tecnicas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_ListaTecnicas.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Listado de técnicas</h2>
        </div>
        <div class="panel-body">
            <div class="pull-right" style="margin-bottom: 10px;">
                <button id="btnNuevoRegistro" class="btn btn-success">Nuevo registro</button>
            </div>
            <hr />
            <div>
                <table id="tblTecnicas" class="table table-bordered table-hover table-responsive"></table>
            </div>
        </div>
    </div>

    <div id="mdlTecnicas" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de técnicas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Id:</label>
                            <input id="txtIdTecnica" type="text" disabled class="form-control" value="0"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre:</label>
                            <input id="txtNombreTecnica" type="text" maxlength="50" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Estado:</label>
                            <select id="selEstadoTecnica" class="form-control">
                                <option value="0">-Seleccione-</option>
                                <option value="Activo">Activo</option>
                                <option value="Inactivo">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnGuardarTecnica" class="btn btn-primary">Guardar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>   
</asp:Content>