﻿<%@ Page Title="Listado de programas" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaProgramas.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_programas" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMantenedores/frm_ListaProgramas.js") %>'}?${new Date().getTime()}` });
    </script>
    <%--<script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Programas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Programas").find("tr:first"))).dataTable();
        }
    </script>--%>
</asp:Content>


<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="text-center">Listado de programas</h2>
                </div>
                <div class="panel-body">
                    <div class="pull-right" style="margin-bottom: 10px;">
                        <button id="btnNuevoRegistro" class="btn btn-success">Nuevo registro</button>
                    </div>
                    <hr />
                    <div>
                        <table id="tblProgramas" class="table table-bordered table-hover table-responsive"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mdlEditarPrograma" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de programas</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Id:</label>
                            <input id="txtIdPrograma" type="text" disabled class="form-control" value="0"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Nombre:</label>
                            <input id="txtNombrePrograma" type="text" maxlength="50" class="form-control"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Estado:</label>
                            <select id="selEstadoPrograma" disabled class="form-control">
                                <option value="0">-Seleccione-</option>
                                <option selected value="Activo">Activo</option>
                                <option value="Inactivo">Inactivo</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnGuardarPrograma" class="btn btn-primary">Guardar</button>
                    <button class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>   



    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listado de Programas
                    <asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                    <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"
                       Enabled="True" TargetControlID="nuevo" CancelControlID="cmd_cancelar"
                        PopupControlID="pnl_nuevo">
                    </asp:ModalPopupExtender>
                </div>

                <div class="panel-body">
                    <asp:Panel ID="pnl_nuevo" runat="server" Style="display: none;">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Registro de Programas</div>
                            <div class="panel-body">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td>
                                            <label>Id:</label>
                                            <asp:TextBox ID="txt_id" runat="server" CssClass="form-control" ReadOnly="True" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Nombre:</label>
                                            <asp:TextBox ID="txt_nombre" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Estado:</label>
                                            <asp:DropDownList ID="ddl_estado" runat="server" CssClass="form-control" >
                                                <asp:ListItem Text="Activo" ></asp:ListItem>
                                                <asp:ListItem Text="Inactivo" ></asp:ListItem>
                                            </asp:DropDownList>                                            
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" class="btn btn-warning" />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False"
                                                class="alert alert-danger" role="alert">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:GridView ID="gdv_Programas" runat="server"
                        AutoGenerateColumns="False" DataSourceID="dts_Programas"
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="GEN_IdPrograma" HeaderText="Id" SortExpression="GEN_IdPrograma" />
                            <asp:BoundField DataField="GEN_NomPrograma" HeaderText="Descripción" SortExpression="GEN_NomPrograma" />
                            <asp:BoundField DataField="GEN_estadoPrograma" HeaderText="Estado" SortExpression="GEN_estadoPrograma" />

                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:Button ID="cmd_editar" runat="server" Text="Editar Programa"
                                        CssClass="btn btn-success" CommandArgument='<%# Eval("GEN_IdPrograma") %>'
                                        OnClick="cmd_editar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_Programas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>--%>

</asp:Content>
