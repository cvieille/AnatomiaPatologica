﻿Imports System.Web.Services

Public Class frm_Clave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    <WebMethod>
    Public Shared Function claveActual(sPass, idUsuario)
        'sPass = sPass.ToString().ToLower()
        Dim sql As New StringBuilder
        sql.AppendLine(String.Format("select gen_claveusuarios from gen_usuarios where gen_idusuarios = {0}", idUsuario))
        Dim d As DataTable = consulta_sql_datatable(sql.ToString())
        Dim p As String = d.Rows(0)("gen_claveusuarios").ToString()
        If p.ToLower() = sPass Then
            Return True
        End If
        Return False
    End Function

    <WebMethod>
    Public Shared Function cambioClave(idUsuario, sPass)
        Dim sql As New StringBuilder
        sql.AppendLine(String.Format("update GEN_Usuarios set GEN_claveUsuarios = '{0}' where GEN_idUsuarios = {1}", sPass, idUsuario))
        ejecuta_sql(sql.ToString())
        Return True
    End Function
End Class