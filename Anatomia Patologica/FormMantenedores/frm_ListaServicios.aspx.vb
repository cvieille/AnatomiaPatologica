﻿Public Partial Class WebForm14
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
        If IsPostBack = False Then
            Dim cgm As New ConsultaGrillaMantenedor
            dts_servicios.SelectCommand = cgm.Ds_ListaServicios()
            dts_servicios.DataBind()
        End If
    End Sub
End Class