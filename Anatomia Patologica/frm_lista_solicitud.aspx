﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_lista_solicitud.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_solicitud" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
        <h2>Listado de Solicitudes Histopatológicas y Citológicas</h2>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True">
</asp:ToolkitScriptManager>
    <center>
    <table border="0">
    <tr>
        <td>Estado de la Solicitud:</td>
        <td> 
            <asp:DropDownList ID="list_estado" runat="server" AutoPostBack="True">
                    <asp:ListItem>Generada</asp:ListItem>
                    <asp:ListItem>Recepcionada</asp:ListItem>
                    <asp:ListItem>Ingresada</asp:ListItem>                    
            </asp:DropDownList>
        </td>
        <td>Desde:</td>
        <td style="width: 143px">
            <asp:TextBox ID="txt_fecha_desde" runat="server" MaxLength="10" CssClass="CuadroFecha"></asp:TextBox>
            <asp:CalendarExtender ID="txt_fecha_desde_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_fecha_desde">
            </asp:CalendarExtender>
        </td>
        <td>Hasta:</td>
        <td>
            <asp:TextBox ID="txt_fecha_hasta" runat="server" MaxLength="10" CssClass="CuadroFecha"></asp:TextBox>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txt_fecha_hasta">
            </asp:CalendarExtender>
        </td>
        <td>
            <asp:Button ID="cmd_buscar" runat="server" Text="Buscar" CssClass="BotonNegro" />
        </td>
    </tr>
    </table>
    </center>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="Sqlds_lista_solicitud" 
        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" Width="916px">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="ID" DataTextField="OT" Text="OT" 
                DataNavigateUrlFormatString="frm_pdf_solicitud.aspx?id={0}" HeaderText="OT" />
            <asp:BoundField DataField="AÑO" HeaderText="AÑO" SortExpression="AÑO" />
            <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" DataFormatString="{0:dd-MM-yyyy}" />
            <asp:BoundField DataField="PACIENTE" HeaderText="PACIENTE" SortExpression="PACIENTE" ReadOnly="True" />
            <asp:BoundField DataField="ANA_GesRegistro_Biopsias" HeaderText="GES" SortExpression="ANA_GesRegistro_Biopsias" />
            <asp:BoundField DataField="MEDICO" HeaderText="MEDICO" SortExpression="MEDICO" ReadOnly="True" />
            <asp:BoundField DataField="MUESTRA" HeaderText="MUESTRA" SortExpression="MUESTRA" />
            <asp:BoundField DataField="ORIGEN" HeaderText="ORIGEN" SortExpression="ORIGEN" />
            <asp:BoundField DataField="SERVICIO" HeaderText="SERVICIO" SortExpression="SERVICIO" />
            <asp:HyperLinkField DataNavigateUrlFields="ID" Text="Ingresar Muestra" 
                DataNavigateUrlFormatString="~/FormSolicitudes/frm_recepcion.aspx?id=0&id_soli={0}" HeaderText="INGRESO" Visible="true"/>
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="Sqlds_lista_solicitud" runat="server" 
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
        SelectCommand="SELECT * FROM [vi_lista_solicitud_biopsia] WHERE ([ESTADO] = @ESTADO) ORDER BY [OT]">
        <SelectParameters>
            <asp:ControlParameter ControlID="list_estado" Name="ESTADO" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Content>
