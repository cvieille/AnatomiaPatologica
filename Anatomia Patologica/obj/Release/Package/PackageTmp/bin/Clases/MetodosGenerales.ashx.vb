﻿Imports System.Web
Imports System.Web.Services
Imports System.Collections.Generic
Imports Newtonsoft.Json
Imports System.IO

Public Class MetodosGenerales
    Implements System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Select Case context.Request("method")
            Case "DeleteSession"
                DeleteSession(context.Request("key").ToString())
            Case "GetSession"
                GetSession()
            Case "SetSession"
                SetSession(context.Request("key").ToString(), context.Request("value").ToString())
            Case "GetMenu"
                GetMenu(context.Request("idPerfil").ToString())
            Case "TablaMovBiopsia"
                TablaMovBiopsia(context.Request("idBiopsia").ToString())
	    Case "TablaMuestrasporBiopsia"
                TablaMuestrasporBiopsia(context.Request("idBiopsia").ToString())
            Case "GetPatologosCombo"
                GetPatologosCombo()
            Case "GuardarArchivo"
                GuardarArchivo(context.Request("url"))
            Case "GetArchivos"
                GetArchivos(context.Request("perfilUsuario"), context.Request("idBiopsia"))
            Case "InsertArchivo"
                InsertArchivo(context.Request("fn").ToString(), context.Request("idb").ToString(), context.Request("idu").ToString(), context.Request("f").ToString())
            Case "DeleteArchivo"
                DeleteArchivo(context.Request("idArchivo").ToString(), context.Request("nombreArchivo").ToString())
            Case Else
                Console.WriteLine("")
        End Select


    End Sub

    Public Sub GuardarArchivo(ByVal url As String)
        Dim contexto As HttpContext = HttpContext.Current
        Dim ColeccionArchivos As HttpFileCollection = contexto.Request.Files
        Dim nombreArchivo As String = ""
        For i = 0 To ColeccionArchivos.Count - 1
            nombreArchivo = ColeccionArchivos(i).FileName

            Dim fi As New IO.FileInfo(ColeccionArchivos(i).FileName)
            Dim DatosArchivo As String = fi.Name
            DatosArchivo = DatosArchivo.Replace("+", " ")
            DatosArchivo = DatosArchivo.Replace("°", " ")
            DatosArchivo = DatosArchivo.Replace("  ", " ")
            DatosArchivo = DatosArchivo.Replace("   ", " ")

            If (DatosArchivo.Length > 100) Then
                DatosArchivo = DatosArchivo.Substring(0, 95)
                DatosArchivo &= ".pdf"
            End If

            Dim CarpetaParaGuardar As String = url
            Directory.CreateDirectory(CarpetaParaGuardar)
            ColeccionArchivos(i).SaveAs(CarpetaParaGuardar + DatosArchivo)
        Next
    End Sub

    Public Sub DeleteSession(ByVal key As String)
        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session.Remove(key)
        contexto.Response.End()
    End Sub

    Public Sub GetArchivos(ByVal perfilUsuario As String, ByVal idBiopsia As String)
        Dim contexto As HttpContext = HttpContext.Current

        Dim jsonList As New List(Of Dictionary(Of String, String))
        Dim cons As String = String.Format("select * from ANA_Archivos_Biopsias where ANA_IdBiopsia = {0}", idBiopsia)
        Dim d As DataTable = consulta_sql_datatable(cons)
        For i = 0 To d.Rows.Count - 1
            'DataRow dtr = dt.Rows[i];
            'Dictionary<string, string> json = new Dictionary<string, string>();
            Dim dtr As DataRow = d.Rows(i)
            Dim json As New Dictionary(Of String, String)
            json("ANA_IdArchivos_Biopsias") = dtr("ANA_IdArchivos_Biopsias").ToString
            json("ANA_NomArchivos_Biopsias") = dtr("ANA_NomArchivos_Biopsias").ToString
            json("ANA_IdBiopsia") = dtr("ANA_IdBiopsia").ToString
            json("ANA_FecArchivos_Biopsias") = dtr("ANA_FecArchivos_Biopsias").ToString
            json("GEN_IdUsuarios") = dtr("GEN_IdUsuarios").ToString
            jsonList.Add(json)
        Next

        contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
        contexto.Response.End()
    End Sub

    Public Sub TablaMovBiopsia(ByVal idBiopsia As String)
        Dim contexto As HttpContext = HttpContext.Current

        Dim sb As New StringBuilder
        sb.AppendLine("SELECT")
        sb.AppendLine("am.ANA_IdMovimientos,")
        sb.AppendLine("am.ANA_FechaMovimientos,")
        sb.AppendLine("am.ANA_DescMovimientos,")
        sb.AppendLine("gu.GEN_loginUsuarios,")
        sb.AppendLine("mo.GEN_descripcionTipo_Movimientos_Sistemas")
        sb.AppendLine("FROM")
        sb.AppendLine("ANA_Movimientos AS am INNER JOIN")
        sb.AppendLine("GEN_Usuarios AS gu ON")
        sb.AppendLine("am.GEN_IdUsuarios = gu.GEN_idUsuarios LEFT OUTER JOIN")
        sb.AppendLine("GEN_Tipo_Movimientos_Sistemas as mo ON")
        sb.AppendLine("am.GEN_idTipo_Movimientos_Sistemas = mo.GEN_idTipo_Movimientos_Sistemas")
        sb.AppendLine("WHERE")
        sb.AppendLine(String.Format("am.ANA_IdBiopsia = {0} ", idBiopsia))
        sb.AppendLine("order by ANA_FechaMovimientos desc")

        Dim jsonList As New List(Of Dictionary(Of String, String))
        Dim d As DataTable = consulta_sql_datatable(sb.ToString())

        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            json("ANA_IdMovimientos") = dtr("ANA_IdMovimientos").ToString
            json("ANA_FechaMovimientos") = dtr("ANA_FechaMovimientos").ToString
            json("ANA_DescMovimientos") = dtr("ANA_DescMovimientos").ToString
            json("GEN_loginUsuarios") = dtr("GEN_loginUsuarios").ToString
            json("GEN_descripcionTipo_Movimientos_Sistemas") = dtr("GEN_descripcionTipo_Movimientos_Sistemas").ToString
            jsonList.Add(json)
        Next

        contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
        contexto.Response.End()
    End Sub
    Public Sub TablaMuestrasporBiopsia(ByVal idBiopsia As String)
        Dim contexto As HttpContext = HttpContext.Current

        Dim sb As New StringBuilder
        sb.AppendLine("SELECT")
        sb.AppendLine("drm.ANA_FrascoBiopsia,")
        sb.AppendLine("drm.ANA_CantidadMuestras,")
        sb.AppendLine("dc.ANA_DetalleCatalogo_Muestras,")
        sb.AppendLine("drm.ANA_DescripcionBiopsia")
        sb.AppendLine("FROM")
        sb.AppendLine("ANA_Detalle_Registro_Muestra AS drm INNER JOIN")
        sb.AppendLine("ANA_Detalle_Catalogo_Muestras AS dc ON")
        sb.AppendLine("drm.ANA_IdDetalleCatalogo_Muestras = dc.ANA_IdDetalleCatalogo_Muestras")
        sb.AppendLine("WHERE")
        sb.AppendLine("(drm.ANA_IdBiopsia =" & idBiopsia & ")")
        sb.AppendLine("order by ANA_IdDetalleBiopsia")


        Dim jsonList As New List(Of Dictionary(Of String, String))
        Dim d As DataTable = consulta_sql_datatable(sb.ToString())

        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            json("ANA_FrascoBiopsia") = dtr("ANA_FrascoBiopsia").ToString
            json("ANA_CantidadMuestras") = dtr("ANA_CantidadMuestras").ToString
            json("ANA_DetalleCatalogo_Muestras") = dtr("ANA_DetalleCatalogo_Muestras").ToString
            json("ANA_DescripcionBiopsia") = dtr("ANA_DescripcionBiopsia").ToString
            jsonList.Add(json)
        Next

        contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
        contexto.Response.End()
    End Sub

    Public Sub GetPatologosCombo()
        Dim contexto As HttpContext = HttpContext.Current

        Dim sb As New StringBuilder
        sb.AppendLine("SELECT")
        sb.AppendLine("u.GEN_IdUsuarios,")
        sb.AppendLine("u.GEN_loginUsuarios")
        sb.AppendLine("FROM GEN_Usuarios AS u INNER JOIN")
        sb.AppendLine("GEN_Sist_Usuario AS su ON")
        sb.AppendLine("u.GEN_IdUsuarios = su.GEN_idUsuarios INNER JOIN")
        sb.AppendLine("GEN_Perfil AS gp ON")
        sb.AppendLine("su.GEN_idPerfil = gp.GEN_idPerfil")
        sb.AppendLine("WHERE (su.GEN_estadoSist_Usuario = 'Activo') AND")
        sb.AppendLine("(gp.GEN_codigoPerfil = 3) AND")
        sb.AppendLine("gp.GEN_IdSistemas=1")

        Dim jsonList As New List(Of Dictionary(Of String, String))
        Dim d As DataTable = consulta_sql_datatable(sb.ToString())

        For Each dtr As DataRow In d.Rows
            Dim json As New Dictionary(Of String, String)
            json("GEN_IdUsuarios") = dtr("GEN_IdUsuarios").ToString
            json("GEN_loginUsuarios") = dtr("GEN_loginUsuarios").ToString
            jsonList.Add(json)
        Next

        contexto.Response.Write(JsonConvert.SerializeObject(jsonList))
        contexto.Response.End()
    End Sub

    Public Sub GetMenu(ByVal perfilUsuario As String)
        Dim contexto As HttpContext = HttpContext.Current
        Dim json1 As New List(Of Object)

        'Dim json As Dictionary(Of String, String) = New Dictionary(Of String, String)
        'json.Add("1", "contsub")
        'json.Add("2", "contsub2")

        'list.Add(json)


        'If contexto.Session("MENU") = "" Then
        Dim acc As New Menu

        Dim dtMenu1 As DataTable = acc.Get_Menu(perfilUsuario)
        For Each filaMenuSuperior As DataRow In dtMenu1.Rows

            Dim sCadena1 As New Dictionary(Of Object, Object)
            sCadena1.Add("titulo", filaMenuSuperior.Item("GEN_tituloMenu").ToString)
            sCadena1.Add("contenido", filaMenuSuperior.Item("GEN_contenidoMenu").ToString)

            Dim dtMenu2 As New DataTable
            dtMenu2 = acc.Get_Menu_Nivel(filaMenuSuperior.Item("GEN_idMenu").ToString, perfilUsuario, 2)

            If dtMenu2.Rows.Count > 0 Then
                Dim json2 As New List(Of Object)
                For Each filaSegundoNivel As DataRow In dtMenu2.Rows

                    Dim sCadena2 As New Dictionary(Of Object, Object)
                    sCadena2.Add("titulo", filaSegundoNivel.Item("GEN_tituloMenu").ToString)
                    sCadena2.Add("contenido", filaSegundoNivel.Item("GEN_contenidoMenu").ToString)

                    Dim dtMenu3 As New DataTable
                    dtMenu3 = acc.Get_Menu_Nivel(filaSegundoNivel.Item("GEN_idMenu").ToString, perfilUsuario, 3)

                    If dtMenu3.Rows.Count > 0 Then
                        Dim json3 As New List(Of Object)
                        For Each filaTercerNivel As DataRow In dtMenu3.Rows
                            Dim sCadena3 As New Dictionary(Of Object, Object)
                            sCadena3.Add("titulo", filaTercerNivel.Item("GEN_tituloMenu").ToString)
                            sCadena3.Add("contenido", filaTercerNivel.Item("GEN_contenidoMenu").ToString)

                            json3.Add(sCadena3)
                        Next
                        sCadena2.Add("submenus", json3)
                    End If
                    json2.Add(sCadena2)
                Next
                sCadena1.Add("submenus", json2)
            End If
            json1.Add(sCadena1)
        Next
        'End If

        'json.Add("FECHA_ACTUAL", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        contexto.Response.Write(JsonConvert.SerializeObject(json1))
        contexto.Response.End()
    End Sub

    Public Sub GetSession()
        Dim contexto As HttpContext = HttpContext.Current
        Dim json As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

        For index = 0 To contexto.Session.Count - 1
            If contexto.Session(index) IsNot Nothing Then
                Dim key As String = contexto.Session.Keys(index).ToString()
                Dim value As String = contexto.Session(index).ToString()
                json.Add(key, value)
            End If
        Next

        json.Add("FECHA_ACTUAL", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"))
        contexto.Response.Write(JsonConvert.SerializeObject(json))
        contexto.Response.End()
    End Sub

    Public Sub DeleteArchivo(ByVal idArchivo As String, ByVal nombreArchivo As String)
        Dim contexto As HttpContext = HttpContext.Current

        Dim cons As String = String.Format("select * from ANA_Archivos_Biopsias where ANA_IdArchivos_Biopsias = {0}", idArchivo)
        Dim d As DataTable = consulta_sql_datatable(cons)
        If d.Rows.Count > 0 Then
            cons = String.Format("delete from ANA_Archivos_Biopsias where ANA_IdArchivos_Biopsias = {0}", idArchivo)
            ejecuta_sql(cons)
        End If

        Dim sPath As String = Path.Combine("C:/GIAP/GIAP/dcto/adj/", nombreArchivo)

        If File.Exists(sPath) Then
            File.Delete(sPath)
        End If

        contexto.Response.Write("abcdefasd")
        contexto.Response.End()
    End Sub

    Public Sub InsertArchivo(ByVal nArchivo As String, ByVal idBiopsia As String, ByVal idUsuario As String, ByVal fecha As String)
        Dim contexto As HttpContext = HttpContext.Current

        Dim cons As String = String.Format("select * from ANA_Archivos_Biopsias where ANA_NomArchivos_Biopsias = '{0}' and ANA_IdBiopsia = {1} and GEN_IdUsuarios = {2}", nArchivo, idBiopsia, idUsuario)
        Dim d As DataTable = consulta_sql_datatable(cons)
        If d.Rows.Count > 0 Then
            'ya existe nombre de archivo con el mismo nombre de usuario y biopsia
        Else
            cons = String.Format("insert into ANA_Archivos_Biopsias(ANA_NomArchivos_Biopsias, ANA_IdBiopsia, ANA_FecArchivos_Biopsias, GEN_IdUsuarios) values('{0}',{1},'{2}',{3})", nArchivo, idBiopsia, fecha, idUsuario)
            ejecuta_sql(cons)
        End If

        contexto.Response.Write("")
        contexto.Response.End()
    End Sub


    Public Sub SetSession(ByVal key As String, ByVal value As String)

        Dim contexto As HttpContext = HttpContext.Current
        contexto.Session(key) = value
        contexto.Response.Write("{'" + key + "':'" + value.ToString() + "'}")
        contexto.Response.End()
    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class