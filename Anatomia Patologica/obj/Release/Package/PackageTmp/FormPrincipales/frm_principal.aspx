<%@ Page Title="Pagina de Inicio" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_principal.aspx.vb" Inherits="Anatomia_Patologica.frm_principal" %>

<%@ Register Src="~/ControlesdeUsuario/Principal/TecnicasPendientes.ascx" TagPrefix="uc1" TagName="TecnicasPendientes" %>
<%@ Register Src="~/ControlesdeUsuario/Principal/InmunoHistoquimicas.ascx" TagPrefix="uc1" TagName="InmunoHistoquimicas" %>
<%@ Register Src="~/ControlesdeUsuario/Principal/Interconsultas.ascx" TagPrefix="uc1" TagName="Interconsultas" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_TecnicasPendientes_gdv_tecnicas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_TecnicasPendientes_gdv_tecnicas").find("tr:first"))).dataTable({
                "aaSorting": []
            });
            $("#ctl00_Cnt_Principal_InmunoHistoquimicas_gdv_inmunoHistoquimica").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_InmunoHistoquimicas_gdv_inmunoHistoquimica").find("tr:first"))).dataTable();
            $("#ctl00_Cnt_Principal_Interconsultas_gdv_BiopsiasInterconsulta").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_Interconsultas_gdv_BiopsiasInterconsulta").find("tr:first"))).dataTable();
            
            if ($("#ctl00_Cnt_Principal_gdv_principal").find('thead').length !== 0)
                $("#ctl00_Cnt_Principal_gdv_principal").dataTable({ "aaSorting": [] });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:Panel ID="panel_tecnicas" runat="server">
        <div style="margin: 20px;">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading clearfix">
                    <ul class="nav nav-tabs" runat="server" id="tabFrm">
                        <li id="li_tecnicas" runat="server" class="active">
                            <a href="#tabTecnicas" data-toggle="tab">
                                <label>T�cnicas Especiales Pendientes</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_totalTecnica" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                        <li id="li_inmuno" runat="server">
                            <a href="#tabInmuno" data-toggle="tab">
                                <label>Inmunohistoqu�micas Pendientes</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_totalInmuno" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                        <li id="li_interconsulta" runat="server">
                            <a href="#tabInterconsulta" data-toggle="tab">
                                <label>Interconsultas</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_totalInterconsulta" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tabTecnicas">
                            <!-- Grilla de Tecnicas Especiales-->
                            <uc1:TecnicasPendientes runat="server" ID="TecnicasPendientes" />
                        </div>
                        <div class="tab-pane fade" id="tabInmuno">
                            <!-- Grilla de Inmunos-->
                            <asp:UpdatePanel ID="upp_Inmuno" runat="server">
                                <ContentTemplate>
                                    <uc1:InmunoHistoquimicas runat="server" ID="InmunoHistoquimicas" />
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                        <div class="tab-pane fade" id="tabInterconsulta">
                            <!-- Grilla de Interconsultas-->
                            <uc1:Interconsultas runat="server" ID="Interconsultas" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">M�dulo de B�squeda</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Mostrar:</label>
                            <asp:DropDownList ID="cmb_tipo_busqueda" runat="server" AutoPostBack="True" CssClass="form-control" TabIndex="1">
                            </asp:DropDownList><br />
                        </div>
                        <div class="col-md-2">
                            <label>Patologo:</label>
                            <asp:DropDownList ID="cmb_patologo" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <label>Desde:</label>
                            <asp:TextBox ID="txt_fecha_desde" runat="server" CssClass="form-control" type="date" required="required">
                            </asp:TextBox>
                        </div>

                        <div class="col-md-2">
                            <label>Hasta:</label>
                            <asp:TextBox ID="txt_fecha_hasta" runat="server" CssClass="form-control" type="date" required="required">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>N�:</label>
                            <asp:TextBox ID="txt_numero" runat="server" CssClass="form-control" type="number">0</asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>A�o:</label>
                            <asp:TextBox ID="txt_numero0" runat="server" CssClass="form-control" type="number">0</asp:TextBox>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <asp:CheckBox ID="chk_interconsulta" runat="server" Text="C/Interconsulta" />
                            <asp:CheckBox ID="chk_SinCodificar" runat="server" Text="S/Codificar" />
                            <asp:CheckBox ID="chk_Criticos" runat="server" Text="Criticos" />
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-3">
                            <label>N� de Documento:</label>
                            <div class="form-inline">
                                <asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                <asp:TextBox ID="txt_digito" runat="server" ReadOnly="True" Width="40px" MaxLength="1" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Nombre:</label>
                            <asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>Ap. Paterno:</label>
                            <asp:TextBox ID="txt_ap_paterno" runat="server" MaxLength="100" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>Ap. Materno:</label>
                            <asp:TextBox ID="txt_ap_materno" runat="server" MaxLength="100" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <br />
                            <asp:Button ID="cmd_buscar" runat="server" Text="Buscar" CssClass="btn btn-success" Width="100px" />
                            <asp:Button ID="cmd_limpiar" runat="server" Text="Limpiar" CssClass="btn btn-warning" Width="100px" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="Upd_panel_grilla" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server">
                <ProgressTemplate>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:GridView ID="gdv_principal" runat="server"
                AutoGenerateColumns="False" DataKeyNames="ANA_IdBiopsia"
                DataSourceID="dts_Principal"
                CssClass="table table-bordered table-hover table-responsive">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormSolicitudes/frm_recepcion.aspx?id={0}&amp;id_soli=0"
                        DataTextField="n_biopsia" FooterText="N� Biopsia" HeaderText="N� Biopsia" SortExpression="n_biopsia" />
                    <asp:BoundField DataField="NombreOrgano" HeaderText="Organo" SortExpression="NombreOrgano" />
                    <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" DataFormatString="{0:dd-MM-yyyy}" HeaderText="F.Recepci�n" SortExpression="ANA_fec_RecepcionRegistro_Biopsias" />
                    <asp:BoundField DataField="ANA_GesRegistro_Biopsias" HeaderText="GES" SortExpression="ANA_GesRegistro_Biopsias" />

                    <asp:BoundField DataField="nombre1" HeaderText="Solic." SortExpression="nombre1" />
                    <asp:ImageField HeaderText="D�as">
                        <ControlStyle CssClass="badge" />
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:ImageField>

                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                        DataTextField="cortes" HeaderText="Cortes" Text="Cortes" SortExpression="cortes" />
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_NotasBiopsia.aspx?id={0}"
                        DataTextField="notas" HeaderText="Notas" Target="_blank" SortExpression="notas" />
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormImpresion/frm_MovimientosBiopsia.aspx?id={0}"
                        DataTextField="estado" HeaderText="Mov." Text="Mov." Target="_blank" SortExpression="estado" />
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_InformeBiopsia.aspx?id={0}"
                        DataTextField="reporte" HeaderText="Repo." />
                    <asp:BoundField DataField="ANA_EstadoRegistro_Biopsias" HeaderText="Estado" SortExpression="ANA_EstadoRegistro_Biopsias" />
                    <asp:BoundField DataField="ANA_fecValidaBiopsia" HeaderText="Fec. Valida" SortExpression="ANA_fecValidaBiopsia">
                        <ControlStyle Width="0px" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <!----- PANEL POPUP COMO MENSAJE DE ALERTA ----->
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />
    <asp:SqlDataSource ID="dts_InmunoHistoquimica" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dts_TecBiopsia" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dts_Principal" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dts_Interconsultas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
    
   </asp:Content>