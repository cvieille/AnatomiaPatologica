<%@ Page Title="Consulta Clinicos" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_PrincipalClinico.aspx.vb" Inherits="Anatomia_Patologica.frm_PrincipalClinico" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            if ($("#ctl00_Cnt_Principal_gdv_Notificaciones").find('thead').length !== 0)
                $("#ctl00_Cnt_Principal_gdv_Notificaciones").dataTable({ "aaSorting": [] });

            $("#ctl00_Cnt_Principal_gdv_biopsias").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_biopsias").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="upd_todo" runat="server">
        <ContentTemplate>

            <asp:Panel ID="pnl_bandeja" runat="server" Visible="False">
                <!-----------------------------------------------------------------------------------------
            Biopsias Solicitadas por usuario clinico
            ------------------------------------------------------------------------------------------>

                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <h2 class="text-center">Biopsias Solicitadas</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-md-2">
                                <label>Desde:</label>
                                <asp:TextBox ID="txt_fecha_desde" runat="server" type="date" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <label>Hasta:</label>
                                <asp:TextBox ID="txt_fecha_hasta" runat="server" type="date" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <br />
                                <asp:Button ID="cmd_buscar" runat="server" CssClass="btn btn-success" Text="Buscar" TabIndex="2" />
                            </div>
                        </div>
                        <br />
                        <div>
                            <asp:GridView ID="gdv_biopsias" runat="server" AutoGenerateColumns="False"
                                DataSourceID="dts_Busqueda" CssClass="table table-bordered table-hover table-responsive">
                                <Columns>
                                    <asp:BoundField DataField="n_biopsia" HeaderText="N� de Biopsia" SortExpression="n_biopsia" />
                                    <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" HeaderText="Recep. Anatomia"
                                        SortExpression="ANA_fec_RecepcionRegistro_Biopsias" DataFormatString="{0:dd-MM-yyyy}" />
                                    <asp:BoundField DataField="GEN_nombrePaciente" HeaderText="Paciente" SortExpression="GEN_nombrePaciente" />
                                    <asp:BoundField DataField="Gen_nombrePatologo" HeaderText="Patologo" SortExpression="Gen_nombrePatologo" />
                                    <asp:BoundField DataField="GEN_nombreServicio" HeaderText="Destino" SortExpression="GEN_nombreServicio" Visible="false" />
                                    <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                                    <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="GEN_idTipo_Estados_Sistemas" SortExpression="GEN_idTipo_Estados_Sistemas" />
                                    <asp:BoundField DataField="ANA_fecValidaBiopsia" HeaderText="ANA_fecValidaBiopsia" SortExpression="ANA_fecValidaBiopsia" />
                                    <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            Informaci�n
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkb_VerDetalle" runat="server" Text="ver" OnClick="lkb_VerDetalle_Click"
                                                CommandArgument='<%# Eval("ANA_IdBiopsia") %>'
                                                CssClass="btn btn-info">
                                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Imprimir
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkb_Imprimir" runat="server" Text="ver" Style="color: white;" OnClick="lkb_Imprimir_Click"
                                                CommandArgument='<%# Eval("ANA_IdBiopsia") %>'
                                                CssClass="btn btn-info" Enabled="false">
                                                <span class="glyphicon glyphicon-print" aria-hidden="true"></span></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="frm_ver_biopsia.aspx?id={0}"
                                        DataTextField="Reporte" HeaderText="Reporte" Text="Reporte" Target="_blank" />--%>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="dts_Busqueda" runat="server"
                                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                            <asp:Label ID="lbl_error" runat="server" class="alert alert-warning"
                                role="alert" Text="Label" Visible="False" Width="80%"></asp:Label>
                        </div>
                    </div>
                </div>
                <!-----------------------------------------------------------------------------------------            
            ------------------------------------------------------------------------------------------>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnl_notificacion" runat="server">
                <!-----------------------------------------------------------------------------------------  
            Bandeja de Notificaciones
            ------------------------------------------------------------------------------------------>
                <!-- Panel Principal -->
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <h2 class="text-center">Bandeja de Notificaciones</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>N� de Documento:</label>
                                <div class="form-inline">
                                    <asp:TextBox ID="txt_rut0" runat="server" MaxLength="10" TabIndex="4"
                                        CssClass="form-control" AutoPostBack="True"></asp:TextBox>
                                    <asp:TextBox ID="txt_digito" runat="server" Enabled="False" ReadOnly="True" CssClass="form-control"
                                        Width="40px" TabIndex="5"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Ficha:</label>
                                <asp:TextBox ID="txt_ficha0" runat="server" MaxLength="6" type="number" CssClass="form-control" TabIndex="6"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <label>Nombre:</label>
                                <asp:TextBox ID="txt_nombre0" runat="server" MaxLength="100" TabIndex="7" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-5">
                                <label>Apellidos:</label>
                                <div class="form-inline">
                                    <asp:TextBox ID="txt_ap1" runat="server" MaxLength="100" TabIndex="8" CssClass="form-control"></asp:TextBox>
                                    <asp:TextBox ID="txt_ap2" runat="server" MaxLength="100" CssClass="form-control" TabIndex="9"></asp:TextBox>
                                </div>

                            </div>

                        </div>
                        <br />
                        <div>
                            <asp:Button ID="cmd_buscar0" runat="server" CssClass="btn btn-success"
                                Text="Buscar" TabIndex="10" />
                        </div>

                        <asp:GridView ID="gdv_Notificaciones" runat="server" AutoGenerateColumns="False"
                            DataSourceID="dts_ListaNotificacion" CssClass="table table-bordered table-hover table-responsive">
                            <Columns>
                                <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="Id.Biopsia" ReadOnly="True" SortExpression="ANA_IdBiopsia" Visible="False" />
                                <asp:BoundField DataField="n_biopsia" HeaderText="N� de Biopsia" ReadOnly="True" SortExpression="n_biopsia">
                                    <ControlStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" DataFormatString="{0:dd-MM-yyyy}"
                                    HeaderText="Recep. Anatomia" SortExpression="ANA_fec_RecepcionRegistro_Biopsias" />
                                <asp:BoundField DataField="Paciente" HeaderText="Paciente" ReadOnly="True" SortExpression="Paciente" />
                                <asp:BoundField DataField="nombre_patologo" HeaderText="Patologo" SortExpression="nombre_patologo" />
                                <asp:BoundField DataField="estado_biopsia" HeaderText="Estado" SortExpression="estado_biopsia" />
                                <asp:TemplateField FooterStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        Informaci�n
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkb_VerDetalle" runat="server" Text="ver" OnClick="lkb_VerDetalle_Click"
                                            CommandArgument='<%# Eval("ANA_IdBiopsia") %>'
                                            CssClass="btn btn-info">
                                                <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Imprimir
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkb_Imprimir" runat="server" Text="ver" Style="color: white;" OnClick="lkb_Imprimir_Click"
                                            CommandArgument='<%# Eval("ANA_IdBiopsia") %>'
                                            CssClass="btn btn-info">
                                                <span class="glyphicon glyphicon-print" aria-hidden="true"></span></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Revisar">
                                    <ItemTemplate>
                                        <asp:Button ID="cmd_ver_notificacion" runat="server" OnClick="cmd_ver_notificacion_Click" Text="Ver Notificaci�n"
                                            CommandArgument='<%# Eval("ANA_IdBiopsias_Critico") %>' CssClass="btn btn-success" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ANA_IdBiopsias_Critico" HeaderText="Id. Notif." ReadOnly="True" Visible="false" />
                            </Columns>
                        </asp:GridView>

                        <asp:SqlDataSource ID="dts_ListaNotificacion" runat="server"
                            ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                        <br />
                        <asp:Button ID="btnShow" runat="server" BackColor="White" BorderColor="White" BorderStyle="None"
                            Height="0px" Text="Show Modal Popup" Visible="true" Width="100px" />
                        <asp:ModalPopupExtender ID="mp1" runat="server" BackgroundCssClass="modalBackground"
                            DropShadow="true" PopupControlID="Panel1" TargetControlID="btnShow">
                        </asp:ModalPopupExtender>

                    </div>
                </div>
                <!-----------------------------------------------------------------------------------------            
            ------------------------------------------------------------------------------------------>
            </asp:Panel>

            <!-- ModalPopupExtender -->
            <asp:Panel ID="Panel1" runat="server">
                <!-----------------------------------------------------------------------------------------    
    	    Notificaci�n paciente critico        
            ------------------------------------------------------------------------------------------>
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Notificaci�n paciente critico</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label>N�</label>
                                <asp:TextBox ID="txt_numero" runat="server" Enabled="False" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                            <div class="col-md-2">
                               <br />
                                <asp:CheckBox ID="chk_leido" runat="server" Text="Leido" />
                            </div>
                            <div class="col-md-2">
                                <label>Ubic.Interna:</label>
                                <asp:TextBox ID="txt_ficha" runat="server" Enabled="False" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-4">
                                <label>Documento</label>
                                <asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" Enabled="False" CssClass="form-control"> 
                                </asp:TextBox>
                            </div>
                            <div class="col-md-8">
                                <label>Nombre:</label>
                                <asp:TextBox ID="txt_nombre" runat="server" Enabled="False" CssClass="form-control">
                                </asp:TextBox>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Servicio Origen:</label>
                                <asp:TextBox ID="txt_servicio" runat="server" Enabled="False" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label>Solicitado Por:</label>
                                <asp:TextBox ID="txt_solicitado_por" runat="server" Enabled="false" CssClass="form-control"> 
                                </asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label>Validado Por:</label>
                                <asp:TextBox ID="txt_quien_valida" runat="server" Enabled="false" CssClass="form-control"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Estado Informe:</label>
                                <asp:TextBox ID="txt_estado" runat="server" Enabled="false" CssClass="form-control"> </asp:TextBox>
                            </div>
                            <div class="col-md-8">
                                <label>Organo:</label>
                                <asp:TextBox ID="txt_organo" runat="server" Enabled="false" CssClass="form-control"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Nota:</label>
                                <asp:TextBox ID="txt_notificacion" runat="server" Enabled="false" TextMode="MultiLine" CssClass="form-control"> </asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Respuesta Notificaci�n: </label>
                                <asp:TextBox ID="txt_respuesta_notificacion" runat="server" TextMode="MultiLine" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>
                        <table>
                            <tr>

                                <td style="margin-left: 40px">
                                    <asp:TextBox ID="txt_recepcion" runat="server" Height="24px" Visible="False" Width="20px">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_edad" runat="server" Enabled="false" Visible="False" Width="20px">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_fnac" runat="server" Enabled="false" Visible="False" Width="20px">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_id_biopsia" runat="server" Enabled="false" Width="20px" Visible="False">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_id_notificacion" runat="server" Width="20px"
                                        Visible="False"></asp:TextBox>

                                </td>
                            </tr>
                            
                        </table>
                        <p style="text-align: center">
                            <br />
                            <asp:Label ID="lbl_mensaje" runat="server" Visible="False" class="alert alert-warning" role="alert">
                            </asp:Label>
                            <br />
                        </p>
                        <p style="text-align: center">
                            <asp:Button ID="cmd_guardar" runat="server" CssClass="btn btn-primary" Text="Guardar" />
                            <asp:Button ID="cmd_cerrar_notificacion0" runat="server" CssClass="btn btn-warning" Text="Cerrar Notificaci�n" />
                            <asp:Button ID="btnClose" runat="server" Text="Salir" CssClass="btn btn-success" />
                        </p>
                    </div>
                </div>
                <!-----------------------------------------------------------------------------------------            
                ------------------------------------------------------------------------------------------>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="modalInformacion" role="dialog" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upp_informacion" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Panel Principal Informaci�n-->
                    <div class="panel panel-info">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Informaci�n de la Biopsia</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Fecha de Recepci�n:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_fechaRecepcion" runat="server" Text="Label"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Rut Paciente:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_rutpaciente" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Nombre Paciente:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_nombrePaciente" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Organo:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_organo" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Patologo:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_patologo" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Medico Solicitante:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_medicosolicitante" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Servicio Origen:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_servicioOrigen" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Servicio Destino:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_servicioDestino" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Estado:</label>
                                </div>
                                <div class="col-md-8">
                                    <asp:Label ID="lbl_estado" runat="server"></asp:Label>
                                </div>
                            </div>


                        </div>
                        <div class="panel-footer clearfix">
                            <div class="pull-right">
                                <asp:Button ID="cmd_salirInformacion" runat="server" CssClass="btn btn-warning" Text="Salir" data-dismiss="modal" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
