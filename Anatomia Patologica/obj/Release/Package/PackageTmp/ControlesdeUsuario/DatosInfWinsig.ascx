﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatosInfWinsig.ascx.vb" Inherits="Anatomia_Patologica.DatosInfWinsig" %>
<link rel="stylesheet" type="text/css" href="seriousface/style.css" />


<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <h2 class="text-center">Informe Winsig - Por Servicio</h2>
    </div>
    <div class="panel-body">
        <div class="col-md-2">
            <label>Desde:</label>
            <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required">
            </asp:TextBox>
        </div>
        <div class="col-md-2">
            <label>Hasta:</label>
            <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required">
            </asp:TextBox>
        </div>
        <div class="col-md-3">
            <br />
            <div class="form-inline">
                <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
            </div>
        </div>
    </div>


    <br />

    <p>
        <asp:Label ID="Lbl_error" runat="server" Text="Revise el rango de fechas a consultar ....."
            CssClass="alert alert-danger" Visible="False"></asp:Label>
    </p>
</div>
<br />
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="DivFullCentrado">
            <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false"></asp:Label>
            <asp:GridView ID="gdv_datos" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                CellPadding="4" DataSourceID="sqldata_cuenta_biopsias" ForeColor="#333333"
                PageSize="20" Width="847px">
                <RowStyle BackColor="#EFF3FB" BorderStyle="None" />
                <Columns>
                    <asp:BoundField DataField="GEN_codwinsigServicio" HeaderText="Cód. WinSig" SortExpression="GEN_codwinsigServicio" />
                    <asp:BoundField DataField="origen" HeaderText="Origen" SortExpression="origen" ReadOnly="True" />
                    <asp:HyperLinkField DataNavigateUrlFields="cod_origen" DataNavigateUrlFormatString="/FormInformes/frm_DetalleWinsigporServicio.aspx?id={0}"
                        DataTextField="serv_origen" HeaderText="Servicio" Text="Servicio" Target="_blank" />
                    <asp:BoundField DataField="serv_origen" HeaderText="Servicio" SortExpression="serv_origen" ReadOnly="True" />
                    <asp:BoundField DataField="tot_biopsia" HeaderText="Total" SortExpression="tot_biopsia" ReadOnly="True" />
                </Columns>
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <asp:SqlDataSource ID="sqldata_cuenta_biopsias" runat="server"
                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
    </Triggers>
</asp:UpdatePanel>
