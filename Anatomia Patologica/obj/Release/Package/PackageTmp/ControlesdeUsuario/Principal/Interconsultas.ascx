﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Interconsultas.ascx.vb" Inherits="Anatomia_Patologica.Interconsultas" %>
<!-------------------- Panel Principal Interconsultas-->
<%--<div class="panel panel-primary">
    <!-------------------- Default panel contents -------------------->
    <div class="panel-heading">Interconsultas</div>
    <div class="panel-body">--%>
<div class="text-right" style="margin-bottom: 10px;">
    <asp:Button ID="cmd_ExcelInterconsulta" runat="server" Text="Exportar" CssClass="btn btn-success" />
</div>
<asp:GridView ID="gdv_BiopsiasInterconsulta" runat="server" AutoGenerateColumns="False"
    DataSourceID="dts_Interconsultas"
    CssClass="table table-bordered table-hover table-responsive"
    OnDataBound="gdv_BiopsiasInterconsulta_DataBound"
    OnPreRender="gdv_BiopsiasInterconsulta_PreRender">
    <Columns>        
        <asp:BoundField DataField="ANA_NumBiopsia" HeaderText="N° de Biopsia" SortExpression="ANA_NumBiopsia" />
        <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" SortExpression="ANA_OrganoBiopsia" />
        <asp:BoundField DataField="nombre_tecnologo" HeaderText="Tecnólogo" SortExpression="nombre_tecnologo" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="cmd_VerInterconsulta" runat="server" Text="Ver" CssClass="btn btn-info"
                 CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_VerCaso_Click" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<%--</div>
</div>--%>