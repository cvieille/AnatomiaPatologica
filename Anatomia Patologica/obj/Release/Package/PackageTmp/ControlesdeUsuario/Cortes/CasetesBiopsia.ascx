﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CasetesBiopsia.ascx.vb" Inherits="Anatomia_Patologica.casetesbiopsia" %>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <h2 class="text-center">Listado de Casete</h2>
    </div>
    <div class="panel-body">
        <asp:GridView ID="gdv_cortes" runat="server" AutoGenerateColumns="False"
            DataKeyNames="GEN_loginUsuarios" DataSourceID="dts_cortes" EnableTheming="True"
            HorizontalAlign="Center" RowHeaderColumn="id"
            CssClass="table table-bordered table-hover table-responsive">
            <EmptyDataTemplate>
                <div class="alert alert-warning" role="alert">
                    ¡No existen casetes para esta muestra!
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Sel.">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSeleccion" runat="server" CssClass="controlSeleccion" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Corte" ReadOnly="True" SortExpression="ANA_NomCortes_Muestras" />
                <asp:BoundField DataField="ANA_FecCortes_Muestras" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha"
                    ReadOnly="True" SortExpression="ANA_FecCortes_Muestras" />
                <asp:BoundField DataField="nombre_patologo" HeaderText="Patólogo" ReadOnly="True" SortExpression="nombre_patologo" />
                <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Creado por" ReadOnly="True"
                    SortExpression="GEN_loginUsuarios" />
                <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" ReadOnly="True" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="Id. Casete" ReadOnly="True" SortExpression="ANA_IdCortes_Muestras" />
            </Columns>
        </asp:GridView>
        <div style="text-align: center">
            <asp:Button ID="cmd_select_all" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
            <asp:Button ID="cmd_select_all0" runat="server" CssClass="btn btn-defaultbtn btn-default" Text="Quitar Selección" />
        </div>
        <div class="alert alert-info text-center" style="padding: 5px">
            <table style="margin: 0 auto;">
                <tr>
                    <td>
                        <asp:Button ID="cmd_enviar_procesador" runat="server" CssClass="btn btn-success"
                            Enabled="False" Text="Listo para Procesador" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_EliminarCortes" runat="server" CssClass="btn btn-danger"
                            OnClientClick="return confirm('¿Esta seguro que desea eliminar este(os) casete (s)?');"
                            Enabled="False" Text="Eliminar Selección" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_especiales" runat="server" CssClass="btn btn-success"
                            Enabled="False" Text="Solicitar Especial" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_inmuno" runat="server" CssClass="btn btn-success"
                            Enabled="False" Text="Solicitar Inmuno" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_descal" runat="server" CssClass="btn btn-warning"
                            Enabled="False" Text="Enviar a Descalcificar" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="cmd_volver_inicio" runat="server" CssClass="btn btn-warning" Enabled="False"
                            Text="Volver a Est. Encasetada" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_imprimir" runat="server" CssClass="btn btn-info" Text="Imprimir Selección" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_solicitar_macro" runat="server" CssClass="btn btn-success" Enabled="False"
                            Text="Solicitar Macrosc." />
                    </td>
                    <td>
                        <asp:Button ID="cmd_solicitar_casete" runat="server" CssClass="btn btn-success" Enabled="False" OnClientClick="return confirm('¿Esta seguro que desea solicitar este (os) Casete (s)?');"
                            Text="Solicitar Casete" />
                    </td>
                    <td>
                        <asp:Button ID="cmd_para_interconsulta" runat="server" CssClass="btn btn-warning" Enabled="False" OnClientClick="return confirm('¿Esta seguro que desea enviar este (os) casete (s) a Interconsulta?');"
                            Text="Para Interconsulta" />
                    </td>
                </tr>
            </table>
        </div>
        <!-- BLOQUE PARA SOLICITAR TECNICAS -->
        <asp:Panel ID="pnl_tecnica" runat="server" CssClass="alert alert-info  text-center"
            Visible="false">
            <asp:DropDownList ID="cmb_tecnicas_especiales" runat="server" Width="380px">
            </asp:DropDownList>
            <asp:Button ID="cmd_agregar_especiales" runat="server" CssClass="btn btn-success"
                Text="Agregar Técnica Especial" />
        </asp:Panel>

        <!-- BLOQUE PARA SOLICITAR INMUNO HISTOQUIMICA -->
        <asp:Panel ID="pnl_inmuno" runat="server" CssClass="alert alert-info" HorizontalAlign="Center" Visible="false">
            <asp:DropDownList ID="cmb_inmuno_histoquimica" runat="server" Width="380px">
            </asp:DropDownList>
            <asp:Button ID="cmd_agregar_inmuno" runat="server" CssClass="btn btn-success"
                Text="Agregar Inmunohistoquímica" />
        </asp:Panel>
        <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" CssClass="alert alert-danger" role="alert">
        </asp:Label>
    </div>
</div>
