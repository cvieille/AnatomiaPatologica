﻿var postBack;
$(document).ready(function () {
    postBack = false;
});

function incorrecto(pnl) {
    pnl.addClass("go");
}


/***************************************************************************************************************************
 OBTIENE EL HOST ABSOLUTO DE LA PAGINA
****************************************************************************************************************************/
function getHost() {
    //SE LLAMA A LA FUNCTION AJAX DE JQUERY, Y SE LE PASA POR PARAMETROS UN OBJETO CON NOTACION JSON
    var host = "";
    if (window.location.origin.indexOf("localhost") > -1)
        return window.location.origin;
    return window.location.origin + "/Anatomia";
}

/***************************************************************************************************************************
DAR LISTA A TXT
****************************************************************************************************************************/
var esta_validado = false;
function darDataListTxt(txt, cantCaracteres, nombreLista) {

    if (txt.val().length >= cantCaracteres && !esta_validado) {
        txt.attr("list", nombreLista);
        esta_validado = true;
    } else if (txt.val().length < 2 && esta_validado) {
        txt.removeAttr("list");
        esta_validado = false;
    }

}

/****************************************************************************************************************************
    SELECCIONAR TODO EL TEXTO
****************************************************************************************************************************/
function seleccionatTodoTexto(txt){
    txt.select();
}

/****************************************************************************************************************************
    AL PRESIONAR ENTER
*****************************************************************************************************************************/
function pressEnter(e, btn){
    // 13 = tecla enter
    if(e.which == 13)
        btn.click();
}

/***************************************************************************************************************************
    DAR ESTILOS A TABLA CON PAGINACIÓN
***************************************************************************************************************************/
function darEstiloDeDataTable(grilla) {
    
    if (grilla.find('thead').length !== 0)
        grilla.dataTable();
    
}

/**********************************************************************************************************************************
    CUENTA EL TOTAL DE FILAS DE LA GRILLA
**********************************************************************************************************************************/

function totalFilasGrilla(idGrilla) {
    var arrayInfo = $('#MainContent_' + idGrilla + "_info").text().split(" ");
    return arrayInfo[7];
}

/**********************************************************************************************************************************
    PINTA SPAN SEGUN SEA LA CANTIDAD DE REGISTROS QUE SE QUIERAN PINTAR
**********************************************************************************************************************************/
function pintarContadorSpan(spn, total) {
    spn.text(total);
    if (parseInt(total) > 0) {
        spn.removeClass("badge-warning");
        spn.addClass("badge-success");
    } else {
        spn.removeClass("badge-success");
        spn.addClass("badge-warning");
        spn.text(0);
    }
} 

/**
 * EVITA EL DOBLE CLICK A TRAVES DE UN CONTROL DE BOOTSTRAP
 */
function evita_doble_click(nombre_boton) {
    $(this).button('loading');
}

/**
 * DA ESTILO PARA DESPLEGAR TOOLTIP DE BOOTSTRAP CUANDO SE PASA POR EL COMPONENTE
 */
function tooltipHover() {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
    });
}

/**
* SE OBTIENEN TODOS LOS NRS DE FILAS Y SE GUARDAN EN UN HIDDENFIELD 
*/
function cargarNumerosFilas(tabla, hidden) {

    if (tabla.find("thead").length !== 0) {

        hidden.val("");

        tabla.find("tbody tr").each(function () {

            if ($(this).find("td").attr("class") === "dataTables_empty") {
                return false;
            }

            var id = $(this).find("td a").attr("id");
            var numFilaServidor = id.substring(id.lastIndexOf("_") + 1 , id.length);
            hidden.val(hidden.val() + numFilaServidor + ",");
            
        });

        hidden.val(hidden.val().substring(0, hidden.val().length - 1));

    }

}

/**
* SE ESTABLECE UN FILTRO MANUAL
*/
var tablaOficial, tablaOficialEnvio;
function filtrar(grilla) {

    if (table !== null) {

        if (grilla.attr('id') === "MainContent_gdv_tablaOficial")
            tablaOficial = table;
        else
            tablaOficialEnvio = table;

        var idcmbFiltro = "cmb_busPab_" + grilla.attr('id');
        var html = "<div class='centrado-vertical' style='display: inline-block;'>" +
                        "Pabellón:" +
                        "<select id='" + idcmbFiltro + "' class='form-control' style='width:150px;'>" +
                        "</select>" +
                    "</div>"

        $('#' + grilla.attr('id') + '_filter label').before(html);
        $('#' + idcmbFiltro).html($('#MainContent_cmb_pabellones').html());
        $('#' + idcmbFiltro + ' option').first().text("");

        if (sessionStorage.getItem("opcion_seleccionada_" + grilla.attr('id')) !== null) {
            $("#" + idcmbFiltro).val(sessionStorage.getItem("opcion_seleccionada_" + grilla.attr('id')));
        } else {
            table.columns().search('').draw();
        }

        $('#' + idcmbFiltro).on('change', function () {

            var tablaTemp;

            if (grilla.attr('id') === "MainContent_gdv_tablaOficial")
                tablaTemp = tablaOficial;
            else
                tablaTemp = tablaOficialEnvio;

            sessionStorage.setItem("opcion_seleccionada_" + grilla.attr('id'), $('#' + idcmbFiltro + ' option:selected').val());
            
            var val = $.fn.dataTable.util.escapeRegex(
                $('#' + idcmbFiltro + ' option:selected').text()
            );
            
            tablaTemp.column(0)
                .search( val ? '^'+val+'$' : '', true, false )
                .draw();

        });
    }
}

function bajarScrollLow() {
    $('body').animate({
        scrollTop: document.body.scrollHeight
    }, 1000);
}

function bajarScroll(componente){
    componente.scrollIntoView();
}

function irHastaElFinalPagina() {
    window.scrollTo(0, document.body.scrollHeight);
}

function showNotificacion(texto, tipo, posicion, direccion, espacio){
    setTimeout(function () {
        $.bootstrapGrowl(texto, {
            ele: 'body',
            type: tipo,
            align: posicion,
            width: 290,
            delay: 3000, // TIEMPO QUE QUEDA EL MENSAJE ABIERTO
            offset: { from: direccion, amount: espacio },
            allow_dismiss: true, // SI ES VERDADERO MUESTRA LA CRUZ PARA CERRAR LA NOTIFICACION.
            stackup_spacing: 5 // ESPACIO ENTRE NOTIFICACIONES
        });
    }, 500);
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}


/**********************************************************************************************************************************
    DA FUNCIONES A RADIOS PERSONALIZADOS // LIBRERIA bootstrap-switch
**********************************************************************************************************************************/
function darFuncionRadios(idPadre) {

    $("input[type='radio']").bootstrapSwitch();

    $(idPadre + " input[type='radio']").each(function () {
        $(this).on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                var radioChecked = $(this);
                $(this).attr('checked', 'checked');

                $("input[name='" + radioChecked.attr("name") + "']").each(function () {
                    if (radioChecked.attr("id") !== $(this).attr("id")) {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                $(this).removeAttr("checked");
            }
        });
    });
}

/**********************************************************************************************************************************
    DA FUNCIONES A CHECKBOX PERSONALIZADOS // LIBRERIA bootstrap-switch
**********************************************************************************************************************************/
function darFuncionCheckBox(idPadre) {

    $(idPadre + " input[type='checkbox']").bootstrapSwitch();
    $(idPadre + " input[type='checkbox']").each(function () {
        $(this).on('switchChange.bootstrapSwitch', function (event, state) {
            if (state)
                $(this).attr('checked', 'checked');
            else
                $(this).removeAttr("checked");
        });
    });
}

/**********************************************************************************************************************************
    VALIDA DIGITO VERIFICADOR 
**********************************************************************************************************************************/
function esValidoDigitoVerificador(rut, digito) {
    
    if (rut.length >= 6 && tieneSoloNumeros(rut)) {
        var suma = 0;
        for (var i = rut.length - 1, j = 2; i >= 0; i--) {
            suma += (parseInt(rut.charAt(i)) * j);
            j = (j === 7) ? 2 : ++j;
        }

        return (digito.toLowerCase() === getDigitoOficial(11 - (suma % 11)));
    }

    return false;
}

/**********************************************************************************************************************************
    SE OBTIENE EL DIGITO OFICIAL 
**********************************************************************************************************************************/
function getDigitoOficial(digitoOficial) {
    if (digitoOficial === 10)
        return 'k';
    else if (digitoOficial === 11)
        return '0';
    else
        return digitoOficial.toString();
}

/**********************************************************************************************************************************
    VALIDA SI LA CADENA SOLO TIENE NUMEROS 
**********************************************************************************************************************************/
function tieneSoloNumeros(str) {
    return /^[0-9]*$/.test(str);
}

