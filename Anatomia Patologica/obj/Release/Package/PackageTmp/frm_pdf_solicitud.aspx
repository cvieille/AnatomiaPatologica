﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_pdf_solicitud.aspx.vb" Inherits="Anatomia_Patologica.frm_pdf_solicitud" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <form>
    <asp:TextBox ID="txt_id" runat="server" AutoPostBack="True" Visible="false"></asp:TextBox>
    
    <h3><center>Solicitud de Estudio Histopatológico y Citológico</center></h3> 
    <br />
    <table border="0" align="center" width="80%">
    <tr>
        <td>N° OT:</td>
        <td><asp:TextBox ID="txt_ot" runat="server" Enabled="false"></asp:TextBox></td>
        <td>GES:</td>
        <td><asp:TextBox ID="txt_ges" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Nombre del Paciente:</td>
        <td><asp:TextBox ID="txt_nombre" runat="server" Enabled="false"></asp:TextBox></td>
        <td>Apellidos:</td>
        <td><asp:TextBox ID="txt_apellidos" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>    
    <tr>
        <td>Rut:</td>
        <td><asp:TextBox ID="txt_rut" runat="server" Enabled="false"></asp:TextBox></td>
        <td>Ficha Clínica:</td>
        <td><asp:TextBox ID="txt_ficha" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>  
    <tr>
        <td>Edad:</td>
        <td><asp:TextBox ID="txt_edad" runat="server" Enabled="false"></asp:TextBox></td>
        <td>Fecha de Nacimiento:</td>
        <td><asp:TextBox ID="txt_fnac" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>    
    </table> 
    <br />
    <table border="0" align="center" width="80%">
    <tr>
        <td align="center"><asp:Label ID="txt_catalogo" runat="server" Font-Bold="True" Width="543px" Text="Label" Visible="true"></asp:Label>
        </td>
    </tr>
    </table>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id_detalle_soli" 
        DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" align="center" Width="543px">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="frascos" HeaderText="Frascos" SortExpression="frascos" />
            <asp:BoundField DataField="detalle" HeaderText="Detalle" SortExpression="detalle" />
            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" SortExpression="descripcion" />
            <asp:BoundField DataField="muestras" HeaderText="Muestras" SortExpression="muestras" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
        
        SelectCommand="SELECT z.id_detalle_soli, z.id_soli, z.frascos, x.detalle, z.descripcion, z.muestras FROM [detalle_solicitud_biopsia] as z inner join [detalle_catalogo] as x on z.detalle = x.id WHERE ([id_soli] = @id_soli)">
        <SelectParameters>
            <asp:ControlParameter ControlID="txt_id" Name="id_soli" PropertyName="Text" Type="Decimal" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <table border="0" align="center" width="80%">
    <tr>
        <td style="width:40%">Derivado de:</td>
        <td style="width:60%"><asp:TextBox ID="txt_derivado" runat="server" Enabled="false" Width="400px"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Hipótesis Clínica:</td>
        <td><asp:Label ID="txt_hipo" runat="server" Visible="True"></asp:Label></td>
    </tr>
        <tr>
        <td>Antecedentes Clínicos:</td>
        <td><asp:Label ID="txt_ante" runat="server" Visible="True"></asp:Label></td>
    </tr>
    <tr>
        <td>Fecha Toma de Muestra:</td>
        <td><asp:TextBox ID="txt_fmuestra" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Fecha Recepción de Muestra:</td>
        <td><asp:TextBox ID="txt_frecibe" runat="server" Enabled="false"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Lugar de Destino:</td>
        <td><asp:TextBox ID="txt_destino" runat="server" Enabled="false" Width="400px"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Médico Solicitante:</td>
        <td><asp:TextBox ID="txt_medico" runat="server" Enabled="false" Width="400px"></asp:TextBox></td>
    </tr>
    </table>
    <br />
    <table border="0" width="80%" align="center">
    <tr>
        <td align="center">
            <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False"></asp:Label>
        </td>
    </tr>
    </table>
    </form>
</asp:Content>
