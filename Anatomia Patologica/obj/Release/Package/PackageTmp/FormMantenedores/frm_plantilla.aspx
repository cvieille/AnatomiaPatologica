﻿<%@ Page Title="Lista de Plantillas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_plantilla.aspx.vb" Inherits="Anatomia_Patologica.WebForm3" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <!-- Panel Principal Detalle de Plantilla-->
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <h2 class="text-center">Gestión del Plantillas</h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Nombre de Plantilla:</label>
                                <asp:TextBox ID="txt_nombre_plantilla" runat="server" MaxLength="100" CssClass="form-control" required="required"></asp:TextBox>
                            </div>
                        </div>

                        <br />
                        <!-- Panel Principal Antecedentes Clinicos-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Antecedentes Clinicos</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_antecedentes_clinicos" runat="server" CssClass="form-control" Height="120px" MaxLength="300" TextMode="MultiLine">
                                </asp:TextBox>
                            </div>
                        </div>

                        <!-- Panel Principal Descripción Macroscopica-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Descripción Macroscopica</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_desc_macro" runat="server" Height="120px" TextMode="MultiLine" CssClass="form-control" MaxLength="3000">
                                </asp:TextBox>
                            </div>
                        </div>

                        <!-- Panel Principal Examen Microscópico-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Examen Microscópico</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_desc_micro" runat="server" Height="120px" TextMode="MultiLine" CssClass="form-control" MaxLength="3000">
                                </asp:TextBox>
                            </div>
                        </div>

                        <!-- Panel Principal Diagnostico-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Diagnostico</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_desc_diag" runat="server" Height="120px" TextMode="MultiLine" CssClass="form-control" MaxLength="3000">
                                </asp:TextBox>
                            </div>
                        </div>

                        <!-- Panel Principal Estudio Inmunohistoquímico-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Estudio Inmunohistoquímico</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_estu_inmuno" runat="server" Height="120px" TextMode="MultiLine" CssClass="form-control" MaxLength="3000">
                                </asp:TextBox>
                            </div>
                        </div>

                        <!-- Panel Principal Nota Adicional (opcional)-->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Nota Adicional (opcional)</div>
                            <div class="panel-body">
                                <asp:TextBox ID="txt_nota" runat="server" Height="120px" TextMode="MultiLine" CssClass="form-control" MaxLength="3000">
                                </asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-danger" role="alert">
                        </asp:Label>
                        <br />
                        <asp:Button ID="cmd_guardar_p" runat="server" CssClass="btn btn-primary" Text="Guardar Plantilla"
                            OnClientClick="return confirm('¿La información esta correcta?');"/>
                        <asp:Button ID="cmd_cancelar" runat="server" CssClass="btn btn-warning" Text="Cancelar" />
                        <asp:Button ID="cmd_eliminar" runat="server" CssClass="btn btn-danger" Text="Eliminar"
                            OnClientClick="return confirm('¿Esta seguro que desea eliminar esta plantilla?');"/>

                        <asp:TextBox ID="txt_id" runat="server" Visible="False"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
