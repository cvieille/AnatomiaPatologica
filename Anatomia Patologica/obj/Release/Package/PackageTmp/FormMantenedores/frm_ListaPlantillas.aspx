<%@ Page Title="Plantillas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaPlantillas.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_plantillas" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_plantillas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_plantillas").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Listado de Plantillas</h2>
            <a href="frm_plantilla.aspx?id=0" class="btn btn-default">Nuevo Registro</a>            
        </div>
        <div class="panel-body">
            <asp:GridView ID="gdv_plantillas" runat="server"
                AutoGenerateColumns="False" DataSourceID="dts_plantillas"
                CssClass="table table-bordered table-hover table-responsive">
                <Columns>
                    <asp:BoundField DataField="ANA_idPlantilla" HeaderText="ID." InsertVisible="False" ReadOnly="True"
                        SortExpression="ANA_idPlantilla"></asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_idPlantilla" DataNavigateUrlFormatString="frm_plantilla.aspx?id={0}"
                        DataTextField="ANA_nombreplantilla" HeaderText="Nombre Plantilla" Text="Nombre Plantilla" />
                    <asp:BoundField DataField="ANA_fec_creacionPlantilla" HeaderText="Fecha de Creaci�n"
                        SortExpression="ANA_fec_creacionPlantilla" />
                    <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Usuario " SortExpression="GEN_loginUsuarios" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="dts_plantillas" runat="server"
                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
        </div>
    </div>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>

</asp:Content>
