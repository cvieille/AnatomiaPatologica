﻿<%@ Page Title="Listado de Servicios" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaServicios.aspx.vb" Inherits="Anatomia_Patologica.WebForm14" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Servicios</h2>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="upd_servicios" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gdv_servicios" runat="server"
                        AutoGenerateColumns="False" DataKeyNames="GEN_idServicio"
                        DataSourceID="dts_servicios"
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="GEN_idServicio" HeaderText="Id Servicio"
                                InsertVisible="False" ReadOnly="True" SortExpression="GEN_idServicio" />
                            <asp:BoundField DataField="GEN_dependenciaServicio" HeaderText="Dependencia" SortExpression="GEN_dependenciaServicio" />
                            <asp:BoundField DataField="GEN_nombreServicio" HeaderText="Nombre de Servicio" SortExpression="GEN_nombreServicio" />
                            <asp:BoundField DataField="GEN_clasificacionServicio" HeaderText="Clasificación" SortExpression="GEN_clasificacionServicio" />
                            <asp:BoundField DataField="GEN_codwinsigServicio" HeaderText="Cód. Winsig" SortExpression="GEN_codwinsigServicio" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_servicios" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
