﻿<%@ Page Title="Cambiar Correlativo" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_CorrelativosSistema.aspx.vb" Inherits="Anatomia_Patologica.frm_CorrelativosSistema" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Panel Principal Correlativo-->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Correlativo</h2>
                </div>
                <div class="panel-body">

                    <div style="float: left; width: 50%">
                        <div class="Titulo">Valores Actuales</div>
                        <table style="border: 1; width: 85%">
                            <tr style="text-align: left">
                                <td><label>Nº de Biopsia:</label></td>
                                <td>
                                    <asp:Label ID="lbl_n_biopsia" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                            <tr style="text-align: left">
                                <td><label>Año Biopsia:</label></td>
                                <td>
                                    <asp:Label ID="lbl_año_biopsia" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                            <tr style="text-align: left">
                                <td><label>Nº de Solicitud:</label></td>
                                <td>
                                    <asp:Label ID="lbl_n_solicitud" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                            <tr style="text-align: left">
                                <td><label>Año Solicitud:</label></td>
                                <td>
                                    <asp:Label ID="lbl_año_solicitud" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                            <tr style="text-align: left">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>

                    <div style="float: inherit; margin-left: 200px;">
                        <div class="Titulo">Valores Nuevos</div>
                        <table>
                            <tr>
                                <th>Nº de Biopsia:</th>
                                <td>
                                    <asp:TextBox ID="txt_n_biopsia" runat="server" MaxLength="6">0</asp:TextBox>
                                    <label>(*)</label>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Año Biopsia:</label></td>
                                <td>
                                    <asp:TextBox ID="txt_a_biopsia" runat="server" MaxLength="4">0</asp:TextBox>
                                    <label>(*)</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Nº de Solicitud:</label></td>
                                <td>
                                    <asp:TextBox ID="txt_n_solicitud" runat="server" MaxLength="6">0</asp:TextBox>
                                    <label>(*)</label>
                                </td>
                            </tr>
                            <tr>
                                <th>Año Solicitud:</th>
                                <td>
                                    <asp:TextBox ID="txt_a_solicitud" runat="server" MaxLength="4">0</asp:TextBox>
                                    <label>(*)</label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:Button ID="cmd_guardar" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClientClick="return confirm('¿La información es correcta?');" />

                                    <asp:Button ID="cmd_cancelar" runat="server" CssClass="btn btn-warning" Text="Cancelar" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
</asp:Content>