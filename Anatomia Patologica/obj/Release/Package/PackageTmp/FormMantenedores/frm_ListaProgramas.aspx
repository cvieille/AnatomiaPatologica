﻿<%@ Page Title="Listado de Programas" Language="vb" EnableEventValidation="false" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaProgramas.aspx.vb" Inherits="Anatomia_Patologica.frm_lista_programas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Programas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Programas").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    Listado de Programas
                    <asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                    <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"
                       Enabled="True" TargetControlID="nuevo" CancelControlID="cmd_cancelar"
                        PopupControlID="pnl_nuevo">
                    </asp:ModalPopupExtender>
                </div>

                <div class="panel-body">
                    <asp:Panel ID="pnl_nuevo" runat="server" Style="display: none;">
                        <!-- Panel Principal Gestion de tecnicas -->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Registro de Programas</div>
                            <div class="panel-body">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td>
                                            <label>Id:</label>
                                            <asp:TextBox ID="txt_id" runat="server" CssClass="form-control" ReadOnly="True" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Nombre:</label>
                                            <asp:TextBox ID="txt_nombre" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Estado:</label>
                                            <asp:DropDownList ID="ddl_estado" runat="server" CssClass="form-control" >
                                                <asp:ListItem Text="Activo" ></asp:ListItem>
                                                <asp:ListItem Text="Inactivo" ></asp:ListItem>
                                            </asp:DropDownList>                                            
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" class="btn btn-warning" />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False"
                                                class="alert alert-danger" role="alert">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:GridView ID="gdv_Programas" runat="server"
                        AutoGenerateColumns="False" DataSourceID="dts_Programas"
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="GEN_IdPrograma" HeaderText="Id" SortExpression="GEN_IdPrograma" />
                            <asp:BoundField DataField="GEN_NomPrograma" HeaderText="Descripción" SortExpression="GEN_NomPrograma" />
                            <asp:BoundField DataField="GEN_estadoPrograma" HeaderText="Estado" SortExpression="GEN_estadoPrograma" />

                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:Button ID="cmd_editar" runat="server" Text="Editar Programa"
                                        CssClass="btn btn-success" CommandArgument='<%# Eval("GEN_IdPrograma") %>'
                                        OnClick="cmd_editar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_Programas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>

</asp:Content>
