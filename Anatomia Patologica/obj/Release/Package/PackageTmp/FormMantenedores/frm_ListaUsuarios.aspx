﻿<%@ Page Title="Listado de Usuarios" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaUsuarios.aspx.vb" Inherits="Anatomia_Patologica.frm_Lista_Usuarios" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True">
    </asp:ToolkitScriptManager>
    
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="panel panel-primary">
        <!-- Default panel contents -->
            <div class="panel-heading">
                Listado de Usuarios
                <asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"  
                    DynamicServicePath="" Enabled="True" TargetControlID="nuevo"  
                    PopupControlID="pnl_nuevo">
                </asp:ModalPopupExtender>
                <asp:SqlDataSource ID="dts_usuarios" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>">
                </asp:SqlDataSource>
            </div>
            <div class="panel-body">
                <asp:Panel ID="pnl_nuevo" runat="server" style="display:none;" >
        	        <!-- Panel Principal Gestion de usuarios -->
	                <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Registro de Usuarios</div>                
                        <div class="panel-body">                
                            <table style="margin: 0 auto;" >
                                <tr>
                                    <td>Rut:</td>
                                    <td>
                                        <asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" MaxLength="8" Width="70px" >
                                        </asp:TextBox>
                                        <asp:TextBox ID="txt_digito" runat="server" Width="34px" MaxLength="1"></asp:TextBox>
                                        <asp:TextBox ID="txt_id" runat="server" Width="34px" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txt_contra_resp" runat="server" Visible="False" Width="34px"></asp:TextBox>
                                        <asp:CheckBox ID="chk_cambiarClave" runat="server" AutoPostBack="True" />Cambiar Clave
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nombre:</td>
                                    <td>
                                        <asp:TextBox ID="txt_nombre" runat="server" MaxLength="50" Width="250px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Apellido Paterno:</td>
                                    <td>
                                        <asp:TextBox ID="txt_ape1" runat="server" MaxLength="25" Width="250px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Apellido Materno:</td>
                                    <td>
                                        <asp:TextBox ID="txt_ape2" runat="server" MaxLength="25" Width="250px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Usuario:</td>
                                    <td>
                                        <asp:TextBox ID="txt_usuario" runat="server" MaxLength="8" Width="250px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Contraseña:</td>
                                    <td>
                                        <asp:TextBox ID="txt_contraseña" runat="server" Width="250px" MaxLength="32" 
                                            TextMode="Password" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Repetir Contraseña:</td>
                                    <td>
                                        <asp:TextBox ID="txt_contraseña2" runat="server" TextMode="Password" Width="250px" MaxLength="32">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Perfil:</td>
                                    <td>
                                        <asp:DropDownList ID="cmb_perfil_user" runat="server" >
                                        </asp:DropDownList>                                
                                    </td>
                                </tr>
                                <tr>
                                    <td>Activo:</td>
                                    <td>
                                        <asp:DropDownList ID="cmb_estado" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fono:</td>
                                    <td>
                                        <asp:TextBox ID="txt_telefono" runat="server" MaxLength="10">
                                        </asp:TextBox>                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Recibe Notificaciones:</td>
                                    <td>
                                        <asp:DropDownList ID="cmb_notificaciones" runat="server" >
                                            <asp:ListItem>NO</asp:ListItem>
                                            <asp:ListItem>SI</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <br />
                                        <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                        <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" class="btn btn-warning" />
                                        <br /><br />
                                        <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-danger" role="alert">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
	                </div>
                </asp:Panel>
        
                <asp:GridView ID="gdv_usuarios" runat="server" AllowSorting="True" 
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="GEN_IdUsuarios" 
                    DataSourceID="dts_usuarios" ForeColor="#333333" Width="983px" >
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="GEN_idUsuarios" HeaderText="Id" InsertVisible="False" ReadOnly="True" 
                        SortExpression="GEN_idUsuarios" Visible="false" />
                        <asp:BoundField DataField="rut" HeaderText="Rut" SortExpression="rut" />
                        <asp:BoundField DataField="nombre" HeaderText="Nombre de Usuario" SortExpression="nombre" />
                        <asp:BoundField DataField="GEN_estadoSist_Usuario" HeaderText="Estado" SortExpression="GEN_estadoSist_Usuario" />
                        <asp:TemplateField HeaderText="Editar">
                            <ItemTemplate>
                                <asp:Button ID="cmd_editar" runat="server" Text="Editar Usuario" 
                                 CssClass="btn btn-success" CommandArgument='<%# Eval("GEN_idUsuarios") %>' 
                                    onclick="cmd_editar_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
        
                
	        </div>
	    </div>
    </ContentTemplate>
</asp:UpdatePanel>
    <!-- Panel Principal de la Pagina-->
  
</asp:Content>