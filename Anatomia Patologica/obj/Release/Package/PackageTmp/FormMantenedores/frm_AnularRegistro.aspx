﻿<%@ Page Title="Anular Registro" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_AnularRegistro.aspx.vb" Inherits="Anatomia_Patologica.frm_AnularRegistro" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:Panel ID="Panel1" runat="server" Style="text-align: center" Width="996px">
        <asp:UpdatePanel ID="upd_anular" runat="server">
            <ContentTemplate>

                <!-- Panel Principal -->
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <h2 class="text-center">Anular Biopsias</h2>
                    </div>
                    <div class="panel-body">
                        <table style="width: 100%;">
                            <tr>
                                <th style="width: 26px">Nº:</th>
                                <td style="width: 406px">
                                    <asp:TextBox ID="txt_numero" runat="server" MaxLength="7" Width="100px">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_año" runat="server" MaxLength="2" Width="100px">
                                    </asp:TextBox>
                                    <asp:Button ID="cmd_buscar" runat="server" CssClass="btn btn-success" Text="Buscar" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 26px">&nbsp;</th>
                                <td style="width: 406px">
                                    <asp:GridView ID="gdv_anular" runat="server" AutoGenerateColumns="False" DataKeyNames="ANA_IdBiopsia" DataSourceID="dts_anular" CssClass="table table-bordered table-hover table-responsive">
                                        <EmptyDataTemplate>
                                            <div class="alert alert-warning" role="alert">
                                                ¡No hay casos que mostrar! 
                                            </div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="Id" InsertVisible="False"
                                                ReadOnly="True" SortExpression="ANA_IdBiopsia" />
                                            <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" ReadOnly="True"
                                                SortExpression="n_biopsia" />
                                            <asp:BoundField DataField="ANA_EstadoRegistro_Biopsias" HeaderText="Estado"
                                                SortExpression="ANA_EstadoRegistro_Biopsias" />
                                            <asp:BoundField DataField="paciente" HeaderText="Paciente" ReadOnly="True"
                                                SortExpression="paciente" />
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Button ID="cmd_Seleccionar" runat="server" CommandArgument='<%# Eval("ANA_IdBiopsia") %>'
                                                        CssClass="btn btn-success" Text="Seleccionar" OnClick="cmd_Seleccionar_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 26px">Paciente:</th>
                                <td style="width: 406px">
                                    <asp:TextBox ID="txt_NomPaciente" runat="server" Width="482px" Enabled="False"></asp:TextBox>
                                    <asp:TextBox ID="txt_idBiopsia" runat="server" Visible="False">0</asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 26px">Motivo:</th>
                                <td style="width: 406px">
                                    <asp:TextBox ID="txt_motivo_anula" runat="server" Height="127px"
                                        TextMode="MultiLine" Width="386px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 26px">&nbsp;</td>
                                <td style="width: 406px">
                                    <asp:Button ID="cmd_anular" runat="server" CssClass="btn btn-primary" Text="Anular" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!----- PANEL POPUP COMO MENSAJE DE ALERTA ----->
                <wuc:Alert ID="wuc_alert" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <asp:SqlDataSource ID="dts_anular" runat="server"
            ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    </asp:Panel>
</asp:Content>