<%@ Page Title="Gesti�n de Pacientes" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Paciente.aspx.vb" Inherits="Anatomia_Patologica.frm_Paciente" Culture="Auto" Buffer="true" UICulture="Auto" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server">
                <ProgressTemplate>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Gesti�n de Pacientes</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Tipo de Documento:</label>
                            <asp:DropDownList ID="cmb_identificacion" runat="server" AutoPostBack="True" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>N� de Documento:</label>
                            <div class="form-inline">
                                <asp:TextBox ID="txt_rut" runat="server" required="required" AutoPostBack="True" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                <asp:TextBox ID="txt_digito" runat="server" Enabled="False" ReadOnly="True" Width="40px" MaxLength="1" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>N� de Ubicaci�n Int.:</label>
                            <asp:TextBox ID="txt_ficha" runat="server" MaxLength="10" required="required" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Estado:</label>
                            <asp:DropDownList ID="cmb_estado" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Nombre:</label>
                            <asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Ap. Paterno:</label>
                            <asp:TextBox ID="txt_ap_paterno" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Ap. Materno:</label>
                            <asp:TextBox ID="txt_ap_materno" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Direcci�n:</label>
                            <asp:TextBox ID="txt_direccion" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>N�mero:</label>
                            <asp:TextBox ID="txt_direccion0" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Telefono Fijo:</label>
                            <asp:TextBox ID="txt_telefono" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                            <asp:TextBox ID="txt_id" runat="server" Visible="False"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Otros N�meros:</label>
                            <asp:TextBox ID="txt_celular" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Sexo:</label>
                            <asp:DropDownList ID="cmb_sexo" runat="server" CssClass="form-control" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>F. de Nacimiento:</label>
                            <asp:TextBox ID="txt_fecha_nacimiento" runat="server" CssClass="form-control" type="date">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Edad:</label>
                            <asp:TextBox ID="txt_edad" runat="server" MaxLength="10"
                                CssClass="form-control" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Pais:</label>
                            <asp:DropDownList ID="cmb_pais" CssClass="form-control" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>Regi�n:</label>
                            <asp:DropDownList ID="cmb_region" runat="server" AutoPostBack="True" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>Comuna:</label>
                            <asp:DropDownList ID="cmb_comuna" runat="server" AutoPostBack="True" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>Ciudad:</label>
                            <asp:DropDownList ID="cmb_ciudad" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <table class="table table-condensed">
                        <tr>
                        </tr>
                        <tr>
                            <td style="width: 182px">&nbsp;</td>
                            <td style="text-align: left"></td>
                        </tr>
                    </table>
                </div>
            </div>


            <asp:Button ID="cmd_guardar" runat="server" CssClass="btn btn-primary" Text="Guardar" />
            <asp:Button ID="cmd_cancelar" runat="server" CssClass="btn btn-warning" Text="Cancelar" />
            <br />
            <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                ControlToValidate="txt_nombre" ErrorMessage="Falta Ingresar Nombre">Falta Ingresar Nombre</asp:RequiredFieldValidator>
            <!----- PANEL POPUP COMO MENSAJE DE ALERTA ----->
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>
</asp:Content>
