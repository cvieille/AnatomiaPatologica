﻿<%@ Page Title="Lista de Técnicas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaTecnicas.aspx.vb" Inherits="Anatomia_Patologica.frm_Lista_Tecnicas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_tecnicas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_tecnicas").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="upd_tecnicas" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Listado de Tecnicas</h2>
                    <asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                    <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"
                        Enabled="True" TargetControlID="nuevo" PopupControlID="pnl_nuevo">
                    </asp:ModalPopupExtender>
                </div>
                <div class="panel-body">
                    <asp:Panel ID="pnl_nuevo" runat="server" Style="display: none;">
                        <!-- Panel Principal Gestion de tecnicas -->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Registro de Tecnicas</div>
                            <div class="panel-body">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td>
                                            <label>Id:</label>
                                            <asp:TextBox ID="txt_id" runat="server"  ReadOnly="True" CssClass="form-control" Text="0"
                                                Width="187px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <label>Nombre:</label>
                                            <asp:TextBox ID="txt_nombre" runat="server" Width="310px" MaxLength="50"  CssClass="form-control">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <label>Estado:</label>
                                            <asp:DropDownList ID="cmb_estado" runat="server" Width="150px"  CssClass="form-control">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <br />
                                            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" class="btn btn-warning" />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-danger" role="alert">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:GridView ID="gdv_tecnicas" runat="server" AutoGenerateColumns="False"
                        DataKeyNames="ANA_IdTecnica" DataSourceID="dts_tecnicas" CssClass="table table-bordered table-hover table-responsive" >
                        <Columns>
                            <asp:BoundField DataField="ANA_IdTecnica" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                                SortExpression="ANA_IdTecnica" Visible="false" />
                            <asp:BoundField DataField="ANA_nomTecnica" HeaderText="Nombre Técnica" SortExpression="ANA_nomTecnica" />
                            <asp:BoundField DataField="ANA_estadoTecnica" HeaderText="Estado" SortExpression="ANA_estadoTecnica" />
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:Button ID="cmd_editar" runat="server" Text="Editar Técnica"
                                        CssClass="btn btn-success" CommandArgument='<%# Eval("ANA_IdTecnica") %>'
                                        OnClick="cmd_editar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource ID="dts_tecnicas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Principal de la Pagina-->
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>