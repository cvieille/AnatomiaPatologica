<%@ Page Title="Listado de M�dicos" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaMedicos.aspx.vb" Inherits="Anatomia_Patologica.frm_ListaMedicos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    Listado de Medicos
            <%--<asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                    <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"
                        DynamicServicePath="" Enabled="True" TargetControlID="Label1" PopupControlID="pnl_nuevo">
                    </asp:ModalPopupExtender>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>--%>
                </div>
                <div class="panel-body">
                    <asp:Panel ID="pnl_nuevo" runat="server" Style="display: none;">
                        <!-- Panel Principal Gestion de usuarios -->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Registro de Profesional</div>
                            <div class="panel-body">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td>Rut:</td>
                                        <td>
                                            <asp:TextBox ID="txt_rut" runat="server" MaxLength="8" Width="100px" AutoPostBack="true"
                                                onkeypress="return event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57">
                                            </asp:TextBox>

                                            <asp:TextBox ID="txt_dv" runat="server" Enabled="False" Width="50px"
                                                MaxLength="1"></asp:TextBox>
                                            &nbsp;
                                    <asp:TextBox ID="txt_id" runat="server" Enabled="False" MaxLength="1"
                                        Visible="False" Width="50px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nombres:</td>
                                        <td>
                                            <asp:TextBox ID="txt_nombre" runat="server" MaxLength="50"
                                                onkeypress="return (event.charCode &gt;= 97 &amp;&amp; event.charCode &lt;= 122) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || event.charCode == 241 || event.charCode == 209 || event.charCode == 32 || event.charCode == 225 || event.charCode == 233 || event.charCode == 237 || event.charCode == 243 || event.charCode == 250 || event.charCode == 193 || event.charCode == 201 || event.charCode == 205 || event.charCode == 211 || event.charCode == 218"
                                                Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Primer Apellido:</td>
                                        <td>
                                            <asp:TextBox ID="txt_apellido" runat="server" MaxLength="50" Width="200px"
                                                onkeypress="return (event.charCode &gt;= 97 &amp;&amp; event.charCode &lt;= 122) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || event.charCode == 241 || event.charCode == 209 || event.charCode == 32 || event.charCode == 225 || event.charCode == 233 || event.charCode == 237 || event.charCode == 243 || event.charCode == 250 || event.charCode == 193 || event.charCode == 201 || event.charCode == 205 || event.charCode == 211 || event.charCode == 218"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Segundo Apellido:&nbsp; </td>
                                        <td>
                                            <asp:TextBox ID="txt_sapellido" runat="server" Width="200px" MaxLength="50"
                                                onkeypress="return (event.charCode &gt;= 97 &amp;&amp; event.charCode &lt;= 122) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || (event.charCode &gt;= 65 &amp;&amp; event.charCode &lt;= 90) || event.charCode == 241 || event.charCode == 209 || event.charCode == 32 || event.charCode == 225 || event.charCode == 233 || event.charCode == 237 || event.charCode == 243 || event.charCode == 250 || event.charCode == 193 || event.charCode == 201 || event.charCode == 205 || event.charCode == 211 || event.charCode == 218"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sexo:</td>
                                        <td>
                                            <asp:DropDownList ID="cmb_sexo" runat="server" Width="130px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dependencia:</td>
                                        <td>
                                            <asp:DropDownList ID="cmb_dependencia" runat="server" Width="130px">
                                                <asp:ListItem>HCM</asp:ListItem>
                                                <asp:ListItem>Externo</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_estado" runat="server" Text="Estado:">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmb_estado" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <!-- -- PROFESION Y ESPECIALIDAD DEL PROFESIONAL ---------------------------------------------------------------------------------------- -->
                                    <tr>
                                        <td>Profesi�n:&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmb_profesion" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;
                                    <asp:Button ID="cmd_agregar_profesion" runat="server"
                                        CssClass="btn btn-success" Text="AGREGAR" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp; </td>
                                        <td lang="es-cl">
                                            <asp:GridView ID="gdv_profesion" runat="server" CellPadding="4"
                                                ForeColor="#333333" AutoGenerateColumns="false" Width="456px">
                                                <RowStyle BackColor="#EFF3FB" />
                                                <Columns>
                                                    <asp:BoundField DataField="PROFESION" HeaderText="PROFESION" />
                                                    <asp:BoundField DataField="ESTADO" HeaderText="ESTADO" />
                                                </Columns>
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <EditRowStyle BackColor="#2461BF" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Especialidad:&nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmb_especialidad" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;
                                    <asp:Button ID="cmd_agregar_especialidad" runat="server"
                                        CssClass="btn btn-success" Text="AGREGAR" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td>
                                            <asp:GridView ID="gdv_especialidad" runat="server" CellPadding="4" ForeColor="#333333"
                                                GridLines="None" AutoGenerateColumns="False"
                                                DataKeyNames="GEN_idPro_Especialidad"
                                                Width="552px">
                                                <RowStyle BackColor="#EFF3FB" />
                                                <Columns>
                                                    <asp:BoundField DataField="ESPECIALIDAD" HeaderText="ESPECIALIDAD"
                                                        SortExpression="ESPECIALIDAD" />
                                                    <asp:BoundField DataField="ESTADO" HeaderText="ESTADO"
                                                        SortExpression="ESTADO" />
                                                </Columns>
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <EditRowStyle BackColor="#2461BF" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <asp:Button ID="cmd_guardar" runat="server" class="btn btn-primary" Text="Guardar" />
                                <asp:Button ID="cmd_cancelar" runat="server" class="btn btn-warning" Text="Cancelar" />
                                <asp:Label ID="lbl_mensaje" runat="server" class="alert alert-danger" role="alert" Text="Label" Visible="False">
                                </asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:GridView ID="gdv_medicos" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" AllowSorting="True"
                        DataSourceID="dts_medico" ForeColor="#333333" DataKeyNames="GEN_idProfesional"
                        Width="97%">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                            <asp:BoundField DataField="Rut" HeaderText="Rut" SortExpression="Rut" ReadOnly="True" />
                            <asp:BoundField DataField="Nombre Profesional" HeaderText="Nombre Profesional" SortExpression="Nombre Profesional" />
                            <asp:BoundField DataField="Dependencia" HeaderText="Dependencia" SortExpression="Dependencia" />
                            <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" />
                            <asp:BoundField DataField="Sexo" HeaderText="Sexo" SortExpression="Sexo" />
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:Button ID="cmd_editar" runat="server" Text="Editar Profesional"
                                        CssClass="btn btn-success" CommandArgument='<%# Eval("GEN_idProfesional") %>'
                                        OnClick="cmd_editar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_medico" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
