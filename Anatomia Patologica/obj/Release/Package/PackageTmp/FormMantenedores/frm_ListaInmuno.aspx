﻿<%@ Page Title="Listado de Inmunohistoquimica" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ListaInmuno.aspx.vb" Inherits="Anatomia_Patologica.frm_ListaInmuno" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_inmuno").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_inmuno").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
    <asp:UpdatePanel ID="upd_inmuno" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Inmunohistoquímica</h2>
                    <asp:Button ID="nuevo" runat="server" Text="Nuevo Registro" CssClass="btn btn-default" />
                    <asp:ModalPopupExtender ID="nuevo_MpeNuevo" runat="server" BackgroundCssClass="modalBackground"
                        Enabled="True" TargetControlID="nuevo" PopupControlID="pnl_nuevo">
                    </asp:ModalPopupExtender>
                </div>
                <div class="panel-body">
                    <asp:Panel ID="pnl_nuevo" runat="server" Style="display: none;">
                        <!-- Panel Principal Gestion de tecnicas -->
                        <div class="panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Registro de Inmunohistoquímica</div>
                            <div class="panel-body">
                                <table style="margin: 0 auto;">
                                    <tr>
                                        <td>Id:</td>
                                        <td>
                                            <asp:TextBox ID="txt_id" runat="server" Height="20px" ReadOnly="True" Text="0"
                                                Width="187px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nombre:</td>
                                        <td>
                                            <asp:TextBox ID="txt_nombre" runat="server" Width="310px" MaxLength="50">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Estado:</td>
                                        <td>
                                            <asp:DropDownList ID="cmb_estado" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <br />
                                            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" class="btn btn-primary" />
                                            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" class="btn btn-warning" />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-danger" role="alert">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:GridView ID="gdv_inmuno" runat="server" AllowSorting="True"
                        AutoGenerateColumns="False"  CssClass="table table-bordered table-hover table-responsive" 
                        DataKeyNames="ANA_idInmunoHistoquimica" DataSourceID="dts_inmuno" >
                        <Columns>
                            <asp:BoundField DataField="ANA_idInmunoHistoquimica" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                                SortExpression="ANA_IdTecnica" Visible="false" />
                            <asp:BoundField DataField="ANA_NomInmunoHistoquimica" HeaderText="Nombre Inmuno" SortExpression="ANA_NomInmunoHistoquimica" />
                            <asp:BoundField DataField="ANA_EstInmunoHistoquimica" HeaderText="Estado" SortExpression="ANA_EstInmunoHistoquimica" />
                            <asp:TemplateField HeaderText="Editar">
                                <ItemTemplate>
                                    <asp:Button ID="cmd_editar" runat="server" Text="Editar Inmuno"
                                        CssClass="btn btn-success" CommandArgument='<%# Eval("ANA_idInmunoHistoquimica") %>'
                                        OnClick="cmd_editar_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource ID="dts_inmuno" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Panel Principal de la Pagina-->
        <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
