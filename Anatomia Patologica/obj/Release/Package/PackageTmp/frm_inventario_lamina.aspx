﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_inventario_lamina.aspx.vb" Inherits="Anatomia_Patologica.frm_inventario_lamina" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <p>
        Inventario de Laminas:<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </p>
    
    <table style="width: 40%; margin: 0 auto">
        <tr>
            <td>Estado Lamina:</td>
            <td><asp:DropDownList ID="cmb_estado" runat="server" AutoPostBack="True"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:Button ID="cmb_exportar" runat="server" CssClass="BtnA-Grande" Text="Exportar" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="udp_laminas" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <p>
                <asp:GridView ID="grilla_lamina" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataSourceID="sql_ds_todas_laminas" ForeColor="#333333" 
                    GridLines="None">
                    <RowStyle BackColor="#EFF3FB" />
                    <Columns>
                        <asp:BoundField DataField="ANA_idLamina" HeaderText="Id" InsertVisible="False" 
                            ReadOnly="True" SortExpression="ANA_idLamina" />
                        <asp:BoundField DataField="ANA_nomLamina" HeaderText="Nombre" 
                            SortExpression="ANA_nomLamina" />
                        <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha" 
                            SortExpression="ANA_FecLamina" />
                        <asp:BoundField DataField="almacenada" HeaderText="Almac." 
                            SortExpression="almacenada" />
                        <asp:BoundField DataField="desechada" HeaderText="Desec." 
                            SortExpression="desechada" />
                        <asp:BoundField DataField="solicitada" HeaderText="Solic." 
                            SortExpression="solicitada" />
                        <asp:BoundField DataField="fec_interconsulta" HeaderText="Fec. Inter." 
                            SortExpression="fec_interconsulta" DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="interconsultista" HeaderText="Interconsultista" 
                            SortExpression="interconsultista" />
                        <asp:BoundField DataField="fec_reg_interconsulta" 
                            HeaderText="Reg. Interconsulta" SortExpression="fec_reg_interconsulta" 
                            DataFormatString="{0:dd-MM-yyyy}" />
                        <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" 
                            SortExpression="ANA_EstLamina" />
                    </Columns>
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#2461BF" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="sql_ds_todas_laminas" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
                    
                    SelectCommand="SELECT ANA_idLamina, ANA_nomLamina, ANA_FecLamina, almacenada, desechada, solicitada, fec_interconsulta, interconsultista, fec_reg_interconsulta, ANA_EstLamina FROM laminas WHERE (ANA_EstLamina LIKE '%' + @estado + '%') ORDER BY ANA_nomLamina, ANA_IdCortes_Muestras">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cmb_estado" Name="estado" 
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cmb_estado" />
        </Triggers>
    </asp:UpdatePanel>
    
</asp:Content>