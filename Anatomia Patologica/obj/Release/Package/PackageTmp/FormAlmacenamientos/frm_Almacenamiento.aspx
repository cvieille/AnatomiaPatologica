<%@ Page Title="Almacenamiento" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Almacenamiento.aspx.vb" Inherits="Anatomia_Patologica.frm_almacenamiento" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_almacenaBiopsia").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_almacenaBiopsia").find("tr:first"))).dataTable();
            $("#ctl00_Cnt_Principal_gdv_casete").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_casete").find("tr:first"))).dataTable();
            $("#ctl00_Cnt_Principal_gdv_laminas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_laminas").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <!-- Panel Principal Solicitud de Macroscopia-->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Gesti�n de Almacenamiento</h2>
        </div>
        <div class="panel-body">
            <div style="margin: 20px;">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading clearfix">
                        <ul class="nav nav-tabs" runat="server" id="tabFrm">
                            <li id="li_tecnicas" runat="server" class="active">
                                <a href="#tabBiopsia" data-toggle="tab">
                                    <label>Solicitudes de Biopsia</label>
                                    <asp:UpdatePanel RenderMode="Inline" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lbl_totalBiopsias" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </a>
                            </li>
                            <li id="li_inmuno" runat="server">
                                <a href="#tabCasete" data-toggle="tab">
                                    <label>Solicitud de Casete</label>
                                    <asp:UpdatePanel RenderMode="Inline" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lbl_totalCasete" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </a>
                            </li>
                            <li id="li_interconsulta" runat="server">
                                <a href="#tabInterconsulta" data-toggle="tab">
                                    <label>Solicitud de Lamina</label>
                                    <asp:UpdatePanel RenderMode="Inline" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lbl_totalLamina" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tabBiopsia">
                                <!-- Grilla de Solicitudes de biopsia-->
                                <!-- Default panel contents -->
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <!-------------------- Default panel contents -------------------->
                                    <div class="panel-heading">
                                        <h2 class="text-center">Solicitudes de Macroscopia</h2>
                                    </div>

                                    <div class="panel-body">
                                        <asp:UpdatePanel ID="upd_RetiraMacro" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gdv_almacenaBiopsia" runat="server" AutoGenerateColumns="False"
                                                    DataSourceID="dts_biopsias" CssClass="table table-bordered table-hover table-responsive">
                                                    <Columns>
                                                        <asp:BoundField DataField="n_biopsia" HeaderText="Biopsia" SortExpression="n_biopsia" ReadOnly="True" />
                                                        <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" SortExpression="ANA_OrganoBiopsia" />
                                                        <asp:BoundField DataField="ANA_AlmacenadaBiopsia" HeaderText="Alm." SortExpression="ANA_AlmacenadaBiopsia" />
                                                        <asp:BoundField DataField="ANA_TumoralBiopsia" HeaderText="Tum." SortExpression="ANA_TumoralBiopsia" />
                                                        <asp:BoundField DataField="ANA_DesechadaBiopsia" HeaderText="Des." SortExpression="ANA_DesechadaBiopsia" />
                                                        <asp:BoundField DataField="ANA_RapidaBiopsia" HeaderText="Rap." SortExpression="ANA_RapidaBiopsia" />
                                                        <asp:BoundField DataField="login_patologo" HeaderText="Patologo" SortExpression="login_patologo" />
                                                        <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia" ReadOnly="True" SortExpression="ANA_IdBiopsia" Visible="false" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="cmd_RetirarMacro" runat="server" Text="Retirar" CssClass="btn btn-success"
                                                                    CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_RetirarMacro_Click"
                                                                    OnClientClick="return confirm('�Esta seguro que desea Retirar esta Muestra?');" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tabCasete">
                                <!-- Panel Principal Solicitud de Casete-->
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">
                                        <h2 class="text-center">Solicitud de Casete</h2>
                                    </div>
                                    <div class="panel-body">
                                        <asp:UpdatePanel ID="Upd_Cortes" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gdv_casete" runat="server" AutoGenerateColumns="False"
                                                    DataSourceID="dts_cortes" CssClass="table table-bordered table-hover table-responsive">
                                                    <Columns>
                                                        <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                                                            DataTextField="ANA_NomCortes_Muestras" HeaderText="N� Corte" Text="N� Corte" Target="_blank" />
                                                        <asp:BoundField DataField="ANA_FecCortes_Muestras" HeaderText="Fecha" SortExpression="ANA_FecCortes_Muestras"
                                                            ReadOnly="True" DataFormatString="{0:dd-MM-yyyy}" />
                                                        <asp:BoundField DataField="ANA_EstCortes_Muestras" HeaderText="Estado" SortExpression="ANA_EstCortes_Muestras" ReadOnly="True" />
                                                        <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="id" Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="ANA_SolporCortes_Muestras" HeaderText="Solicitado Por" SortExpression="ANA_SolporCortes_Muestras" />
                                                        <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="GEN_idTipo_Estados_Sistemas" SortExpression="GEN_idTipo_Estados_Sistemas" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="cmd_RetirarCasete" runat="server" Text="Retirar" CssClass="btn btn-success"
                                                                    CommandArgument='<%# Eval("ANA_IdCortes_Muestras") %>' OnClick="cmd_RetirarCasete_Click"
                                                                    OnClientClick="return confirm('�Esta seguro que desea Retirar este Corte?');" />

                                                                <asp:Button ID="cmd_EliminarSolicitud" runat="server" Text="Eliminar Solicitud" CssClass="btn btn-danger"
                                                                    CommandArgument='<%# Eval("ANA_IdCortes_Muestras") %>' OnClick="cmd_EliminarSolicitud_Click"
                                                                    OnClientClick="return confirm('�Esta seguro que desea Eliminar la solicidut de Corte?');" />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>

                                                <asp:SqlDataSource ID="dts_cortes" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tabInterconsulta">
                                <!-- Panel Principal Solicitud de Lamina-->
                                <div class="panel panel-default">
                                    <!-- Default panel contents -->
                                    <div class="panel-heading">
                                        <h2 class="text-center">Solicitud de Lamina</h2>
                                    </div>
                                    <div class="panel-body">
                                        <asp:UpdatePanel ID="upd_laminas" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:GridView ID="gdv_laminas" runat="server" AutoGenerateColumns="False" DataSourceID="dts_laminas"
                                                    CssClass="table table-bordered table-hover table-responsive">
                                                    <Columns>
                                                        <asp:BoundField DataField="ANA_NomLamina" HeaderText="Lamina" SortExpression="ANA_NomLamina" />
                                                        <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha" SortExpression="ANA_FecLamina" />
                                                        <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" SortExpression="ANA_EstLamina" />
                                                        <asp:BoundField DataField="ANA_AlmLamina" HeaderText="Alm." SortExpression="ANA_AlmLamina" />
                                                        <asp:BoundField DataField="ANA_SolLamina" HeaderText="Sol." SortExpression="ANA_SolLamina" />
                                                        <asp:BoundField DataField="ANA_IdLamina" HeaderText="Id. Lamina" ReadOnly="True" />
                                                        <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="GEN_idTipo_Estados_Sistemas" ReadOnly="True" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Button ID="cmd_RetirarLamina" runat="server" Text="Retirar" CssClass="btn btn-success"
                                                                    CommandArgument='<%# Eval("ANA_idLamina") %>' OnClick="cmd_RetirarLamina_Click"
                                                                    OnClientClick="return confirm('�Esta seguro que desea Retirar esta Lamina?');" />

                                                                <asp:Button ID="cmd_EliminarSolicitudLamina" runat="server" Text="Eliminar Solicitud" CssClass="btn btn-danger"
                                                                    CommandArgument='<%# Eval("ANA_idLamina") %>' OnClick="cmd_EliminarSolicitudLamina_Click"
                                                                    OnClientClick="return confirm('�Esta seguro que desea Eliminar esta Solicitud?');" />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="dts_laminas" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div style="text-align: center;">
            <asp:Button ID="cmd_desalmacenar0" runat="server" Text="Muestras por Almacenar" CssClass="btn btn-primary" />
            <asp:Button ID="cmd_desalmacenar1" runat="server" Text="Casete por Almacenar" CssClass="btn btn-primary" />
            <asp:Button ID="cmd_almacenar_laminas" runat="server" Text="Laminas por Almacenar" CssClass="btn btn-primary" />
        </div>
        <asp:SqlDataSource ID="dts_biopsias" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    </div>





    <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" CssClass="LblMsjAdvertencia">
    </asp:Label>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <wuc:Modal_Cargando ID="modal_cargando" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
