﻿<%@ Page Language="vb" Title="Registro de Muestras" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Recepcion.aspx.vb" Inherits="Anatomia_Patologica.WebForm7" Culture="es-ES" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="Upd_General" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Registro de Muestras</h2>
                </div>
                <div class="panel-body">
                    <div class="table table-condensed">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Nº de Registro:</label>
                                <asp:TextBox ID="txt_biopsia_n" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Fecha de Ingreso:</label>
                                <asp:TextBox ID="txt_fecha_ingreso" runat="server" Type="date" CssClass="form-control" ReadOnly="true" Enabled="false"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Fecha Recepción:</label>
                                <asp:TextBox ID="txt_fecha_recepcion" runat="server" type="date" CssClass="form-control" required="required"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="cmd_VerMovimientos" runat="server" CssClass="btn btn-info" Text="Ver Movimientos" Visible="false" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tipo de Documento:</label>
                                <asp:DropDownList ID="cmb_identificacion" runat="server" AutoPostBack="True" Enabled="False" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>Nº de Documento:</label>
                                <div class="form-inline">
                                    <asp:TextBox ID="txt_rut" runat="server" required="required" AutoPostBack="True" MaxLength="10" CssClass="form-control" Width="170px"
                                        Enabled="False"></asp:TextBox>
                                    <asp:TextBox ID="txt_digito" runat="server" Enabled="False" ReadOnly="True" Width="40px" MaxLength="1" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Nº de Ubicación Int.:</label>
                                <asp:TextBox ID="txt_ficha" runat="server" MaxLength="10" required="required" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Nombre:</label>
                                <asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Ap. Paterno:</label>
                                <asp:TextBox ID="txt_ap_paterno" runat="server" MaxLength="100" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Ap. Materno:</label>
                                <asp:TextBox ID="txt_ap_materno" runat="server" MaxLength="100" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>GES:</label>
                                <asp:DropDownList ID="cmb_ges" runat="server" CssClass="form-control" Enabled="False">
                                    <asp:ListItem>SI</asp:ListItem>
                                    <asp:ListItem Selected="True">NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>F. de Nacimiento:</label>
                                <asp:TextBox ID="txt_fecha_nacimiento" runat="server" required="required" Enabled="false" CssClass="form-control" type="date"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Edad:</label>
                                <asp:TextBox ID="txt_edad" runat="server" MaxLength="10" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Nacionalidad:</label>
                                <asp:TextBox ID="txt_nacionalidad" runat="server" Enabled="False" MaxLength="100" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Tipo de Paciente:</label>
                                <asp:DropDownList ID="cmb_tipo_paciente" CssClass="form-control" runat="server" Enabled="False"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tipo de Muestra:</label>
                                <asp:DropDownList ID="cmb_catalogo" runat="server" AutoPostBack="True" Enabled="False" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>Detalle de Muestra:</label>
                                <asp:DropDownList ID="cmb_detalle_catalogo" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <label>Organo:</label>
                                <asp:TextBox ID="txt_organo" runat="server" MaxLength="100" CssClass="form-control" Enabled="False"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>N° de Muestras por Frasco:</label>
                                <div class="form-inline">
                                    <asp:DropDownList ID="cmb_muestras_frasco" runat="server" CssClass="form-control" Enabled="False">
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>6</asp:ListItem>
                                        <asp:ListItem>7</asp:ListItem>
                                        <asp:ListItem>8</asp:ListItem>
                                        <asp:ListItem>9</asp:ListItem>
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>11</asp:ListItem>
                                        <asp:ListItem>12</asp:ListItem>
                                        <asp:ListItem>13</asp:ListItem>
                                        <asp:ListItem>14</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>>15</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinkButton ID="lkb_agregarmuestra" runat="server" Visible="false" CssClass="btn btn-success" Style="color: white;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></asp:LinkButton>
                                    <asp:LinkButton ID="lkb_quitarmuestras" runat="server" Visible="false" CssClass="btn btn-danger" Style="color: white;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gdv_DetalleMuestra" runat="server" CssClass="table table-bordered table-hover table-responsive">
                                </asp:GridView>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-5">
                                <label>Derivado de:</label>
                                <div class="form-inline">
                                    <asp:DropDownList ID="cmb_tipo_origen" runat="server" AutoPostBack="True" CssClass="form-control" Enabled="False">
                                        <asp:ListItem>HCM</asp:ListItem>
                                        <asp:ListItem>Externo</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="cmb_servicio_sol" runat="server" DataTextField="nombre" DataValueField="IdSERVICIO" CssClass="form-control" AutoPostBack="True"
                                        Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label>Lugar de Destino:</label>
                                <div class="form-inline">
                                    <asp:DropDownList ID="cmb_tipo_destino" runat="server" AutoPostBack="True" CssClass="form-control" Enabled="False">
                                        <asp:ListItem>HCM</asp:ListItem>
                                        <asp:ListItem>Externo</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="cmb_servicio_dest" runat="server" DataTextField="nombre" CssClass="form-control" DataValueField="IdSERVICIO" AutoPostBack="True" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>Programa:</label>
                                <asp:DropDownList ID="cmb_programa" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="False">
                                </asp:DropDownList>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label>Solicitado por:</label>
                                <asp:DropDownList ID="cmb_medico_solicita" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="False">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txt_id" runat="server" Width="37px" Visible="false">
                                </asp:TextBox>
                                <asp:TextBox ID="txt_id_pac" runat="server" Visible="false">
                                </asp:TextBox>
                                <asp:TextBox ID="txt_id_soli" runat="server" Width="37px" Visible="False">
                                </asp:TextBox>
                                <asp:TextBox ID="txt_estado" runat="server" ReadOnly="True" Width="400px"
                                    Visible="False">Ingresado</asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Button ID="cmd_guardar" runat="server" Text="Guardar"
                                    CssClass="btn btn-primary" Visible="False" />

                                <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar"
                                    CssClass="btn btn-warning" Visible="False" />
                            </div>
                            <div class="col-lg-3">
                                <asp:Button ID="cmd_eliminar" runat="server" CssClass="btn btn-danger"
                                    Text="Eliminar" Visible="False" />
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="lbl_mensaje" runat="server" Text="Mensajes de Advertencia" Visible="False" class="alert alert-danger" role="alert">
                    </asp:Label>
                </div>
            </div>
            <br />
            <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
            <!----- PANEL POPUP COMO MENSAJE DE ALERTA ----->
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
