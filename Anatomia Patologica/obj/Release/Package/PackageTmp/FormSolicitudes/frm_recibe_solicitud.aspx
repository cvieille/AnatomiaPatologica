﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_recibe_solicitud.aspx.vb" Inherits="Anatomia_Patologica.frm_recibe_solicitud" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <center>
        <div class="Titulo">Recepción de Solicitudes Histopatológicas y Citológicas</div>
        <br />
        <asp:Label ID="lbl_mensaje" runat="server" Text="Label" CssClass="LblMsjAdvertencia" Visible="false">
        </asp:Label>
    </center>
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="Sqlds_recibe_solicitud" 
        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" Width="916px">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:TemplateField HeaderText="Sel."><ItemTemplate>
                <asp:CheckBox ID="chkSeleccion" runat="server" CssClass="controlSeleccion" />
            </ItemTemplate></asp:TemplateField>
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" ItemStyle-CssClass="ColumnaOculta" HeaderStyle-CssClass="ColumnaOculta"/>
            <asp:BoundField DataField="OT" HeaderText="OT" SortExpression="OT" />
            <asp:BoundField DataField="AÑO" HeaderText="AÑO" SortExpression="AÑO" />
            <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" DataFormatString="{0:dd-MM-yyyy}" />
            <asp:BoundField DataField="PACIENTE" HeaderText="PACIENTE" SortExpression="PACIENTE" ReadOnly="True" />
            <asp:BoundField DataField="ANA_GesRegistro_Biopsias" HeaderText="GES" SortExpression="ANA_GesRegistro_Biopsias" />
            <asp:BoundField DataField="MEDICO" HeaderText="MEDICO" SortExpression="MEDICO" ReadOnly="True" />
            <asp:BoundField DataField="MUESTRA" HeaderText="MUESTRA" SortExpression="MUESTRA" />
            <asp:BoundField DataField="ORIGEN" HeaderText="ORIGEN" SortExpression="ORIGEN" />
            <asp:BoundField DataField="SERVICIO" HeaderText="SERVICIO" SortExpression="SERVICIO" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <div style="text-align:center">
    <asp:Button ID="cmd_seleccionar_todo" runat="server" Text="Seleccionar Todo" 
        Enabled="true" CssClass="btn btn-default" />
    <asp:Button ID="cmd_seleccionar_nada" runat="server" Text="Quitar Selección" 
        Enabled="true" CssClass="btn btn-default" />
    </div>
    <div style="text-align:center">
    <asp:Button ID="cmd_recibir" runat="server" Text="Recibir Seleccionadas" Enabled="true" CssClass="BtnA-MGrande" />
    <asp:Button ID="cmd_ver" runat="server" Text="Ver Seleccionada" Enabled="true" CssClass="BtnA-MGrande" />
    <asp:Button ID="cmd_imprimir" runat="server" Text="Imprimir Selección" Enabled="true" CssClass="BtnA-MGrande" />
    </div>
    <asp:SqlDataSource ID="Sqlds_recibe_solicitud" runat="server" 
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
        SelectCommand="SELECT * FROM [vi_recibe_solicitud] ORDER BY [FECHA] DESC, [ID] DESC"></asp:SqlDataSource>

</asp:Content>
