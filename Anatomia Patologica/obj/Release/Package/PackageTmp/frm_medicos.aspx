<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_medicos.aspx.vb" Inherits="Anatomia_Patologica.frm_medicos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <h3>Registro de M�dicos:</h3>
    <br />
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:UpdatePanel ID="upd_todo" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    <table style="width: 62%; margin: 0 auto;">
        <tr>
            <td>Rut:</td>
            <td><asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" MaxLength="10" TabIndex="1" Width="120px"></asp:TextBox>
                <asp:TextBox ID="txt_digito" runat="server" Enabled="False" ReadOnly="True" Width="17px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_rut" 
                ErrorMessage="Falta el Rut del Paciente">Falta el Rut del Paciente</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td><asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" Width="306px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_nombre" 
                ErrorMessage="Faltan Datos">Faltan Datos</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Apellidos:</td>
            <td><asp:TextBox ID="txt_apellidos" runat="server" MaxLength="100" Width="304px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_apellidos" 
                ErrorMessage="RequiredFieldValidator">Faltan Datos</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Sexo:</td>
            <td><asp:DropDownList ID="cmb_sexo" runat="server" Width="150px" AutoPostBack="True"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>Dependencia:</td>
            <td><asp:DropDownList ID="cmb_dependencia" runat="server" Width="150px">
                <asp:ListItem>HCM</asp:ListItem>
                <asp:ListItem>Externo</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txt_id" runat="server" Height="20px" ReadOnly="True" Width="78px" Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td><asp:DropDownList ID="cmb_estado" runat="server" Width="150px" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width: 447px">
                <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" CssClass="BtnA-Grande" />
                <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" CssClass="BtnR-Grande" />
                <br />
                <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </ContentTemplate>
</asp:UpdatePanel>
    
</asp:Content>