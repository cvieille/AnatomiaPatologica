<%@ Page Title="Biopsias con Neoplasia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasconNeoplasia.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasconNeoplasia" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Biopsias con Neoplasia</h2>
                </div>
                <div class="panel-body">
                    <div class="col-md-6 col-sm-6 centrado-horizontal">
                        <div class="form-group col-md-3 col-sm-3">
                            <label for="txt_fec_inicio">
                                Desde:</label>
                            <asp:TextBox ID="txt_FechaDesde" runat="server" MaxLength="10" type="date" class="form-control input-sm"
                                required="true">
                            </asp:TextBox>
                        </div>
                        <div class="form-group col-md-3 col-sm-3">
                            <label for="txt_fec_final">
                                Hasta:</label>
                            <asp:TextBox ID="txt_FechaHasta" runat="server" MaxLength="10" type="date" class="form-control input-sm"
                                required="true">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="centrado-horizontal">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="cmd_buscar" runat="server" CssClass="btn btn-success" Text="Buscar" />
                            <asp:Button ID="cmd_Exportar" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <br />
            </div>
            <p>
                <asp:Label ID="Lbl_error" runat="server" Text="Revise el rango de fechas a consultar ....."
                    CssClass="alert alert-danger" Visible="False"></asp:Label>
            </p>
            <asp:GridView ID="grilla_datos" runat="server" AutoGenerateColumns="False" CellPadding="4"
                DataSourceID="sql_ds_neoplasias" ForeColor="#333333" Font-Size="Small" CssClass="table table-bordered table-hover table-responsive">
                <RowStyle BackColor="#EFF3FB" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormExternos/frm_VerDetalleBiopsia.aspx?id={0}"
                        DataTextField="ANA_NumeroRegistro_Biopsias" FooterText="N� Biopsia" HeaderText="N� Biopsia"
                        Target="_blank" SortExpression="ANA_NumeroRegistro_Biopsias" />
                    <asp:BoundField DataField="ANA_NumeroRegistro_Biopsias" HeaderText="N�Biopsia" SortExpression="ANA_NumeroRegistro_Biopsias" />
                    <asp:BoundField DataField="RutPaciente" HeaderText="Documento" SortExpression="RutPaciente" />
                    <asp:BoundField DataField="GEN_nuiPaciente" HeaderText="N� Ubic." SortExpression="GEN_nuiPaciente" />
                    <asp:BoundField DataField="NomPaciente" HeaderText="Paciente" SortExpression="NomPaciente" />
                    <asp:BoundField DataField="ANA_DesOrganoCie" HeaderText="Organo" SortExpression="ANA_DesOrganoCie" />
                    <asp:BoundField DataField="ANA_DesTopografia" HeaderText="Topografia" SortExpression="ANA_DesTopografia" />
                    <asp:BoundField DataField="Lateralidad" HeaderText="Lateralidad" SortExpression="Lateralidad" />
                    <asp:BoundField DataField="ANA_DesMorfologia" HeaderText="Morfologia" SortExpression="ANA_DesMorfologia" />
                    <asp:BoundField DataField="ANA_descripcionComportamiento" HeaderText="Comportamiento"
                        SortExpression="ANA_descripcionComportamiento" />
                    <asp:BoundField DataField="ANA_DescGrado_Diferenciacion" HeaderText="Grado" SortExpression="ANA_DescGrado_Diferenciacion" />
                    <asp:BoundField DataField="Her2" HeaderText="Her2" SortExpression="Her2" />
                </Columns>
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <asp:SqlDataSource ID="sql_ds_neoplasias" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="cmd_Exportar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
