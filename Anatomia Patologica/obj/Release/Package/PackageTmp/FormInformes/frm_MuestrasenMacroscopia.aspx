﻿<%@ Page Title="Muestras en Macroscopia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MuestrasenMacroscopia.aspx.vb" Inherits="Anatomia_Patologica.frm_MuestrasenMacroscopia" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe de Muestras en Macroscopia</h2>            
        </div>

        <div class="panel-body">
            <div class="text-right" >
                <asp:Button ID="cmd_exportar" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
            </div>
            
            <asp:GridView ID="grilla_macro" runat="server" AutoGenerateColumns="False" DataSourceID="dts_informe"
                 CssClass="table table-bordered table-hover table-responsive">
                <Columns>
                    <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" ReadOnly="True" SortExpression="n_biopsia" />
                    <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" SortExpression="ANA_OrganoBiopsia" />
                    <asp:BoundField DataField="cant_cortes" HeaderText="Cortes" SortExpression="cant_cortes" />
                    <asp:BoundField DataField="ANA_TipoBiopsia" HeaderText="Tipo" SortExpression="ANA_TipoBiopsia" />
                    <asp:BoundField DataField="nombre_patologo" HeaderText="Patologo" SortExpression="nombre_patologo" />
                    <asp:BoundField DataField="nombre_tecnologo" HeaderText="Tecnólogo" SortExpression="nombre_tecnologo" />
                    <asp:BoundField DataField="cant_tecnica" HeaderText="Técnicas" SortExpression="cant_tecnica" />
                    <asp:BoundField DataField="cant_descal" HeaderText="Descalc." SortExpression="cant_descal" />
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <asp:SqlDataSource ID="dts_informe" runat="server"
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
</asp:Content>
