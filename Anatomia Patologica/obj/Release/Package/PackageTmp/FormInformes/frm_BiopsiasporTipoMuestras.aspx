﻿<%@ Page Title="Informe por Tipo de Muestra" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasporTipoMuestras.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasporTipoMuestras" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Detalle").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Detalle").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Informe por Tipo de Muestra</h2>
                </div>
                <div class="panel-body">
                    <div class="col-md-2">
                        <label>Desde:</label>
                        <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <label>Hasta:</label>
                        <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required" CssClass="form-control"> </asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <br />
                        <label>Todos:</label>
                        <asp:CheckBox ID="CheckBoxTodos" runat="server" Checked="false" AutoPostBack="true" />
                    </div>
                    <div class="col-md-2">
                        <label>Tipo de Muestra:</label>
                        <asp:DropDownList ID="cmb_catalogo" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <label>Detalle de Muestra:</label>
                        <asp:DropDownList ID="cmb_detalle_catalogo" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <br />
                        <div class="form-inline">
                            <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                            <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                        </div>

                    </div>

                    <br />
                    <hr />
                    <br />

                    <asp:GridView ID="gdv_Detalle" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                        DataSourceID="dts_cuenta" CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" SortExpression="n_biopsia" />
                            <asp:BoundField DataField="ANA_Catalogo_Muestras" HeaderText="Tipo Biopsia" SortExpression="ANA_Catalogo_Muestras" />
                            <asp:BoundField DataField="ANA_DetalleCatalogo_Muestras" HeaderText="Detalle Catalogo" SortExpression="ANA_DetalleCatalogo_Muestras" />
                            <asp:BoundField DataField="cortes" HeaderText="Cortes" SortExpression="cortes" />
                            <asp:BoundField DataField="laminas" HeaderText="Laminas" SortExpression="laminas" />
                            <asp:BoundField DataField="tecnicas" HeaderText="Técnicas" SortExpression="tecnicas" />
                            <asp:BoundField DataField="inmuno" HeaderText="Inmuno" SortExpression="inmuno" />
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource ID="dts_cuenta" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <br />

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Excel" />
        </Triggers>
    </asp:UpdatePanel>   
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
