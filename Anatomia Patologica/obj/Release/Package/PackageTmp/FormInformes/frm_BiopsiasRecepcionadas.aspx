﻿<%@ Page Title="Inf. Biopsias Recibidas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasRecepcionadas.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasRecepcionadas" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_biopsiasRecepcionadas").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_biopsiasRecepcionadas").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Biopsias Recepcionadas</h2>
        </div>
        <div class="panel-body">

            <asp:UpdatePanel ID="upd_general" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Desde</label>
                            <asp:TextBox ID="txt_fec_inicio" runat="server" CssClass="form-control" type="date"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Hasta</label>
                            <asp:TextBox ID="txt_fec_final" runat="server" CssClass="form-control" type="date"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <div class="form-inline">
                                <br />
                                <asp:Button ID="cmd_Buscar" runat="server" Text="Buscar Biopsias" CssClass="btn btn-success" />
                                <asp:Button ID="cmd_Exportar" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </div>

                    <div style="text-align: center">
                        <asp:UpdateProgress ID="upp_general" runat="server">
                            <ProgressTemplate>
                                <asp:Label ID="Label5" runat="server" Text="Espere un momento..." CssClass="alert alert-info"></asp:Label>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <p>
                        </p>
                        <p>
                            <asp:Label ID="lbl_error" runat="server"
                                Text="Revise el rango de fechas a consultar ....."
                                CssClass="alert alert-danger" Visible="False"></asp:Label>
                        </p>
                    </div>
                    <asp:GridView ID="gdv_biopsiasRecepcionadas" runat="server" AutoGenerateColumns="false"
                        DataSourceID="dts_biopsiasrecepcionadas" 
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" SortExpression="n_biopsia"
                                ItemStyle-Wrap="true" />
                            <asp:BoundField DataField="nombre" HeaderText="Nombre Paciente" ReadOnly="True" SortExpression="nombre" />
                            <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" HeaderText="F. Recepción"
                                ItemStyle-Wrap="true" SortExpression="ANA_fec_RecepcionRegistro_Biopsias" DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField DataField="GEN_dependenciaServicio" HeaderText="Origen" SortExpression=" GEN_dependenciaServicio" />
                            <asp:BoundField DataField="servicio_sol" HeaderText="Servicio" SortExpression="servicio_sol" />
                            <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Órgano" SortExpression="ANA_OrganoBiopsia" />
                            <asp:BoundField DataField="ANA_fecDespBiopsia" HeaderText="Fec. Desp." ItemStyle-Wrap="true"
                                SortExpression="ANA_fecDespBiopsia" DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField HeaderText="Servicio" />
                            <asp:BoundField HeaderText="Recibe" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_biopsiasrecepcionadas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="cmd_Exportar" />
                </Triggers>

            </asp:UpdatePanel>
        </div>
    </div>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
