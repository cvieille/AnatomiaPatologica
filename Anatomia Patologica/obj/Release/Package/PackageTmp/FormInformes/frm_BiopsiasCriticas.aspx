﻿<%@ Page Title="Inf. Biopsias Criticas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_BiopsiasCriticas.aspx.vb" Inherits="Anatomia_Patologica.frm_BiopsiasCriticas" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_grilla_critico").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_grilla_critico").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">


    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe de Criticos</h2>
        </div>
        <div class="panel-body">
            <div class="row text-left">
                <div class="col-md-2">
                    <label for="txt_fec_inicio">Desde:</label>
                    <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required" CssClass="form-control">
                    </asp:TextBox>
                </div>
                <div class="col-md-2">
                    <label>Hasta:</label>
                    <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required" CssClass="form-control">
                    </asp:TextBox>
                </div>
                <div class="col-md-3">
                    <label>Patologo:</label>
                    <div class="form-inline">
                        <div class="row">
                            <div class="col-md-2">
                                <asp:CheckBox ID="chk_patologo" runat="server" />
                            </div>
                            <div class="col-md-8">
                                <asp:DropDownList ID="cmb_patologo" runat="server" AppendDataBoundItems="True" CssClass="form-control"
                                    Enabled="False">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label>Leido:</label>
                    <div class="row">

                        <div class="form-inline">
                            <div class="col-md-2">
                                <asp:CheckBox ID="chk_leido" runat="server" />
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList ID="cmb_leido" runat="server" Enabled="False" CssClass="form-control">
                                    <asp:ListItem>SI</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <br />
                    <div class="form-inline">
                        <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                        <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                    </div>

                </div>

            </div>
            <hr />

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <asp:UpdateProgress ID="upp_general" runat="server">
                            <ProgressTemplate>
                                <asp:Label ID="Label5" runat="server" Text="Espere un momento..." CssClass="alert alert-info"></asp:Label>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:GridView ID="grilla_critico" runat="server" AutoGenerateColumns="False" role="grid"
                            DataSourceID="dts_ListaCriticos" CssClass="table table-bordered table-hover table-responsive">
                            <Columns>
                                <asp:BoundField DataField="NomPaciente" HeaderText="Nombre Paciente" ReadOnly="True" />

                                <asp:BoundField DataField="RutPaciente" HeaderText="Rut" ReadOnly="True">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="n_biopsia" HeaderText="Nº de Examen" ReadOnly="True" />
                                <asp:BoundField DataField="ANA_fecValidaBiopsia" HeaderText="Fecha Validación" />
                                <asp:BoundField DataField="ANA_FechaBiopsias_Critico" HeaderText="Fecha Notificación" />
                                <asp:BoundField DataField="DiasNotificacion" HeaderText="Días Not." />
                                <asp:BoundField DataField="NomPatologo" HeaderText="Patologo" />
                                <asp:BoundField DataField="ANA_DescDiagnostico" HeaderText="Diagnóstico" />
                                <asp:BoundField DataField="UsuarioNotificado" HeaderText="Nombre de Quien Recibe Notificación" />
                                <asp:BoundField DataField="ANA_RespuestaBiopsias_Critico" HeaderText="Respuesta" />

                            </Columns>
                        </asp:GridView>

                    </div>
                    <br />
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                        <wuc:Alert ID="wuc_alert" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
                    <asp:PostBackTrigger ControlID="Btn_Excel" />
                </Triggers>
            </asp:UpdatePanel>


        </div>
    </div>
    <asp:SqlDataSource ID="dts_ListaCriticos" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_Cnt_Principal_chk_patologo").click(function () {
                if ($(this).is(":checked")) {
                    $("#ctl00_Cnt_Principal_cmb_patologo").prop("disabled", false);
                } else {
                    $("#ctl00_Cnt_Principal_cmb_patologo").prop("disabled", true);
                }
            });
            $("#ctl00_Cnt_Principal_chk_leido").click(function () {
                if ($(this).is(":checked")) {
                    $("#ctl00_Cnt_Principal_cmb_leido").prop("disabled", false);
                } else {
                    $("#ctl00_Cnt_Principal_cmb_leido").prop("disabled", true);
                }
            });
        });
    </script>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
