﻿<%@ Page Language="vb" Title="Informe Consolidado" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_ConsolidadoBiopsias.aspx.vb" Inherits="Anatomia_Patologica.frm_impr_consolidado" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Totales").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Totales").find("tr:first"))).dataTable();
            $("#ctl00_Cnt_Principal_gdv_Detalle").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Detalle").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe Consolidado</h2>
        </div>
        <div class="panel-body">
            <div class="col-md-2">
                <label>Desde:</label>
                <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required">
                </asp:TextBox>
            </div>
            <div class="col-md-2">
                <label>Hasta:</label>
                <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required">
                </asp:TextBox>
            </div>
            <div class="col-md-3">
                <br />
                <div class="form-inline">
                    <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                    <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />
                </div>
            </div>
        </div>
    </div>
    <br />   
    <p>
        <asp:Label ID="Lbl_error" runat="server" Text="Revise el rango de fechas a consultar ....."
            CssClass="alert alert-danger" Visible="False"></asp:Label>
    </p>

    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    Espere un momento .....
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false"></asp:Label>
            <asp:GridView ID="gdv_Totales" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover table-responsive"
                DataSourceID="dts_ConsolidadoGeneral">
                <RowStyle BorderStyle="None" />
                <Columns>
                    <asp:BoundField DataField="PAB_nombreModalidad" HeaderText="Tipo Paciente"
                        SortExpression="PAB_nombreModalidad" />
                    <asp:BoundField DataField="001" HeaderText="08-01-001" ReadOnly="True" SortExpression="001" />
                    <asp:BoundField DataField="002" HeaderText="08-01-002" ReadOnly="True" SortExpression="002" />
                    <asp:BoundField DataField="003" HeaderText="08-01-003" ReadOnly="True" SortExpression="003" />
                    <asp:BoundField DataField="004" HeaderText="08-01-004" ReadOnly="True" SortExpression="004" />
                    <asp:BoundField DataField="005" HeaderText="08-01-005" ReadOnly="True" SortExpression="005" />
                    <asp:BoundField DataField="006" HeaderText="08-01-006" ReadOnly="True" SortExpression="006" />
                    <asp:BoundField DataField="007" HeaderText="08-01-007" ReadOnly="True" SortExpression="007" />
                    <asp:BoundField DataField="008" HeaderText="08-01-008" ReadOnly="True" SortExpression="008" />
                </Columns>
                <HeaderStyle Font-Size="Small" />
            </asp:GridView>
            <asp:SqlDataSource ID="dts_ConsolidadoGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <br />
            <asp:GridView ID="gdv_Detalle" runat="server" AutoGenerateColumns="False"  CssClass="table table-bordered table-hover table-responsive" DataSourceID="dts_BiopsiaConsolidado">
                <RowStyle BorderStyle="None" />
                <Columns>
                    <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" SortExpression="n_biopsia" ReadOnly="True" />
                    <asp:BoundField DataField="PAB_nombreModalidad" HeaderText="Tipo Paciente" SortExpression="PAB_nombreModalidad" />
                    <asp:BoundField DataField="ANA_Cod001Codificacion_Biopsia" HeaderText="08-01-001" SortExpression="ANA_Cod001Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod002Codificacion_Biopsia" HeaderText="08-01-002" SortExpression="ANA_Cod002Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod003Codificacion_Biopsia" HeaderText="08-01-003" SortExpression="ANA_Cod003Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod004Codificacion_Biopsia" HeaderText="08-01-004" SortExpression="ANA_Cod004Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod005Codificacion_Biopsia" HeaderText="08-01-005" SortExpression="ANA_Cod005Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod006Codificacion_Biopsia" HeaderText="08-01-006" SortExpression="ANA_Cod006Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod007Codificacion_Biopsia" HeaderText="08-01-007" SortExpression="ANA_Cod007Codificacion_Biopsia" />
                    <asp:BoundField DataField="ANA_Cod008Codificacion_Biopsia" HeaderText="08-01-008" SortExpression="ANA_Cod008Codificacion_Biopsia" />
                    <asp:BoundField DataField="cant_cortes" HeaderText="Cortes" SortExpression="cant_cortes" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="dts_BiopsiaConsolidado" runat="server"
                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
            <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
        </Triggers>
    </asp:UpdatePanel>
        <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
