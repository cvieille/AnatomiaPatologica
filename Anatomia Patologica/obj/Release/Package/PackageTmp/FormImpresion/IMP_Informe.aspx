﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IMP_Informe.aspx.vb"
    Inherits="Anatomia_Patologica.IMP_Informe" MasterPageFile="~/Master/IMP.master" %>

<asp:Content ContentPlaceHolderID="_HeadContent" runat="server">
</asp:Content>

<asp:Content ID="content" ContentPlaceHolderID="_ContentPlaceHolder" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <article style="margin-left: 20px; margin-right: 20px;">
                <div class="row">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 65%;">
                                <img src="../imagenes/logo.jpg" style="width: 180px; height: 80px;" /></td>
                            <td>
                                <table class="text-right">
                                    <tr>
                                        <td>N° de Biopsia:</td>
                                        <td class="text-left"><asp:Label runat="server" ID="lblNumBiopsia3"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Recepción:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblFechaRecepcion"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Fec. Informe:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblFechaInforme"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="text-center" style="margin-top: 40px;">
                        <h5><b>INFORME DE BIOPSIA N°<asp:Label runat="server" ID="lblNumBiopsia"></asp:Label></b></h5>
                        <br />
                    </div>
                </div>
                <div class="row">
                    <table style="width: 100%;">
                        <tr>
                            <th>Nombre Paciente:</th>
                            <td>
                                <asp:Label runat="server" ID="lblNombre"></asp:Label></td>
                            <th>Apellidos:</th>
                            <td><asp:Label runat="server" ID="lblApellidos"></asp:Label></td>
                        </tr>
                        <tr>
                            <th>Rut:</th>
                            <td>
                                <asp:Label runat="server" ID="lblRut"></asp:Label></td>
                            <th>Nº Ubicación Interna:</th>
                            <td>
                                <asp:Label ID="lblUbInterna" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <th>Edad:</th>
                            <td>
                                <asp:Label runat="server" ID="lblEdad"></asp:Label></td>
                            <th>Fecha Nacimiento:</th>
                            <td>
                                <asp:Label ID="lblFechaNac" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <th>Servicio Origen:</th>
                            <td>
                                <asp:Label ID="lblServOrigen" runat="server"></asp:Label>
                            </td>
                            <th>Servicio Destino:</th>
                            <td>
                                <asp:Label ID="lblServDestino" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <th>Derivado por:</th>
                            <td>
                                <asp:Label ID="lblDerivado" runat="server"></asp:Label>
                            </td>
                            <th>Muestra:</th>
                            <td>
                                <asp:Label ID="lblMuestra" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div id="descripciones">
                        <asp:Label runat="server" ID="lblAntecedentes"></asp:Label>
                        <asp:Label runat="server" ID="lblDescripcionMacroscopica"></asp:Label>
                        <asp:Label runat="server" ID="lblDescripcionMicroscopica"></asp:Label>
                        <asp:Label runat="server" ID="lblDiagnostico"></asp:Label>
                        <asp:Label runat="server" ID="lblNotas"></asp:Label>
                        <asp:Label runat="server" ID="lblInmunoHistoquimica"></asp:Label>
                    </div>
                </div>

                <div>
                    <br />
                    Responsable: Anatomopatologo
                    <asp:Label runat="server" ID="lblFirmaNombre"></asp:Label></b><br />



                    <table border="1">
                        <tr>
                            <td class="text-center">El presente Informe deberá contar con firma del Médico Patálogo Responsable y 
                    timbre de Anatomía Patológica para su validez</td>
                        </tr>
                    </table>
                    <label class="text-center" style="border-bottom: thick"></label>
                </div>
            </article>


            <footer>
                <div class="text-center">
                    <div>
                        Datos de Impresión: Nº de Biopsia:
                        <asp:Label runat="server" ID="lblNumBiopsia2"></asp:Label>
                        - Fecha:
                        <asp:Label runat="server" ID="lblFechaHoy"></asp:Label>
                        - Usuario: <asp:Label runat="server" ID="lblUsuario"></asp:Label>
                        
                    </div>
                </div>
            </footer>
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>
