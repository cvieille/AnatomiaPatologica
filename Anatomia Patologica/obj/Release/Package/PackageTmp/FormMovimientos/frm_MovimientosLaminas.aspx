﻿<%@ Page Title="Movimientos Laminas" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MovimientosLaminas.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosLaminas" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:Panel ID="panel_laminas" runat="server" Style="text-align: center" Width="100%">
        <!-- Panel Principal -->
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <h2 class="text-center">Movimiento de Laminas</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-inline">
                        <label>Patologo:</label>
                        <asp:DropDownList ID="cmb_patologo" runat="server" CssClass="form-control" AutoPostBack="True" >
                        </asp:DropDownList>
                        
                    </div>
                </div>
                <br />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
                    <ContentTemplate>
                        <asp:SqlDataSource ID="dts_laminas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                        <asp:GridView ID="gdv_laminas" runat="server" AllowPaging="True" AllowSorting="True" CssClass="table table-bordered table-hover table-responsive"
                            AutoGenerateColumns="False" DataSourceID="dts_laminas" PageSize="40">
                            <Columns>
                                <asp:TemplateField HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccion" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ANA_nomLamina" HeaderText="Lamina" SortExpression="ANA_nomLamina" />
                                <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fec. Lamina" SortExpression="ANA_FecLamina" />
                                <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" SortExpression="ANA_EstLamina" />
                                <asp:BoundField DataField="nombre_patologo" HeaderText="Patologo" SortExpression="nombre_patologo" />
                                <asp:BoundField DataField="nombre_tecnologo" HeaderText="Tecnólogo" SortExpression="nombre_tecnologo" />
                                <asp:BoundField DataField="ANA_idLamina" HeaderText="Id. Lamina" InsertVisible="False"
                                    ReadOnly="True" SortExpression="ANA_idLamina" />
                            </Columns>
                        </asp:GridView>

                        <div style="text-align: center">
                            <asp:Button ID="cmd_sel_all3" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                            <asp:Button ID="cmd_sel_all4" runat="server" CssClass="btn btn-default" Text="Quitar Selección" />
                        </div>

                        <div class="bloque" style="width: 90%; text-align: center; margin: 0 auto;">
                            <asp:Button ID="cmd_entregar_medico" runat="server" CssClass="btn btn-primary" Text="Entregar a Patologo"
                                OnClientClick="return confirm('¿Esta seguro que desea Enviar a Patologo estas Laminas?');" />

                            <asp:Button ID="cmd_imprimir0" runat="server" CssClass="btn btn-info" Text="Imprimir Selección" />
                        </div>

                        <div style="text-align: center">
                            <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-warning" role="alert">
                            </asp:Label>
                            <asp:Image ID="Image1" runat="server" Height="105px" ImageUrl="~/imagenes/3dman-stop.jpg" Visible="False" Width="132px" />
                            <asp:Label ID="lbl_debe_guardar" runat="server" Text="Label" Visible="False" class="alert alert-danger">
                            </asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>
    <br />

</asp:Content>
