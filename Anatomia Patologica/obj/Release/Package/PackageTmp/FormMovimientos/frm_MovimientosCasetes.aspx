﻿<%@ Page Title="Movimientos Casete" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MovimientosCasetes.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosCasetes" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_movimientoCasete").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_movimientoCasete").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="upd_casetes" runat="server">
        <ContentTemplate>
            <!-- Panel Principal -->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Movimiento de Casete</h2>
                </div>
                <div class="panel-body">
                    <div style="padding: 10px">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-inline">
                                    <label>Estado a buscar:</label>
                                    <asp:DropDownList ID="cmb_TipoBusqueda" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-inline">
                                    <label>Patologo:</label>
                                    <asp:DropDownList ID="cmb_patologo" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <asp:Button ID="cmd_buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
                                <asp:Button ID="cmd_exportar" runat="server" Text="Exportar" CssClass="btn btn-success" />
                            </div>
                        </div>
                        <hr />
                    </div>

                    <asp:GridView ID="gdv_movimientoCasete" runat="server"
                        AutoGenerateColumns="False" DataKeyNames="ANA_IdCortes_Muestras" DataSourceID="dts_ListaCortes"
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccion" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                                DataTextField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" Text="Nº Corte" />
                            <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" SortExpression="ANA_NomCortes_Muestras" ReadOnly="True" />
                            <asp:BoundField DataField="ANA_FecCortes_Muestras" HeaderText="Fecha" SortExpression="ANA_FecCortes_Muestras" ReadOnly="True"
                                DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField DataField="NombrePatologo" HeaderText="Patologo" SortExpression="NombrePatologo" ReadOnly="True" />
                            <asp:BoundField DataField="GEN_loginUsuariosCrea" HeaderText="Creado por" SortExpression="GEN_loginUsuariosCrea" ReadOnly="True" />
                            <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" SortExpression="GEN_nombreTipo_Estados_Sistemas" ReadOnly="True" />
                            <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="ANA_IdCortes_Muestras" ReadOnly="True" />
                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia" ReadOnly="true" />
                            <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="GEN_idTipo_Estados_Sistemas" ReadOnly="true" />
                            <asp:BoundField  HeaderText="Chequeo N°1" ReadOnly="true" />
                            <asp:BoundField  HeaderText="Chequeo N°2" ReadOnly="true" />
                        </Columns>
                    </asp:GridView>
                    <div class="text-center">
                        <asp:Button ID="cmd_sel_all" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                        <asp:Button ID="cmd_sel_all0" runat="server" CssClass="btn btn-default" Text="Quitar Selección" />
                    </div>
                    <div style="text-align: center">
                        <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" class="alert alert-warning" role="alert">
                        </asp:Label>
                        <asp:Image ID="Image1" runat="server" Height="105px" ImageUrl="~/imagenes/3dman-stop.jpg" Visible="False" Width="132px" />
                        <asp:Label ID="lbl_debe_guardar" runat="server" Text="Label" Visible="False" class="alert alert-danger">
                        </asp:Label>
                    </div>

                    <asp:SqlDataSource ID="dts_ListaCortes" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>



                    <div class="alert alert-info">
                        <asp:Button ID="cmd_EnviarProcesador" runat="server" Text="Enviar a Procesador" CssClass="btn btn-primary" Visible="false" />
                        <asp:Button ID="cmd_recibir" runat="server" Text="Recibir de Procesador" CssClass="btn btn-primary" Visible="false" />
                        <asp:Button ID="cmd_tincion" runat="server" Text="Tinción y Montaje" CssClass="btn btn-primary" Visible="false" />
                        <asp:Button ID="cmd_enviar_almacenamiento" runat="server" Text="Enviar a Almacenamiento" CssClass="btn btn-primary" Visible="false" />
                        <asp:Button ID="cmd_imprimir" runat="server" Text="Imprimir Selección" CssClass="btn btn-info" />
                    </div>

                </div>
            </div>
            <wuc:Alert ID="wuc_alert" runat="server" />
        </ContentTemplate>
        <Triggers  >
            <asp:PostBackTrigger ControlID="cmd_exportar" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>

