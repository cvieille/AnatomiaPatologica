<%@ Page Title="Movimientos Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MovimientosBiopsias.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosBiopsias" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_secretaria").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_secretaria").find("tr:first"))).dataTable();

        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Biopsias para Despachar</h2>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gdv_secretaria" runat="server" 
                        AutoGenerateColumns="False" DataSourceID="dts_BiopsiasDespachar" 
                        CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccion" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormSolicitudes/frm_recepcion.aspx?id={0}"
                                DataTextField="n_biopsia" FooterText="N� Biopsia" HeaderText="N� Biopsia" />
                            <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" ReadOnly="True" SortExpression="ANA_OrganoBiopsia" />
                            <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Recepci�n"
                                ReadOnly="True" SortExpression="ANA_fec_RecepcionRegistro_Biopsias" />
                            <asp:BoundField DataField="ANA_GesRegistro_Biopsias" HeaderText="GES" ReadOnly="True" SortExpression="ANA_GesRegistro_Biopsias" />
                            <asp:BoundField DataField="serv_origen" HeaderText="Solic." ReadOnly="True" SortExpression="serv_origen" />
                            <asp:BoundField DataField="serv_destino" HeaderText="Destino" ReadOnly="True" SortExpression="serv_destino" />
                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="Id.Biopsia" ReadOnly="True" SortExpression="ANA_IdBiopsia" />
                        </Columns>
                    </asp:GridView>
                    <br />
                    <table class="table alert-info">
                        <tr>
                            <th>Entregar Informe a:</th>
                            <td>
                                <asp:TextBox ID="txt_recibe_informe" runat="server" Width="296px">
                                </asp:TextBox>
                                <span style="color: Red">*</span>
                            </td>
                            <td>
                                <asp:Label ID="lbl_despachar1" runat="server" Font-Bold="true" Text="Fecha de Entrega:">
                                </asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_fecha" runat="server" TabIndex="4" type="date">
                                </asp:TextBox>

                                <span style="color: Red">*</span>
                            </td>
                            <td>
                                <asp:Button ID="cmd_entregar" runat="server" CssClass="btn btn-success" Text="Despachar"
                                    OnClientClick="return confirm('�Esta seguro que desea despachar esta(s) biopsia(s)?');" />

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>

    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Biopsia Para Interconsulta</h2>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="gdv_b_interconsulta" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dts_interconsultas" CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccion0" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia" DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                                DataTextField="n_biopsia" HeaderText="N� Biopsia" Text="N� Biopsia" />
                            <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" SortExpression="ANA_OrganoBiopsia" />
                            <asp:BoundField DataField="nombre_tecnologo" HeaderText="Tecn�logo" SortExpression="nombre_tecnologo" />
                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="Id." SortExpression="ANA_IdBiopsia" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="dts_interconsultas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <table class="table alert-info">
                        <tr>
                            <th>Entregar Informe a:</th>
                            <td>
                                <asp:TextBox ID="txt_recibe_informe0" runat="server" Width="296px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lbl_despachar2" runat="server" Text="Fecha de Entrega:" Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_fecha0" runat="server" TabIndex="4" type="date">
                                </asp:TextBox>

                            <td>
                                <asp:Button ID="cmd_entregar0" runat="server" CssClass="btn btn-success" Text="Despachar"
                                    OnClientClick="return confirm('�Esta seguro que desea despachar esta interconsulta?');" />
                            </td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="dts_BiopsiasDespachar" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <asp:Label ID="lbl_mensaje" runat="server" class="alert alert-warning" role="alert" Text="Label" Visible="False">
                    </asp:Label>
                    <br />
                </div>
            </div>
        </ContentTemplate>

    </asp:UpdatePanel>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
