﻿<%@ Page Title="Cortes Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" MaintainScrollPositionOnPostback="true" CodeBehind="frm_Cortes.aspx.vb" Inherits="Anatomia_Patologica.frm_Cortes" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Cortes/ListadodeTecnicas.ascx" TagPrefix="wuc" TagName="ListadodeTecnicas" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../script/formAnatomia/frm_cortes.js?1"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <!-- Panel Principal Gestión de Cortes por Biopsia-->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Gestión de Cortes por Biopsia</h2>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Nº de Biopsia:</label>
                            <asp:TextBox ID="txt_biopsia_n" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Organo:</label>
                            <asp:TextBox ID="txt_organo" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <label>Estado:</label>
                            <asp:TextBox ID="txt_estado" runat="server" Enabled="False" CssClass="form-control" />
                        </div>
                        <div class="col-md-2">
                            <br />
                            <asp:Button ID="cmd_VerMovimientos" runat="server" CssClass="btn btn-info"
                                Text="Ver Movimientos" />
                        </div>
                        <div class="col-md-2">
                            <br />
                            <asp:Label ID="lbl_estado_macro" CssClass="label label-info" runat="server"></asp:Label>

                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-2">
                            <label>Patologo:</label>
                            <asp:DropDownList ID="cmb_patologo" runat="server" AppendDataBoundItems="True"
                                Enabled="False" CssClass="form-control" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <label>Tecnólogo:</label>
                            <asp:DropDownList ID="cmb_tecnologo" runat="server" AppendDataBoundItems="True"
                                Enabled="False" CssClass="form-control" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-3">
                            <br />
                            <asp:CheckBox ID="chk_dictado" runat="server" AutoPostBack="True" Enabled="False"
                                Text="Dictado" />
                            <asp:CheckBox ID="chk_tumoral" runat="server" AutoPostBack="True" Enabled="False"
                                Text="Tumoral" />
                            <asp:CheckBox ID="chk_rapida" runat="server" AutoPostBack="True" Enabled="False"
                                Text="Rapida" />
                        </div>
                        <div class="col-md-2">
                            <label>Corte:</label>
                            <asp:DropDownList ID="cmb_tipo_corte" runat="server" AppendDataBoundItems="true"
                                Enabled="False" AutoPostBack="True">
                                <asp:ListItem>--Seleccione--</asp:ListItem>
                                <asp:ListItem>Parcial</asp:ListItem>
                                <asp:ListItem>Total</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <br />
                            <asp:Button ID="cmd_GuardarMacro" runat="server" CssClass="btn btn-success" Enabled="False"
                                Text="Guardar Macro" />
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-2">
                            <label>Secuencia Cortes:</label>
                            <div class="form-inline">
                                <asp:DropDownList ID="cmb_cant_cortes" runat="server" CssClass="form-control"
                                    Enabled="False">
                                </asp:DropDownList>
                                <asp:LinkButton ID="lkb_SecuenciaCortes" runat="server" Style="color: white;"
                                    CssClass="btn btn-success" Enabled="false"><span class="glyphicon glyphicon-plus"
                                        aria-hidden="true"></span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Rotulo:</label>
                            <div class="form-inline">
                                <asp:TextBox ID="txt_sufijo" runat="server" MaxLength="6" CssClass="form-control"
                                    Enabled="False" Width="70%">
                                </asp:TextBox>
                                <asp:LinkButton ID="lkb_RotuloCorte" runat="server" Style="color: white;"
                                    CssClass="btn btn-success" Enabled="false">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Laminas:</label>
                            <div class="form-inline">
                                <asp:DropDownList ID="cmb_cant_laminas" runat="server" Enabled="False"
                                    CssClass="form-control">
                                </asp:DropDownList>
                                <asp:LinkButton ID="lkb_SecuenciaLaminas" runat="server" Style="color: white;"
                                    CssClass="btn btn-success" Enabled="false">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Rotulo Laminas:</label>
                            <div class="form-inline">
                                <asp:TextBox ID="txt_sufijo_laminas" runat="server" Enabled="False" MaxLength="6"
                                    CssClass="form-control" Width="70%">
                                </asp:TextBox>
                                <asp:LinkButton ID="lkb_RotuloLamina" runat="server" Style="color: white;"
                                    CssClass="btn btn-success" Enabled="false">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <br />
                            <asp:LinkButton ID="lkb_Solicitar_Macro" runat="server" Style="color: white;"
                                CssClass="btn btn-warning" Enabled="false">
                                <span class="glyphicon glyphicon-repeat" aria-hidden="true"> Solicitar Macro.</span>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <asp:Panel ID="pnl_DetalleMacro" runat="server">
                            <div class="panel with-nav-tabs panel-info">
                                <div class="panel-heading clearfix">
                                    <ul class="nav nav-tabs" runat="server" id="tabFrm">
                                        <li id="li_Macroscopia" runat="server" class="active">
                                            <a href="#tabMacroscopia" data-toggle="tab">
                                                <label style="color: black">Detalle Macroscopia</label>
                                            </a>
                                        </li>
                                        <li id="li_Recepcion" runat="server">
                                            <a href="#tabRecepcion" data-toggle="tab">
                                                <label style="color: black">Detalle de Muestra</label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tabMacroscopia">
                                            <!-- Grilla de Tecnicas Especiales-->
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <th>Plantilla:
                                                    </th>
                                                    <td>
                                                        <asp:DropDownList ID="cmb_nom_plantilla" runat="server"
                                                            DataTextField="nombre" CssClass="form-control"
                                                            DataValueField="id" Enabled="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Descripción Macroscopica:
                                                    </th>
                                                    <td>
                                                        <asp:TextBox ID="txt_desc_macro" runat="server"
                                                            TextMode="MultiLine" Rows="10" CssClass="form-control"
                                                            Enabled="False"></asp:TextBox>
                                                        <asp:Button ID="cmd_plantilla_ac" runat="server"
                                                            CssClass="btn btn-success" Text="Cargar Plantilla"
                                                            Visible="False" />
                                                        <asp:Button ID="cmd_GuardarDetalleMacro" runat="server"
                                                            CssClass="btn btn-primary" Text="Guardar D. Macroscop."
                                                            Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tabRecepcion">
                                            <!-- Grilla de Inmunos-->
                                            <asp:Panel ID="pnl_MuestrasBiopsias" runat="server">
                                                <table id="tbl_MuestrasBiopsias" class="table table-bordered table-sm table-hover table-responsive" style="width: 100%;"></table>


                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <%-- <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="chk_dictado" />
                    <asp:AsyncPostBackTrigger ControlID="chk_tumoral" />                  
                    <asp:AsyncPostBackTrigger ControlID="lkb_SecuenciaCortes" />
                    <asp:AsyncPostBackTrigger ControlID="lkb_RotuloCorte" />
                    <asp:AsyncPostBackTrigger ControlID="lkb_SecuenciaLaminas" />
                </Triggers>--%>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="upd_detalle" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="text-align: center;">
                        <asp:Image ID="Image1" runat="server" Height="105px" ImageUrl="~/imagenes/3dman-stop.jpg"
                            Visible="False" Width="132px" />
                        <br />
                        <asp:Label ID="lbl_debe_guardar" runat="server" Text="Label" Visible="False"
                            CssClass="alert alert-warning" role="alert" Width="80%">
                        </asp:Label>
                        <asp:TextBox ID="txt_id" runat="server" Visible="False" Width="51px">
                        </asp:TextBox>
                        <!-- Panel Principal Listado de Casete-->
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <h2 class="text-center">Listado de Casete</h2>
                            </div>
                            <div class="panel-body">
                                <asp:GridView ID="gdv_cortes" runat="server" AutoGenerateColumns="False"
                                    DataKeyNames="GEN_loginUsuarios" DataSourceID="dts_cortes" EnableTheming="True"
                                    HorizontalAlign="Center" RowHeaderColumn="id"
                                    CssClass="table table-bordered table-hover table-responsive">
                                    <EmptyDataTemplate>
                                        <div class="alert alert-warning" role="alert">
                                            ¡No existen casetes para esta muestra!
                                        </div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sel.">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSeleccion" runat="server"
                                                    CssClass="controlSeleccion" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia"
                                            ReadOnly="True" Visible="False" />
                                        <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Corte"
                                            ReadOnly="True" SortExpression="ANA_NomCortes_Muestras" />
                                        <asp:BoundField DataField="ANA_FecCortes_Muestras"
                                            DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha" ReadOnly="True"
                                            SortExpression="ANA_FecCortes_Muestras" />
                                        <asp:BoundField DataField="nombre_patologo" HeaderText="Patólogo"
                                            ReadOnly="True" SortExpression="nombre_patologo" />
                                        <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Creado por"
                                            ReadOnly="True" SortExpression="GEN_loginUsuarios" />
                                        <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado"
                                            ReadOnly="True" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                                        <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="Id. Casete"
                                            ReadOnly="True" SortExpression="ANA_IdCortes_Muestras" />
                                    </Columns>
                                </asp:GridView>
                                <div style="text-align: center">
                                    <asp:Button ID="cmd_seleccionarcortes" runat="server" CssClass="btn btn-default"
                                        Text="Seleccionar Todo" />
                                    <asp:Button ID="cmd_deseleccionarcortes" runat="server"
                                        CssClass="btn btn-defaultbtn btn-default" Text="Quitar Selección" />
                                </div>
                                <div class="alert alert-info text-center" style="padding: 5px">
                                    <table style="margin: 0 auto;">
                                        <tr>
                                            <td>
                                                <asp:Button ID="cmd_enviar_procesador" runat="server"
                                                    CssClass="btn btn-success" Enabled="False"
                                                    Text="Listo para Procesador" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_EliminarCortes" runat="server"
                                                    CssClass="btn btn-danger"
                                                    OnClientClick="return confirm('¿Esta seguro que desea eliminar este(os) casete (s)?');"
                                                    Enabled="False" Text="Eliminar Selección" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_especiales" runat="server"
                                                    CssClass="btn btn-success" Enabled="False"
                                                    Text="Solicitar Especial" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_inmuno" runat="server" CssClass="btn btn-success"
                                                    Enabled="False" Text="Solicitar Inmuno" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_descal" runat="server" CssClass="btn btn-warning"
                                                    Enabled="False" Text="Enviar a Descalcificar" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="cmd_volver_inicio" runat="server"
                                                    CssClass="btn btn-warning" Enabled="False"
                                                    Text="Volver a Est. Encasetada" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_solicitar_casete" runat="server"
                                                    CssClass="btn btn-success" Enabled="False"
                                                    OnClientClick="return confirm('¿Esta seguro que desea solicitar este (os) Casete (s)?');"
                                                    Text="Solicitar Casete" />
                                            </td>
                                            <td>
                                                <asp:Button ID="cmd_para_interconsulta" runat="server"
                                                    CssClass="btn btn-warning" Enabled="False"
                                                    OnClientClick="return confirm('¿Esta seguro que desea enviar este (os) casete (s) a Interconsulta?');"
                                                    Text="Para Interconsulta" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- BLOQUE PARA SOLICITAR TECNICAS -->
                                <asp:Panel ID="pnl_tecnica" runat="server" CssClass="alert alert-info  text-center"
                                    Visible="false">
                                    <asp:DropDownList ID="cmb_tecnicas_especiales" runat="server" Width="380px">
                                    </asp:DropDownList>
                                    <asp:Button ID="cmd_agregar_especiales" runat="server" CssClass="btn btn-success"
                                        Text="Agregar Técnica Especial" />
                                </asp:Panel>

                                <!-- BLOQUE PARA SOLICITAR INMUNO HISTOQUIMICA -->
                                <asp:Panel ID="pnl_inmuno" runat="server" CssClass="alert alert-info"
                                    HorizontalAlign="Center" Visible="false">
                                    <asp:DropDownList ID="cmb_inmuno_histoquimica" runat="server" Width="380px">
                                    </asp:DropDownList>
                                    <asp:Button ID="cmd_agregar_inmuno" runat="server" CssClass="btn btn-success"
                                        Text="Agregar Inmunohistoquímica" />
                                </asp:Panel>
                                <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False"
                                    CssClass="alert alert-danger" role="alert">
                                </asp:Label>
                            </div>
                        </div>
                    </div>

                    <!-- Panel Principal de Tecnicas Especiales-->
                    <div class="panel panel-default">
                        <!-- Default panel contents Listado de Técnicas Especiales-->
                        <div class="panel-heading">
                            <h2 class="text-center">Listado de Técnicas Especiales</h2>
                        </div>
                        <div class="panel-body">
                            <asp:GridView ID="gdv_tecnicas" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="ANA_UsuCrea" DataSourceID="dts_tecnicas" HorizontalAlign="Center"
                                CssClass="table table-bordered table-hover table-responsive">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" role="alert">
                                        ¡No existen técnicas para esta muestra!
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="ANA_FecTecnica" DataFormatString="{0:dd-MM-yyyy}"
                                        HeaderText="Fecha Técnica" SortExpression="ANA_FecTecnica" />
                                    <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte"
                                        SortExpression="ANA_NomCortes_Muestras" />
                                    <asp:BoundField DataField="ANA_NomTecnica" HeaderText="Técnica"
                                        SortExpression="ANA_NomTecnica" />
                                    <asp:BoundField DataField="ANA_EstTecnica" HeaderText="Estado"
                                        SortExpression="ANA_EstTecnica" />
                                    <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True"
                                        SortExpression="ANA_UsuCrea" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Acción
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkb_EliminarTecnica" runat="server"
                                                CssClass="btn btn-danger" Visible="false"
                                                OnClick="lkb_EliminarTecnica_Click"
                                                OnClientClick="return confirm('¿Esta seguro que desea Eliminar esta técnica?');"
                                                CommandArgument='<%# Eval("ANA_IdTecnicaBiopsia") %>'><span
                                                    class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ANA_IdTecnicaBiopsia" HeaderText="Id. Técnica"
                                        SortExpression="ANA_IdTecnicaBiopsia" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:SqlDataSource ID="dts_tecnicas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <wuc:Alert ID="wuc_alertTecnicas" runat="server" />

                    <!-- Panel Principal de Inmunohistoquímica-->
                    <div class="panel panel-default">
                        <!-- Default panel contents Listado de Inmunohistoquímica-->
                        <div class="panel-heading">
                            <h2 class="text-center">Listado de Inmunohistoquímica </h2>
                        </div>
                        <div class="panel-body">
                            <asp:GridView ID="gdv_InmunoHistoquimica" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="ANA_UsuCrea" DataSourceID="dts_InmunoHistoquimica"
                                HorizontalAlign="Center" CssClass="table table-bordered table-hover table-responsive">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" role="alert">
                                        ¡No existen InmunoHistoquimicas para esta muestra!
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="ANA_FecInmuno_Histoquimica_Biopsia"
                                        DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha Inmuno"
                                        SortExpression="ANA_FecInmuno_Histoquimica_Biopsia" />
                                    <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte"
                                        SortExpression="ANA_NomCortes_Muestras" />
                                    <asp:BoundField DataField="ANA_NomInmuno" HeaderText="Inmunohistoquímicas"
                                        SortExpression="ANA_NomInmuno" />
                                    <asp:BoundField DataField="ANA_EstInmuno_Histoquimica_Biopsia" HeaderText="Estado"
                                        ReadOnly="true" />
                                    <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True"
                                        SortExpression="ANA_UsuCrea" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Acción
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkb_EliminarInmuno" runat="server"
                                                CssClass="btn btn-danger" Visible="false"
                                                OnClick="lkb_EliminarInmuno_Click"
                                                OnClientClick="return confirm('¿Esta seguro que desea Eliminar esta Inmuno?');"
                                                CommandArgument='<%# Eval("ANA_IdInmuno_Histoquimica_Biopsia") %>'><span
                                                    class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ANA_IdInmuno_Histoquimica_Biopsia"
                                        HeaderText="Id. Inmuno" ReadOnly="true" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="dts_InmunoHistoquimica" runat="server"
                                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

                        </div>
                    </div>


                    <!-- Panel Principal de Laminas-->
                    <div class="panel panel-default">
                        <!-- Default panel contents Listado de Laminas-->
                        <div class="panel-heading">
                            <h2 class="text-center">Listado de Laminas</h2>
                        </div>
                        <div class="panel-body">

                            <asp:GridView ID="gdv_laminas" runat="server" AutoGenerateColumns="False"
                                HorizontalAlign="Center" DataSourceID="dts_laminas"
                                CssClass="table table-bordered table-hover table-responsive">
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning" role="alert">
                                        ¡No existen laminas para esta muestra!
                                    </div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel.">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccion" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ANA_NomLamina" HeaderText="Lamina"
                                        SortExpression="ANA_NomLamina" />
                                    <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha Lamina"
                                        SortExpression="ANA_FecLamina" />
                                    <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado"
                                        SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                                    <asp:BoundField DataField="ANA_IdLamina" HeaderText="Id. Lamina"
                                        SortExpression="ANA_IdLamina" />
                                    <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas"
                                        HeaderText="GEN_idTipo_Estados_Sistemas"
                                        SortExpression="GEN_idTipo_Estados_Sistemas" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="cmd_EliminarLamina" runat="server" Text="Eliminar"
                                                CssClass="btn btn-danger"
                                                OnClientClick="return confirm('¿Esta seguro que desea eliminar esta Lamina?');"
                                                Enabled="false" CommandArgument='<%# Eval("ANA_idLamina") %>'
                                                OnClick="cmd_EliminarLamina_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div class="text-center">
                                <asp:Button ID="cmd_select_all3" runat="server" CssClass="btn btn-default"
                                    Text="Seleccionar Todo" />
                                <asp:Button ID="cmd_select_all4" runat="server" CssClass="btn btn-default"
                                    Text="Quitar Selección" />
                            </div>
                            <br />

                            <div class="alert alert-info text-center" style="padding: 5px">
                                <asp:Button ID="cmd_para_interconsulta1" runat="server" CssClass="btn btn-danger"
                                    Enabled="False" Text="Para Interconsulta" />
                                <asp:Button ID="cmd_imprime_laminas" runat="server" CssClass="btn btn-success"
                                    Text="Imprimir Selección" Visible="False" />
                                <asp:Button ID="cmd_solicitar_casete0" runat="server" CssClass="btn btn-warning"
                                    Enabled="False" Text="Solicitar Lamina" />
                            </div>
                            <asp:Label ID="Lbl_AlertLamina" runat="server" Text="Alert" Visible="False"
                                CssClass="alert alert-danger" role="alert" Width="80%">
                            </asp:Label>
                        </div>
                    </div>

                    <asp:SqlDataSource ID="dts_cortes" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dts_laminas" runat="server"
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>


                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="lkb_RotuloCorte" />
                    <asp:AsyncPostBackTrigger ControlID="lkb_SecuenciaLaminas" />
                    <asp:AsyncPostBackTrigger ControlID="lkb_SecuenciaCortes" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <wuc:Alert ID="wuc_alert" runat="server" />

</asp:Content>
