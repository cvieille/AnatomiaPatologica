﻿<%@ Page Title="Notas Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomiaBlanco.Master" CodeBehind="frm_NotasBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_NotasBiopsia" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        	<!-- Panel Principal -->
	        <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">Notas de Biopsia</div>
                <div class="panel-body">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <table style="width:70%; margin:0 auto;">
                        <tr>
                            <td>Nº de Biopsia:</td>
                            <td>
                                <div style="float:left; vertical-align:middle;">
                                    <asp:TextBox ID="txt_biopsia_n" runat="server" Enabled="False" style="width:51px">
                                    </asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Text="  /  "></asp:Label>
                                    <asp:TextBox ID="txt_biopsia_a" runat="server" Enabled="False" style="width:51px">
                                    </asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Organo:</td>
                            <td>
                                <asp:TextBox ID="txt_organo" runat="server" Width="438px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Nota:</td>
                            <td>
                                <asp:TextBox ID="txt_nota" runat="server" Height="111px" TextMode="MultiLine" MaxLength="300" Width="435px">
                                </asp:TextBox>
                                <asp:TextBox ID="txt_id" runat="server" Visible="False" Width="51px">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Button ID="cmd_guardar" runat="server" CssClass="btn btn-success" Text="Guardar" />                              
                                <input class="btn btn-warning" name="button" onclick="window.close();" type="button" value="Salir" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:GridView ID="gdv_notas" runat="server" AllowSorting="True" 
                                    AutoGenerateColumns="False" CellPadding="4" DataSourceID="dst_notas" 
                                    ForeColor="#333333" GridLines="None" Height="16px" Width="857px">
                                    <RowStyle BackColor="#EFF3FB" />
                                    <Columns>
                                        <asp:BoundField DataField="ANA_fechaNota" HeaderText="Fecha" SortExpression="ANA_fechaNota" />
                                        <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Login" SortExpression="GEN_loginUsuarios" />
                                        <asp:TemplateField HeaderText="nota">
                                            <ItemTemplate>
                                                <asp:TextBox ID="textNotes" runat="server" BorderStyle="None" Height="50px" ReadOnly="true" 
                                                Text='<%# Bind("ANA_detallenota") %>' TextMode="MultiLine" Width="400px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ANA_IdNota" HeaderText="Id" InsertVisible="False" SortExpression="ANA_IdNota" />
                                    </Columns>
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                <br />
                                &nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
                    <asp:SqlDataSource ID="dst_notas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" >
                    </asp:SqlDataSource>
                    <br />
                    <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False"></asp:Label>
	            </div>
	        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
