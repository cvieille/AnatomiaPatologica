<%@ Page Title="Informe de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master"
    MaintainScrollPositionOnPostback="true" CodeBehind="frm_InformeBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_InformeBiopsia" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $('[data-toggle="tooltip"]').tooltip();
        };        
    </script>

    <script type="text/html" id="files-template">
        <li class="liElemento">
            <div class="media-body mb-1">
                <p class="mb-2" style="text-align: left; margin-left: 20px;">
                    <div class="pull-left nombreArchivo" style="margin-left:5px;">
                        <label id="date-file" style="display:none;">9999-99-99 - </label>
                        <strong>%%filename%%</strong><label id="progress-status" style="display:none;"> - Estado: <span class="text-muted">Esperando</span></label>
                     </div>
                    <button type="button" class="btnQuitar btn btn-danger btn-sm pull-right" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Quitar
                    </button>
                </p>
                <div id="progress-bar-entire" style="display:none;">
                    <div class="progress mb-2">
                    <div id="progress-bar" class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
                        role="progressbar"
                        style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </div>
                <hr class="mt-1 mb-1" />
            </div>
        </li>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Informe de Biopsia</h2>
        </div>
        <!-- Panel Principal -->
        <div class="panel-body">

            <!-- SECCI�N DATOS DE PACIENTE -->
            <asp:UpdatePanel ID="upp_DatosBiopsia" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-2">
                            <label>N�</label>
                            <asp:TextBox ID="txt_id" runat="server" Width="51px" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txt_numero" runat="server" CssClass="form-control" ReadOnly="True" Enabled="False">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>Fec. Recepci�n</label>
                            <asp:TextBox ID="txt_recepcion" runat="server" CssClass="form-control" Enabled="false">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Solicitado Por:</label>
                            <asp:TextBox ID="txt_solicitado_por" runat="server" CssClass="form-control" Enabled="False">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2" >
                            <br />
                            <asp:Button ID="cmd_VerMovimientos" runat="server" CssClass="btn btn-info"
                                Text="Ver Movimientos" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label>N� Ubic. Int.</label>
                            <asp:TextBox ID="txt_ficha" runat="server" Enabled="false" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>Rut Paciente:</label>
                            <asp:TextBox ID="txt_rut" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Paciente:</label>
                            <asp:TextBox ID="txt_nombre" runat="server" Enabled="false" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Edad:</label>
                            <asp:TextBox ID="txt_edad" runat="server" Enabled="false" CssClass="form-control">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_fnac" runat="server" Enabled="false" Visible="False" Width="50px">
                            </asp:TextBox>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Servicio Origen:</label>
                            <asp:TextBox ID="txt_servicio" runat="server" CssClass="form-control" ReadOnly="True" Enabled="False">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <label>Servicio Destino:</label>
                            <asp:TextBox ID="txt_servicio0" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-8">
                            <label>Organo:</label>
                            <asp:TextBox ID="txt_organo" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                    </div>
               
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-2">
                            <label>Fecha Informe:</label>
                            <asp:TextBox ID="txt_finf" runat="server" Enabled="false" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Validado Por:</label>
                            <asp:TextBox ID="txt_quien_valida" runat="server" Enabled="False" CssClass="form-control">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <label>Estado:</label>
                            <asp:TextBox ID="txt_estado" runat="server" CssClass="form-control" Enabled="False">
                            </asp:TextBox>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
            <hr />
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading clearfix">
                    <ul class="nav nav-tabs" runat="server" id="tabFrm">
                        <li id="li_Detalle" runat="server" class="active">
                            <a href="#tabDetalle" data-toggle="tab">
                                <label>Detalle de Informe</label>
                            </a>
                        </li>
                        <li id="li_Resumen" runat="server">
                            <a href="#tabResumen" data-toggle="tab">
                                <label>Resumen de Biopsia</label>
                            </a>
                        </li>
                        <li id="li_Critico" runat="server">
                            <a href="#tabCritico" data-toggle="tab">
                                <label>Critico</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_totalCritico" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                        <li id="li_Neoplasia" runat="server">
                            <a href="#tabNeoplasia" data-toggle="tab">
                                <label>Neoplasia</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_Neoplasia" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                        <li id="li_Adjuntos" runat="server">
                            <a href="#tabAdjuntos" data-toggle="tab">
                                <label>Adjuntos</label>
                                <asp:UpdatePanel RenderMode="Inline" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_Adjuntos" runat="server" CssClass="badge" Text="0" BackColor="#f0ad4e" ForeColor="White"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tabDetalle">
                            <!-- Grilla de Tecnicas Especiales-->
                            <!-- SECCI�N DATOS EXAMEN -->
                            <asp:UpdatePanel ID="upp_general" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <hr />
                                        <div class="col-md-6">
                                            <label>Plantilla:</label>
                                            <asp:DropDownList ID="cmb_nom_plantilla" runat="server" Enabled="false" CssClass="form-control" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <br />
                                            <asp:CheckBox ID="chk_c_macro" runat="server" Enabled="false" AutoPostBack="True" Text="C/Macro" />
                                            <asp:CheckBox ID="chk_c_micro" runat="server" Enabled="false" AutoPostBack="True" Text="C/Micro" />
                                            <asp:CheckBox ID="chk_c_diag" runat="server" Enabled="false" AutoPostBack="True" Text="C/Diag." Checked="true" />
                                        </div>

                                    </div>
                                    <hr />
                                    <div class="row">
                                        <!-- Panel Principal Antecedentes Clinicos-->
                                        <div class="panel panel-primary">
                                            <asp:TextBox ID="txt_id_descripcion" runat="server" Width="44px" Visible="False"></asp:TextBox>
                                            <!-- Panel Principal Antecedentes Clinicos-->
                                            <div class="panel-heading">Antecedentes Clinicos</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_antecedentes_clinicos" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_ac" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>

                                            <!-- Panel Principal Macroscopia-->
                                            <div class="panel-heading">Descripci�n Macroscopica:</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_desc_macro" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_d_ma" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>

                                            <!-- Panel Principal Microscopia-->
                                            <div class="panel-heading">Examen Microsc�pico</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_desc_micro" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_d_mi" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>


                                            <!-- Panel Principal Diagnostico-->
                                            <div class="panel-heading">Diagnostico</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_desc_diag" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_d_di" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>

                                            <!-- Panel Principal Inmunohistoquimica-->
                                            <div class="panel-heading">Estudio Inmunohistoqu�mico</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_estu_inmuno" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_estudio" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>

                                            <!-- Panel Principal Nota-->
                                            <div class="panel-heading">Nota Adicional (opcional)</div>
                                            <div class="panel-body">
                                                <asp:TextBox ID="txt_nota" runat="server" Height="160px" TextMode="MultiLine" Enabled="false" CssClass="form-control">
                                                </asp:TextBox>
                                                <asp:Button ID="cmd_plantilla_d_no" runat="server" CssClass="btn btn-success" Text="Cargar Plantilla" Visible="false" />
                                            </div>
                                        </div>
                                        <asp:Button ID="cmd_guardar" runat="server" CssClass="btn btn-success" Text="Guardar Borrador" OnClientClick="return confirm('�La Informaci�n es Correcta?');" Enabled="false" />
                                        <asp:Button ID="cmd_convertir_p" runat="server" CssClass="btn btn-primary" Enabled="false" Text="Convertir en Plantilla" />

                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div class="tab-pane fade" id="tabResumen">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <!-- Panel Principal -->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Codificaci�n de Muestra</h2>
                                        </div>

                                        <div class="panel-body">
                                            <table class="table table-bordered table-hover table-responsive">

                                                <tr>
                                                    <td>
                                                        <label>Total 08-01-001</label>
                                                        <asp:TextBox ID="txt_0801001" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Citodiagn�stico corriente (PAP) " MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-002</label>
                                                        <asp:TextBox ID="txt_0801002" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Citolog�a aspirativa (por punci�n)" MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-003</label>
                                                        <asp:TextBox ID="txt_0801003" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Estudio histopatol�gico con microscop�a electr�nica (por cada �rgano)" MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-004</label>
                                                        <asp:TextBox ID="txt_0801004" runat="server" CssClass="form-control" MaxLength="3" type="number"
                                                            ToolTip="Est. histopatol�gico c/t�cnicas de inmunohistoqu�mica o inmunofluorescencia (por cada �rgano)">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-005</label>
                                                        <asp:TextBox ID="txt_0801005" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Estudio histopatol�gico con t�cnicas histoqu�micas especiales (incluye descalcificaci�n) (por cada �rgano)"
                                                            MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-006</label>
                                                        <asp:TextBox ID="txt_0801006" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Est. histopatol. de biopsia contempor�nea (r�p.) a intervenc. quir�rgicas (X cada �rgano, no incl. biopsia diferida)"
                                                            MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-007</label>
                                                        <asp:TextBox ID="txt_0801007" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Estudio histopatol�gico con tinci�n corriente de biopsia diferida con estudio seriado (m�nimo 10 muestras) 
                                de un �rgano o parte de �l (no incluye estudio con t�cnica habitual de otros �rganos incluidos en la muestra)"
                                                            MaxLength="3">0</asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <label>Total 08-01-008</label>
                                                        <asp:TextBox ID="txt_0801008" runat="server" CssClass="form-control" type="number"
                                                            ToolTip="Estudio histopatol�gico corriente de biopsia diferida (por cada �rgano)"
                                                            MaxLength="3">0</asp:TextBox>
                                                    </td>


                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <asp:Button ID="cmd_GuardarCodificacion" runat="server" CssClass="btn btn-primary" Text="Guardar" />
                                                        <asp:Button ID="cmd_RecalcularCodificacion" runat="server" CssClass="btn btn-success" Text="Calcular" />
                                                    </td>
                                                </tr>
                                            </table>

                                            <br />
                                            <div style="text-align: center">
                                                <asp:Label ID="lbl_MensajeCodificacion" runat="server" Style="text-align: center" class="alert alert-success" role="alert"
                                                    Text="Aca Van los Mensajes" Visible="False"></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Panel Principal -->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Listado de Casete</h2>
                                        </div>
                                        <br />
                                        <asp:GridView ID="gdv_cortes" runat="server" AutoGenerateColumns="False"
                                            DataKeyNames="GEN_loginUsuarios" DataSourceID="dts_cortes" EnableTheming="True"
                                            HorizontalAlign="Center" RowHeaderColumn="id"
                                            CssClass="table table-bordered table-hover table-responsive">
                                            <EmptyDataTemplate>
                                                <div class="alert alert-warning" role="alert">
                                                    �No existen casetes para esta muestra!
                                                </div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sel.">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSeleccion" runat="server" CssClass="controlSeleccion" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ANA_IdBiopsia" ReadOnly="True" Visible="False" />
                                                <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Corte" ReadOnly="True" SortExpression="ANA_NomCortes_Muestras" />
                                                <asp:BoundField DataField="ANA_FecCortes_Muestras" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha"
                                                    ReadOnly="True" SortExpression="ANA_FecCortes_Muestras" />
                                                <asp:BoundField DataField="nombre_patologo" HeaderText="Pat�logo" ReadOnly="True" SortExpression="nombre_patologo" />
                                                <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Creado por" ReadOnly="True"
                                                    SortExpression="GEN_loginUsuarios" />
                                                <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" ReadOnly="True" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                                                <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="Id. Casete" ReadOnly="True" SortExpression="ANA_IdCortes_Muestras" />
                                            </Columns>
                                        </asp:GridView>
                                        <div style="text-align: center">
                                            <asp:Button ID="cmd_seleccionarcortes" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                                            <asp:Button ID="cmd_deseleccionarcortes" runat="server" CssClass="btn btn-defaultbtn btn-default" Text="Quitar Selecci�n" />
                                        </div>

                                        <div style="width: 95%; text-align: center; margin: 0 auto; padding: 5px">
                                            <asp:Button ID="cmd_especiales" runat="server" Text="Solicitar Especial" CssClass="btn btn-success" />
                                            <asp:Button ID="Button1" runat="server" Text="Imprimir Selecci�n" CssClass="btn btn-primary" />
                                            <asp:Button ID="cmd_solicitar_macro" runat="server" Text="Solicitar Macro" CssClass="btn btn-success" />
                                            <asp:Button ID="cmd_solicitar_casete" runat="server" Text="Solicitar Casete" CssClass="btn btn-success" />
                                            <asp:Button ID="cmd_inmuno" runat="server" CssClass="btn btn-success" Text="Solicitar Inmuno" />
                                            <asp:Button ID="cmd_para_interconsulta" runat="server" Text="Para Interconsulta" CssClass="btn btn-warning" />
                                            <asp:Button ID="cmd_enviar_almacena_casete" runat="server" Text="Enviar a Almacenamiento" CssClass="btn btn-primary" />
                                        </div>
                                        <asp:Panel ID="Panel1" runat="server" CssClass="alert alert-info" HorizontalAlign="Center" Visible="false">
                                            <asp:DropDownList ID="cmb_tecnicas_especiales" runat="server" AutoPostBack="True" Visible="False" Width="380px">
                                            </asp:DropDownList>
                                            <asp:Button ID="cmd_agregar_especiales" runat="server" CssClass="btn btn-success" Text="Agregar T�cnica Especial"
                                                Visible="False" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnl_inmuno" runat="server" CssClass="alert alert-info"
                                            HorizontalAlign="Center" Visible="false">
                                            <asp:DropDownList ID="cmb_inmuno_histoquimica" runat="server" Width="380px">
                                            </asp:DropDownList>
                                            <asp:Button ID="cmd_agregar_inmuno" runat="server" CssClass="btn btn-success"
                                                Text="Agregar Inmunohistoqu�mica" />
                                        </asp:Panel>

                                        <br />
                                        <div style="text-align: center">
                                            <asp:Label ID="lbl_mensaje" runat="server" Style="text-align: center" class="alert alert-success" role="alert"
                                                Text="Aca Van los Mensajes" Visible="False"></asp:Label>
                                        </div>
                                    </div>

                                    <!-- Panel Principal -->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Listado de T�cnicas Especiales</h2>
                                        </div>
                                        <asp:GridView ID="gdv_tecnicas" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover table-responsive"
                                            DataKeyNames="ANA_UsuCrea" DataSourceID="dts_tecnicas">
                                            <Columns>
                                                <asp:BoundField DataField="ANA_FecTecnica" DataFormatString="{0:dd-MM-yyyy}"
                                                    HeaderText="Fecha T�cnica" SortExpression="ANA_FecTecnica" />
                                                <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="N� Corte" SortExpression="ANA_NomCortes_Muestras" />
                                                <asp:BoundField DataField="ANA_NomTecnica" HeaderText="T�cnica" SortExpression="ANA_NomTecnica" />
                                                <asp:BoundField DataField="ANA_EstTecnica" HeaderText="Estado" SortExpression="ANA_EstTecnica" />
                                                <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True" SortExpression="ANA_UsuCrea" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                    <!-- Panel Principal de Inmunohistoqu�mica-->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents Listado de Inmunohistoqu�mica-->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Listado de Inmunohistoqu�mica</h2>
                                        </div>
                                        <div class="panel-body">
                                            <asp:GridView ID="gdv_InmunoHistoquimica" runat="server" AutoGenerateColumns="False"
                                                DataKeyNames="ANA_UsuCrea" DataSourceID="dts_InmunoHistoquimica"
                                                HorizontalAlign="Center" CssClass="table table-bordered table-hover table-responsive">
                                                <Columns>
                                                    <asp:BoundField DataField="ANA_FecInmuno_Histoquimica_Biopsia" DataFormatString="{0:dd-MM-yyyy}"
                                                        HeaderText="Fecha Inmuno" SortExpression="ANA_FecInmuno_Histoquimica_Biopsia" />
                                                    <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="N� Corte" SortExpression="ANA_NomCortes_Muestras" />
                                                    <asp:BoundField DataField="ANA_NomInmuno" HeaderText="Inmunohistoqu�micas" SortExpression="ANA_NomInmuno" />
                                                    <asp:BoundField DataField="ANA_EstInmuno_Histoquimica_Biopsia" HeaderText="Estado" ReadOnly="true" />
                                                    <asp:BoundField DataField="ANA_UsuCrea" HeaderText="Creado por" ReadOnly="True" SortExpression="ANA_UsuCrea" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:SqlDataSource ID="dts_InmunoHistoquimica" runat="server"
                                                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <!-- Panel Principal -->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Listado de Laminas</h2>
                                        </div>
                                        <br />
                                        <asp:GridView ID="gdv_laminas" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" DataSourceID="dts_laminas"
                                            CssClass="table table-bordered table-hover table-responsive">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sel.">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSeleccion0" runat="server" Checked="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ANA_nomLamina" HeaderText="Lamina" SortExpression="ANA_nomLamina" />
                                                <asp:BoundField DataField="ANA_FecLamina" HeaderText="Fecha Lamina" SortExpression="ANA_FecLamina" />
                                                <asp:BoundField DataField="ANA_EstLamina" HeaderText="Estado" SortExpression="ANA_EstLamina" />
                                                <asp:BoundField DataField="ANA_idLamina" HeaderText="Id. Lamina" SortExpression="ANA_idLamina" />
                                            </Columns>
                                        </asp:GridView>


                                        <div style="width: 90%; text-align: center; margin: 0 auto; padding: 5px">
                                            <asp:Button ID="cmd_select_all3" runat="server" CssClass="btn btn-default" Text="Seleccionar Todo" />
                                            <asp:Button ID="cmd_select_all4" runat="server" CssClass="btn btn-default" Text="Quitar Selecci�n" />
                                            <br />
                                            <asp:Button ID="cmd_solicitar_casete0" runat="server" Text="Solicitar Lamina" CssClass="btn btn-success" />
                                            <asp:Button ID="cmd_para_interconsulta1" runat="server" Text="Para Interconsulta" CssClass="btn btn-warning" />
                                            <asp:Button ID="cmd_solicitar_casete1" runat="server" Text="Enviar a Almacenamiento" CssClass="btn btn-primary" />
                                            <asp:Button ID="cmd_imprime_laminas" runat="server" Text="Imprimir Selecci�n" Visible="False" CssClass="btn btn-primary" />
                                        </div>
                                    </div>

                                    <asp:SqlDataSource ID="dts_cortes" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                    <asp:SqlDataSource ID="dts_tecnicas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                    <asp:SqlDataSource ID="dts_laminas" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                    <wuc:Alert ID="Alert1" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="tab-pane fade" id="tabCritico">
                            <!-- Grilla de Criticos-->
                            <asp:UpdatePanel ID="uppCritico" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading clearfix">
                                            <h2 class="text-center">Detalle de Critico</h2>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-bordered table-hover">
                                                <tr>
                                                    <td>Informado A:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cmb_usuario_notificado" CssClass="form-control" runat="server" Enabled="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Observaci�n:</td>
                                                    <td>
                                                        <asp:TextBox ID="txt_observacion" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                                        </asp:TextBox>
                                                        <asp:Button ID="cmd_notificarCritico" runat="server" CssClass="btn btn-warning" Enabled="false" Text="Notificar a Usuario" OnClientClick="return confirm('�La Informaci�n es Correcta?');" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <asp:SqlDataSource ID="dts_noti" runat="server"
                                                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
                                            <asp:GridView ID="gdv_notificaciones" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered table-hover table-responsive" DataSourceID="dts_noti">
                                                <Columns>
                                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                                                    <asp:BoundField DataField="Observacion" HeaderText="Observaci�n" SortExpression="Observacion" />
                                                    <asp:BoundField DataField="Patologo" HeaderText="Patologo" SortExpression="Patologo" />
                                                    <asp:BoundField DataField="Notificado" HeaderText="Notificado" SortExpression="Notificado" />
                                                    <asp:BoundField DataField="ANA_LeidoBiopsias_Critico" HeaderText="Leido" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Image ID="Img_leido" runat="server" Width="25px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="tab-pane fade" id="tabNeoplasia">
                            <asp:UpdatePanel ID="uppNeoplasia" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <!-- Panel Principal Detalle de Neoplasia -->
                                    <div class="panel panel-default">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading">
                                            <h2 class="text-center">Detalle de Neoplasia</h2>
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-6">
                                                <label>Comportamiento:</label>
                                                <asp:DropDownList ID="cmb_comportamiento" CssClass="form-control" runat="server" Enabled="false" AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txt_id_neo" runat="server" Visible="False" Width="65px">0</asp:TextBox>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Morfologia:</label>
                                                <asp:DropDownList ID="cmb_morfologia" runat="server" Enabled="false" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Grado de Diferenciaci�n:</label>
                                                <asp:DropDownList ID="cmb_grado_dif" runat="server" Enabled="false" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Organo:</label>
                                                <asp:DropDownList ID="cmb_organo_cie" runat="server" Enabled="false" CssClass="form-control" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Topografia:</label>
                                                <asp:DropDownList ID="cmb_topografia" runat="server" Enabled="false" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Lateralidad:</label>
                                                <asp:DropDownList ID="cmb_lateralidad" runat="server" Enabled="false" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Her2:</label>
                                                <asp:DropDownList ID="cmb_her2" runat="server" Enabled="false" CssClass="form-control">
                                                    <asp:ListItem>No Realizado</asp:ListItem>
                                                    <asp:ListItem>Positivo</asp:ListItem>
                                                    <asp:ListItem>Negativo</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer clearfix">
                                            <div class="pull-right">
                                                <asp:Button ID="cmd_guardar1" runat="server" CssClass="btn btn-primary" Enabled="false" Text="Guardar Neoplasia" OnClientClick="return confirm('�La Informaci�n es Correcta?');" />
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="tab-pane fade" id="tabAdjuntos">
                            <!-- Grilla de Adjuntos-->
                            <asp:UpdatePanel ID="upp_Adjuntos" runat="server">
                                <ContentTemplate>
                                    <div style="margin: 0 auto; text-align: center;">
                                        <h3 style="width: 100%">Documentos Adjuntos:</h3>
                                        <div class="rowb4">
                                            <div id="adjuntosServerArrastrar" class="col-md-6 col-sm-12" runat="server">
                                                <div id="drag-and-drop-zone" class="dm-uploader p-5" style="height:300px;">
                                                    <h3 class="mb-5 mt-5 text-muted">Arrastrar documentos ac�</h3>
                                                    <div class="btn btn-primary btn-block mb-5">
                                                        <span>Abrir explorador de archivos</span>
                                                        <input type="file" title='Click para seleccionar archivos' />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adjuntosServerEnviados" class="col-md-6 col-sm-12" runat="server">
                                                <div class="card h-100" style="max-height: 300px;" >
                                                    <div class="card-header">
                                                        Documentos enviados
                                                    </div>
                                                    <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                                        <li class="text-muted text-center empty">No hay documentos cargados.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divError" class="rowb4" style="display:none;">
                                            <div class="col-12">
                                                <div class="card h-100">
                                                    <div class="card-header">
                                                        Mensaje
                                                    </div>
                                                    <ul class="list-group list-group-flush" id="debug">
                                                        <li class="list-group-item text-muted empty"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 





                                        <%--<asp:Panel ID="pnl_adj" runat="server" HorizontalAlign="Center">
                                            <label>Subir Archivo:</label>
                                            <asp:FileUpload ID="FileUpload1" runat="server" Height="31px" Width="305px" />
                                            <asp:Button ID="cmd_subir_archivo" runat="server" CssClass="btn btn-success" Text="Adjuntar documento" />
                                            <asp:Button ID="cmd_SalirAdjunto" runat="server" CssClass="btn btn-warning" Text="Cancelar" />
                                        </asp:Panel>

                                        <asp:GridView ID="gdv_archivos" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ANA_IdArchivos_Biopsias"
                                            DataSourceID="dts_archivos" ForeColor="#333333" GridLines="None" Width="908px">
                                            <RowStyle BackColor="#EFF3FB" />
                                            <Columns>
                                                <asp:BoundField DataField="ANA_FecArchivos_Biopsias" HeaderText="Fecha" SortExpression="ANA_FecArchivos_Biopsias" />
                                                <asp:BoundField DataField="ANA_NomArchivos_Biopsias" HeaderText="Nombre Archivo" SortExpression="ANA_NomArchivos_Biopsias" />
                                                <asp:BoundField DataField="ANA_IdArchivos_Biopsias" HeaderText="Id. Archivo" InsertVisible="False"
                                                    ReadOnly="True" SortExpression="ANA_IdArchivos_Biopsias" Visible="False" />
                                                <asp:HyperLinkField DataNavigateUrlFields="ruta_archivo" DataNavigateUrlFormatString="{0}" DataTextField="ANA_NomArchivos_Biopsias"
                                                    HeaderText="Campo" Target="_blank" Text="Campo" />
                                                <asp:TemplateField HeaderText="Ver Archivo">
                                                    <ItemTemplate>
                                                        <asp:Button ID="cmd_VerArchivo" runat="server" CssClass="btn btn-success"
                                                            OnClick="cmd_VerArchivo_Click" Text="Ver Archivo" CommandArgument='<%# Eval("ANA_IdArchivos_Biopsias") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Acci�n">
                                                    <ItemTemplate>
                                                        <asp:Button ID="cmd_eliminarDcto" runat="server" CssClass="btn btn-danger"
                                                            OnClick="cmd_eliminarDcto_Click1" Text="Eliminar" CommandArgument='<%# Eval("ANA_IdArchivos_Biopsias") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <EditRowStyle BackColor="#2461BF" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="dts_archivos" runat="server"
                                            ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>--%>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>

            <hr />


            <asp:UpdatePanel ID="upp_botones" runat="server">
                <ContentTemplate>
                    <div class="alert alert-info" style="margin: 0 auto; padding: 5px">

                        <asp:Button ID="cmd_validar_autorizar" runat="server" CssClass="btn btn-danger" Enabled="false" Text="Validar y Autorizar" />
                        <asp:Button ID="cmd_imprimir" runat="server" CssClass="btn btn-info" Text="Imprimir" />
                        <asp:Button ID="cmd_cerrar_inter" runat="server" CssClass="btn btn-warning" Text="Cerrar Interconsulta" Enabled="false" />
                        <%--<asp:Label ID="lbl_PopUpValidar" runat="server"></asp:Label>--%>
                    </div>
                    <wuc:Alert ID="wuc_alert" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="cmd_imprimir" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODALES AC� -->
    <!-- MODAL CONVERTIR EN PLANTILLA -->
    <div id="modalPlantilla" role="dialog" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upp_plantilla" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Panel Principal Convertir en plantilla -->
                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Crear Plantilla</div>
                        <div class="panel-body">
                            <div>
                                <label>Nombre de la Plantilla:</label>
                                <asp:TextBox ID="txt_nombre_plantilla" runat="server" MaxLength="50" CssClass="form-control">
                                </asp:TextBox>
                            </div>
                            <div class="panel-footer clearfix">
                                <div class="pull-right">
                                    <asp:Button ID="cmd_guardar_p" runat="server" CssClass="btn btn-primary" Text="Guardar Plantilla" OnClientClick="return confirm('�La Informaci�n es Correcta?');" />
                                    <asp:Button ID="cmd_salirPlantilla" runat="server" CssClass="btn btn-warning" Text="Salir" data-dismiss="modal" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div id="modalValidar" role="dialog" class="modal fade" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upp_validar" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Panel Principal Validar y Autorizar Publicaci�n-->
                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Validar y Autorizar Publicaci�n</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Nombre de Usuario:</label>
                                    <asp:TextBox ID="txt_user_name" runat="server" CssClass="form-control" AutoPostBack="false">
                                    </asp:TextBox>
                                </div>
                                <div class="col-md-4">
                                    <label>Contrase�a:</label>
                                    <asp:TextBox ID="txt_clave" runat="server" CssClass="form-control" TextMode="Password">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                            <div class="pull-right">

                                <asp:Button ID="cmd_validarInforme" runat="server" CssClass="btn btn-success" Text="Validar y Autorizar Publicaci�n" />
                                <asp:Button ID="cmd_desvalidarInforme" runat="server" CssClass="btn btn-success" Text="Desvalidar Informe" OnClientClick="return confirm('�La Informaci�n es Correcta?');" />
                                <asp:Button ID="cmd_salirValidarInforme" runat="server" CssClass="btn btn-warning" Text="Salir" data-dismiss="modal" />
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <div id="modalQuitar" role="dialog" class="modal fade" aria-labelledby="modalQuitar" aria-hidden="true">
        <div class="modal-dialog">
            <div class="panel panel-info">
                <div class="panel-heading">Confirmaci�n</div>
                <div class="panel-body">
                    <br />
                    <div class="alert alert-warning" role="alert">�Est� seguro que desea quitar el documento?</div>
                </div>
                <div class="panel-footer clearfix">
                    <div class="pull-right">
                        <button id="btnQuitarConfirmar" class="btn btn-danger">QUITAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <link href="/CSS/jquery.dm-uploader.css" rel="stylesheet" />
    <script src="/JS/jquery.dm-uploader.js"></script>

    <script>
        var sSession;
        var vID;
        var vNombre;
        $(document).ready(function () { 
            sSession = getSession();
            
            ContarAdjuntos(false);
            //console.log(sSession);
            $('#ctl00_Cnt_Principal_li_Adjuntos').click(function () {
                ContarAdjuntos(false);
                if (sSession.GEN_CodigoPerfil != 3)
                    $('.btnQuitar').hide();
            });

            if (sSession.GEN_CodigoPerfil != 3)
                $('.btnQuitar').hide();
            
            
            $('body').on('click', '#btnQuitarConfirmar', function (e) {
                //delete this
                $.ajax({
                    type: 'GET',
                    url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=DeleteArchivo&idArchivo=' + vID + '&nombreArchivo=' + vNombre,
                    contentType: 'application/json',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                    }
                });
                ContarAdjuntos(false);
                e.preventDefault();
                $('#modalQuitar').modal('hide');
            });
            $('body').on('click', '.btnQuitar', function (e) {
                vID = $(this).parents('.liElemento').attr('id');        //para sacar de la base de datos
                vNombre = $(this).parent().children('strong').html();   //para sacar del disco
                $('#modalQuitar').modal('show');
            });

            $('body').on('click', '.nombreArchivo', function (e) {
                window.open('../Documento.aspx?nombre=' + $(this).children('strong').html());
                //vID = $(this).parents('.liElemento').attr('id');        //para sacar de la base de datos
                //vNombre = $(this).parent().children('strong').html();   //para sacar del disco
                //$('#modalQuitar').modal('show');
            });
            

        });

        function ContarAdjuntos(soloContar) {
            var vID = getUrlParameter('id');
            setParametro('C:/GIAP/GIAP/dcto/adj/', vID, sSession.perfil_del_usuario, sSession.FECHA_ACTUAL);
            var json;
            $.ajax({
                type: 'GET',
                url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=GetArchivos&perfilUsuario=' + sSession.perfil_del_usuario + '&idBiopsia=' + vID,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    json = data;
                    $('#ctl00_Cnt_Principal_lbl_Adjuntos').html(json.length);
                    if (!soloContar) {
                        $('#files').html('<li class="text-muted text-center empty">No hay documentos cargados.</li>');
                        for (var i = 0; i < json.length; i++) {
                            var template = $('#files-template').text();
                            var fn = json[i].ANA_NomArchivos_Biopsias;
                            var idf = json[i].ANA_IdArchivos_Biopsias;
                            //

                            fn = fn.replace(/\+/g, ' ');
                            fn = fn.replace(/�/g, ' ');
                            fn = fn.replace(/,/g, ' ');
                            fn = fn.replace(/  /g, ' ');
                            fn = fn.replace(/   /g, ' ');

                            template = template.replace('%%filename%%', fn);
                            template = $(template);

                            template.find('.progress-bar').attr('style', 'width: 100%');
                            template.find('#date-file').show();
                            template.find('.btnQuitar').show();
                            template.find('#date-file').html(json[i].ANA_FecArchivos_Biopsias);

                            template.prop('id', idf);
                            template.data('file-id', idf);
                            $('#files').find('li.empty').fadeOut();
                            $('#files').prepend(template);
                        }
                    }
                }
            });
        }

    </script>

    <style>
        .nombreArchivo:hover {
            transition: transform .2s;
            background-color: #f1f1f1;
            cursor:pointer;
            transform: translateX(3px);
        }
        
    </style>

    <script type="text/html" id="debug-template">
        <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>

    <script src="/JS/uploader.js"></script>
</asp:Content>
