﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm_nomina_mov.aspx.vb" Inherits="Anatomia_Patologica.frm_nomina_mov" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource1" DataKeyNames="id">
            <Columns>
                <asp:BoundField DataField="fec_macro" HeaderText="fec_macro" 
                    SortExpression="fec_macro" />
                <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="ANA_NomCortes_Muestras" 
                    SortExpression="ANA_NomCortes_Muestras" />
                <asp:BoundField DataField="estado_ant" HeaderText="estado_ant" 
                    SortExpression="estado_ant" />
                <asp:BoundField DataField="estado_nuevo" HeaderText="estado_nuevo" 
                    SortExpression="estado_nuevo" />
                <asp:BoundField DataField="fec_nomina" HeaderText="fec_nomina" 
                    SortExpression="fec_nomina" />
                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" 
                    ReadOnly="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
            
            SelectCommand="SELECT * FROM [vi_impr_nomina] ORDER BY [ANA_NomCortes_Muestras], [fec_macro]">
        </asp:SqlDataSource>
    
    </div>
    <asp:Button ID="Button1" runat="server" Text="Button" />
    <asp:TextBox ID="txtTexto" runat="server"></asp:TextBox>
    </form>
</body>
</html>
