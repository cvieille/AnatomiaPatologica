﻿<%@ Page Title="Busqueda por Paciente" Language="vb" AutoEventWireup="false" MasterPageFile="~/master-clinico.Master" CodeBehind="frm_consulta_clinicos.aspx.vb" Inherits="Anatomia_Patologica.frm_consulta_clinicos" %>

<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_Pacientes").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_Pacientes").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>
<asp:Content ID="Cnt_Principal" ContentPlaceHolderID="Cnt_Principal" runat="server">



    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Busqueda de Resultados por Paciente</h2>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="row text-center">
                            <div class="col-md-2">
                                <label>Rut:</label>
                                <div class="form-inline">
                                    <asp:TextBox ID="txt_rut" runat="server" MaxLength="10" TabIndex="1" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                    <asp:TextBox ID="txt_digito" runat="server" CssClass="form-control" Enabled="False" ReadOnly="True" Width="40px"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 text-left">
                                <label>Nº Ubic. Int:</label>
                                <asp:TextBox ID="txt_ficha" type="number" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-2 text-left">
                                <label>Nombre:</label>
                                <asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4 text-left">
                                <label>Apellidos:</label>
                                <div class="form-inline">
                                    <asp:TextBox ID="txt_ap1" runat="server" MaxLength="100" CssClass="form-control">
                                    </asp:TextBox>
                                    <asp:TextBox ID="txt_ap2" runat="server" MaxLength="100" CssClass="form-control">
                                    </asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-2 text-left">
                                <br />
                                <asp:Button ID="cmd_buscar" runat="server" CssClass="btn btn-success" Text="Buscar Exámenes" />
                            </div>
                        </div>
                        <br />
                        <br />

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <div style="position: absolute">Espere un momento.....</div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:GridView ID="gdv_Pacientes" runat="server" AutoGenerateColumns="False" DataSourceID="sql_data_busqueda"
                        CssClass="table table-bordered table-hover table-responsive" OnPreRender="gdv_Pacientes_PreRender" RowStyle-HorizontalAlign="Center">
                        <Columns>
                            <asp:BoundField DataField="n_biopsia" HeaderText="N° de Biopsia" ReadOnly="True" SortExpression="n_biopsia"></asp:BoundField>
                            <asp:BoundField DataField="ANA_fec_RecepcionRegistro_Biopsias" HeaderText="Recep. Anatomia" SortExpression="ANA_fec_RecepcionRegistro_Biopsias"
                                DataFormatString="{0:dd-MM-yyyy}" />
                            <asp:BoundField DataField="GEN_nombrePaciente" HeaderText="Paciente" SortExpression="GEN_nombrePaciente" ReadOnly="True" />
                            <asp:BoundField DataField="Gen_nombrePatologo" HeaderText="Patologo" SortExpression="Gen_nombrePatologo" />
                            <asp:BoundField DataField="Gen_nombreTecnologo" HeaderText="Tecnologo" SortExpression="Gen_nombreTecnologo" />
                            <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
                            <asp:BoundField DataField="GEN_nombreServicio" HeaderText="Destino" SortExpression="Destino" ReadOnly="True" ItemStyle-Width="50" />
                            <asp:TemplateField HeaderText="Reporte">
                                <ItemTemplate>
                                    <div class="btn btn-info">
                                        <a style="color: white;" target="_blank" href='<%# "FormExternos/frm_VerDetalleBiopsia.aspx?id=" + Eval("ANA_IdBiopsia").ToString() %>'>Ver Reporte
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contacto"></asp:TemplateField>
                            <asp:TemplateField HeaderText="Observación" HeaderStyle-Width="200px"></asp:TemplateField>
                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="ID Biopsia" SortExpression="ANA_IdBiopsia" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource ID="sql_data_busqueda" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
