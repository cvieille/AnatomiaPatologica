﻿var vPerfiles;
$(document).ready(function () {
    deleteSession('GEN_CodigoPerfil');
    deleteSession('id_usuario');
    deleteSession('nom_usuario');
    deleteSession('rut_usuario');
    deleteSession('perfil_del_usuario');
    deleteSession('NombrePerfil');
    deleteSession('VPERFILES');

    $('#btnEntrar').click(function (e) {
        Login();
        e.preventDefault();
    });

    $('#btnEntrarLogin').click(function (e) {
        var idPerfil = $('#selectPerfiles').val();
        var vSeleccion = vPerfiles.filter(function (vPerfiles) { return vPerfiles.GEN_idPerfil == idPerfil });
        setSession('GEN_CodigoPerfil', vSeleccion[0].GEN_codigoPerfil);
        setSession('perfil_del_usuario', vSeleccion[0].GEN_idPerfil);
        setSession('NombrePerfil', vSeleccion[0].GEN_descripcionPerfil);
        redirecciona(vSeleccion[0].GEN_codigoPerfil);
        e.preventDefault();
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        //"positionClass": "toast-top-full-width",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": 150,
        "hideDuration": 150,
        "timeOut": 3000,
        "extendedTimeOut": 1500,
        "showEasing": "linear",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
});
function Login() {
    var success = false;

    $('#btnEntrar').addClass('disabled');
    $('#btnEntrar').attr('disabled', 'disabled');
    $('#btnEntrar').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $('#txtLogin').attr('disabled', 'disabled');
    $('#txtClave').attr('disabled', 'disabled');

    try
    {
        var resp;
        var user = $('#txtLogin').val();
        var pass = CryptoJS.MD5($('#txtClave').val());
        var webapi = GetWebApiUrl() + 'recuperarToken';
        var datos = {
            'grant_type': 'password',
            'username': user,
            'password': '' + pass,
            'clientid': '1' 
        }

        if ($.ajaxSettings.headers['Authorization'] != undefined)
            delete $.ajaxSettings.headers['Authorization'];

        $.ajax({
            type: 'POST',
            url: webapi,
            data: datos,
            async: false,
            success: function (response) {
                resp = response;
                success = true;
            },
            error: function (err) {
                console.log('ERROR no se pudo obtener el token: ' + err.message);
            }
        });
    }
    catch (err)
    {
        console.log(err.message);
    }
    if (success) {
        setSession('TOKEN', resp.access_token);
        $.ajaxSetup({
            headers: { 'Authorization': GetToken() }
        });
        getUsuario();
    }
    else
        loginIncorrecto();
}

function loginIncorrecto()
{
    toastr.error('Usuario o contraseña incorrectos');
    $('#btnEntrar').removeClass('disabled');
    $('#btnEntrar').removeAttr('disabled');
    $('#btnEntrar').html('Entrar');
    $('#txtLogin').removeAttr('disabled');
    $('#txtClave').removeAttr('disabled');
}

function getUsuario() {
    try
    {
        var usuario = $('#txtLogin').val();
        var pass = CryptoJS.MD5($('#txtClave').val());
        var url = GetWebApiUrl() + '/GEN_Usuarios/Acceso?GEN_idSistemas=' + 1 + '&GEN_loginUsuarios=' + usuario + '&GEN_claveUsuarios=' + pass;

        $.ajax({
            type: 'GET',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data)
            {
                if ($.isEmptyObject(data))
                {
                    loginIncorrecto();
                }
                else
                {
                    setSession('id_usuario', data[0].GEN_idUsuarios);
                    setSession('nom_usuario', usuario.toUpperCase());
                    setSession('rut_usuario', data[0].GEN_numero_documentoPersonas);

                    if (data[0].GEN_Perfiles.length > 1)
                    {
                        vPerfiles = data[0].GEN_Perfiles;
                        setSession('VPERFILES', JSON.stringify(vPerfiles));
                        $.each(data[0].GEN_Perfiles, function (key, val)
                        {$("#selectPerfiles").append("<option value='" + val.GEN_idPerfil + "'>" + val.GEN_descripcionPerfil + "</option>");});
                        $('#modalPerfiles').modal('show');
                        return;
                    }
                    
                    setSession('GEN_CodigoPerfil', data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                    setSession('perfil_del_usuario', data[0].GEN_Perfiles[0].GEN_idPerfil);
                    setSession('NombrePerfil', data[0].GEN_Perfiles[0].GEN_descripcionPerfil);
                    redirecciona(data[0].GEN_Perfiles[0].GEN_codigoPerfil);
                }
            }
        });
    } catch (err) {
        console.log('Error: ' + err.message);
    }
}

