function GetWebApiUrl() {
    return "http://10.6.180.236/WebApi/Api/"
    //return "http://10.6.180.235/WebApi/Api/"
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

function ObtenerHost() {
    var host = "";
    if (window.location.origin.indexOf("localhost") > -1)
        return window.location.origin;
    return window.location.origin + "/BORRAR_ANA";
}
function GetToken() {
    return 'Bearer ' + getSession().TOKEN;
}

function deleteSession(key) {
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=DeleteSession&key=' + key,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
        }
    });
}

function setSession(key, value) {
    $.ajax({
        type: "GET",
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=SetSession&key=' + key + '&value=' + value,
        contentType: "application/json",
        dataType: "json",
        async: false,
        success: function (data) {
        }
    });
}

function getSession() {
    var sSession;
    $.ajax({
        type: 'GET',
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=GetSession',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            sSession = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error:' + xhr.responseText);
        }
    });
    return sSession;
}

function redirecciona(idPerfil) {
    switch (idPerfil) {
        case 9: window.location.replace(ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx'); break;
        case 11: window.location.replace(ObtenerHost() + '/FormPrincipales/frm_PrincipalGestionClinica.aspx'); break;
        case 12: window.location.replace(ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx'); break;

        case 2:
        case 7:
        case 6:
        case 8:
        case 10:
        case 4:
        case 3: window.location.replace(ObtenerHost() + '/FormPrincipales/frm_principal.aspx'); break;
        default: window.location.replace(ObtenerHost() + '/frm_login.aspx'); break;
    }
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};