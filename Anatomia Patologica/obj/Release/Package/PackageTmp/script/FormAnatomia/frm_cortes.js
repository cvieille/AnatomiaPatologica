﻿$(document).ready(function () {
    var idBiopsia = getUrlParameter('id');

    $.ajax({
        type: 'POST',
        url: ObtenerHost() + '/Clases/MetodosGenerales.ashx?method=TablaMuestrasporBiopsia&idBiopsia=' + idBiopsia,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_FrascoBiopsia,
                    val.ANA_CantidadMuestras,
                    val.ANA_DetalleCatalogo_Muestras,
                    val.ANA_DescripcionBiopsia
                ]);
            });

            $('#tbl_MuestrasBiopsias').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
              //  columnDefs: [{ targets: 1, sType: 'date-ukLong' }],
                columns: [
                    { title: 'Frascos' },
                    { title: 'Muestras' },
                    { title: 'Detalle' },
                    { title: 'Descripción' }
                ],
                bDestroy: true
            });
        }
    });
});