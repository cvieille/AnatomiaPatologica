﻿Public Class ComboBoxAnatomia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    '*** CATALOGO DE MUESTRAS DE BIOPSIAS. SE USA POR EJ. EN RECEPCION
    '==========================================================
    Public Sub CatalogoBiopsias_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable

        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)
        Dim consulta As String = "SELECT " _
        & "ANA_IdCatalogo_Muestras, " _
        & "ANA_DetalleCatalogo_Muestras " _
        & "FROM " _
        & "ANA_Catalogo_Muestras " _
        & "ORDER BY " _
        & "ANA_DetalleCatalogo_Muestras"

        tb = fun_sql.consulta_sql_datatable(consulta)
        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DetalleCatalogo_Muestras").ToString(), CStr(tb.Rows(i).Item("ANA_IdCatalogo_Muestras").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    '*** DETALLE DE CATALOGO DE MUESTRAS DE BIOPSIAS. SE USA POR EJ. EN RECEPCION
    '==========================================================
    Public Sub DetalleCatalogoBiopsias_combobox(ByVal combo As DropDownList, ByVal CodMuestra As String)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable

        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)
        Dim consulta As String = "SELECT " _
        & "ANA_IdDetalleCatalogo_Muestras, ANA_DetalleCatalogo_Muestras " _
        & "FROM ANA_Detalle_Catalogo_Muestras " _
        & "WHERE ANA_IdCatalogo_Muestras = " & CodMuestra & " " _
        & "ORDER BY ANA_DetalleCatalogo_Muestras"
        tb = fun_sql.consulta_sql_datatable(consulta)
        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DetalleCatalogo_Muestras").ToString(), CStr(tb.Rows(i).Item("ANA_IdDetalleCatalogo_Muestras").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    '*** COMBOBOX DE ESTADOS EN QUE SE PUEDE ENCONTRAR UNA MUESTRA
    '==========================================================
    Public Sub EstadoBiopsia_ComboBox(ByVal combo As DropDownList)
        combo.Items.Clear()

        Dim lista As New ListItem("---Seleccionar---", "0")
        combo.Items.Add("Lista de Trabajo")
        combo.Items.Add("Ingresado")
        combo.Items.Add("Recepcionado")
        combo.Items.Add("En Macroscopia")
        combo.Items.Add("Despachado")
        combo.Items.Add("Validado y Autorizado para Publicar")
        combo.Items.Add("Entregada a Patologo")
        combo.Items.Add("En Proceso")
        combo.Items.Add("Todas")
        combo.DataBind()

    End Sub




    '*** COMBOBOX DE ESTADOS EN QUE SE PUEDE ENCONTRAR UNA LAMINA
    '==========================================================
    Public Sub EstadoLamina_ComboBox(ByVal combo As DropDownList)
        combo.Items.Clear()

        Dim lista As New ListItem("---Seleccionar---", "0")
        combo.Items.Add("---Seleccionar---")
        combo.Items.Add("Almacenado")
        combo.Items.Add("Solicitado")
        combo.Items.Add("Desechado")
        combo.Items.Add("Para Interconsulta")
        combo.Items.Add("Para Almacenar")
        combo.Items.Add("Desp. Interconsulta")
        combo.DataBind()

    End Sub

    

    '*** LISTADO DE PATOLOGOS PARA PANTALLA PRINCIPAL E INFORMES
    '==========================================================
    Public Sub Patologos_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem("---Seleccionar---", "0")
        Dim tb As New DataTable
        Dim consulta As String = "SELECT u.GEN_IdUsuarios, u.GEN_loginUsuarios " _
        & "FROM GEN_Usuarios AS u INNER JOIN " _
        & "GEN_Sist_Usuario AS su ON " _
        & "u.GEN_IdUsuarios = su.GEN_idUsuarios INNER JOIN " _
        & "GEN_Perfil AS gp ON " _
        & "su.GEN_idPerfil = gp.GEN_idPerfil " _
        & "WHERE (su.GEN_estadoSist_Usuario = 'Activo') AND " _
        & "(gp.GEN_codigoPerfil = 3) AND " _
        & "gp.GEN_IdSistemas=1"
        tb = fun_sql.consulta_sql_datatable(consulta)

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_loginUsuarios").ToString(), CStr(tb.Rows(i).Item("GEN_IdUsuarios").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    '*** LISTADO DE TECNOLOGOS PARA PANTALLA PRINCIPAL E INFORMES
    '==========================================================
    Public Sub Tecnologos_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable
        Dim consulta As String = "SELECT u.GEN_IdUsuarios, u.GEN_loginUsuarios " _
        & "FROM GEN_Usuarios AS u INNER JOIN " _
        & "GEN_Sist_Usuario AS su ON " _
        & "u.GEN_IdUsuarios = su.GEN_IdUsuarios INNER JOIN " _
        & "GEN_Perfil AS gp ON " _
        & "su.GEN_idPerfil = gp.GEN_idPerfil " _
        & "WHERE (su.GEN_estadoSist_Usuario = 'Activo') AND " _
        & "(gp.GEN_codigoPerfil = 4) AND gp.GEN_IdSistemas=1"
        tb = fun_sql.consulta_sql_datatable(consulta)

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_loginUsuarios").ToString(), CStr(tb.Rows(i).Item("GEN_IdUsuarios").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub NEO_Lateralidad_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        lista = New ListItem("NO CORRESPONDE", "NO CORRESPONDE")
        combo.Items.Add(lista)

        lista = New ListItem("BILATERAL", "BILATERAL")
        combo.Items.Add(lista)

        lista = New ListItem("DERECHA", "DERECHA")
        combo.Items.Add(lista)

        lista = New ListItem("DESCONOCIDO", "DESCONOCIDO")
        combo.Items.Add(lista)

        lista = New ListItem("IZQUIERDA", "IZQUIERDA")
        combo.Items.Add(lista)

        combo.DataBind()

    End Sub

    Public Sub NEO_Morfologia_combobox(ByVal combo As DropDownList, ByVal CodComportamiento As String)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_DesMorfologia, ANA_IdMorfologia " _
        & "FROM ANA_morfologia " _
        & "WHERE (ANA_CodMorfologia LIKE '%" & CodComportamiento & "%')"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DesMorfologia").ToString(), CStr(tb.Rows(i).Item("ANA_IdMorfologia").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Plantilla_con_macro_para_corte(ByVal combo As DropDownList)
        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_idPlantilla, ANA_nombreplantilla " _
        & "FROM ANA_plantillas " _
        & "WHERE (NOT (ANA_desc_macroscopicaPlantilla LIKE '')) " _
        & "ORDER BY ANA_nombreplantilla"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_nombreplantilla").ToString(), CStr(tb.Rows(i).Item("ANA_idPlantilla").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    Public Sub Plantilla_combobox(ByVal combo As DropDownList, ByVal WHERE As String)

        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable

        Dim strsql As String

        If where = "" Then
            strsql = "SELECT ANA_idPlantilla, ANA_nombreplantilla " _
            & "FROM ANA_plantillas ORDER BY ANA_nombreplantilla"
        Else
            strsql = "SELECT ANA_idPlantilla, ANA_nombreplantilla " _
            & "FROM ANA_plantillas WHERE  " & WHERE & " ORDER BY ANA_nombreplantilla"
        End If
        tb = fun_sql.consulta_sql_datatable(strsql)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_nombreplantilla").ToString(), CStr(tb.Rows(i).Item("ANA_idPlantilla").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    Public Sub NEO_Grado_Diferenciacion_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_IdGrado_Diferenciacion, ANA_DescGrado_Diferenciacion " _
        & "FROM ANA_Grado_Diferenciacion " _
        & "ORDER BY ANA_DescGrado_Diferenciacion"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DescGrado_Diferenciacion").ToString(), Trim(CStr(tb.Rows(i).Item("ANA_IdGrado_Diferenciacion").ToString())))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    Public Sub NEO_Organo_Cie_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable("SELECT ANA_DesOrganoCie, ANA_IdOrganoCie FROM ANA_organo_cie")

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_DesOrganoCie").ToString(), (tb.Rows(i).Item("ANA_IdOrganoCie").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    Public Sub Tipo_Inmuno_Histoquimicas_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_idInmunoHistoquimica, ANA_NomInmunoHistoquimica " _
        & "FROM ANA_Inmuno_Histoquimica " _
        & "order by ANA_NomInmunoHistoquimica"

        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_NomInmunoHistoquimica").ToString(), CStr(tb.Rows(i).Item("ANA_idInmunoHistoquimica").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Tipo_Tecnicas_especiales_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        Dim tb As New DataTable
        Dim consulta As String = "SELECT ANA_idTecnica, ANA_nomTecnica " _
        & "FROM ANA_Tecnicas_Especiales " _
        & "order by ANA_nomTecnica"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("ANA_nomTecnica").ToString(), CStr(tb.Rows(i).Item("ANA_idTecnica").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Usuarios_notificaciones_combobox(ByVal combo As DropDownList)
        Try
            Dim lista As New ListItem
            combo.Items.Clear()

            Dim tb As New DataTable
            Dim consulta As String = "SELECT u.GEN_IdUsuarios, " _
            & "ISNULL(RTRIM(GEN_nombreUsuarios) + ' ','') + " _
            & "ISNULL(rtrim(GEN_apellido_paternoUsuarios) + ' ','') + " _
            & "ISNULL(rtrim(GEN_apellido_maternoUsuarios),'') as nombre " _
            & "FROM GEN_Usuarios AS u INNER JOIN " _
            & "GEN_Sist_Usuario AS su ON " _
            & "u.GEN_IdUsuarios = su.GEN_IdUsuarios INNER JOIN " _
            & "GEN_Perfil AS gp ON " _
            & "su.GEN_idPerfil = gp.GEN_idPerfil " _
            & "WHERE (su.GEN_estadoSist_Usuario = 'Activo') AND " _
            & "u.GEN_recibe_notificacionesUsuarios='SI' AND " _
            & "gp.GEN_IdSistemas=1"
            tb = fun_sql.consulta_sql_datatable(consulta)

            For i = 0 To (tb.Rows.Count - 1)
                lista = New ListItem(tb.Rows(i).Item("nombre").ToString(), CStr(tb.Rows(i).Item("GEN_IdUsuarios").ToString()))
                combo.Items.Add(lista)
            Next
            combo.DataBind()

        Catch ex As Exception

        End Try

    End Sub

End Class