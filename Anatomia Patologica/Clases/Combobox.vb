﻿Public Class Combobox
    Inherits ComboBoxAnatomia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

    ' *** CARGA TIPOS DE IDENTIFICACION EN COMBO
    '==========================================================
    Public Sub Get_Identificacion_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem

        combo.Items.Clear()

        Dim consulta As String = ""
        consulta = "SELECT " _
        & "GEN_idIdentificacion, GEN_nombreIdentificacion " _
        & "FROM " _
        & "GEN_Identificacion " _
        & "order by GEN_idIdentificacion asc"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombreIdentificacion").ToString, CStr(tb.Rows(i).Item("GEN_idIdentificacion").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    ' *** CARGA TIPOS DE ORIGENES DE SERVICIO EN COMBO
    '==========================================================
    Public Sub Get_Origen_Servicios_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("HCM", "HCM")
        combo.Items.Add(lista)

        lista = New ListItem("Externo", "Externo")
        combo.Items.Add(lista)

        combo.DataBind()

    End Sub

    ' *** CARGA PAISES EN COMBO
    '==========================================================
    Public Sub Get_Pais_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)
        Dim consulta As String = ""
        consulta = "select " _
        & "GEN_nombrePais, GEN_idPais " _
        & "FROM " _
        & "GEN_Pais " _
        & "order by GEN_nombrePais"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombrePais").ToString, CStr(tb.Rows(i).Item("GEN_idPais").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    ' *** CARGA PERFILES DISPONIBLES PARA SISTEMA DE ANATOMIA (ANATOMIA =1)
    '==========================================================
    Public Sub Get_Perfil_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)
        Dim consulta As String = ""
        consulta = "SELECT " _
        & "GEN_idPerfil, GEN_descripcionPerfil " _
        & "FROM " _
        & "GEN_Perfil " _
        & "WHERE (GEN_IdSistemas = 1)"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(Trim(tb.Rows(i).Item("GEN_descripcionPerfil").ToString()), CStr(tb.Rows(i).Item("GEN_idPerfil").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    ' *** CARGA TIPOS DE PREVISION O TIPOS DE ASEGURADORA
    '==========================================================
    Public Sub Get_Prevision_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim consulta As String = ""
        consulta = "SELECT " _
        & "GEN_idPrevision, GEN_nombrePrevision " _
        & "FROM " _
        & "GEN_Prevision " _
        & "WHERE " _
        & "GEN_Prevision.GEN_estadoPrevision = 'ACTIVO'"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombrePrevision").ToString, Trim(CStr(tb.Rows(i).Item("GEN_idPrevision").ToString())))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    ' *** CARGA TIPOS DE PROFESIONES DE TIPO ACTIVO EN COMBO
    '==========================================================
    Public Sub Get_Profesion_combobox(ByVal combo As DropDownList)
        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT " _
        & "GEN_idProfesion, GEN_nombreProfesion " _
        & "FROM " _
        & "GEN_Profesion " _
        & "WHERE " _
        & "GEN_estadoProfesion = 'ACTIVO' " _
        & "ORDER BY GEN_nombreProfesion"

        tb = fun_sql.consulta_sql_datatable(consulta)
        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombreProfesion").ToString, CStr(tb.Rows(i).Item("GEN_idProfesion").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub


    Public Sub Get_ServiciosHCM_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem

        combo.Items.Clear()
        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)
        Dim consulta As String = "SELECT " _
        & "GEN_idServicio, GEN_nombreServicio " _
        & "FROM " _
        & "GEN_Servicio " _
        & "WHERE " _
        & "GEN_dependenciaServicio='HCM' AND " _
        & "not (GEN_nombreServicio is NULL) " _
        & "order by GEN_nombreServicio"
        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombreServicio").ToString, CStr(tb.Rows(i).Item("GEN_idServicio").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Get_ProgramasHCM_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem

        combo.Items.Clear()
        Dim consulta As String = "SELECT " _
        & "GEN_IdPrograma, GEN_NomPrograma " _
        & "FROM GEN_programa " _
        & "where GEN_estadoPrograma = 'Activo' " _
        & "order by GEN_NomPrograma"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_NomPrograma").ToString, CStr(tb.Rows(i).Item("GEN_IdPrograma").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Get_ServiciosHCM_combobox(ByVal combo As DropDownList, ByVal tipo As String)

        Dim lista As New ListItem

        combo.Items.Clear()
        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim consulta As String = "SELECT GEN_idServicio, GEN_nombreServicio " _
        & "FROM GEN_Servicio " _
        & "WHERE GEN_dependenciaServicio='" & tipo & "' AND " _
        & "not (GEN_nombreServicio is NULL) " _
        & "order by GEN_nombreServicio"

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombreServicio").ToString, CStr(tb.Rows(i).Item("GEN_idServicio").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub


    Public Sub Get_Profesionales_combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT " _
        & "ISNULL(RTRIM(gpl.GEN_apellidoProfesional) + ' ', '')  + " _
        & "ISNULL(RTRIM(gpl.GEN_sapellidoProfesional) + ' ', '') + " _
        & "ISNULL(RTRIM(gpl.GEN_nombreProfesional), '') AS nombre_m, gpl.GEN_idProfesional " _
        & "FROM GEN_Pro_Profesion AS gpp INNER JOIN " _
        & "GEN_Profesion AS gpn ON " _
        & "gpp.GEN_idProfesion = gpn.GEN_idProfesion INNER JOIN " _
        & "GEN_Profesional AS gpl ON " _
        & "gpp.GEN_idProfesional = gpl.GEN_idProfesional " _
        & "WHERE ORDER BY nombre_m"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("nombre_m").ToString, CStr(tb.Rows(i).Item("GEN_idProfesional").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    '========================================
    'PROFESIONALES PARA LISTADO DE INGRESO DE MUESTRAS
    '========================================
    Public Sub Get_Profesionales_combobox(ByVal combo As DropDownList, ByVal dependencia As String)

        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)
        Dim sql As New StringBuilder()

        sql.AppendLine("SELECT")
        sql.AppendLine("GEN_Profesional.GEN_idProfesional,")
        sql.AppendLine("GEN_Personas.GEN_apellido_paternoPersonas + ' ' +  ISNULL(GEN_Personas.GEN_apellido_maternoPersonas + ' ' , '') 
                        + GEN_Personas.GEN_nombrePersonas
                         AS nombre_m")
        sql.AppendLine("FROM")
        sql.AppendLine("GEN_Profesional INNER JOIN")
        sql.AppendLine("GEN_Personas ON GEN_Personas.GEN_idPersonas = GEN_Profesional.GEN_idPersonas INNER JOIN")
        sql.AppendLine("GEN_Pro_Profesion ON GEN_Profesional.GEN_idProfesional = GEN_Pro_Profesion.GEN_idProfesional")
        sql.AppendLine("where GEN_Pro_Profesion.GEN_idProfesion=1")



        If dependencia <> Nothing Then
            sql.AppendLine("AND GEN_Profesional.GEN_dependenciaProfesional='" & dependencia & "')")
        End If
        sql.AppendLine("ORDER BY nombre_m")

        Dim tb As New DataTable
        tb = fun_sql.consulta_sql_datatable(sql.ToString())

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("nombre_m").ToString, CStr(tb.Rows(i).Item("GEN_idProfesional").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Get_Region_combobox(ByVal combo As DropDownList, ByVal IdPais As Integer)
        Dim lista As New ListItem

        combo.Items.Clear()

        lista = New ListItem("--Seleccione--", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT GEN_nombreRegion, GEN_idRegion " _
        & "FROM GEN_Region " _
        & "WHERE GEN_idPais=" & IdPais & " " _
        & "order by GEN_nombreRegion"
        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombreRegion").ToString, CStr(tb.Rows(i).Item("GEN_idRegion").ToString()))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub


    '---------------------------------------------------------------------
    ' LLENA EL DROPDOWNLIST DE TRAMO PREVISIONAL
    '---------------------------------------------------------------------
    Public Sub Get_tramo_prevision_combobox(ByVal combo As DropDownList, ByVal id As String)
        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String = "SELECT GEN_idPrevision_Tramo, GEN_descripcionPrevision_Tramo as descripcion " _
        & "FROM GEN_Prevision_Tramo " _
        & "WHERE GEN_idPrevision = " & id & " " _
        & "ORDER BY descripcion"

        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("descripcion").ToString, Trim(CStr(tb.Rows(i).Item("GEN_idPrevision_Tramo").ToString())))
            combo.Items.Add(lista)
        Next
        combo.DataBind()
    End Sub

    '-----------------------------------------------------------------------------------------------------------------------------------------
    ' LLENA EL DROPDOWNLIST DE 1RA JERARQUIA
    '-----------------------------------------------------------------------------------------------------------------------------------------
    Public Sub Get_1rajerarquia_Combobox(ByVal combo As DropDownList)

        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String
        consulta = "SELECT GEN_id1ra_Jerarquia, GEN_nombre1ra_Jerarquia " _
        & "FROM GEN_1ra_Jerarquia " _
        & "WHERE GEN_estado1ra_Jerarquia = 'Activo' " _
        & " ORDER BY GEN_id1ra_Jerarquia "

        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombre1ra_Jerarquia").ToString, Trim(CStr(tb.Rows(i).Item("GEN_id1ra_Jerarquia").ToString())))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

    Public Sub Get_2dajerarquia_Combobox(ByVal combo As DropDownList, ByVal id1ra As String)
        Dim lista As New ListItem
        combo.Items.Clear()
        lista = New ListItem("- Seleccione -", "0")
        combo.Items.Add(lista)

        Dim tb As New DataTable
        Dim consulta As String

        consulta = "SELECT GEN_id2da_Jerarquia ,GEN_nombre2da_Jerarquia " _
        & "FROM GEN_2da_Jerarquia " _
        & "WHERE GEN_id1ra_Jerarquia = " & id1ra & " " _
        & " AND GEN_estado2da_Jerarquia = 'Activo' " _
        & "ORDER BY GEN_nombre2da_Jerarquia"

        tb = fun_sql.consulta_sql_datatable(consulta)

        For i = 0 To (tb.Rows.Count - 1)
            lista = New ListItem(tb.Rows(i).Item("GEN_nombre2da_Jerarquia").ToString, Trim(CStr(tb.Rows(i).Item("GEN_id2da_Jerarquia").ToString())))
            combo.Items.Add(lista)
        Next
        combo.DataBind()

    End Sub

End Class