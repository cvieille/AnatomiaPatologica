﻿Imports iTextSharp.text.pdf

Public Class InformeBiopsia
    Inherits DatoMuestra

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Public RutaInformes As String = "C://GIAP/GIAP/dcto/inf/"
    Public RutaInformesValidados As String = "C://GIAP/GIAP/dcto/InfValidados/"
    Public RutaLogo As String = "C://GIAP/GIAP/imagenes/logo.jpg"
    Public RutaPiePagina As String = "C://GIAP/GIAP/imagenes/pie.jpg"

    Public ruta_InfPDF As String = "C://GIAP/GIAP/dcto/varios/"
    Public cadena_archivos As String = "/GIAP/dcto/inf/"
    Public TituloInforme As String = " INFORME DE BIOPSIA Nº"

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region


    Public Function GenerarInforme(ByVal TipoImpresion As String)
        Try

        
            Dim reporte As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER) 'tipo carta 
            Dim Frase As iTextSharp.text.Phrase
            Dim Parrafo As iTextSharp.text.Paragraph
            Dim archivo As String

            Dim rm As New Registro_Biopsia
            rm.Registro_Biopsia(Get_ANA_IdBiopsia())

            '??
            Me.Set_ANA_NumBiopsia(rm.Get_ANA_NumBiopsia().ToString())
            Me.Set_ANA_AñoBiopsia(rm.Get_ANA_AñoBiopsia().ToString())

            'SE DECLARA LA RUTA EN SERVIDOR DONDE QUEDA UNA COPIA DEL PDF
            Dim nombre_archivo As String = "Biopsia" + rm.Get_NumBiopsiaCompleto() & "-" + DateTime.Now.ToString("yyyymmddhhmmss") + ".pdf"

            If TipoImpresion = "Temporal" Then
                archivo = RutaInformes & nombre_archivo
            Else
                ' GUARDA UNA COPIA DEL INFORME AL MOMENTO DE VALIDAR. ESTO PARA TENER TRAZABILIDAD DE INFORME VALIDADO.
                archivo = RutaInformesValidados & nombre_archivo
            End If

            Dim Writer As PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(reporte, New IO.FileStream(archivo, IO.FileMode.Create))

            Dim Tabla As iTextSharp.text.pdf.PdfPTable
            Dim Celda As iTextSharp.text.pdf.PdfPCell

            Parrafo = New iTextSharp.text.Paragraph()
            Parrafo.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            Parrafo.Leading = 10.0!

            'ABRO EL DOCUMENTO
            reporte.Open()

            'AGREGO EL LOGO EN EL BORDE SUPERIOR IZQUIERDO
            Dim jpga As iTextSharp.text.Image 'Declaracion de una imagen
            jpga = iTextSharp.text.Image.GetInstance(RutaLogo) 'Dirreccion a la imagen que se hace referencia
            jpga.ScaleAbsoluteWidth(180) 'Ancho de la imagen
            jpga.ScaleAbsoluteHeight(80) 'Altura de la imagen
            'reporte.Add(jpga)

            'AGREGO LA PRIMERA TABLA 
            Tabla = New iTextSharp.text.pdf.PdfPTable(4)
            'Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT

            Tabla.WidthPercentage = "100"
            Celda = New iTextSharp.text.pdf.PdfPCell(jpga)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
            Tabla.SetWidths(New Single() {35.0!, 30.0!, 20.0!, 15.0!})
            Celda.Rowspan = 8
            Celda.Colspan = 2
            Celda.Border = 0
            Tabla.AddCell(Celda)

            'NÂº de biopsia
            Frase = New iTextSharp.text.Phrase("N° Biopsia:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = 0
            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(rm.Get_NumBiopsiaCompleto(), iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
            Tabla.AddCell(Celda)

            'fecha de recepcion
            Frase = New iTextSharp.text.Phrase("Recepción:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = 0
            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(rm.Get_ANA_fec_RecepcionRegistro_Biopsias(), iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
            Tabla.AddCell(Celda)

            'fecha de informe:
            Frase = New iTextSharp.text.Phrase("Fec. Informe:", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = 0
            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(rm.Get_ANA_fecValidaBiopsia, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
            Tabla.AddCell(Celda)

            reporte.Add(Tabla)
            'AGREGO EL TITULO DEL REPORTE
            Tabla = New iTextSharp.text.pdf.PdfPTable(1)
            Tabla.WidthPercentage = "100"
            Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            TituloInforme = TituloInforme & rm.Get_NumBiopsiaCompleto()

            Frase = New iTextSharp.text.Phrase(TituloInforme)
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.Colspan = 4
            Celda.Border = 0
            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(" ")
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.Colspan = 4
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)

            'ESTA ES LA PRIMERA FILA DEL DOCUMENTO
            Tabla = New iTextSharp.text.pdf.PdfPTable(4)
            Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Tabla.WidthPercentage = "100"
            Tabla.SetWidths(New Single() {20.0!, 30.0!, 20.0!, 30.0!})

            ' *** INSTANCIA EL CONSTRUCTOR DE PACIENTES
            '==========================================================
            Dim p As New Pacientes
            p.Pacientes(rm.Get_GEN_Id_Paciente)

            imprime_campos_bold("Nombre Paciente :", Celda, Tabla)
            imprime_campos_bold_no(p.Get_GEN_Nombre(), Celda, Tabla)

            imprime_campos_bold("Apellidos :", Celda, Tabla)
            imprime_campos_bold_no(p.Get_GEN_ApePaterno() & " " & p.Get_GEN_ApeMaterno(), Celda, Tabla)

            imprime_campos_bold("Rut :", Celda, Tabla)
            If p.Get_GEN_digitoPaciente() = "z" Then
                imprime_campos_bold_no(p.Get_GEN_numero_documentoPaciente(), Celda, Tabla)
            Else
                imprime_campos_bold_no(p.Get_GEN_numero_documentoPaciente() & "-" & p.Get_GEN_digitoPaciente(), Celda, Tabla)
            End If


            imprime_campos_bold("Nº Ubicación Interna:", Celda, Tabla)
            imprime_campos_bold_no(p.Get_GEN_NuiPaciente(), Celda, Tabla)

            imprime_campos_bold("Edad:", Celda, Tabla)
            imprime_campos_bold_no(p.Get_Edad(), Celda, Tabla)

            imprime_campos_bold("Fecha Nacimiento:", Celda, Tabla)
            imprime_campos_bold_no(p.Get_GEN_FecNacimientoPaciente(), Celda, Tabla)

            imprime_campos_bold("Servicio Origen:", Celda, Tabla)
            imprime_campos_bold_no(rm.Get_Nom_Servicio_Solicita(rm.Get_GEN_IdServicioOrigen()), Celda, Tabla)

            imprime_campos_bold("Servicio Destino:", Celda, Tabla)
            imprime_campos_bold_no(rm.Get_Nom_Servicio_Destino(rm.Get_GEN_IdServicioDestino), Celda, Tabla)

            imprime_campos_bold("Derivado por:", Celda, Tabla)
            imprime_campos_bold_no(rm.Get_Nom_Medico_Solicita(rm.Get_ANA_IdMedSolicitaBiopsia), Celda, Tabla)

            ' *** LLAMA A CLASE DE NEGOCIOS PARA TRAER DETALLE DE TODOS LOS FRASCOS.
            ' *** LOS JUNTA EN UN STRING Y LOS DEVUELVE EN GET_DETALLEFRASCOSBIOPSIA
            Dim nrm As New NegocioRegistroMuestra
            nrm.Set_ANA_IdBiopsia(rm.Get_ANA_IdBiopsia())
            imprime_campos_bold("Muestra:", Celda, Tabla)
            imprime_campos_bold_no(nrm.Get_DetalleFrascosBiopsia, Celda, Tabla)

            Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.Colspan = 4
            Celda.Border = 0
            Celda.Border = 0
            Tabla.AddCell(Celda)


            'SE AGREGA LA SEGUNDA TABLA
            reporte.Add(Tabla)

            'COMIENZO A CREAR LA SEGUNDA TABLA
            Tabla = New iTextSharp.text.pdf.PdfPTable(1)
            Tabla.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {100.0!})

            'FILA 1
            Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)

            Dim det As New DescripcionBiopsia
            det.DescripcionBiopsia(rm.Get_ANA_IdBiopsia())

            If det.Get_Ana_DescAntClinicos() <> "" Then

                imprime_campos_bold("ANTECEDENTES CLÍNICOS:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_DescAntClinicos(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)

            End If

            'FILA 2
            If det.Get_Ana_DescMacroscopica() <> "" Then

                imprime_campos_bold("DESCRIPCIÓN MACROSCOPICA:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_DescMacroscopica(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)

            End If

            'FILA 3        
            If det.Get_Ana_DescMicroscopica() <> "" Then
                imprime_campos_bold("EXAMEN MICROSCOPICO:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_DescMicroscopica(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)
            End If

            'FILA 4        
            If det.Get_Ana_DescDiagnostico() <> "" Then
                imprime_campos_bold("DIAGNOSTICO:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_DescDiagnostico(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)
            End If

            'FILA 5
            If det.Get_Ana_DescNota() <> "" Then
                imprime_campos_bold("NOTAS:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_DescNota(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)
            End If

            'FILA 6
            If det.Get_Ana_EstuInmuno() <> "" Then
                imprime_campos_bold("Estudio Inmunohistoquímico:", Celda, Tabla)
                imprime_campos_bold_no(det.Get_Ana_EstuInmuno(), Celda, Tabla)
                imprime_espacios_en_blanco(Celda, Tabla)
            End If

            'FILA 7 - MEDICO Y FIRMA
            ' *** INSTANCIA EL CLASE USUARIOS
            '==========================================================
            Dim uv As New Usuarios
            uv.Usuarios(CInt(rm.Get_GEN_idUsuarioValida()))
            imprime_campos_bold("Patólogo Responsable: Anatomopatologo " & uv.Get_GEN_NombreCompleto(), Celda, Tabla)

            Frase = New iTextSharp.text.Phrase("El presente Informe deberá contar con firma del Médico Patálogo Responsable y timbre de Anatomía Patológica para su validez", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER
            Celda.Border = 1
            Tabla.AddCell(Celda)


            Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.Colspan = 2
            Celda.Border = 1
            Tabla.AddCell(Celda)

            Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.Colspan = 2
            Celda.Border = 0
            Tabla.AddCell(Celda)

            ' *** INSTANCIA EL CLASE USUARIOS
            '==========================================================
            Dim u As New Usuarios
            u.Usuarios(CInt(Me.Get_GEN_IdUsuarios()))

            Frase = New iTextSharp.text.Phrase("Datos de Impresión: Nº de Biopsia: " & rm.Get_NumBiopsiaCompleto() & " - Fecha: " & Now & " - Usuario: " & u.Get_GEN_loginUsuarios(), iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 6, iTextSharp.text.BaseColor.BLACK))
            Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
            Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_RIGHT
            Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
            Celda.Border = 0
            Tabla.AddCell(Celda)

            imprime_espacios_en_blanco(Celda, Tabla)

            'SE AGREGA LA CUARTA TABLA
            reporte.Add(Tabla)

            'AGREGO EL PIE DE PAGINA
            jpga = iTextSharp.text.Image.GetInstance(RutaPiePagina) 'Direccion a la imagen que se hace referencia
            jpga.SetAbsolutePosition(40, 35) 'Posicion en el eje cartesiano
            jpga.ScaleAbsoluteWidth(525) 'Ancho de la imagen
            jpga.ScaleAbsoluteHeight(30) 'Altura de la imagen
            reporte.Add(jpga)



            'SE CIERRA EL DOCUMENTO
            reporte.Close()
            Dim FILECREADO As String = nombre_archivo
            Return nombre_archivo
            'System.Diagnostics.Process.Start(archivo)
        Catch ex As Exception

        End Try
    End Function

    Private Sub imprime_campos_bold(ByVal texto As String, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase
        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA_BOLD, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.Border = 0
        tabla.AddCell(celda)
    End Sub

    Private Sub imprime_campos_bold_no(ByVal texto As Object, ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase

        Frase = New iTextSharp.text.Phrase(texto, iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_JUSTIFIED
        celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE
        celda.Border = 0
        tabla.AddCell(celda)

    End Sub

    Public Sub imprime_espacios_en_blanco(ByVal celda As Object, ByVal tabla As Object)
        Dim Frase As iTextSharp.text.Phrase
        Frase = New iTextSharp.text.Phrase(" ", iTextSharp.text.FontFactory.GetFont(iTextSharp.text.FontFactory.HELVETICA, 10, iTextSharp.text.BaseColor.BLACK))
        Celda = New iTextSharp.text.pdf.PdfPCell(Frase)
        Celda.Colspan = 2
        Celda.Border = 0
        Celda.Border = 0
        Tabla.AddCell(Celda)
    End Sub

End Class