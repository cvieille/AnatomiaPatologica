﻿
Public Class Registro_Biopsia : Inherits DatoBiopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Dim consulta As String
    Dim ANA_SolicitadaBiopsia As String = Nothing
    Dim ANA_ValidadaBiopsia As String = Nothing
    Dim ANA_DespachadaBiopsia As String = Nothing

    Dim ANA_DictadaBiopsia As String = Nothing
    Dim ANA_TumoralBiopsia As String = Nothing

    Dim ANA_MotivoAnulaBiopsia As String
    Dim GEN_IdUsuarioTecnologo As Integer = Nothing


    Dim ANA_CriticoBiopsia As String
    Dim ANA_fecDespBiopsia As Date = Nothing
    Dim ANA_fecValidaBiopsia As Date
    Dim GEN_idUsuarioValida As Integer = Nothing, GEN_IdUsuarioDespacha As Integer = Nothing
    Dim ANA_NomRecibeBiopsia As String


    Dim ANA_InterconsultaBiopsia As String = "NO"
    Dim ANA_CodificadaEstadoRegistro_Biopsias As String
    Dim ANA_UltActualizacionBiopsia As String
    Dim ANA_RevalidadaBiopsia As String

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Registro_Biopsia(ByVal id_b As Integer)
        ' *** CONSTRUCTOR DE LA CLASE
        '==========================================================
        Try
            consulta = "SELECT * FROM  ANA_Registro_Biopsias WHERE ANA_IdBiopsia=" & id_b
            Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

            ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
            ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
            '==========================================================
            If tablas.Rows.Count > 0 Then

                Dim filas As Data.DataRow = tablas.Rows(0)

                Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())

                Me.Set_ANA_NumBiopsia(filas.Item("ANA_numeroRegistro_Biopsias").ToString())

                Me.Set_ANA_AñoBiopsia(filas.Item("ANA_AñoRegistro_Biopsias").ToString())

                If Not IsDBNull(Trim(filas.Item("ANA_AlmacenadaBiopsia").ToString())) Then
                    Me.Set_ANA_Almacenada(Trim(filas.Item("ANA_AlmacenadaBiopsia").ToString()))
                End If

                If Not IsDBNull(filas.Item("ANA_SolicitadaBiopsia").ToString()) Then
                    Me.Set_ANA_SolicitadaBiopsia(filas.Item("ANA_SolicitadaBiopsia").ToString())
                End If

                If Not IsDBNull(Trim(filas.Item("GEN_IdServicioOrigen").ToString())) Then
                    Me.Set_GEN_IdServicioOrigen(Trim(filas.Item("GEN_IdServicioOrigen").ToString()))
                End If

                If Not IsDBNull(Trim(filas.Item("GEN_IdServicioDestino").ToString())) Then
                    Me.Set_GEN_IdServicioDestino(Trim(filas.Item("GEN_IdServicioDestino").ToString()))
                End If

                If IsNumeric(filas.Item("GEN_IdUsuarioPatologo").ToString()) Then
                    Me.Set_GEN_IdUsuarioPatologo(filas.Item("GEN_IdUsuarioPatologo").ToString())
                End If

                If IsNumeric(filas.Item("GEN_IdUsuarioTecnologo").ToString()) Then
                    Me.Set_GEN_IdUsuarioTecnologo(filas.Item("GEN_IdUsuarioTecnologo").ToString())
                End If

                Me.Set_ANA_TipoDestino(Trim(filas.Item("ANA_TipoDestino").ToString()))
                Me.Set_ANA_TipoOrigen(Trim(filas.Item("ANA_TipoOrigen").ToString()))
                Me.Set_ANA_CriticoBiopsia(filas.Item("ANA_CriticoBiopsia").ToString())

                If IsNumeric(filas.Item("GEN_idTipo_Estados_Sistemas").ToString()) Then
                    Me.Set_GEN_idTipo_Estados_Sistemas(filas.Item("GEN_idTipo_Estados_Sistemas").ToString())
                    Me.Set_ANA_EstadoRegistro_Biopsias(Getnombre_estado_sistema(Get_GEN_idTipo_Estados_Sistemas()))
                End If

                Me.Set_ANA_fec_IngresoRegistro_Biopsias(filas.Item("ANA_fec_IngresoRegistro_Biopsias").ToString())
                Me.Set_ANA_fec_RecepcionRegistro_Biopsias(filas.Item("ANA_fec_RecepcionRegistro_Biopsias").ToString())

                If IsDate(filas.Item("ANA_fecValidaBiopsia").ToString()) Then
                    Me.Set_ANA_fecValidaBiopsia(filas.Item("ANA_fecValidaBiopsia").ToString())
                End If

                Me.Set_ANA_DespachadaBiopsia(filas.Item("ANA_DespachadaBiopsia").ToString())

                If IsDate(filas.Item("ANA_fecDespBiopsia").ToString()) Then
                    Me.Set_ANA_fecDespBiopsia(filas.Item("ANA_fecDespBiopsia").ToString())
                End If

                If Not IsDBNull(filas.Item("ANA_NomRecibeBiopsia").ToString()) Then
                    Me.Set_ANA_NomRecibeBiopsia(filas.Item("ANA_NomRecibeBiopsia").ToString())
                End If

                Me.Set_ANA_RevalidadaBiopsia(filas.Item("ANA_RevalidadaBiopsia").ToString())
                Me.Set_ANA_UltActualizacionBiopsia(filas.Item("ANA_UltActualizacionBiopsia").ToString())

                Me.Set_ANA_GesRegistro_Biopsias(filas.Item("ANA_GesRegistro_Biopsias").ToString())
                Me.Set_ANA_IdCatalogoBiopsia(filas.Item("ANA_IdCatalogo_Muestras").ToString())
                Me.Set_ANA_IdDetalleCatalogoBiopsia(filas.Item("ANA_IdDetalleCatalogo_Muestras").ToString())

                Me.ANA_MotivoAnulaBiopsia = filas.Item("ANA_MotivoAnulaBiopsia").ToString

                If IsNumeric(filas.Item("GEN_idPaciente").ToString()) Then
                    Me.Set_GEN_idPaciente(filas.Item("GEN_idPaciente").ToString())
                Else
                    Set_GEN_idPaciente(0)
                End If

                If IsNumeric(filas.Item("GEN_idUsuarioValida").ToString()) Then
                    Me.Set_GEN_idUsuarioValida(filas.Item("GEN_idUsuarioValida").ToString())
                End If

                Me.Set_ANA_IdMedSolicitaBiopsia(filas.Item("ANA_IdMedSolicitaBiopsia").ToString())

                Me.Set_ANA_OrganoBiopsia(Trim(filas.Item("ANA_OrganoBiopsia").ToString()))
                If Not IsDBNull(filas.Item("GEN_IdPrograma").ToString()) Then
                    Me.Set_GEN_IdPrograma(filas.Item("GEN_IdPrograma").ToString())
                End If

                Me.Set_ANA_CodificadaEstadoRegistro_Biopsias(filas.Item("ANA_CodificadaEstadoRegistro_Biopsias").ToString())

                If Not IsDBNull(filas.Item("ANA_TumoralBiopsia").ToString()) Then
                    Me.Set_ANA_TumoralBiopsia(filas.Item("ANA_TumoralBiopsia").ToString())
                End If

                If Not IsDBNull(filas.Item("ANA_DictadaBiopsia").ToString()) Then
                    Me.Set_ANA_DictadaBiopsia(Trim(filas.Item("ANA_DictadaBiopsia").ToString()))
                End If

                If IsNumeric(filas.Item("PAB_idModalidad").ToString()) Then
                    Me.Set_PAB_idModalidad(filas.Item("PAB_idModalidad").ToString())
                Else
                    Me.Set_PAB_idModalidad(1)
                End If

                Me.Set_ANA_NumBiopsia(CLng(filas.Item("ANA_NumeroRegistro_Biopsias").ToString()))
                Me.ANA_ValidadaBiopsia = filas.Item("ANA_ValidadaBiopsia").ToString()

                Me.Set_ANA_InterconsultaBiopsia(filas.Item("ANA_InterconsultaBiopsia").ToString())
                Me.Set_ANA_Desechada(filas.Item("ANA_DesechadaBiopsia").ToString())



            End If

        Catch ex As Exception

        End Try

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    'Public Function Set_Consulta_CrearRegistroBiopsia()
    '    'Dim v As New Valores_Sistema

    '    'v.Valores_Sistema()

    '    'Me.Set_ANA_NumBiopsia(v.Get_NumBiopsia)
    '    'Me.Set_ANA_AñoBiopsia(v.Get_AñoBiopsia)
    '    Dim m As New MetodosGenerales

    '    Me.Set_ANA_NumBiopsia(m.GetPropiedad("n_biopsia", True))
    '    Me.Set_ANA_AñoBiopsia(m.GetPropiedad("a_biopsia", True))

    '    consulta = "INSERT INTO ANA_Registro_Biopsias (" _
    '    & "ANA_NumeroRegistro_Biopsias, " _
    '    & "ANA_AñoRegistro_Biopsias, " _
    '    & "ANA_fec_IngresoRegistro_Biopsias, " _
    '    & "ANA_fec_RecepcionRegistro_Biopsias, " _
    '    & "GEN_idPaciente, " _
    '    & "ANA_GesRegistro_Biopsias, " _
    '    & "ANA_TipoOrigen, " _
    '    & "GEN_IdServicioOrigen, " _
    '    & "ANA_TipoDestino, " _
    '    & "GEN_IdServicioDestino, " _
    '    & "GEN_IdPrograma, " _
    '    & "ANA_IdMedSolicitaBiopsia, " _
    '    & "ANA_EstadoRegistro_Biopsias, " _
    '    & "ANA_IdCatalogo_Muestras, " _
    '    & "ANA_IdDetalleCatalogo_Muestras, " _
    '    & "PAB_idModalidad, " _
    '    & "ANA_OrganoBiopsia, " _
    '    & "GEN_idTipo_Estados_Sistemas, ANA_UltActualizacionBiopsia ) " _
    '    & "VALUES(" & Me.Get_ANA_NumBiopsia() & ", " _
    '    & Me.Get_ANA_AñoBiopsia() & ", " _
    '    & "'" & Me.Get_ANA_fec_IngresoRegistro_Biopsias() & "', " _
    '    & "'" & Me.Get_ANA_fec_RecepcionRegistro_Biopsias() & "', " _
    '    & Me.Get_GEN_Id_Paciente() & ", " _
    '    & "'" & Me.Get_ANA_GesRegistro_Biopsias() & "', " _
    '    & "'" & Me.Get_ANA_TipoOrigen() & "', " _
    '    & Me.Get_GEN_IdServicioOrigen() & ", " _
    '    & "'" & Me.Get_ANA_TipoDestino() & "', " _
    '    & Me.Get_GEN_IdServicioDestino() & ", " _
    '    & Me.Get_GEN_IdPrograma() & ", " _
    '    & Me.Get_ANA_IdMedSolicitaBiopsia() & ", " _
    '    & "'" & Me.Get_ANA_EstadoRegistro_Biopsias() & "', " _
    '    & Me.Get_ANA_IdCatalogoBiopsia() & ", " _
    '    & Me.Get_ANA_IdDetalleCatalogoBiopsia() & ", '" _
    '    & Me.Get_PAB_idModalidad() & "', " _
    '    & "'" & Me.Get_ANA_OrganoBiopsia() & "', " _
    '    & "42, '" & Now() & "')"

    '    ejecuta_sql(consulta)

    '    consulta = "Select max(ANA_IdBiopsia) as ANA_IdBiopsia FROM  ANA_Registro_Biopsias " _
    '    & "WHERE(ANA_NumeroRegistro_Biopsias = " & Me.Get_ANA_NumBiopsia() & " " _
    '    & "And ANA_AñoRegistro_Biopsias = " & Me.Get_ANA_AñoBiopsia() & ")"

    '    'v.Set_ANA_Num_BiopsiaConfiguracion(v.Get_NumBiopsia() + 1)
    '    m.SetPropiedad("n_biopsia", m.GetPropiedad("n_biopsia", True) + 1)


    '    Return consulta_sql_devuelve_string(consulta)

    'End Function

    'Public Sub Set_ModificaRegistroBiopsia()

    '    Try
    '        Dim sql As String =
    '            "UPDATE ANA_Registro_Biopsias SET " _
    '                & "ANA_fec_IngresoRegistro_Biopsias='" & Me.Get_ANA_fec_IngresoRegistro_Biopsias() & "', " _
    '                & "GEN_idPaciente=" & Get_GEN_IdPaciente() & ", " _
    '                & "ANA_GesRegistro_Biopsias='" & Me.Get_ANA_GesRegistro_Biopsias() & "', " _
    '                & "ANA_TipoOrigen='" & Me.Get_ANA_TipoOrigen() & "', " _
    '                & "GEN_IdServicioOrigen=" & Me.Get_GEN_IdServicioOrigen() & ", " _
    '                & "ANA_TipoDestino='" & Me.Get_ANA_TipoDestino() & "', " _
    '                & "GEN_IdServicioDestino=" & Me.Get_GEN_IdServicioDestino() & ", " _
    '                & "GEN_IdPrograma=" & Me.Get_GEN_IdPrograma() & ", " _
    '                & "PAB_idModalidad=" & Me.Get_PAB_idModalidad() & ", " _
    '                & "ANA_IdMedSolicitaBiopsia=" & Me.Get_ANA_IdMedSolicitaBiopsia() & ", " _
    '                & "ANA_DesechadaBiopsia='" & Me.Get_ANA_Desechada() & "', " _
    '                 & "ANA_EstadoRegistro_Biopsias = '" & Me.Get_ANA_EstadoRegistro_Biopsias() & "', " _
    '                & "ANA_CriticoBiopsia ='" & Me.Get_ANA_CriticoBiopsia() & "', "

    '        If Not Me.Get_ANA_Almacenada() = Nothing Then
    '            sql &= "ANA_AlmacenadaBiopsia='" & Me.Get_ANA_Almacenada() & "', "
    '        End If

    '        If Not Me.Get_GEN_IdUsuarioPatologo() = Nothing Then
    '            sql &= "GEN_IdUsuarioPatologo = " & Me.Get_GEN_IdUsuarioPatologo() & ", "
    '        Else
    '            sql &= "GEN_IdUsuarioPatologo = NULL, "
    '        End If

    '        If Not Me.Get_ANA_IdCatalogoBiopsia() = Nothing Then
    '            sql &= "ANA_IdCatalogo_Muestras=" & Me.Get_ANA_IdCatalogoBiopsia() & ", "
    '        End If

    '        If Not Me.Get_ANA_IdDetalleCatalogoBiopsia() = Nothing Then
    '            sql &= "ANA_IdDetalleCatalogo_Muestras=" & Me.Get_ANA_IdDetalleCatalogoBiopsia() & ", "
    '        End If

    '        If Not Me.Get_ANA_fec_RecepcionRegistro_Biopsias() = Nothing Then
    '            sql &= "ANA_fec_RecepcionRegistro_Biopsias='" & Me.Get_ANA_fec_RecepcionRegistro_Biopsias() & "', "
    '        End If

    '        If Not Get_ANA_RevalidadaBiopsia() = Nothing Then
    '            sql &= "ANA_RevalidadaBiopsia='" & Me.Get_ANA_RevalidadaBiopsia() & "', "
    '        End If

    '        If Not Get_GEN_idTipo_Estados_Sistemas() = Nothing Then
    '            sql &= "GEN_idTipo_Estados_Sistemas = " & Me.Get_GEN_idTipo_Estados_Sistemas() & ", "
    '        End If

    '        If Not Me.Get_GEN_idUsuarioValida() = Nothing Then
    '            sql &= "GEN_idUsuarioValida=" & Me.Get_GEN_idUsuarioValida() & ", "
    '        End If

    '        If Not Me.Get_ANA_InterconsultaBiopsia() = Nothing Then
    '            sql &= "ANA_InterconsultaBiopsia='" & Me.Get_ANA_InterconsultaBiopsia() & "', "
    '        End If

    '        If Not Get_ANA_fecValidaBiopsia() = Nothing Then
    '            sql &= "ANA_fecValidaBiopsia='" & Me.Get_ANA_fecValidaBiopsia() & "', "
    '        End If

    '        If Not Get_ANA_NomRecibeBiopsia() = Nothing Then
    '            sql &= "ANA_NomRecibeBiopsia='" & Me.Get_ANA_NomRecibeBiopsia() & "', "
    '        End If

    '        If Not Get_GEN_IdUsuarioDespacha() = Nothing Then
    '            sql &= "GEN_IdUsuarioDespacha=" & Me.Get_GEN_IdUsuarioDespacha() & ", "
    '        End If

    '        If Not Me.Get_GEN_IdUsuarioTecnologo() = Nothing Then
    '            sql &= "GEN_IdUsuarioTecnologo=" & Me.Get_GEN_IdUsuarioTecnologo() & ", "
    '        End If

    '        If Not Me.Get_ANA_fecDespBiopsia() = Nothing Then
    '            sql &= "ANA_fecDespBiopsia='" & Me.Get_ANA_fecDespBiopsia() & "', "
    '        End If

    '        sql &= "ANA_NumeroRegistro_Biopsias='" & Me.Get_ANA_NumBiopsia() & "', " _
    '                & "ANA_AñoRegistro_Biopsias='" & Me.Get_ANA_AñoBiopsia() & "', " _
    '                & "ANA_DictadaBiopsia='" & Me.Get_ANA_DictadaBiopsia() & "', " _
    '                & "ANA_TumoralBiopsia='" & Me.Get_ANA_TumoralBiopsia() & "', " _
    '
    '             
    '                & "ANA_UltActualizacionBiopsia='" & Now() & "', " _
    '                & "ANA_ValidadaBiopsia='" & Me.Get_ANA_ValidadaBiopsia() & "', " _
    '                & "ANA_SolicitadaBiopsia='" & Me.Get_ANA_SolicitadaBiopsia() & "', " _
    '                & "ANA_DespachadaBiopsia='" & Me.Get_ANA_DespachadaBiopsia() & "' " _
    '            & "WHERE ANA_IdBiopsia=" & Me.Get_ANA_IdBiopsia()

    '        ejecuta_sql(sql)

    '    Catch ex As Exception
    '        Dim fail As String = ex.Message & ex.StackTrace
    '    End Try

    'End Sub

    Public Sub Set_Consulta_EliminaBiopsia(ByVal id_b)
        Dim consulta As String = "DELETE " _
        & "FROM " _
        & "ANA_Registro_Biopsias " _
        & "WHERE(ANA_IdBiopsia = " & id_b & ")"
        ejecuta_sql(consulta)
    End Sub

    Public Function Get_Registro_biopsia_n_biopsia(ByVal n_biopsia As String, ByVal ANA_AñoRegistro_Biopsias As String)
        consulta = "SELECT ANA_IdBiopsia " _
        & "FROM " _
        & "ANA_Registro_Biopsias " _
        & "WHERE " _
        & "(ANA_NumeroRegistro_Biopsias = " & n_biopsia & " And " _
        & "ANA_AñoRegistro_Biopsias = " & ANA_AñoRegistro_Biopsias & ")"
        Return CLng(consulta_sql_devuelve_string(consulta))

    End Function

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================
    Public Sub Set_ANA_CodificadaEstadoRegistro_Biopsias(ByVal ANA_CodificadaEstadoRegistro_Biopsias)
        Me.ANA_CodificadaEstadoRegistro_Biopsias = ANA_CodificadaEstadoRegistro_Biopsias
    End Sub

    Public Sub Set_ANA_CriticoBiopsia(ByVal ANA_CriticoBiopsia)
        Me.ANA_CriticoBiopsia = ANA_CriticoBiopsia
    End Sub

    Public Sub Set_ANA_DespachadaBiopsia(ByVal ANA_DespachadaBiopsia)
        Me.ANA_DespachadaBiopsia = ANA_DespachadaBiopsia
    End Sub

    Public Sub Set_ANA_DictadaBiopsia(ByVal ANA_DictadaBiopsia)
        Me.ANA_DictadaBiopsia = ANA_DictadaBiopsia
    End Sub

    Public Sub Set_ANA_TumoralBiopsia(ByVal ANA_TumoralBiopsia)
        Me.ANA_TumoralBiopsia = ANA_TumoralBiopsia
    End Sub


    Public Sub Set_ANA_SolicitadaBiopsia(ByVal ANA_SolicitadaBiopsia)
        Me.ANA_SolicitadaBiopsia = ANA_SolicitadaBiopsia
    End Sub

    Public Sub Set_ANA_ValidadaBiopsia(ByVal ANA_ValidadaBiopsia)
        Me.ANA_ValidadaBiopsia = ANA_ValidadaBiopsia
    End Sub

    Public Sub Set_ANA_fecValidaBiopsia(ByVal ANA_fecValidaBiopsia)
        Me.ANA_fecValidaBiopsia = ANA_fecValidaBiopsia
    End Sub

    Public Sub Set_ANA_InterconsultaBiopsia(ByVal ANA_InterconsultaBiopsia)
        Me.ANA_InterconsultaBiopsia = ANA_InterconsultaBiopsia
    End Sub

    Public Sub Set_GEN_idUsuarioValida(ByVal GEN_idUsuarioValida)
        Me.GEN_idUsuarioValida = GEN_idUsuarioValida
    End Sub

    Public Sub Set_GEN_IdUsuarioTecnologo(ByVal GEN_IdUsuarioTecnologo)
        If IsNumeric(GEN_IdUsuarioTecnologo) Then
            If GEN_IdUsuarioTecnologo > 0 Then
                Me.GEN_IdUsuarioTecnologo = GEN_IdUsuarioTecnologo
            End If
        End If
    End Sub

    Public Sub Set_ANA_fecDespBiopsia(ByVal ANA_fecDespBiopsia)
        Me.ANA_fecDespBiopsia = ANA_fecDespBiopsia
    End Sub

    Public Sub Set_ANA_NomRecibeBiopsia(ByVal ANA_NomRecibeBiopsia)
        Me.ANA_NomRecibeBiopsia = ANA_NomRecibeBiopsia
    End Sub

    Public Sub Set_GEN_IdUsuarioDespacha(ByVal GEN_IdUsuarioDespacha)
        Me.GEN_IdUsuarioDespacha = GEN_IdUsuarioDespacha
    End Sub

    Public Sub Set_ANA_UltActualizacionBiopsia(ByVal ANA_UltActualizacionBiopsia)
        Me.ANA_UltActualizacionBiopsia = ANA_UltActualizacionBiopsia
    End Sub

    Public Sub Set_ANA_RevalidadaBiopsia(ByVal ANA_RevalidadaBiopsia)
        Me.ANA_RevalidadaBiopsia = ANA_RevalidadaBiopsia
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================
    Public Function Get_ANA_CodificadaEstadoRegistro_Biopsias()
        Return Me.ANA_CodificadaEstadoRegistro_Biopsias
    End Function

    Public Function Get_ANA_CriticoBiopsia()
        Return Me.ANA_CriticoBiopsia
    End Function

    Public Function Get_ANA_DespachadaBiopsia()
        Return Me.ANA_DespachadaBiopsia
    End Function

    Public Function Get_ANA_DictadaBiopsia()
        Return Me.ANA_DictadaBiopsia
    End Function

    Public Function Get_ANA_fecDespBiopsia()
        Return Me.ANA_fecDespBiopsia
    End Function

    Public Function Get_ANA_fecValidaBiopsia()
        Dim retorno As String = DBNull.Value.ToString
        ' *** SON LOS DOS FORMATOS DE FECHA QUE SE DEBEN EVALUAR PARA DECIR QUE TODAVIA NO SE DEFINE
        '==========================================================
        If Me.ANA_fecValidaBiopsia <> "01-01-1900 0:00:00" And Me.ANA_fecValidaBiopsia <> "#12:00:00 AM#" Then
            retorno = Me.ANA_fecValidaBiopsia
        End If
        Return retorno
    End Function

    Public Function Get_ANA_InterconsultaBiopsia()
        Return Me.ANA_InterconsultaBiopsia
    End Function

    Public Function Get_ANA_NomRecibeBiopsia()
        Return Me.ANA_NomRecibeBiopsia
    End Function

    Public Function Get_ANA_RevalidadaBiopsia()
        Return Me.ANA_RevalidadaBiopsia
    End Function

    Public Function Get_ANA_SolicitadaBiopsia()
        Return Me.ANA_SolicitadaBiopsia
    End Function

    Public Function Get_ANA_TumoralBiopsia()
        Return Me.ANA_TumoralBiopsia
    End Function

    Public Function Get_ANA_UltActualizacionBiopsia()
        Return Me.ANA_UltActualizacionBiopsia
    End Function

    Public Function Get_ANA_ValidadaBiopsia()
        Return Me.ANA_ValidadaBiopsia
    End Function

    Public Function Get_GEN_IdUsuarioDespacha()
        Return Me.GEN_IdUsuarioDespacha
    End Function

    Public Function Get_GEN_IdUsuarioTecnologo()
        Return Me.GEN_IdUsuarioTecnologo
    End Function

    Public Function Get_GEN_idUsuarioValida()
        Return Me.GEN_idUsuarioValida
    End Function

    Public Function Getnombre_estado_sistema(id As Integer)
        Dim sql As New StringBuilder()
        sql.AppendLine("select GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas from GEN_Tipo_Estados_Sistemas where GEN_idTipo_Estados_Sistemas=" & id)
        Return consulta_sql_devuelve_string(sql.ToString())
    End Function


    ' *** FIN DE GET
    '==========================================================
#End Region

End Class