﻿Imports System.IO

Public Class Log

    Public Shared Sub WriteLog(ByVal message As String)

        Dim filePath As String = HttpContext.Current.Server.MapPath("~/log.txt")
        Using fileStream As New FileStream(filePath, FileMode.Append)
            Using writter As New StreamWriter(fileStream)
                writter.WriteLine(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"))
                writter.WriteLine(message)
                writter.Close()
            End Using
        End Using

    End Sub

End Class
