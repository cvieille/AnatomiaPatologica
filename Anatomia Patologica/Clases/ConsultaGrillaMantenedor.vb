﻿Public Class ConsultaGrillaMantenedor

    Public Function Ds_ListaServicios()
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("GEN_idServicio,")
        sql.AppendLine("GEN_dependenciaServicio,")
        sql.AppendLine("GEN_nombreServicio,")
        sql.AppendLine("GEN_clasificacionServicio,")
        sql.AppendLine("GEN_codwinsigServicio")
        sql.AppendLine("FROM GEN_Servicio")
        sql.AppendLine("ORDER BY GEN_nombreServicio")
        Return sql.ToString()
    End Function

    Public Function Ds_ListaPlantillas()
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("p.ANA_idPlantilla,")
        sql.AppendLine("p.ANA_nombreplantilla,")
        sql.AppendLine("p.ANA_fec_creacionPlantilla,")
        sql.AppendLine("p.ANA_estadoPlantillas,")
        sql.AppendLine("u.GEN_loginUsuarios")
        sql.AppendLine("FROM ANA_plantillas AS p LEFT OUTER JOIN")
        sql.AppendLine("GEN_Usuarios AS u ON p.GEN_IdUsuarios = u.GEN_IdUsuarios")
        sql.AppendLine("order by p.ANA_nombreplantilla")
        Return sql.ToString()

    End Function

    Public Function Ds_ListaProgramas()
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("GEN_IdPrograma,")
        sql.AppendLine("GEN_NomPrograma,")
        sql.AppendLine("GEN_estadoPrograma")
        sql.AppendLine("FROM GEN_Programa")
        sql.AppendLine("ORDER BY GEN_NomPrograma")
        Return sql.ToString()

    End Function

    Public Function Ds_ListaTecnicas()
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT ")
        sql.AppendLine("ANA_IdTecnica, ")
        sql.AppendLine("ANA_nomTecnica, ")
        sql.AppendLine("ANA_estadoTecnica ")
        sql.AppendLine("FROM ")
        sql.AppendLine("ANA_Tecnicas_Especiales ")
        sql.AppendLine("ORDER BY ANA_nomTecnica")

        Return sql.ToString()
    End Function

    Public Function Ds_ListaInmuno()
        Dim sql As New StringBuilder()
        sql.AppendLine("SELECT")
        sql.AppendLine("ANA_idInmunoHistoquimica,")
        sql.AppendLine("ANA_NomInmunoHistoquimica,")
        sql.AppendLine("ANA_EstInmunoHistoquimica")
        sql.AppendLine("FROM ANA_Inmuno_Histoquimica")
        sql.AppendLine("ORDER BY")
        sql.AppendLine("ANA_NomInmunoHistoquimica")

        Return sql.ToString()
    End Function
End Class
