﻿Public Class DescripcionBiopsia : Inherits Registro_Biopsia

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Dim ANA_DescAntClinicos, ANA_DescDiagnostico, ANA_DescMacroscopica, ANA_DescMicroscopica, ANA_DescNota, ANA_EstuInmuno As String
    Dim ANA_IdDescripcion As Long
    Dim consulta As String

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub DescripcionBiopsia(ByVal IdBiopsia As Integer)

        consulta = "SELECT * " _
        & "FROM " _
        & "ANA_descripcion_biopsia " _
        & "WHERE " _
        & "(ANA_IdBiopsia = " & IdBiopsia & ")"

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)

        ' *** SI ENCUENTRA DATOS CARGA VALORES SOBRE ATRIBUTOS DE CLASE
        ' *** EN CASO CONTRARIO, LO DEJA CON LOS VALORES INICIALES DEFINIDOS A CADA ATRIBUTO
        '==========================================================
        Me.Set_ANA_IdBiopsia(IdBiopsia)

        If tablas.Rows.Count > 0 Then

            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.Set_Ana_DescAntClinicos(filas.Item("ANA_DescAntClinicos").ToString())
            Me.Set_Ana_DescDiagnostico(filas.Item("ANA_DescDiagnostico").ToString())
            Me.Set_Ana_DescMacroscopica(filas.Item("ANA_DescMacroscopica").ToString())
            Me.Set_Ana_DescMicroscopica(filas.Item("ANA_DescMicroscopica").ToString())
            Me.Set_Ana_DescNota(filas.Item("ANA_DescNota").ToString())
            Me.Set_Ana_EstuInmuno(filas.Item("ANA_EstuInmuno").ToString())
            Me.Set_Ana_Iddescripcion(filas.Item("ANA_IdDescripcion").ToString())
            Me.Set_ANA_IdBiopsia(filas.Item("ANA_IdBiopsia").ToString())

        End If

    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

    ' *** COMIENZA EL CRUD


    Public Sub Set_CrearDescripcionBiopsia()
        consulta = "INSERT INTO ANA_descripcion_biopsia(" _
        & "ANA_IdBiopsia, " _
        & "ANA_DescAntClinicos, " _
        & "ANA_DescMacroscopica, " _
        & "ANA_DescDiagnostico, " _
        & "ANA_DescNota, " _
        & "ANA_DescMicroscopica, " _
        & "ANA_EstuInmuno) " _
        & "VALUES (" _
        & Me.Get_ANA_IdBiopsia() & ", " _
        & "'" & Me.Get_Ana_DescAntClinicos() & "', " _
        & "'" & Me.Get_Ana_DescMacroscopica() & "', " _
        & "'" & Me.Get_Ana_DescDiagnostico() & "', " _
        & "'" & Me.Get_Ana_DescNota() & "', " _
        & "'" & Me.Get_Ana_DescMicroscopica() & "', " _
        & "'" & Me.Get_Ana_EstuInmuno() & "')"

        ejecuta_sql(consulta)
    End Sub

    Public Sub Set_UpdateDescripcionBiopsia()

        consulta = "UPDATE ANA_descripcion_biopsia SET " _
        & "ANA_IdBiopsia = " & Me.Get_ANA_IdBiopsia() & ", " _
        & "ANA_DescAntClinicos ='" & Me.Get_Ana_DescAntClinicos() & "', " _
        & "ANA_DescMacroscopica ='" & Me.Get_Ana_DescMacroscopica() & "', " _
        & "ANA_DescDiagnostico ='" & Me.Get_Ana_DescDiagnostico() & "', " _
        & "ANA_DescNota ='" & Me.Get_Ana_DescNota() & "', " _
        & "ANA_DescMicroscopica ='" & Me.Get_Ana_DescMicroscopica() & "', " _
        & "ANA_EstuInmuno ='" & Me.Get_Ana_EstuInmuno() & "' " _
        & "WHERE ANA_IdDescripcion = " & Me.Get_Ana_IdDescripcion()

        ejecuta_sql(consulta)

    End Sub

    Public Function Get_Ana_DescAntClinicos()
        Return Me.ANA_DescAntClinicos
    End Function

    Public Function Get_Ana_DescDiagnostico()
        Return Me.ANA_DescDiagnostico
    End Function

    Public Function Get_Ana_DescMacroscopica()
        Return Me.ANA_DescMacroscopica
    End Function

    Public Function Get_Ana_DescMicroscopica()
        Return Me.ANA_DescMicroscopica
    End Function

    Public Function Get_Ana_DescNota()
        Return Me.ANA_DescNota
    End Function

    Public Function Get_Ana_EstuInmuno()
        Return Me.ANA_EstuInmuno
    End Function

    Public Function Get_Ana_IdDescripcion()
        Return Me.ANA_IdDescripcion
    End Function

    Public Sub Set_Ana_DescAntClinicos(ByVal ANA_DescAntClinicos)
        Me.ANA_DescAntClinicos = ANA_DescAntClinicos
    End Sub

    Public Sub Set_Ana_DescDiagnostico(ByVal ANA_DescDiagnostico)
        Me.ANA_DescDiagnostico = ANA_DescDiagnostico
    End Sub

    Public Sub Set_Ana_DescMacroscopica(ByVal ANA_DescMacroscopica)
        Me.ANA_DescMacroscopica = ANA_DescMacroscopica
    End Sub

    Public Sub Set_Ana_DescMicroscopica(ByVal ANA_DescMicroscopica)
        Me.ANA_DescMicroscopica = ANA_DescMicroscopica
    End Sub

    Public Sub Set_Ana_DescNota(ByVal ANA_DescNota)
        Me.ANA_DescNota = ANA_DescNota
    End Sub

    Public Sub Set_Ana_EstuInmuno(ByVal ANA_EstuInmuno)
        Me.ANA_EstuInmuno = ANA_EstuInmuno
    End Sub

    Public Sub Set_Ana_Iddescripcion(ByVal ANA_IdDescripcion)
        Me.ANA_IdDescripcion = ANA_IdDescripcion
    End Sub

End Class
