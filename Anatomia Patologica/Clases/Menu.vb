﻿Public Class Menu

#Region "VARIABLES DE CLASE"
    '==========================================================
    'VARIABLES DE CLASE

    'FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region


    '----------------------------------------------------------------------------------------------------------------------------------------
    ' OBTIENE EL MENU PRINCIPAL DEL PERFIL
    '----------------------------------------------------------------------------------------------------------------------------------------
    Public Function Get_Menu(ByVal perfil As String)
        Dim sql As String =
            "SELECT " _
                & "gm.GEN_idMenu, " _
                & "gm.GEN_tituloMenu, " _
                & "SUBSTRING(gm.GEN_contenidoMenu,3,len(gm.GEN_contenidoMenu)) as GEN_contenidoMenu, " _
                & "gm.GEN_seleccionableMenu, " _
                & "gm.GEN_TargetMenu " _
            & "FROM " _
                & "Gen_Perfil AS gp " _
                & "RIGHT OUTER JOIN " _
                & "GEN_Menu_Perfil AS gmp " _
                & "ON gp.GEN_idPerfil = gmp.GEN_idPerfil " _
                & "LEFT OUTER JOIN " _
                & "GEN_Menu AS gm " _
                & "ON gmp.GEN_idMenu = gm.GEN_idMenu " _
            & "WHERE " _
                & "gp.GEN_codigoPerfil = '" & perfil & "' " _
                & "AND gp.GEN_IdSistemas = '1' " _
                & "AND gm.GEN_nivelMenu = '1' " _
                & "AND gm.GEN_estadoMenu = 'ACTIVO' " _
                & "AND gmp.GEN_estadoMenu_Perfil = 'Activo' " _
        & "ORDER BY " _
            & "gmp.GEN_idMenu_Perfil"
        'activados los menús usados en base de datos
        Return consulta_sql_datatable(sql)

    End Function

    '----------------------------------------------------------------------------------------------------------------------------------------
    ' OBTIENE EL SUB MENU DEL PERFIL
    '----------------------------------------------------------------------------------------------------------------------------------------
    Public Function Get_Menu_Nivel(idMenu_padre As Integer, perfil As Integer, nivel As Integer)
        Dim sql As String = "SELECT gm.GEN_idMenu, gm.GEN_tituloMenu, SUBSTRING(gm.GEN_contenidoMenu,2,len(gm.GEN_contenidoMenu)) as GEN_contenidoMenu, " _
        & "gm.GEN_seleccionableMenu, gm.GEN_TargetMenu " _
        & "FROM GEN_Menu_Niveles as gmn INNER JOIN " _
        & "GEN_Menu as gm ON " _
        & "gmn.GEN_idMenu_hijo = gm.GEN_idMenu " _
        & "WHERE (gmn.GEN_idMenu_padre = " & idMenu_padre & ") AND " _
        & "gmn.GEN_idMenu_hijo in " _
        & "(SELECT gm.GEN_idMenu as GEN_idMenu_hijo " _
        & "FROM GEN_Perfil as gp RIGHT OUTER JOIN " _
        & "GEN_Menu_Perfil as gmp ON " _
        & "gp.GEN_idPerfil = gmp.GEN_idPerfil LEFT OUTER JOIN " _
        & "GEN_Menu as gm ON " _
        & "gmp.GEN_idMenu = gm.GEN_idMenu " _
        & "WHERE gp.GEN_codigoPerfil = " & perfil & " AND " _
        & "gp.GEN_IdSistemas = 1 AND " _
        & "gm.GEN_nivelMenu = " & nivel & " and gmp.GEN_estadoMenu_Perfil = 'Activo' and gm.GEN_estadoMenu = 'Activo')"

        Return consulta_sql_datatable(sql)
    End Function
End Class
