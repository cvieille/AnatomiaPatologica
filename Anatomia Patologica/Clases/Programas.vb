﻿Public Class Programas

#Region "VARIABLES DE CLASE"
    '==========================================================
    ' *** VARIABLES DE CLASE

    Dim consulta As String
    Dim GEN_IdPrograma As Integer = 0
    Dim GEN_NomPrograma As String
    Dim GEN_estadoPrograma As String = "Activo"

    ' *** FIN DE VARIABLES DE CLASE
    '==========================================================
#End Region

#Region "CONSTRUCTOR DE LA CLASE"
    ' *** CONSTRUCTOR DE LA CLASE
    '==========================================================

    Public Sub Programas(ByVal IdPrograma)

        Dim consulta As String = ""

        consulta = "SELECT * FROM GEN_Programa WHERE GEN_IdPrograma=" & IdPrograma

        Dim tablas As Data.DataTable = consulta_sql_datatable(consulta)
        If tablas.Rows.Count > 0 Then
            Dim filas As Data.DataRow = tablas.Rows(0)
            Me.GEN_IdPrograma = (filas.Item("GEN_IdPrograma").ToString())
            Me.GEN_NomPrograma = Trim(filas.Item("GEN_NomPrograma").ToString())
            Me.GEN_estadoPrograma = Trim(filas.Item("GEN_estadoPrograma").ToString())
        End If
    End Sub

    ' *** FIN DE CONSTRUCTOR DE LA CLASE
    '==========================================================
#End Region

#Region "COMIENZA EL CRUD"
    ' *** COMIENZA EL CRUD
    '==========================================================

    Public Sub Set_CrearPrograma()
        Dim sql As New StringBuilder()
        sql.AppendLine("insert into GEN_Programa (")
        sql.AppendLine("GEN_NomPrograma,")
        sql.AppendLine("GEN_estadoPrograma)")
        sql.AppendLine("values(")
        sql.AppendLine("'" + Me.GEN_NomPrograma + "',")
        sql.AppendLine("'" + Me.GEN_estadoPrograma + "')")
        ejecuta_sql(sql.ToString())
    End Sub

    Public Sub Set_UpdatePrograma()
        Dim sql As New StringBuilder()
        sql.AppendLine("UPDATE GEN_Programa SET ")
        sql.AppendLine("GEN_nomPrograma='" & Me.GEN_NomPrograma & "',")
        sql.AppendLine("GEN_estadoPrograma='" & Me.GEN_estadoPrograma & "'")
        sql.AppendLine("WHERE(GEN_idPrograma = " & Me.GEN_IdPrograma & ")")
        ejecuta_sql(sql.ToString())
    End Sub

    ' *** FIN DE CRUD
    '==========================================================
#End Region

#Region "COMIENZA LOS SET"
    ' *** COMIENZA LOS SET
    '==========================================================
    Public Sub Set_GEN_estadoPrograma(ByVal GEN_estadoPrograma)
        Me.GEN_estadoPrograma = GEN_estadoPrograma
    End Sub
    Public Sub Set_Gen_Idprograma(ByVal GEN_IdPrograma)
        Me.GEN_IdPrograma = GEN_IdPrograma
    End Sub

    Public Sub Set_Gen_Nomprograma(ByVal GEN_NomPrograma)
        Me.GEN_NomPrograma = GEN_NomPrograma
    End Sub

    ' *** FIN DE SET
    '==========================================================
#End Region

#Region "COMIENZA LOS GET"
    ' *** COMIENZA LOS GET
    '==========================================================

    Public Function Get_Gen_Idprograma()
        Return Me.GEN_IdPrograma
    End Function

    Public Function Get_Gen_Nomprograma()
        Return Me.GEN_NomPrograma
    End Function

    Public Function Get_GEN_estadoPrograma()
        Return Me.GEN_estadoPrograma
    End Function
    ' *** FIN DE GET
    '==========================================================
#End Region

End Class
