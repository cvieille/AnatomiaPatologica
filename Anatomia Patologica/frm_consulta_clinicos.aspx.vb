﻿Imports iTextSharp.text.pdf
Imports System.Data
Imports System.Data.SqlClient
Imports Newtonsoft.Json
Imports System.Web.Services

Partial Public Class frm_consulta_clinicos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("/frm_login.aspx")
        End If
    End Sub

    <WebMethod>
    Public Shared Function grillaReportes(s)

        Dim json As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(s)
        Dim jsonList As New List(Of Dictionary(Of String, String))

        Dim con As New Consultas
        Dim d As DataTable = consulta_sql_datatable(con.Ds_con_clinicos(json("rut").ToString(), json("nui").ToString(), json("nombre").ToString(), json("app").ToString(), json("apm").ToString(), "", "", ""))
        For Each dtr As DataRow In d.Rows
            Dim tjson As New Dictionary(Of String, String)
            For i = 0 To dtr.Table.Columns.Count - 1
                tjson(dtr.Table.Columns(i).ColumnName) = dtr(dtr.Table.Columns(i).ColumnName).ToString
            Next
            jsonList.Add(tjson)
        Next
        Return JsonConvert.SerializeObject(jsonList)
    End Function
End Class