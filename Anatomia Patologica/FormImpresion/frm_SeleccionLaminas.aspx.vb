﻿Partial Public Class frm_SeleccionLaminas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        gdv_lamina.DataSource = CType(Session("dt_Impr_MovLamina"), DataTable)
        gdv_lamina.DataBind()
    End Sub

    Protected Sub cmd_exportar_excel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_exportar_excel.Click

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm

        gdv_lamina.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(gdv_lamina)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Listado de Lamina</h1>")
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=listado de Laminas.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class