﻿Public Class IMP_Informe
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim inf As New InformeBiopsia
        inf.Set_ANA_IdBiopsia(CType(Request.QueryString("idb"), Integer))
        inf.Set_GEN_IdUsuarios(Session("id_usuario"))

        Dim rm As New Registro_Biopsia
        rm.Registro_Biopsia(inf.Get_ANA_IdBiopsia())

        'FECHA
        '====================================================================================================
        lblNumBiopsia.Text = rm.Get_NumBiopsiaCompleto()
        lblFechaInforme.Text = rm.Get_ANA_fecValidaBiopsia
        lblFechaRecepcion.Text = rm.Get_ANA_fec_RecepcionRegistro_Biopsias()

        'DATOS PACIENTE
        '====================================================================================================
        Dim p As New Pacientes
        p.Pacientes(rm.Get_GEN_IdPaciente)
        lblRut.Text = p.Get_GEN_numero_documentoPaciente() + "-" + p.Get_GEN_digitoPaciente()
        lblNombre.Text = p.Get_GEN_Nombre()
        lblApellidos.Text = p.Get_GEN_ApePaterno() & " " & p.Get_GEN_ApeMaterno()
        lblUbInterna.Text = p.Get_GEN_NuiPaciente()
        lblFechaNac.Text = p.Get_GEN_FecNacimientoPaciente()
        lblEdad.Text = p.Get_Edad()

        'OTROS DATOS
        '====================================================================================================
        lblServOrigen.Text = rm.Get_Nom_Servicio_Solicita(rm.Get_GEN_IdServicioOrigen())
        lblServDestino.Text = rm.Get_Nom_Servicio_Destino(rm.Get_GEN_IdServicioDestino())
        Dim pro As New Profesionales
        pro.Profesionales(rm.Get_ANA_IdMedSolicitaBiopsia)

        lblDerivado.Text = pro.Get_GEN_NombreCompleto()
        Dim nrm As New NegocioRegistroMuestra
        nrm.Set_ANA_IdBiopsia(rm.Get_ANA_IdBiopsia())
        lblMuestra.Text = nrm.Get_DetalleFrascosBiopsia

        'DESCRIPCION MACROSCOPICA
        '====================================================================================================
        Dim det As New DescripcionBiopsia
        det.DescripcionBiopsia(rm.Get_ANA_IdBiopsia())
        If Not String.IsNullOrEmpty(det.Get_Ana_DescAntClinicos()) Then
            lblAntecedentes.Text = "<>ANTECEDENTES CLÍNICOS:</b><br />" & det.Get_Ana_DescAntClinicos().Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If

        If Not String.IsNullOrEmpty(det.Get_Ana_DescMacroscopica()) Then
            lblDescripcionMacroscopica.Text = "<p class='estilo-titulo'>DESCRIPCIÓN MACROSCOPICA:</p><br />" & RTrim(det.Get_Ana_DescMacroscopica()).Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If

        If Not String.IsNullOrEmpty(det.Get_Ana_DescMicroscopica()) Then
            lblDescripcionMicroscopica.Text = "<b>EXAMEN MICROSCOPICO:</b><br />" & det.Get_Ana_DescMicroscopica().Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If

        If Not String.IsNullOrEmpty(det.Get_Ana_DescDiagnostico()) Then
            lblDiagnostico.Text = "<b>DIAGNOSTICO:</b><br />" & det.Get_Ana_DescDiagnostico().Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If
        If Not String.IsNullOrEmpty(det.Get_Ana_DescNota()) Then
            lblNotas.Text = "<b>NOTAS:</b><br />" & det.Get_Ana_DescNota().Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If

        If Not String.IsNullOrEmpty(det.Get_Ana_EstuInmuno()) Then
            lblInmunoHistoquimica.Text = "<b>Estudio Inmunohistoquímico:</b><br />" & det.Get_Ana_EstuInmuno().Replace(ChrW(10), "<br/>").Replace(ChrW(13), "") & "<br />"
        End If

        'FIRMA
        '====================================================================================================
        Dim uv As New Usuarios
        uv.Usuarios(CInt(rm.Get_GEN_idUsuarioValida()))
        lblFirmaNombre.Text = uv.Get_GEN_NombreCompleto()

        'arreglo provisorio para que rto pueda ingresar adjuntos como patologo y validar
        'pero que no salga como anatomopatologo ----- fecha 30/12/2020
        If (CInt(rm.Get_GEN_idUsuarioValida() <> 17)) Then
            lblResponsable.Text = "Responsable: anatomopatólogo"
        Else
            lblResponsable.Text = "Responsable: Tecnólogo Médico"
        End If

        'FOOTER
        '====================================================================================================
        lblNumBiopsia2.Text = rm.Get_NumBiopsiaCompleto()
        lblFechaHoy.Text = Now
        Dim u As New Usuarios
        u.Usuarios(CType(Request.QueryString("idu"), Integer))
        lblUsuario.Text = u.Get_GEN_loginUsuarios()

        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri
        funciones.ExportarPDF(url, Me.Page, "INFORME DE BIOPSIA " + inf.Get_ANA_IdBiopsia().ToString())
    End Sub

End Class