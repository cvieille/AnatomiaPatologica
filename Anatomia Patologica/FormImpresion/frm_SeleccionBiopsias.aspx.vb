﻿Partial Public Class frm_SeleccionBiopsias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        gdv_Biopsias.AllowSorting = False
        gdv_Biopsias.DataSource = CType(Session("dt_Impr_Biopsia"), DataTable)
        gdv_Biopsias.DataBind()
    End Sub

    Protected Sub cmd_impr_tec_pendientes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_impr_tec_pendientes.Click
        '*** exporta la grilla
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        gdv_Biopsias.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(gdv_Biopsias)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Detalle de Biopsias</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=Biopsias.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class