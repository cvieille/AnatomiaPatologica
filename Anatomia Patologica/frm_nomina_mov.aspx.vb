﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf


Partial Public Class frm_nomina_mov
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        'creamos el documento
        '...ahora configuramos para que el tamaño de hoja sea A4
        Dim document As Document = New Document(iTextSharp.text.PageSize.A4)
        'document.PageSize.BackgroundColor = New iTextSharp.text.Color(255, 255, 255)
        document.PageSize.Rotate()

        '...definimos el autor del documento.
        document.AddAuthor("Arbis Percy Reyes Paredes") '

        '//...el creador, que será el mismo eh!
        document.AddCreator("Arbis Percy Reyes Paredes")

        'hacemos que se inserte la fecha de creación para el documento
        document.AddCreationDate()

        '...título
        document.AddTitle("Generación de un pdf con itextSharp")

        '... el asunto
        document.AddSubject("Este es un paso muy important")

        '... palabras claves
        document.AddKeywords("pdf, PdfWriter; Documento; iTextSharp")

        'creamos un instancia del objeto escritor de documento
        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New System.IO.FileStream _
                                                  ("Code.pdf", System.IO.FileMode.Create))


        'encriptamos el pdf, dándole como clave de usuario "key" y la clave del dueño será "owner"
        'si quitas los comentarios (en writer.SetEncryption...), entonces el documento generado
        'no mostrarà tanto la información de autor, titulo, fecha de creacion... 
        'que habiamos establecio más arriba. y sólo podrás abrirlo con una clave

        'writer.SetEncryption(PdfWriter.STRENGTH40BITS,"key","owner", PdfWriter.CenterWindow);

        'definimos la manera de inicialización de abierto del documento.
        'esto, hará que veamos al inicio, todas la páginas del documento
        'en la parte izquierda
        writer.ViewerPreferences = PdfWriter.PageModeUseThumbs


        'con esto conseguiremos que el documento sea presentada de dos en dos 
        writer.ViewerPreferences = PdfWriter.PageLayoutTwoColumnLeft

        'con esto podemos oculta las barras de herramienta y de menú respectivamente.
        '(quite las dos barras de comentario de la siguiente línea para ver los efectos)
        'PdfWriter.HideToolbar | PdfWriter.HideMenubar

        'abrimos el documento para agregarle contenido
        document.Open()

        'este stream es para jalar el código
        'Dim TemPath As String = Application.StartupPath.ToString()
        'TemPath = TemPath.Substring(0, TemPath.Length - 4)
        'Dim pathFileForm1cs As String = TemPath + "\Form1.vb"
        Dim reader As System.IO.StreamReader = New System.IO.StreamReader("c:\")

        'leemos primera línea
        Dim linea As String = reader.ReadLine()
        'creamos la fuente
        Dim myfont As iTextSharp.text.Font = New iTextSharp.text.Font( _
        FontFactory.GetFont(FontFactory.COURIER, 10, iTextSharp.text.Font.ITALIC))

        'creamos un objeto párrafo, donde insertamos cada una de las líneas que 
        'se vaya leyendo mediante el reader 
        Dim myParagraph As Paragraph = New Paragraph("Código fuente en Visual C# \n\n", myfont)

        Do
            'leyendo linea de texto
            linea = reader.ReadLine()
            'concatenando cada parrafo que estará formado por una línea
            myParagraph.Add(New Paragraph(linea, myfont))
        Loop While Not (linea Is Nothing) 'mientras no llegue al final de documento, sigue leyendo


        'agregar todo el paquete de texto
        document.Add(myParagraph)

        'esto es importante, pues si no cerramos el document entonces no se creara el pdf.
        document.Close()

        'esto es para abrir el documento y verlo inmediatamente después de su creación
        System.Diagnostics.Process.Start("AcroRd32.exe", "Code.pdf")
    End Sub

End Class