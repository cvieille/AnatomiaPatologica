﻿Imports iTextSharp.text.pdf
Partial Public Class frm_impr_informe

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
        If IsPostBack = False Then

            Me.txt_id.Text = Request.QueryString("id")
            Dim id_biopsia As Long = Request.QueryString("id")
            If IsNumeric(id_biopsia) Then

                Try
                    ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
                    '==========================================================
                    Dim rm As New Registro_Biopsia
                    rm.Registro_Biopsia(id_biopsia)

                    With rm

                        Me.txt_numero.Text = .Get_ANA_NumBiopsia() & "-" & .Get_ANA_AñoBiopsia()
                        Me.txt_servicio.Text = .Get_Nom_Servicio_Solicita(.Get_GEN_IdServicioOrigen())
                        Me.txt_servicio0.Text = .Get_Nom_Servicio_Destino(.Get_GEN_IdServicioDestino)

                        Me.txt_organo.Text = .Get_ANA_OrganoBiopsia()
                        Me.txt_solicitado_por.Text = .Get_Nom_Medico_Solicita(.Get_ANA_IdMedSolicitaBiopsia)
                        Me.txt_recepcion.Text = .Get_ANA_fec_RecepcionRegistro_Biopsias()


                        ' *** INSTANCIA EL CONSTRUCTOR DE DESCRIPCION DE PACIENTES
                        '==========================================================
                        Dim desc As New DescripcionBiopsia
                        desc.DescripcionBiopsia(.Get_ANA_IdBiopsia())
                        Me.txt_antecedentes_clinicos.Text = desc.Get_Ana_DescAntClinicos()
                        Me.txt_desc_diag.Text = desc.Get_Ana_DescDiagnostico()
                        Me.txt_desc_macro.Text = desc.Get_Ana_DescMacroscopica()
                        Me.txt_desc_micro.Text = desc.Get_Ana_DescMicroscopica()
                        Me.txt_nota.Text = desc.Get_Ana_DescNota()
                        Me.txt_estu_inmuno.Text = desc.Get_Ana_EstuInmuno()

                        Me.txt_id_descripcion.Text = desc.Get_Ana_Iddescripcion


                        ' *** INSTANCIA EL CONSTRUCTOR DE PACIENTES
                        '==========================================================
                        Dim p As New Pacientes
                        p.Pacientes(rm.Get_GEN_Id_Paciente)

                        Me.txt_fnac.Text = CDate(p.Get_GEN_FecNacimientoPaciente())

                        Me.txt_ficha.Text = p.Get_GEN_NuiPaciente()
                        Me.txt_rut.Text = p.Get_GEN_numero_documentoPaciente() & "-" & p.Get_GEN_digitoPaciente()
                        Me.txt_nombre.Text = p.Get_GEN_Nombre()
                        Me.txt_ap_paterno.Text = p.Get_GEN_ApePaterno() & " " & p.Get_GEN_ApeMaterno()

                        Dim Meses_Totales = DateDiff("m", Me.txt_fnac.Text, Date.Today)
                        Dim Years = Int(Meses_Totales / 12)
                        Me.txt_edad.Text = Years & " años " & Meses_Totales - (Years * 12) & " meses"

                        ' *** INSTANCIA USUARIOS PARA OBTENER NOMBRE DE PATOLOGO  QUE VALIDA 
                        '==========================================================
                        Dim uValida As New Usuarios
                        uValida.Usuarios(CInt(rm.Get_GEN_idUsuarioValida()))
                        Me.txt_quien_valida.Text = uValida.Get_GEN_NombreCompleto()

                        Me.txt_finf.Text = Trim(.Get_ANA_fecValidaBiopsia)

                        imprimir_informe()
                    End With

                Catch ex As Exception
                End Try
            Else

                Me.txt_nombre.Text = ""
                Me.txt_servicio.Text = ""
                Me.txt_organo.Text = ""
                Me.txt_solicitado_por.Text = ""
                Me.txt_antecedentes_clinicos.Text = ""
                Me.txt_desc_diag.Text = ""
                Me.txt_desc_macro.Text = ""
                Me.txt_desc_micro.Text = ""
                Me.txt_nota.Text = ""
                Me.txt_estu_inmuno.Text = ""
            End If
        End If
    End Sub

    Private Sub imprimir_informe()
        Try
            Dim inf As New InformeBiopsia
            inf.Set_ANA_IdBiopsia(Request.QueryString("id"))
            inf.Set_GEN_IdUsuarios(Session("id_usuario"))

            'System.Diagnostics.Process.Start(inf.InformeCodigoPaciente)

            Dim url As String = inf.GenerarInforme("Temporal")
            Dim s As String = "window.open('" & url + "', 'popup_window', 'width=1300,height=900,left=100,top=100,resizable=yes,scrollbars=yes');"
            ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)

        Catch ex As Exception

        End Try
    End Sub
End Class