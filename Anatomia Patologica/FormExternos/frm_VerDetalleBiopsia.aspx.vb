﻿Public Partial Class frm_VerDetalleBiopsia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
        'si recibo por id el 'c' es por que viene de RCE
        If Not (Request.QueryString("c") Is Nothing) Then
            Dim sCript As String = Server.HtmlDecode(Request.QueryString("c"))
            sCript = sCript.Replace(" ", "+")
            Dim sfull = funciones.AESDec(sCript)
            Dim sPart = sfull.Split("|")
            'sPart(0) == idusuario
            'sPart(1) == login
            'sPart(2) == pass
            'sPart(3) == idbiop

            Dim inf As New InformeBiopsia
            inf.Set_ANA_IdBiopsia(sPart(3))
            inf.Set_GEN_IdUsuarios(sPart(0))

            Dim nombreArchivo As String = inf.GenerarInforme("Temporal")
            Dim url As String = "../ImprimirInforme.aspx?nombre=" & inf.Get_NumBiopsiaCompleto() & "&archivo=" & nombreArchivo
            Response.Redirect(url)

        Else
            ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
            '==========================================================
            Dim con As New Consultas
            dts_detalle.SelectCommand = con.Ds_Con_VerDescripcionBiopsia(Request.QueryString("id"))
            dts_detalle.DataBind()
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim inf As New InformeBiopsia
        inf.Set_ANA_IdBiopsia(Request.QueryString("id"))
        inf.Set_GEN_IdUsuarios(Session("id_usuario"))

        Dim nombreArchivo As String = inf.GenerarInforme("Temporal")
        Dim url As String = "../ImprimirInforme.aspx?nombre=" & inf.Get_NumBiopsiaCompleto() & "&archivo=" & nombreArchivo
        Response.Redirect(url)
    End Sub
End Class