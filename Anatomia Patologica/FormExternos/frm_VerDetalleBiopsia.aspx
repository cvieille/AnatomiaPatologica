﻿<%@ Page Title="Detalle de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_VerDetalleBiopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_VerDetalleBiopsia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cnt_Principal" runat="server">

<!-- Panel Principal -->
	<div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Biopsia de Paciente</div>        
        <div class="panel-body">
        
            
        
            <asp:FormView ID="FormView1" runat="server" HorizontalAlign="Center" CssClass="panel panel-default"
                DataSourceID="dts_detalle" Width="700px" Caption="Datos de Biopsia" 
                CellSpacing="1">
                <EditItemTemplate>
                    Fecha de Recepción:
                    <asp:TextBox ID="Fecha_de_RecepciónTextBox" runat="server" 
                        Text='<%# Bind("[Fecha de Recepción]") %>' />
                    <br />
                    Rut Paciente: 
                    <asp:TextBox ID="Rut_PacienteTextBox" runat="server" 
                        Text='<%# Bind("[Rut Paciente]") %>' />
                    <br />
                    Nombre Paciente:
                    <asp:TextBox ID="Nombre_PacienteTextBox" runat="server" 
                        Text='<%# Bind("[Nombre Paciente]") %>' />
                    <br />
                    Organo:
                    <asp:TextBox ID="OrganoTextBox" runat="server" Text='<%# Bind("Organo") %>' />
                    <br />
                    Patologo:
                    <asp:TextBox ID="PatologoTextBox" runat="server" 
                        Text='<%# Bind("Patologo") %>' />
                    <br />
                    Tecnologo:
                    <asp:TextBox ID="TecnologoTextBox" runat="server" 
                        Text='<%# Bind("Tecnologo") %>' />
                    <br />
                    Medico Solicitante:
                    <asp:TextBox ID="Medico_SolicitanteTextBox" runat="server" 
                        Text='<%# Bind("[Medico Solicitante]") %>' />
                    <br />
                    Servicio Destino:
                    <asp:TextBox ID="Servicio_DestinoTextBox" runat="server" 
                        Text='<%# Bind("[Servicio Destino]") %>' />
                    <br />
                    Servicio Origen:
                    <asp:TextBox ID="Servicio_OrigenTextBox" runat="server" 
                        Text='<%# Bind("[Servicio Origen]") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Actualizar" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    Fecha de Recepción:
                    <asp:TextBox ID="Fecha_de_RecepciónTextBox" runat="server" 
                        Text='<%# Bind("[Fecha de Recepción]") %>' />
                    <br />
                    Rut Paciente:
                    <asp:TextBox ID="Rut_PacienteTextBox" runat="server" 
                        Text='<%# Bind("[Rut Paciente]") %>' />
                    <br />
                    Nombre Paciente:
                    <asp:TextBox ID="Nombre_PacienteTextBox" runat="server" 
                        Text='<%# Bind("[Nombre Paciente]") %>' />
                    <br />
                    Organo:
                    <asp:TextBox ID="OrganoTextBox" runat="server" Text='<%# Bind("Organo") %>' />
                    <br />
                    Patologo:
                    <asp:TextBox ID="PatologoTextBox" runat="server" 
                        Text='<%# Bind("Patologo") %>' />
                    <br />
                    Tecnologo:
                    <asp:TextBox ID="TecnologoTextBox" runat="server" 
                        Text='<%# Bind("Tecnologo") %>' />
                    <br />
                    Medico Solicitante:
                    <asp:TextBox ID="Medico_SolicitanteTextBox" runat="server" 
                        Text='<%# Bind("[Medico Solicitante]") %>' />
                    <br />
                    Servicio Destino:
                    <asp:TextBox ID="Servicio_DestinoTextBox" runat="server" 
                        Text='<%# Bind("[Servicio Destino]") %>' />
                    <br />
                    Servicio Origen:
                    <asp:TextBox ID="Servicio_OrigenTextBox" runat="server" 
                        Text='<%# Bind("[Servicio Origen]") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Insertar" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Fecha de Recepción:
                    <asp:Label ID="Fecha_de_RecepciónLabel" runat="server" 
                        Text='<%# Bind("[Fecha de Recepción]") %>' />
                    <br />
                    Rut Paciente:
                    <asp:Label ID="Rut_PacienteLabel" runat="server" 
                        Text='<%# Bind("[Rut Paciente]") %>' />
                    <br />
                    Nombre Paciente:
                    <asp:Label ID="Nombre_PacienteLabel" runat="server" 
                        Text='<%# Bind("[Nombre Paciente]") %>' />
                    <br />
                    Organo:
                    <asp:Label ID="OrganoLabel" runat="server" Text='<%# Bind("Organo") %>' />
                    <br />
                    Patologo:
                    <asp:Label ID="PatologoLabel" runat="server" Text='<%# Bind("Patologo") %>' />
                    <br />
                    Tecnologo:
                    <asp:Label ID="TecnologoLabel" runat="server" Text='<%# Bind("Tecnologo") %>' />
                    <br />
                    Medico Solicitante:
                    <asp:Label ID="Medico_SolicitanteLabel" runat="server" 
                        Text='<%# Bind("[Medico Solicitante]") %>' />
                    <br />
                    Servicio Destino:
                    <asp:Label ID="Servicio_DestinoLabel" runat="server" 
                        Text='<%# Bind("[Servicio Destino]") %>' />
                    <br />
                    Servicio Origen:
                    <asp:Label ID="Servicio_OrigenLabel" runat="server" 
                        Text='<%# Bind("[Servicio Origen]") %>' />
                    <br />
                </ItemTemplate>
            </asp:FormView>
        
            
        
            <asp:Button ID="cmd_Codificacion" runat="server" Text="Ver Codificación ..." CssClass="btn btn-primary" />        
            <asp:Button ID="Button1" runat="server" Text="Ver Informe ..." CssClass="btn btn-primary" />    
   
        
        <asp:SqlDataSource ID="dts_detalle" runat="server" 
                ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" >
        </asp:SqlDataSource>
    
        </div>
	</div>
	

<!-- Panel Principal -->
	<div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Documentos Adjuntos</div>
        
        <div class="panel-body">
        
            <asp:GridView ID="grilla_archivos" runat="server" AutoGenerateColumns="False" DataKeyNames="ANA_IdArchivos_Biopsias" 
            DataSourceID="sql_ds_archivos" CssClass="table table-bordered table-hover table-responsive" HorizontalAlign="Center" >
                <Columns>
                    <asp:BoundField DataField="ANA_FecArchivos_Biopsias" HeaderText="Fecha" SortExpression="ANA_FecArchivos_Biopsias" />
                    <asp:BoundField DataField="ANA_NomArchivos_Biopsias" HeaderText="Nombre Archivo" SortExpression="ANA_NomArchivos_Biopsias" />
                    <asp:HyperLinkField DataNavigateUrlFields="ruta_archivo" 
                        DataNavigateUrlFormatString="{0}" DataTextField="ANA_NomArchivos_Biopsias" 
                        HeaderText="Archivo" Target="_blank" Text="Archivo" />
                </Columns>
            </asp:GridView>
                
            <asp:SqlDataSource ID="sql_ds_archivos" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" >        
            </asp:SqlDataSource>        
	    </div>
	</div>
     </form>
</asp:Content>
