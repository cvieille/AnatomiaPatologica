﻿Public Partial Class frm_lista_solicitud
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If list_estado.Text = "Generada" Or list_estado.Text = "Ingresada" Then
            GridView1.Columns(9).Visible = False
        Else
            GridView1.Columns(9).Visible = True
        End If
    End Sub

    Protected Sub cmd_buscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_buscar.Click
        Sqlds_lista_solicitud.FilterExpression = ("estado='" & list_estado.Text & "' and FECHA>='" & Me.txt_fecha_desde.Text & "' and FECHA<='" & Me.txt_fecha_hasta.Text & "' ")
        GridView1.DataBind()
    End Sub

End Class