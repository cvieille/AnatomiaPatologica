﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteBlanco.Master" CodeBehind="frm_detalle_tipo_muestra.aspx.vb" Inherits="Anatomia_Patologica.frm_detalle_tipo_muestra" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">

    <div style="text-align:center;"> 
        <asp:HiddenField ID="txt_detalle" runat="server" />
        <asp:HiddenField ID="txt_inicio" runat="server" />
        <asp:HiddenField ID="txt_fecha_fin" runat="server" />
        <div class="Titulo">
            <asp:Label ID="LabelTITULO" runat="server" Text="Detalle de Biopsias por Tipo de Muestra" ></asp:Label>
        </div>
        <br />
        <asp:Label ID="LabelPeriodo" runat="server" Font-Size="Large"></asp:Label>
        <br />        
        <asp:GridView ID="GridViewDetalle" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" DataSourceID="sqldata_det_tipo_muestra" ForeColor="#333333" 
            PageSize="20" DataKeyNames="" Font-Size="Small" Width="877px">
            <RowStyle BackColor="#EFF3FB" BorderStyle="None" />
            <Columns>
                <asp:BoundField DataField="catalogo" HeaderText="Catálogo" SortExpression="catalogo" />
                <asp:BoundField DataField="detalle" HeaderText="Detalle" SortExpression="detalle" />
                <asp:BoundField DataField="corte" HeaderText="Corte" ReadOnly="True" SortExpression="corte" />
                <asp:BoundField DataField="tecnica" HeaderText="Técnica" ReadOnly="True" SortExpression="tecnica" />
                <asp:BoundField DataField="lamina" HeaderText="Lámina" ReadOnly="True" SortExpression="lamina" />
                <asp:BoundField DataField="n_biopsia" HeaderText="Biopsia" InsertVisible="False" ReadOnly="True" SortExpression="n_biopsia" />
                <asp:BoundField DataField="ANA_fecRecepcionBiopsia" HeaderText="Recepción" SortExpression="ANA_fecRecepcionBiopsia" />
                <asp:BoundField DataField="estado" HeaderText="Estado" SortExpression="estado" />
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldata_det_tipo_muestra" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>" 
            SelectCommand="SELECT		catalogo_muestras.descripcion AS catalogo, detalle_catalogo.detalle as detalle, 
			SUM(ANA_vi_CantCortes.cant_cortes) AS corte, 
            SUM(vi_cuenta_descalcificacion.cant_tecnica) + SUM(vi_cuenta_tecnica_especial.cant_tecnica) AS tecnica, 
            SUM(vi_cuenta_laminas.cant_laminas) AS lamina, 
            RTRIM(CONVERT(char(10), registro_muestras.n_muestra)) + '-' + RTRIM(CONVERT(char(10), registro_muestras.año_muestra)) AS n_biopsia, 
            registro_muestras.ANA_fecRecepcionBiopsia, registro_muestras.estado
FROM         catalogo_muestras RIGHT OUTER JOIN
                      detalle_catalogo ON catalogo_muestras.cod_muestra = detalle_catalogo.cod_muestra RIGHT OUTER JOIN
                      detalle_registro_muestras ON detalle_catalogo.id = detalle_registro_muestras.id_detalle RIGHT OUTER JOIN
                      registro_muestras ON detalle_registro_muestras.id_biopsia = registro_muestras.id LEFT OUTER JOIN
                      vi_cuenta_laminas ON registro_muestras.id = vi_cuenta_laminas.id_biopsia LEFT OUTER JOIN
                      vi_cuenta_tecnica_especial ON registro_muestras.id = vi_cuenta_tecnica_especial.id_biopsia LEFT OUTER JOIN
                      ANA_vi_CantCortes ON registro_muestras.id = ANA_vi_CantCortes.ANA_IdBiopsia LEFT OUTER JOIN
                      vi_cuenta_descalcificacion ON registro_muestras.id = vi_cuenta_descalcificacion.id_biopsia
WHERE (registro_muestras.ANA_fecRecepcionBiopsia &gt;= @ANA_fecIngresoBiopsia) AND (registro_muestras.ANA_fecRecepcionBiopsia &lt;= @ANA_fecIngresoBiopsia2) AND (registro_muestras.ANA_AnuladaBiopsia='NO') AND (detalle_registro_muestras.id_detalle=@detalle)
GROUP BY catalogo_muestras.descripcion, detalle_catalogo.detalle, detalle_registro_muestras.id_detalle, registro_muestras.id, registro_muestras.n_muestra, 
                      registro_muestras.año_muestra, registro_muestras.ANA_fecRecepcionBiopsia, registro_muestras.estado">
        <SelectParameters>
        <asp:ControlParameter ControlID="txt_inicio" Name="ANA_fecIngresoBiopsia" PropertyName="Value" />
        <asp:ControlParameter ControlID="txt_fecha_fin" Name="ANA_fecIngresoBiopsia2" PropertyName="Value" />
        <asp:ControlParameter ControlID="txt_detalle" Name="detalle" PropertyName="Value" />
        </SelectParameters>
        </asp:SqlDataSource>
        <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false"></asp:Label>
    </div>

</asp:Content>
