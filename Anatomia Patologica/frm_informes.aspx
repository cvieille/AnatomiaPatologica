﻿<%@ Page Title="Informe de Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile ="~/MasterCrAnatomia.Master" CodeBehind="frm_informes.aspx.vb" Inherits="Anatomia_Patologica.WebForm10" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True">
    </asp:ToolkitScriptManager>
    <table style="width:100%;">
        <tr>
            <td style="width: 403px" valign="top" >
                <br />
            </td>
            <td valign="top" > <p>Cortes por Estado:</p>
                <table style="width:72%;">
                    <tr>
                        <td style="width: 6px">
                            <asp:Label ID="Label5" runat="server" Text="Desde:"></asp:Label>
                        </td>
                        <td style="width: 27px">
                            <asp:Label ID="Label6" runat="server" Text="Hasta:"></asp:Label>
                        </td>
                        <td style="width: 6px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 6px">
        <asp:TextBox ID="txt_fec_inicio0" runat="server" Width="90px"></asp:TextBox>
        <asp:CalendarExtender ID="txt_fec_inicio0_CalendarExtender" runat="server" 
            Enabled="True" TargetControlID="txt_fec_inicio0">
        </asp:CalendarExtender>
                        </td>
                        <td style="width: 27px">
        <asp:TextBox ID="txt_fec_final0" runat="server" Width="90px"></asp:TextBox>
        <asp:CalendarExtender ID="txt_fec_final0_CalendarExtender" runat="server" 
            Enabled="True" TargetControlID="txt_fec_final0">
        </asp:CalendarExtender>
                        </td>
                        <td style="width: 6px">
                <asp:DropDownList ID="cmb_estado_cortes" runat="server">                    
                    <asp:ListItem>En Macroscopia</asp:ListItem>
                    <asp:ListItem>En Procesador</asp:ListItem>
                    <asp:ListItem>En Inclusión</asp:ListItem>
                    <asp:ListItem>Tinción y Montaje</asp:ListItem>
                    <asp:ListItem>Entregada a Patologo</asp:ListItem>
                    <asp:ListItem>Listo para Procesar</asp:ListItem>
                </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6px">
                <asp:Button ID="cmb_biopsias_fecha0" runat="server" Text="Buscar" />
                        </td>
                        <td style="width: 27px">
                            &nbsp;</td>
                        <td style="width: 6px">
                            &nbsp;</td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td style="width: 403px">
                Técnicas Pendientes:</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 403px">
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 403px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <asp:Button ID="Button2" runat="server" Text="Button" />
    <asp:ModalPopupExtender ID="Button2_ModalPopupExtender" runat="server" 
        DynamicServicePath="" Enabled="True" 
        PopupControlID="Panel1" TargetControlID="Button2" 
        CancelControlID="btnClose" BackgroundCssClass="modalBackground" >
    </asp:ModalPopupExtender  >
    <br />
    
    
    <asp:Panel ID="Panel1" runat="server"  align="center" style = "display:none">
        frfrfrfrfrfr
        <asp:Button ID="btnClose" runat="server" Text="Close" />
    </asp:Panel>
    <p>
    </p>

</asp:Content>