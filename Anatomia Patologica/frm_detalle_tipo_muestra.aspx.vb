﻿Public Partial Class frm_detalle_tipo_muestra
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            txt_detalle.Value = Request.QueryString("id")
            txt_inicio.Value = Request.Cookies("Fecha_ini").Value
            txt_fecha_fin.Value = Request.Cookies("Fecha_fin").Value
            LabelPeriodo.Text = "Desde: " + txt_inicio.Value + " Hasta: " + txt_fecha_fin.Value
        Catch ex As Exception
            If IsPostBack = False Then
                LabelMensaje.Visible = True
                LabelMensaje.Text = ex.Message
            End If
        End Try
    End Sub
    '-------------------------------------------------------
    'FUNCION QUE AGREGA CERO A LAS CELDAS VACIAS
    '-------------------------------------------------------
    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewDetalle.RowDataBound
        On Error Resume Next
        'EN CASO QUE NO SEA LA PRIMERA FILA <HEADER>
        If (e.Row.Cells(2).Text <> "Corte") Then
            If Not IsNumeric(e.Row.Cells(2).Text) Then
                e.Row.Cells(2).Text = "0"
            End If
            If Not IsNumeric(e.Row.Cells(3).Text) Then
                e.Row.Cells(3).Text = "0"
            End If
            If Not IsNumeric(e.Row.Cells(4).Text) Then
                e.Row.Cells(4).Text = "0"
            End If
        End If
    End Sub

    Protected Sub txt_inicio_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txt_inicio.ValueChanged

    End Sub
End Class