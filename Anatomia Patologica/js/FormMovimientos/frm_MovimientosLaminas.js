﻿var vIdUsuario;
$(document).ready(function () {
    var vSession = getSession();
    vIdUsuario = vSession.id_usuario;

    comboPatologo();

    $('#selPatologo').change(function () {
        getDataLaminasApi($(this).val());
    });

    $('#btnEntregarMedico').click(function (e) {
        var a = [];
        var t = $('#tblMovimientosLaminas').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.checkLamina')[0].checked) {
                a.push($(d[i]).find('.checkLamina')[0].id);
            }
        if (a.length > 0) {
            for (var i = 0; i < a.length; i++) {
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}ANA_Laminas/Enviar/Almacenamiento/${a[i]}`,
                    contentType: 'application/json',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        if (i == a.length - 1) //actualiza cuando llega al ultimo POST
                        {
                            toastr.success('Se han entregado las láminas solicitadas');
                            getDataLaminasApi($('#selPatologo').val());
                        }
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            }
        }
        else
            toastr.info('Debe seleccionar al menos un registro');
        e.preventDefault();
    });
});

function comboPatologo() {
    let url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));
}

function getDataLaminasApi(idPatologo) {
    var vUrl;
    if (idPatologo > 0) {
        vUrl = `ANA_Laminas/Movimientos/${idPatologo}`;

        //console.log(`${GetWebApiUrl()}${vUrl}`);
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}${vUrl}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                let adataset = [];
                //console.log(data);
                $.each(data, function (key, val) {
                    adataset.push([
                        '',
                        val.ANA_idLamina,
                        val.ANA_nomLamina,
                        moment(val.ANA_fecLamina).format('DD/MM/YYYY hh:mm:ss'),
                        val.GEN_idTipo_Estados_Sistemas,
                        val.GEN_nombreTipo_Estados_Sistemas,
                        val.ANA_descripcionTipo_Biopsia,
                        val.GEN_loginUsuariosPatologo,
                        val.GEN_loginUsuarios,
                        val.ANA_idBiopsia
                    ]);
                });

                if (adataset.length == 0)
                    $('#btnExportar').attr('disabled', true);
                else
                    $('#btnExportar').removeAttr('disabled');

                $('#tblMovimientosLaminas').addClass('nowrap').DataTable({
                    data: adataset,
                    order: [],
                    pageLength: 20,
                    lengthMenu: [[20, 50, 100, -1], ["20", "50", "100", "Todos"]],
                    columnDefs:
                        [
                            { targets: 1, searchable: false },
                            { targets: 3, sType: 'date-ukLong' },
                            { targets: [1, 4], visible: false, searchable: false },
                            {
                                targets: 0,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    botones = '<label class="containerCheck">';
                                    botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="checkLamina">';
                                    botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                                    botones += '</label>';
                                    return botones;
                                }
                            },
                            {
                                targets: 9,
                                data: null,
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    let fila = meta.row;
                                    var botones;
                                    botones = `<a class="btn btn-info" data-id='${adataset[fila][9]}' onclick="linkBiopsia(this)">Ver</a>`;
                                    return botones;
                                }
                            },
                        ],
                    columns: [
                        { title: '' },
                        { title: 'IDLamina' },
                        { title: 'Lámina' },
                        { title: 'Fecha' },
                        { title: 'IDEstadoSistema' },
                        { title: 'Estado' },
                        { title: 'Descripción' },
                        { title: 'Usuario patólogo' },
                        { title: 'Usuario ingreso' },
                        { title: 'Cortes' }
                    ],
                    bDestroy: true
                });
            }
        });
    }
}

function linkBiopsia(e) {
    let id = $(e).data('id');
    window.open(`../FormAnatomia/frm_Cortes.aspx?id=${id}`, '_blank');
}