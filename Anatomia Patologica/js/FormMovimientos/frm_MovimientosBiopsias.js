﻿$(document).ready(function () {
    var sSession = getSession();
    let iUsuario = sSession.id_usuario;

    $('#txtDesFechaEntrega').val(moment().format('YYYY-MM-DD'));
    $('#txtIntFechaEntrega').val(moment().format('YYYY-MM-DD'));

    $('#txtDesFechaEntrega').attr("max", moment().format('YYYY-MM-DD'));
    $('#txtIntFechaEntrega').attr("max", moment().format('YYYY-MM-DD'));

    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#btnDesDespachar').click(function (e) {
        var bValido = true;
        if ($('#txtDesEntregar').val() == '' || $('#txtDesEntregar').val() == null || $('#txtDesEntregar').val() == undefined) {
            resaltaElemento($('#txtDesEntregar'));
            bValido = false;
        }
        if (bValido) {
            var cantidad = 0;
            var t = $('#tblBiopsiasDespachar').DataTable();
            var d = t.rows().nodes();
            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkDesBiopsia')[0].checked)
                    cantidad = cantidad + 1;

            if (cantidad > 0) {
                $('#lblConfirmacion').text('¿Confirma el despacho de las biopsias seleccionadas?');
                $('#btnConfirmarModal').attr('data-name', 'informe');
                $('#mdlConfirmacion').modal('show');
            }
            else
                toastr.info('Debe seleccionar al menos un registro');
        }
        e.preventDefault();
    });
    $('#btnIntDespachar').click(function (e) {

        var bValido = true;
        if ($('#txtIntEntregar').val() == '' || $('#txtIntEntregar').val() == null || $('#txtIntEntregar').val() == undefined) {
            resaltaElemento($('#txtIntEntregar'));
            bValido = false;
        }
        if (bValido) {
            var cantidad = 0;
            var t = $('#tblBiopsiasInterconsulta').DataTable();
            var d = t.rows().nodes();
            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkIntBiopsia')[0].checked)
                    cantidad = cantidad + 1;

            if (cantidad > 0) {
                $('#lblConfirmacion').text('¿Confirma el despacho de las interconsultas seleccionadas?');
                $('#btnConfirmarModal').attr('data-name', 'biopsia');
                $('#mdlConfirmacion').modal('show');
            }
            else
                toastr.info('Debe seleccionar al menos un registro');
        }
        e.preventDefault();
    });

    $('#btnConfirmarModal').click(function (e) {
        let tipoAccion = $('#btnConfirmarModal').data('name');
        console.log(tipoAccion);
        if (tipoAccion == 'informe') {
            despacharInforme();            
        }
        else {
            let a = [];
            let t = $('#tblBiopsiasInterconsulta').DataTable();
            let d = t.rows().nodes();
            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkIntBiopsia')[0].checked) {
                    a.push($(d[i]).find('.checkIntBiopsia')[0].id);
                }

            if (a.length > 0) {
                $.ajax({
                    type: 'POST',
                    url: `${ObtenerHost()}/FormMovimientos/frm_MovimientosBiopsias.aspx/despacharInterconsulta`,
                    data: JSON.stringify({ s: JSON.stringify(a), idUsuario: iUsuario, sFecha: $('#txtDesFechaEntrega').val(), sUsuario: $('#txtDesEntregar').val() }),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (data) {
                        toastr.success('Se han despachado los registros seleccionados');
                        $('#mdlConfirmacion').modal('hide');
                        $('#txtIntEntregar').val('');
                        grillaParaInterconsulta();
                    }
                });
            }
            else
                toastr.info('Debe seleccionar al menos un registro');
        }
    });

    grillaParaDespachos();
    grillaParaInterconsulta();
});
function despacharInforme() {
    var t = $('#tblBiopsiasDespachar').DataTable();
    var d = t.rows().nodes();
    var a = [];

    for (var i = 0; i < d.length; i++) {
        if ($(d[i]).find('.checkDesBiopsia')[0].checked) {
            a.push($(d[i]).find('.checkDesBiopsia')[0].id);
        }
    }
    if (a.length > 0) {
        $('#mdlConfirmacion').modal('show');
        $.ajax({
            type: 'PATCH',
            url: GetWebApiUrl() + "/ANA_Registro_Biopsias/DespacharInforme/",
            data: JSON.stringify({ FechaDespacho: $('#txtDesFechaEntrega').val(), NombreUsuarioRecibe: $('#txtDesEntregar').val(), IdBiopsia: a }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se han despachado los registros seleccionados');
               
                $('#txtDesEntregar').val('');
                grillaParaDespachos();
            }
        });
        $('#mdlConfirmacion').modal('hide');
    }
    else
        toastr.info('Debe seleccionar al menos un registro');
}

function grillaParaDespachos() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?idTipoEstadoSistema=45`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.Id,
                    val.Numero,
                    val.OrganoBiopsia,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.GES,
                    val.ServicioOrigen,
                    val.ServicioDestino
                ]);
            });

            $('#tblBiopsiasDespachar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        {
                            targets: 2,
                            data: null,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return `<a data-id="${adataset[fila][1]}" href="#/" onclick="verBiopsia(this)">${adataset[fila][2]}</a>`;
                            }
                        },
                        { targets: [1], visible: false, searchable: false },
                        { targets: [4], sType: 'date-ukShort' },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="checkDesBiopsia">';
                                botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                                botones += '</label>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: '' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'N° biopsia' },
                    { title: 'Órgano' },
                    { title: 'Recepción' },
                    { title: 'GES' },
                    { title: 'Solicitud' },
                    { title: 'Destino' }
                ],
                bDestroy: true
            });
        }
    });
}

function grillaParaInterconsulta() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/INTERCONSULTASPORDESPACHAR`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            //console.log(data);
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.ANA_idBiopsia,
                    val.ANA_gesRegistro_Biopsias,
                    val.ANA_NumeroRegistro_Biopsias,
                    val.ANA_descripcionBiopsia,
                    val.GEN_nombrePersonasTecnologo
                ]);
            });

            $('#tblBiopsiasInterconsulta').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        {
                            targets: 3,
                            data: null,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return `<a data-id="${adataset[fila][1]}" href="#/" onclick="verBiopsia(this)">${adataset[fila][3]}</a>`;
                            }
                        },
                        { targets: [1, 2], visible: false, searchable: false },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="checkIntBiopsia">';
                                botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                                botones += '</label>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: '' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'ANA_GesRegistro_Biopsias' },
                    { title: 'N° biopsia' },
                    { title: 'Órgano' },
                    { title: 'Tecnólogo' }
                ],
                bDestroy: true
            });
        }
    });
}

function verBiopsia(e) {
    let id = $(e).data('id');
    window.open(`../FormSolicitudes/frm_recepcion.aspx?id=${id}&id_soli=0`, '_blank');
}