﻿//var iTecnicaBiopsia;
//var sTipoAlerta;
var vIdBiopsia;
var vSession;
$(document).ready(function () {

    vSession = getSession();
    let vCodigoPerfil = vSession.GEN_CodigoPerfil;

    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    if (vCodigoPerfil != 6 && vCodigoPerfil != 7 && vCodigoPerfil != 8) {
        $('#divPestañas').show();
        grillaTecnicasPendientes();
        grillaInmunoPendientes();
        grillaInterconsultas();
    }

    if (vCodigoPerfil == 4) {
        $.ajax({
            type: 'POST',
            url: `${ObtenerHost()}/FormPrincipales/frm_principal.aspx/updatesTecnologo`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
            }
        });
    }

    //carga grilla principal
    getFiltrosBandejaPrincipal();
    grillaPrincipal();

    $('#btnBuscar').click(function (e) {
        grillaPrincipal();
        e.preventDefault();
    });
    $('#btnLimpiar').click(function (e) {
        getFiltrosBandejaPrincipal();
        $('#selMostrar').val(0); // lo deja en lista de trabajo
        grillaPrincipal();
        e.preventDefault();
    });

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',                
                success: function (data) {
                    if (!$.isEmptyObject(data[0])) {
                        $('#txtNombre').val(data[0].GEN_nombrePaciente);
                        $('#txtApellidoP').val(data[0].GEN_ape_paternoPaciente);
                        $('#txtApellidoM').val(data[0].GEN_ape_maternoPaciente);
                    }
                    else {
                        $('#txtNombre').val('');
                        $('#txtApellidoP').val('');
                        $('#txtApellidoM').val('');
                    }
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });

    $('#btnGuardarNota').click(function (e) {
        let bValido = true;
        if ($('#txtNota').val() == '' || $('#txtNota').val() == null || $('#txtNota').val() == undefined) {
            resaltaElemento($('#txtNota'));
            bValido = false;
        }
        let json = {};

        if (bValido) {         
            json.ANA_idBiopsia = $('#txtIdBiopsia').val();
            json.ANA_detalleNota = $('#txtNota').val();

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Notas`,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha ingresado la nota');
                    $('#txtNota').val('');
                    grillaNotas($('#txtIdBiopsia').val());
                }
            });
        }
        e.preventDefault();
    });
});

function solicitarCaseteAlmacenado(idCorte) {    
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${idCorte}/Solicitar`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha Solicitado Casete');
            grillaInmunoPendientes();
            grillaTecnicasPendientes();
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}

function grillaTecnicasPendientes() {
    vCodigoPerfil = vSession.GEN_CodigoPerfil;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Tecnica_Biopsia/PENDIENTES`,
        contentType: 'application/json',
        dataType: 'json',       
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idTecnicaBiopsia,
                    val.ANA_idBiopsia,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.ANA_idCortes_Muestras,
                    moment(val.ANA_fecTecnica).format('DD/MM/YYYY hh:mm:ss'),
                    val.ANA_nomCortes_Muestras,
                    val.ANA_nomTecnica,
                    val.GEN_loginUsuariosPatologo,
                    val.GEN_loginUsuariosTecnologo,
                    val.GEN_nombreTipo_Estados_SistemasCortes,
                    '',
                    ''
                ]);
            });
            $('#bdgListadoTecnicas').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarTec').attr('disabled', true);
            else
                $('#btnExportarTec').removeAttr('disabled');

            $('#tblListadoTecEspeciales').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2, 3], visible: false, searchable: false },
                        {
                            targets: 10,
                            data: null,
                            visible: vCodigoPerfil == 2 || vCodigoPerfil == 4 || vCodigoPerfil == 10,//admin, tecnólogo, supervisor tecnólogo
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones = "";
                                let estadoCasete = adataset[fila][2];
                                if (estadoCasete == '52')//almacenado
                                {
                                    botones = '<a data-id="' + adataset[fila][3] + '" onclick="solicitarCaseteAlmacenado(' + adataset[fila][3] + ')" class="btn btn-warning">Solicitar casete</a>';
                                }
                                else if (estadoCasete == '60')// tincion y montaje
                                {
                                    botones = '<a data-id="' + adataset[fila][0] + '" data-name="entregarTecnica" onclick="MostrarModalConfirmacion(this)" class="btn btn-success">Entregar</a>';
                                }
                                else
                                    botones = '';

                                return botones;
                            }
                        },
                        {
                            targets: 11,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdTecnicaBiopsia' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'ANA_idCortes_Muestras' },
                    { title: 'Fecha' },
                    { title: 'Nº corte' },
                    { title: 'Técnica' },
                    { title: 'Solicitado por' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado casete' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function grillaInmunoPendientes() {
    vCodigoPerfil = vSession.GEN_CodigoPerfil;
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Inmuno_Histoquimica_Biopsia/PENDIENTES',
        contentType: 'application/json',
        dataType: 'json',         
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idInmuno_Histoquimica_Biopsia,  //0
                    val.ANA_idBiopsia,                      //1
                    val.GEN_idTipo_Estados_Sistemas,//2
                    val.ANA_idCortes_Muestras,//3
                    moment(val.ANA_fecInmuno_Histoquimica_Biopsia).format('DD/MM/YYYY hh:mm:ss'),//4
                    val.ANA_nomCortes_Muestras,//5
                    val.ANA_nomInmunoHistoquimica,//6
                    val.GEN_loginUsuariosPatologo,//7
                    val.GEN_loginUsuariosTecnologo,//8
                    val.GEN_nombreTipo_Estados_SistemasCortes,//9
                    '',//10
                    '',//11
                    val.Acciones.Entregar,//12
                    val.Acciones.Solicitar,//13
                    val.Acciones.EntregarAtrasado,//14
                    val.Acciones.VerCaso//15

                ]);
            });
            $('#bdgListadoInmuno').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarInmuno').attr('disabled', true);
            else
                $('#btnExportarInmuno').removeAttr('disabled');
            $('#tblListadoInmuno').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2, 3], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            visible: vCodigoPerfil == 2 || vCodigoPerfil == 4 || vCodigoPerfil == 10,//admin, tecnólogo, supervisor tecnólogo
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones = "";

                                if (adataset[fila][13] == true)//entregar                             
                                    botones += '<a data-id="' + adataset[fila][0] + '" data-name="entregarInmuno" onclick="MostrarModalConfirmacion(this)" class="btn btn-success">Entregar</a>';

                                if (adataset[fila][14] == true)//Solicitar                             
                                    botones += '<a data-id="' + adataset[fila][3] + '" onclick="solicitarCaseteAlmacenado(' + adataset[fila][3] + ')" class="btn btn-warning">Solicitar casete</a>';

                              //  if (adataset[fila][15] == true)//Entregar Atrasado                             
                                //    botones += '<a data-id="' + adataset[fila][0] + '" data-name="entregarInmunoAtrasada" onclick="MostrarModalConfirmacion(this)" class="btn btn-danger">Entrega Atrasada</a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ANA_IdInmuno_Histoquimica_Biopsia' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'idCortes_Muestras' },
                    { title: 'Fecha' },
                    { title: 'Nº corte' },
                    { title: 'Inmunohistoquímicas' },
                    { title: 'Solicitado por' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado casete	' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function grillaInterconsultas() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/INFORME/INTERCONSULTASPORDESPACHAR',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idBiopsia,
                    val.ANA_NumeroRegistro_Biopsias,
                    val.ANA_descripcionBiopsia,
                    moment(val.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY-MM-DD').format('DD-MM-YYYY'),
                    val.GEN_nombrePersonasTecnologo,
                    val.GEN_nombreTipo_Estados_Sistemas,
                    ''
                ]);
            });
            $('#bdgInterconsultas').html(adataset.length);
            if (adataset.length == 0)
                $('#btnExportarInterconsultas').attr('disabled', true);
            else
                $('#btnExportarInterconsultas').removeAttr('disabled');

            $('#tblInterconsultas').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: 3, sType: 'date-ukShort' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                let botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" onclick="linkVerMacroscopia(this)" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'N° de biopsia' },
                    { title: 'Órgano' },
                    { title: 'Fecha recepción' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function MostrarModalConfirmacion(e) {
    let id = $(e).data('id');
    let name = $(e).data('name');
    if (name == 'entregarTecnica') {
        $('#lblConfirmacion').text('¿Confirma la entrega de esta técnica?');
    }
    else if (name == 'entregarInmuno') {
        $('#lblConfirmacion').text('¿Confirma la entrega de esta Inmuno?');
    }
    else if (name == 'entregarInmunoAtrasada')
        $('#lblConfirmacion').text('¿Confirma la entrega de esta Inmuno en Forma Atrasada?');

    $('#btnConfirmarModal').removeAttr('data-id');  //asegurarse
    $('#btnConfirmarModal').removeAttr('data-name');

    $('#btnConfirmarModal').attr('data-id', id);
    $('#btnConfirmarModal').attr('data-name', name);

    $('#mdlConfirmacion').modal('show');
}
//crea lamina y entrega casete cuando tecnologo confirma.
function confirmaEntregar(e) {

    let id = $('#btnConfirmarModal').attr('data-id');  //asegurarse
    let name = $('#btnConfirmarModal').attr('data-name');    

    if (name == "entregarTecnica") {
        $.ajax({
            type: 'POST',
            url: GetWebApiUrl() + 'ANA_Tecnica_Biopsia/Entregar/' + id,
            contentType: 'application/json',
            dataType: 'json',            
            success: function (data) {
                if (data == undefined)//si es undefined es por que no devolvió error
                    toastr.success('Lámina entregada correctamente.');
                else
                    toastr.error(data);
                grillaTecnicasPendientes();
                $('#mdlConfirmacion').modal('hide');
            }
        });
    } else if (name == "entregarInmuno") {

        $.ajax({
            type: 'POST',
            url: GetWebApiUrl() + 'ANA_Inmuno_Histoquimica_Biopsia/Entregar/' + id,
            contentType: 'application/json',
            dataType: 'json',            
            success: function (data) {
                if (data == undefined)//si es undefined es por que no devolvió error
                    toastr.success('¡Se ha entregado esta Inmuno!');
                else
                    toastr.error(data);
                grillaInmunoPendientes();
                $('#mdlConfirmacion').modal('hide');
            }
        });
    }
    else if (name == "entregarInmunoAtrasada") {
        $.ajax({
            type: 'POST',
            url: `${GetWebApiUrl()}ANA_Inmuno_Histoquimica_Biopsia/${id}/Entregar/Atrasada`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data == undefined)//si es undefined es por que no devolvió error
                    toastr.success('¡Se ha entregado esta Inmuno!');
                else
                    toastr.error(data);
                grillaInmunoPendientes();
                $('#mdlConfirmacion').modal('hide');
            }
        });
    }
}

function grillaNotas(idBiopsia) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Notas`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.id,
                    moment(val.fecha).format('DD/MM/YYYY hh:mm:ss'),
                    val.login,
                    val.detalle
                ]);
            });

            $('#tblNotas').DataTable({
                lengthMenu: [[5, -1], [5, "Todos"]],
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: 0, visible: false, searchable: false }
                    ],
                columns: [
                    { title: 'ANA_IdNota' },
                    { title: 'Fecha' },
                    { title: 'Creado por' },
                    { title: 'Detalle nota' },
                ],
                bDestroy: true
            });
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}

function linkVerMacroscopia(e) {
    let id = $(e).data('id');
    window.open(`../FormAnatomia/frm_Cortes.aspx?id=${id}`, '_blank');
}

function verBiopsia(e) {
    let id = $(e).data('id');
    window.open(`../FormSolicitudes/frm_recepcion.aspx?id=${id}&id_soli=0`, '_self');
}

function verCortes(e) {
    let id = $(e).data('id');
    window.open(`../FormAnatomia/frm_cortes.aspx?id=${id}`, '_blank');
}

function verNotas(e) {
    let id = $(e).data('id');
    let bio = $(e).data('bio');
    let organo = $(e).data('organo');
    
    $('#txtNota').removeClass('_bordeError');
    $('#txtIdNota').val(bio);
    $('#txtOrgano').val(organo);
    $('#txtIdBiopsia').val(id);

    grillaNotas(id);
    $("#mdlNotas").modal('show');
}

function verMov(e) {
    let id = $(e).data('id');
    modalMovimiento(id);
    $('#mdlMovimientos').modal('show');
}

function verReporte(e) {
    let id = $(e).data('id');
    window.open(`../FormAnatomia/frm_InformeBiopsia.aspx?id=${id}`, '_self');
}

function linkAnularBiopsia(e) {
    $("#btnConfirmarModalAnular").data("id", $(e).data("id"));
    $("#txtMotivoAnulacion").removeAttr("style");
    $("#txtMotivoAnulacion").val("");
    $("#mdlConfirmacionAnular").modal("show");
}

function anularBiopsia() {
    if ($.trim($("#txtMotivoAnulacion").val()) != "") {

        let id = $("#btnConfirmarModalAnular").data('id');

        let json = {
            "observacion": $.trim($("#txtMotivoAnulacion").val())
        };

        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`,
            contentType: 'application/json',
            data: JSON.stringify(json),
            success: function (data) {
                toastr.success('¡Se ha Anulado la biopsia!');
                grillaPrincipal();
                $('#mdlConfirmacionAnular').modal('hide');
            }
        });

    } else
        $("#txtMotivoAnulacion").css({ "border": "1px solid red" });
}

function getFiltrosBandejaPrincipal() {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/ANA_Registro_Biopsias/Bandeja/Filtros`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $.each(data.Patologos, function (key, val) {
                $('#selPatologo').append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });

            $.each(data.TiposBiopsia, function (key, val) {
                $('#selTipoBiopsia').append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });

            $.each(data.TiposEstado, function (key, val) {
                $('#selMostrar').append(`<option value='${val.Id}'>${val.Valor}</option>`);
            });

            $('#selMostrar').val(0);
        },
        error: function (err) {
            console.log(`Error en bandeja/filtros`, JSON.stringify(err));
        }
    });

    $('#txtDesde').val(moment().add(-3, 'month').format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').attr("max", moment().format('YYYY-MM-DD'));
    $('#selPatologo').val(0);
    $('#selTipoBiopsia').val(0);
    $('#txtNumero').val(0);
    $('#txtAño').val(0);
    $('#chkInterconsulta').prop('checked', false);
    $('#chkCodificar').prop('checked', false);
    $('#chkCritico').prop('checked', false);
    $('#txtRut').val('');
    $('#txtDigito').html('-');
    $('#txtNombre').val('');
    $('#txtApellidoP').val('');
    $('#txtApellidoM').val('');
}

function grillaPrincipal() {
    vCodigoPerfil = vSession.GEN_CodigoPerfil;
    let iSelect = $('#selMostrar').val();
    let iPatologo = $('#selPatologo').val();
    let iTipoBiopsia = $('#selTipoBiopsia').val();
    let iNumero = (($('#txtNumero').val() > 0)) ? $('#txtNumero').val() : 0;
    let iFolio = (($('#txtAño').val() > 0)) ? $('#txtAño').val() : 0;
    let iInter = $('#selInterconsulta').val();

    let sUrl = `${GetWebApiUrl()}ANA_Registro_Biopsias/Bandeja/${iSelect}/${iPatologo}/${iTipoBiopsia}/${iNumero}/${iFolio}/${iInter}?documento=${
        ($.trim($("#txtRut").val()) !== "") ? $.trim($("#txtRut").val()) : ""}&nombre=${
        ($.trim($("#txtNombre").val()) !== "") ? $.trim($("#txtNombre").val()) : ""}&ap1=${
        ($.trim($("#txtApellidoP").val()) !== "") ? $.trim($("#txtApellidoP").val()) : ""}&ap2=${
        ($.trim($("#txtApellidoM").val()) !== "") ? $.trim($("#txtApellidoM").val()) : ""}&fechaInicial=${
        ($.trim($("#txtDesde").val()) !== "") ? $.trim($("#txtDesde").val()) : ""}&fechaFinal=${
        ($.trim($("#txtHasta").val()) !== "") ? $.trim($("#txtHasta").val()) : ""}`;

    $.ajax({
        type: 'GET',
        url: sUrl,
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {

                adataset.push([
                /*0*/  val.ANA_idBiopsia,
                /*1*/  val.ANA_numeroRegistro_Biopsias,
                /*2*/  val.ANA_idTipo_Biopsia,
                /*3*/  val.ANA_organoBiopsia,
                /*4*/  val.ANA_fec_RecepcionRegistro_Biopsias == '' ? '' : moment(val.ANA_fec_RecepcionRegistro_Biopsias).format('DD/MM/YYYY'),
                /*5*/  val.GEN_nombreTipo_Estados_Sistemas,
                /*6*/  val.ANA_gesRegistro_Biopsias,
                /*7*/  val.ANA_Servicio_Solicita,
                /*8*/  val.ANA_diasTrascurridos,
                /*9*/  val.ANA_fecValidaBiopsia == null ? '' : moment(val.ANA_fecValidaBiopsia).format('DD/MM/YYYY hh:mm:ss'),
                /*10*/ val.ANA_dictadaBiopsia,
                /*11*/ val.Acciones.Cortes,
                /*12*/ val.Acciones.Notas,
                /*13*/ val.Acciones.Movimientos,
                /*14*/ val.Acciones.Reporte,
                /*15*/ val.Acciones.Anular
                ]);
            });

            $('#tblPrincipal').DataTable({
                data: adataset,
                order: [],
                columns: [
                    /*0*/{ title: 'ANA_IdBiopsia' },
                    /*1*/{ title: 'N° biopsia' },
                    /*2*/{ title: 'ANA_idTipo_Biopsia' },
                    /*3*/{ title: 'Órgano' },
                    /*4*/{ title: 'Fecha recepción' },
                    /*5*/{ title: 'Estado' },
                    /*6*/{ title: 'GES' },
                    /*7*/{ title: 'Solicitado' },
                    /*8*/{ title: 'Días' },
                    /*9*/{ title: 'Fecha validación' },
                    /*10*/{ title: 'Dictada Biopsia' },
                    /*11*/{ title: 'Cortes' },
                    /*12*/{ title: 'Notas' },
                    /*13*/{ title: 'Movim. Biopsia' },
                    /*14*/{ title: 'Reporte' },
                    /*15*/{ title: 'Anular' }
                ],
                columnDefs:
                    [
                        { targets: [0, 2, 6, 10], visible: false, searchable: false },
                        {
                            targets: 1,
                            data: null,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return `<a data-id="${adataset[fila][0]}" href="#/" onclick="verBiopsia(this)">${adataset[fila][1]}</a>`;
                            }
                        },
                        {
                            targets: 11,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                if (adataset[fila][11]) {
                                    if (adataset[fila][10] == 'NO') //dictada 'NO' = color rojo
                                        return `
                                            <a class="btn btn-danger" data-id="${adataset[fila][0]}" onclick="verCortes(this)">
                                                <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span>
                                            </a>`;
                                    else
                                        return `
                                            <a class="btn btn-warning" data-id="${adataset[fila][0]}"  onclick="verCortes(this)">
                                                <span class="glyphicon glyphicon-scissors" aria-hidden="true"></span>
                                            </a>`;
                                } else
                                    return `<a class="btn btn-disabled">
                                                <span class="glyphicon glyphicon-ban-circle"></span>
                                            </a>`;
                            }
                        },
                        {
                            targets: 12,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][12]) ?
                                    `<a class="btn btn-success" data-id="${adataset[fila][0]}" data-bio="${adataset[fila][1]}" 
                                        data-organo="${adataset[fila][3]}" onclick="verNotas(this)">
                                        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 13,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][13]) ?
                                    `<a class="btn btn-info" data-id="${adataset[fila][0]}" onclick="verMov(this)">
                                        <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 14,
                            data: null,
                            orderable: false,
                            searchable: false,
                            visible: (vCodigoPerfil == 3 || vCodigoPerfil == 8 || vCodigoPerfil == 2) ? true : false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][14]) ?
                                    `<a class="btn btn-success" data-id="${adataset[fila][0]}" onclick="verReporte(this)">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                            }
                        },
                        {
                            targets: 15,
                            data: null,
                            orderable: false,
                            searchable: false,
                            visible: (vCodigoPerfil == 2 || vCodigoPerfil == 8 || vCodigoPerfil == 6) ? true:false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return (adataset[fila][15]) ?
                                    `<a class="btn btn-danger" data-id="${adataset[fila][0]}" onclick="linkAnularBiopsia(this)">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>` :
                                    `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;

                            }
                        }
                    ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                    let diasTranscurridos = aData[8];

                    if (aData[6].trim() == 'SI') //si es caso GES  pinta de color la fila
                        $('td', nRow).css('background-color', '#DFF0D8');

                    if (aData[2] == 3) //si es tipo "citología"
                        $('td', nRow).css('background-color', '#d8daf0');

                    if (diasTranscurridos >= 30) {
                        $(nRow).find('td:eq(5)').css('background-color', 'red');
                        $(nRow).find('td:eq(5)').css('color', 'white');
                    } else if (diasTranscurridos >= 20) {
                        $(nRow).find('td:eq(5)').css('background-color', 'yellow');
                    } else {
                        $(nRow).find('td:eq(5)').css('background-color', 'green');
                        $(nRow).find('td:eq(5)').css('color', 'white');
                    }
                },
                bDestroy: true
            });
        }
    });
}