﻿$(document).ready(function () {
    $('#txtFechaDesde').val(moment().add(-3, 'month').format('YYYY-MM-DD'));
    $('#txtFechaHasta').val(moment().format('YYYY-MM-DD'));

    var vSession = getSession();

    if (vSession.GEN_CodigoPerfil == 9) //clinico
    {
        $('#pnlBiopsiasSolicitadas').show();
        $('#pnlNotificaciones').hide();
        grillaBiopsias();
    }
    else {
        grillaNotificaciones();
    }

    $('#btnBuscar').click(function (e) {
        grillaBiopsias();
        e.preventDefault();
    });
    $('#btnBuscarNotificacion').click(function (e) {
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnLimpiarFiltro').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtFicha').val('');
        grillaNotificaciones();
        e.preventDefault();
    });

    $('#btnGuardar').click(function (e) {
        $.ajax({
            type: 'POST',
            url: ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/guardarNotificacion',
            data: JSON.stringify({ idBiopsiaCritico: Ana_IdBiopsiaCritico, respuestaNotificacion: $('#txtRespuestaNotificacion').val() }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('La información fue guardada correctamente');
                $('#mdlNotificacion').modal('hide');
            }
        });
        e.preventDefault();
    });

    $('#btnCerrar').click(function (e) {
        let idUsuario = vSession.id_usuario;
        $.ajax({
            type: 'POST',
            url: ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/cerrarNotificacion',
            data: JSON.stringify({ idBiopsiaCritico: Ana_IdBiopsiaCritico, respuestaNotificacion: $('#txtRespuestaNotificacion').val(), idUsuario: idUsuario }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                toastr.success('Se ha cerrado correctamente la notificación');
                $('#mdlNotificacion').modal('hide');
                grillaNotificaciones();
            }
        });
        e.preventDefault();
    });

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    $('#txtNombre').val(data[0].GEN_nombrePaciente);
                    $('#txtApellidoP').val(data[0].GEN_ape_paternoPaciente);
                    $('#txtApellidoM').val(data[0].GEN_ape_maternoPaciente);
                    $('#txtFicha').val(data[0].GEN_nuiPaciente);
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });
});

function grillaNotificaciones() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/Notificaciones',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
          
            $.each(data, function (key, val) {
                adataset.push([
                    val.RegistroBiopsia.numero,
                    val.ANA_idBiopsia,
                    val.ANA_idBiopsias_Critico,
                    moment(val.RegistroBiopsia.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY/MM/DD').format('DD-MM-YYYY'),
                    val.Paciente.GEN_nombrePaciente,
                    val.RegistroBiopsia.PatologoValidador[0] != undefined ? val.RegistroBiopsia.PatologoValidador[0].GEN_NombreUsuarios : "",
                    val.RegistroBiopsia.GEN_nombreTipo_Estados_Sistemas,
                    val.ANA_idBiopsia,
                    'Ver',
                    '',
                    '',
                    ''
                ]);
            });

            $('#tblNotificaciones').addClass('nowrap').DataTable({
                data: adataset,
                order: [],
                stateSave: true,
                iStateDuration: 60,
                columnDefs:
                    [
                        { targets: 3, sType: 'date-ukShort' },
                        { targets: [1, 2, 7, 8], visible: false, searchable: false },
                        {
                            targets: -3,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][1] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a onclick="linkImprimirB(' + adataset[fila][1] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][1] + '" data-idcritico="' + adataset[fila][2] + '" onclick="linkNotificacion(this)" class="btn btn-success">Ver</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Nº de biopsia' },
                    { title: 'ANA_IdBiopsia' },
                    { title: 'ANA_IdBiopsias_Critico' },
                    { title: 'Recep. anatomía' },
                    { title: 'Paciente' },
                    { title: 'Patólogo' },
                    { title: 'Estado' },
                    { title: 'id_biopsia' },
                    { title: 'reporte' },
                    { title: 'Información' },
                    { title: 'Imprimir' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function linkInformacionB(e) {
    let id = $(e).data("id");
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}?tipo=2`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $('#spanFechaRecepcion').html(moment(data.ANA_fec_RecepcionRegistro_Biopsias).format('DD-MM-YYYY'));
            $('#spanRutPaciente').html(data.Paciente[0].GEN_numero_documentoPaciente);
            $('#spanNombrePaciente').html(data.Paciente[0].GEN_nombrePaciente);

            $('#spanPatologo').html(data.GEN_nombrePersonasPatologo[0].GEN_nombrePersonas);
            $('#spanMedico').html(data.GEN_nombrePersonasMedico[0].GEN_nombrePersonas);
            $('#spanOrigen').html(data.GEN_nombreServicioOrigen);
            $('#spanDestino').html(data.GEN_nombreServicioDestino);
            $('#spanEstado').html(data.GEN_nombreTipo_Estados_Sistemas);
        }
    });
    $('#mdlInformacionB').modal('show');
}

function linkImprimirB(idB) {        
    let idUsuario = getSession().id_usuario;
    window.open('../FormImpresion/IMP_Informe.aspx?idb=' + idB + '&idu=' + idUsuario, '_blank');
}

function linkNotificacion(e) {
    let id = $(e).data("id");
    var idCritico = $(e).data("idcritico");
    $.ajax({
        type: 'POST',
        url: ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/informacionNotificacion',
        data: JSON.stringify({ idBiopsiaCritico: idCritico }),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            var json = JSON.parse(data.d);
            if (json.ANA_LeidoBiopsias_Critico == 'SI')
                $('#chkVisto').attr('checked', true);
            else
                $('#chkVisto').removeAttr('checked');

            $('#txtNumero').val(json.ANA_NumBiopsia);
            $('#txtUbInterna').val(json.GEN_NuiPaciente);
            $('#txtDocumento').val(json.GEN_numero_documentoPaciente);
            $('#txtNombrePaciente').val(json.GEN_NombreCompletoPaciente);
            $('#txtServicioOrigen').val(json.Nom_Servicio_Solicita);
            $('#txtSolicitadoPor').val(json.GEN_NombreSolicitante);
            $('#txtValidadoPor').val(json.GEN_NombreCompletoUsuario);
            $('#txtEstadoInforme').val(json.ANA_EstadoRegistro_Biopsias);
            $('#txtOrgano').val(json.ANA_OrganoBiopsia);
            $('#txtNota').val(json.Ana_ObservacionCritico);
            $('#txtRespuestaNotificacion').val(json.ANA_RespuestaBiopsias_Critico);

            Ana_IdBiopsiaCritico = json.Ana_IdBiopsiaCritico;

            $('#mdlNotificacion').modal('show');
        }
    });
}

function grillaBiopsias() {
    let iGEN_idProfesional = 0;

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + '/GEN_Usuarios/LOGEADO',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            iGEN_idProfesional = data[0].GEN_idProfesional;
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormPrincipales/frm_PrincipalClinico.aspx/grillaBiopsias',
                data: JSON.stringify({ idProfesional: iGEN_idProfesional, sDesde: $('#txtFechaDesde').val(), sHasta: $('#txtFechaHasta').val() }),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    var data = JSON.parse(data.d);
                    let adataset = [];
                    $.each(data, function (key, val) {
                        adataset.push([
                            val.ANA_IdBiopsia,
                            val.n_biopsia,
                            moment(val.ANA_fec_RecepcionRegistro_Biopsias, 'DD/MM/YYYY').format('DD-MM-YYYY'),
                            val.NumDocumentoPaciente,
                            val.GEN_nombrePaciente,
                            val.Gen_nombrePatologo,
                            val.Gen_nombreTecnologo,
                            val.GEN_idTipo_Estados_Sistemas,
                            val.GEN_nombreTipo_Estados_Sistemas,
                            val.GEN_nombreServicio,
                            val.ANA_fecValidaBiopsia,
                            '',
                            ''
                        ]);
                    });

                    $('#tblBiopsias').addClass('nowrap').DataTable({
                        data: adataset,
                        order: [],
                        stateSave: true,
                        iStateDuration: 60,
                        columnDefs:
                            [
                                { targets: 2, sType: 'date-ukShort' },
                                { targets: [0, 3, 6, 7, 9, 10], visible: false, searchable: false },
                                {
                                    targets: -2,
                                    data: null,
                                    orderable: false,
                                    render: function (data, type, row, meta) {
                                        let fila = meta.row;
                                        var botones;
                                        botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkInformacionB(this)"><span class="glyphicon glyphicon-info-sign"></span></a>';
                                        return botones;
                                    }
                                },
                                {
                                    targets: -1,
                                    data: null,
                                    orderable: false,
                                    render: function (data, type, row, meta) {
                                        let fila = meta.row;
                                        var botones;

                                        if (adataset[fila][7] == 44)
                                            botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                        else if (adataset[fila][7] == 45) {
                                            var date1 = moment().add(-1, 'days');
                                            var date2 = moment(adataset[fila][10], 'DD/MM/YYYY');
                                            if (date1 > date2)
                                                botones = '<a onclick="linkImprimirB(' + adataset[fila][0] + ')" class="btn btn-info"><span class="glyphicon glyphicon-print"></span></a>';
                                            else
                                                botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';
                                        }
                                        else
                                            botones = '<a class="btn btn-info" disabled><span class="glyphicon glyphicon-print"></span></a>';
                                        return botones;
                                    }
                                },
                            ],
                        columns: [
                            { title: 'ANA_IdBiopsia' },
                            { title: 'Nº de biopsia' },
                            { title: 'Recep. anatomía' },
                            { title: 'NumDocumentoPaciente' },
                            { title: 'Paciente' },
                            { title: 'Patólogo' },
                            { title: 'Gen_nombreTecnologo' },
                            { title: 'GEN_idTipo_Estados_Sistemas' },
                            { title: 'Estado' },
                            { title: 'GEN_nombreServicio' },
                            { title: 'ANA_fecValidaBiopsia' },
                            { title: 'Información' },
                            { title: 'Imprimir' }
                        ],
                        bDestroy: true
                    });
                }
            });
        }
    });
}