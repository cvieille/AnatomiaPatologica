﻿var vIdBiopsia;
var vIdUsuario;
$(document).ready(function () {
    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    if (sSession.GEN_CodigoPerfil == 13)
        $('#btnCodificacion').show();

    $('#txtRut').blur(function (e) {
        if ($(this).val().length > 6 && /^[0-9]*$/.test($(this).val())) {
            $('#txtDigito').html(DigitoVerificador($(this).val()));
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/1/${$(this).val()}`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (!$.isEmptyObject(data[0])) {
                        $('#txtNombre').val(data[0].GEN_nombrePaciente);
                        $('#txtApellidoP').val(data[0].GEN_ape_paternoPaciente);
                        $('#txtApellidoM').val(data[0].GEN_ape_maternoPaciente);
                        $('#txtNUI').val(data[0].GEN_nuiPaciente);
                    }
                    else {
                        $('#txtNombre').val('');
                        $('#txtApellidoP').val('');
                        $('#txtApellidoM').val('');
                        $('#txtNUI').val('');
                    }
                }
            });
        }
        else
            $('#txtDigito').html('-');
    });

    $('#btnBuscar').click(function (e) {
        if (($('#txtRut').val() == '' || $('#txtRut').val() == null) &&
            ($('#txtNombre').val() == '' || $('#txtNombre').val() == null) &&
            ($('#txtApellidoP').val() == '' || $('#txtApellidoP').val() == null) &&
            ($('#txtApellidoM').val() == '' || $('#txtApellidoM').val() == null) &&
            ($('#txtNUI').val() == '' || $('#txtNUI').val() == null)) {
            toastr.error('Debe llenar al menos un filtro de búsqueda');
        }
        else {
            grillaPaciente();
            $('#btnLimpiar').removeAttr('disabled');
        }
        e.preventDefault();
    });

    $('#btnLimpiar').click(function (e) {
        $('#txtRut').val('');
        $('#txtDigito').html('-');
        $('#txtNombre').val('');
        $('#txtApellidoP').val('');
        $('#txtApellidoM').val('');
        $('#txtNUI').val('');
        var t = $('#tblPaciente').DataTable();
        t.clear();
        t.draw(false);
        e.preventDefault();
    });

    $('#btnImprimirInforme').click(function (e) {

        let idBiopsiaImprimir = $('#spanIdBiopsia').text();
        let urlInforme = ObtenerHost() + "/FormImpresion/IMP_Informe.aspx?idb=" + idBiopsiaImprimir + "&idu=" + vIdUsuario;
        window.open(urlInforme, '_blank');
        urlInforme = "";
        e.preventDefault();
    });
});

function detalleArchivo(e) {

    let id = $(e).data('id');
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/ANA_Archivos_Biopsias/${id}`,
        contentType: 'application/json',        
        dataType: 'json',
        success: function (data) {      
            let url = ObtenerHost() + '/Documento.aspx?nombre=' + data.Archivo.Nombre + '&idBiopsia=' + data.Archivo.IdBiopsia ;
            window.open(url);
        }
    });
}

function linkReporte(e) {
    let id = $(e).data('id');
    $('#btnImprimirInforme').attr('data-id', id);

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}?tipo=2`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            $('#spanIdBiopsia').html(data.ANA_idBiopsia);
            $('#spanFechaRecepcion').html(moment(data.ANA_fec_RecepcionRegistro_Biopsias).format('DD-MM-YYYY'));
            $('#spanRutPaciente').html(data.Paciente[0].GEN_numero_documentoPaciente);
            $('#spanNombrePaciente').html(data.Paciente[0].GEN_nombrePaciente);

            $('#spanPatologo').html(data.GEN_nombrePersonasPatologo[0].GEN_nombrePersonas);
            $('#spanTecnologo').html(data.GEN_nombrePersonasTecnologo[0].GEN_nombrePersonas);
            $('#spanMedSolicitante').html(data.GEN_nombrePersonasMedico[0].GEN_nombrePersonas);
            $('#spanServOrigen').html(data.GEN_nombreServicioOrigen);
            $('#spanServDestino').html(data.GEN_nombreServicioDestino);

            var o = '';
            for (var i = 0; i < data.ANA_organoBiopsia.length; i++)
                o += data.ANA_organoBiopsia[i] + '<br/>';
            $('#spanOrgano').html(o);

            $('#txt0801001').val(data.Codificacion[0].ANA_cod001Codificacion_Biopsia);
            $('#txt0801002').val(data.Codificacion[0].ANA_cod002Codificacion_Biopsia);
            $('#txt0801003').val(data.Codificacion[0].ANA_cod003Codificacion_Biopsia);
            $('#txt0801004').val(data.Codificacion[0].ANA_cod004Codificacion_Biopsia);
            $('#txt0801005').val(data.Codificacion[0].ANA_cod005Codificacion_Biopsia);
            $('#txt0801006').val(data.Codificacion[0].ANA_cod006Codificacion_Biopsia);
            $('#txt0801007').val(data.Codificacion[0].ANA_cod007Codificacion_Biopsia);
            $('#txt0801008').val(data.Codificacion[0].ANA_cod008Codificacion_Biopsia);

        }
    });

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/Archivos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Archivo.Id,
                    moment(val.Archivo.Fecha).format('DD/MM/YYYY hh:mm:ss'),
                    val.Archivo.Nombre,
                    val.Archivo.Ruta
                ]);
            });
            $('#tblArchivo').DataTable({
                data: adataset,
                order: [],
                dom: 't',
                columnDefs:
                    [
                        { targets: [3], visible: false, searchable: false },
                        // { targets: 0, sType: 'date-ukLong' },
                        {
                            targets: 2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones = `<a data-id="${adataset[fila][0]}" href="#/" onclick="detalleArchivo(this)">${adataset[fila][2]}</a>`;
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id' },
                    { title: 'Fecha' },
                    { title: 'Archivo' }
                ],
                bDestroy: true
            });
        }
    });

    $('#mdlBiopsia').modal('show');
}

function grillaPaciente() {
    let json = {};
    json.rut = $('#txtRut').val();
    json.nombre = $('#txtNombre').val();
    json.app = $('#txtApellidoP').val();
    json.apm = $('#txtApellidoM').val();
    json.nui = $('#txtNUI').val();

    $.ajax({
        type: 'POST',
        url: `${ObtenerHost()}/frm_consulta_clinicos.aspx/grillaReportes`,
        data: JSON.stringify({ s: JSON.stringify(json) }),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            var data = JSON.parse(data.d);
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_IdBiopsia,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.NumDocumentoPaciente,
                    val.n_biopsia,
                    moment(val.ANA_fec_RecepcionRegistro_Biopsias, 'DD/MM/YYYY').format('DD-MM-YYYY'),
                    val.GEN_nombrePaciente,
                    val.Gen_nombrePatologo,
                    (val.Gen_nombreTecnologo == '') ? 'Sin definir' : val.Gen_nombreTecnologo,
                    val.GEN_nombreTipo_Estados_Sistemas,
                    val.GEN_nombreServicio,
                    val.ANA_fecValidaBiopsia == '' ? '' : (moment(val.ANA_fecValidaBiopsia, 'DD/MM/YYYY hh:mm:ss').format('DD/MM/YYYY HH:mm:ss')),
                    ''
                ]);
            });
            $('#tblPaciente').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2], visible: false, searchable: false },
                        { targets: 4, sType: 'date-ukShort' },
                        { targets: 10, sType: 'date-ukLong' },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="linkReporte(this)"><span class="glyphicon glyphicon-list-alt"></span></a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'NumDocumentoPaciente' },
                    { title: 'N° de biopsia' },
                    { title: 'Fecha recepción' },
                    { title: 'Paciente' },
                    { title: 'Patólogo' },
                    { title: 'Tecnólogo' },
                    { title: 'Estado' },
                    { title: 'Destino' },
                    { title: 'Fecha validación' },
                    { title: 'Reporte' }
                ],
                bDestroy: true
            });
        }
    });
}