﻿/*
 * helper
 */
var parametros = {};
function ui_add_log(message, color) {
    var d = new Date();

    var dateString = (('0' + d.getHours())).slice(-2) + ':' +
        (('0' + d.getMinutes())).slice(-2) + ':' +
        (('0' + d.getSeconds())).slice(-2);

    color = (typeof color === 'undefined' ? 'muted' : color);

    var template = $('#debug-template').text();
    template = template.replace('%%date%%', dateString);
    template = template.replace('%%message%%', message);
    template = template.replace('%%color%%', color);

    $('#debug').find('li.empty').fadeOut(); // remove the 'no messages yet'
    $('#debug').prepend(template);
}

function ui_multi_add_file(id, file, p) {
    var template = $('#files-template').text();
    var fn = file.name;

    fn = fn.replace(/\+/g,' ');
    fn = fn.replace(/°/g,' ');
    fn = fn.replace(/,/g, ' ');
    fn = fn.replace(/  /g,' ');
    fn = fn.replace(/   /g, ' ');
    
    if (fn.length > 100) {
        fn = fn.substring(0, 95);
        fn = fn + '.pdf';
    }

    template = template.replace('%%filename%%', fn);
    template = $(template);
    template.find('#progress-status').show();
    template.find('#progress-bar-entire').show();
    template.prop('id', id);
    template.data('file-id', id);
    $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
    $('#files').prepend(template);

    var json = {};
    //json.ANA_idArchivos_Biopsias = vIdBiopsia;
    //json.ANA_nomArchivos_Biopsias = fn;
    //json.ANA_idBiopsia = p.idb;
    //json.ANA_fecArchivos_Biopsias = p.fecha;
    //json.GEN_idUsuarios = p.idu;
    //json.ANA_estadoArchivos_Biopsias = 'Activo';
    json.IdBiopsia = vIdBiopsia;
    json.Nombre = fn;
    //Esta ruta lleva la ID
    $.ajax({
        type: 'POST',
        url: `${GetWebApiUrl()}ANA_Archivos_Biopsias`,
        data: JSON.stringify(json),
        contentType: 'application/json',
        data: JSON.stringify(json),
        dataType: 'json',        
        success: function (data) {
            toastr.success('Se ha guardado el archivo');
            ContarAdjuntos(true);
        },
        error: function (jqXHR, status) {
            console.log(jqXHR);
        }
    });
 
}

function ui_multi_update_file_status(id, status, message) {
    $('#' + id).find('span').html(message).prop('class', 'status text-' + status);
}

function ui_multi_update_file_progress(id, percent, color, active) {
    color = (typeof color === 'undefined' ? false : color);
    active = (typeof active === 'undefined' ? true : active);

    var bar = $('#' + id).find('div.progress-bar');

    bar.width(percent + '%').attr('aria-valuenow', percent);
    bar.toggleClass('progress-bar-striped progress-bar-animated', active);

    if (percent === 0) {
        bar.html('');
    } else {
        bar.html(percent + '%');
    }
    if (color !== false) {
        bar.removeClass('bg-success bg-info bg-warning bg-danger');
        bar.addClass('bg-' + color);
    }
}

$(function () {
    $('#drag-and-drop-zone').dmUploader({ //
        url: ObtenerHost() + "/Clases/MetodosGenerales.ashx?method=GuardarArchivo&idBiopsia=" + getUrlParameter('id'),
        maxFileSize: 5000000, // 5 Megs 
        multiple: true,
        //extFilter: ['pdf', 'xls', 'xlsx', 'doc', 'docx', 'png', 'jpg', 'gif', 'bmp', 'ppt', 'pptx'],
        extraData: function () {
            return parametros;
        },
        onFileExtError: function (id, file) {
            var fn = id.name;
            fn = fn.replace(/\+/g, ' ');
            fn = fn.replace(/°/g, ' ');
            fn = fn.replace(/  /g, ' ');
            fn = fn.replace(/   /g, ' ');
            ui_add_log('La extensión del archivo [' + fn + '] no es válida', 'danger');
            $('#divError').show('slow');
        },
        onDragEnter: function () {
            // dragging something over the DnD
            this.addClass('active');
        },
        onDragLeave: function () {
            // dragging something OUT of the DnD
            this.removeClass('active');
        },
        onInit: function () {
            //ui_add_log('Penguin initialized :)', 'info');
        },
        onComplete: function () {
            //ui_add_log('All pending tranfers finished');
        },
        onNewFile: function (id, file) {
            //ui_add_log('New file added #' + id);
            ui_multi_add_file(id, file, parametros);
        },
        onBeforeUpload: function (id) {
            //ui_add_log('Starting the upload of #' + id);
            ui_multi_update_file_status(id, 'uploading', 'Enviando...');
            ui_multi_update_file_progress(id, 0, '', true);
        },
        onUploadCanceled: function (id) {
            ui_multi_update_file_status(id, 'warning', 'Cancelado por el usuario');
            ui_multi_update_file_progress(id, 0, 'warning', false);
        },
        onUploadProgress: function (id, percent) {
            ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function (id, data) {
            //ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
            //ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
            ui_multi_update_file_status(id, 'success', 'Subida Completada');
            ui_multi_update_file_progress(id, 100, 'success', false);
        },
        onUploadError: function (id, xhr, status, message) {
            ui_multi_update_file_status(id, 'danger', message);
            ui_multi_update_file_progress(id, 0, 'danger', false);
        },
        onFallbackMode: function () {
            alert('su navegador no es compatible con esta característica');
            //ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
        },
        onFileSizeError: function (file) {
            alert('el tamaño del archivo no debe superar los 5mb');
            //ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
        }
    });
});

function setParametro(url, biopsia, usuario, fecha) {
    parametros = {
        'url': url,
        'idb': biopsia,
        'idu': usuario,
        'fecha': fecha
    };
}