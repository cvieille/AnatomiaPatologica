﻿var vIdUsuario;
$(document).ready(function () {
    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    grillaSolBiopsia();
    grillaSolCasete();
    grillaSolLamina();

    grillaMuestraAlmacenar();
    grillaCaseteAlmacenar();
    grillaCaseteDespachadoparaAlmacenar();
    grillaLaminaAlmacenar();
    grillaLaminaDesechadasporAlmacenar()



    $('body').on('change', '#chkTodoCasete', function () {
        var bValidar = true;
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++) {
            $(d[i]).find('.check')[0].checked = $(this).prop('checked');

            if ($(d[i]).find('.check').attr('s') != 52 && $(d[i]).find('.check').attr('s') != 55)
                bValidar = false;
        }

        if (bValidar || !$(this).prop('checked'))
            $('#btnAlmacenarCasete').removeAttr('disabled');
        else
            $('#btnAlmacenarCasete').attr('disabled', true);
    });
    $('body').on('change', '#chkTodoLamina', function () {
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.check')[0].checked = $(this).prop('checked');
    });

    $('#btnAlmacenarCasete').click(function (e) {
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        var cantCasete = 0;
        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.check')[0].checked) {
                cantCasete = cantCasete + 1;
                idCorte = $(d[i]).find('.check')[0].id;

                AlmacenarCaseteporID(idCorte);
            }
        }

        if (cantCasete == 0)
            toastr.info('Debe seleccionar al menos un casete');
        else
            grillaCaseteAlmacenar();

        e.preventDefault();
    });

    $('#btnDesecharCasete').click(function (e) {
        var a = [];
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.check')[0].checked)
                a.push($(d[i]).find('.check')[0].id);
        if (a.length > 0) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormAlmacenamientos/frm_Almacenamiento.aspx/desecharCasete',
                data: JSON.stringify({ s: JSON.stringify(a), idUsuario: vIdUsuario }),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    var d = JSON.parse(data.d);
                    if (d.length > 0) {
                        var v = '';
                        for (var i = 0; i < d.length; i++)
                            v += ', ' + d[i];
                        $('#lblModalC').text(v.substr(1));
                        $('#lblModalMensaje').text('Solo se pueden desechar las muestras que se encuentren almacenadas');
                        $('#divMensaje').show();
                        $('#mdlDetalle').modal('show');
                    }
                    else
                        toastr.success('Se han desechado las muestras seleccionadas');
                    grillaCaseteAlmacenar();
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        else
            toastr.info('Debe seleccionar al menos un casete');
        e.preventDefault();
    });

    $('#btnAlmacenarLamina').click(function (e) {
        var cantLaminas = 0;
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.check')[0].checked) {
                cantLaminas = cantLaminas + 1;
                idLamina = $(d[i]).find('.check')[0].id;
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}ANA_Laminas/almacenar/${idLamina}`,
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function () {
                        toastr.success('Se han almacenado la lamina', '', { timeOut: 6000 + (i * 1500) });
                    },
                    error: function (jqXHR, status) {
                        console.log(json.stringify(jqXHR));
                    }
                });
            }
        }
        if (cantLaminas == 0)
            toastr.info('Debe seleccionar al menos una lamina');
        else
            grillaLaminaAlmacenar();

        e.preventDefault();
    });
    $('#btnDesecharLamina').click(function (e) {
        var a = [];
        var t = $('#tblLaminaAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.check')[0].checked)
                a.push($(d[i]).find('.check')[0].id);
        if (a.length > 0) {
            $.ajax({
                type: 'POST',
                url: ObtenerHost() + '/FormAlmacenamientos/frm_Almacenamiento.aspx/desecharLamina',
                data: JSON.stringify({ s: JSON.stringify(a), idUsuario: vIdUsuario }),
                contentType: 'application/json',
                dataType: 'json',                
                success: function (data) {
                    var d = JSON.parse(data.d);
                    if (d.length > 0) {
                        var v = '';
                        for (var i = 0; i < d.length; i++)
                            v += ', ' + d[i];
                        $('#lblModalC').text(v.substr(1));
                        $('#lblModalMensaje').text('Solamente se pueden desechar las muestras que se encuentren almacenadas');
                        $('#divMensaje').show();
                        $('#mdlDetalle').modal('show');
                    }
                    else
                        toastr.success('Se han desechado las muestras seleccionadas');
                    grillaLaminaAlmacenar();
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        else
            toastr.info('Debe seleccionar al menos un casete');
        e.preventDefault();
    });

    $('body').on('change', '#tblCaseteAlmacenar .check', function () {
        //arreglín
        var bValidar = true;
        var t = $('#tblCaseteAlmacenar').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            if ($(d[i]).find('.check')[0].checked)
                if ($(d[i]).find('.check').attr('s') != 52 && $(d[i]).find('.check').attr('s') != 55) {
                    bValidar = false;
                    break;
                }
        if (bValidar)
            $('#btnAlmacenarCasete').removeAttr('disabled');
        else
            $('#btnAlmacenarCasete').attr('disabled', true);
    });
});

function mostrarMuestra(e) {
    let id = $(e).data('id');
    let url = `../FormAnatomia/frm_Cortes.aspx?id=${id}`;
    window.open(url, '_blank');
}

function grillaMuestraAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/buscar?idTipoEstadoSistema=55`,
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdTipoEstadoSistemas,
                    val.Numero,
                    val.OrganoBiopsia,
                    val.Tumoral,
                    val.NombreTipoBiopsia,
                    val.FechaRecepcion,
                    val.NombreTipoEstadoSistemas,      
                    '',
                    ''
                ]);
            });
            $('#bdgMuestraAlmacenar').html(adataset.length);

            $('#tblMuestrasAlmacenar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 6], visible: false, searchable: false },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-success" data-id="' + adataset[fila][0] + '" data-name="almacenarmuestra" onclick="modalConfirmacion(this);">Almacenar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="mostrarMuestra(this);">Ver</a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id biopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'N°' },
                    { title: 'Órgano' },
                    { title: 'Tumoral' },
                    { title: 'Tipo Biopsia' },
                    { title: 'ANA_fec_RecepcionRegistro_Biopsias' },
                    { title: 'Estado' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaCaseteAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=55`,
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.Id,
                    val.IdBiopsia,
                    val.IdTipoEstadoSistemas,
                    val.IdUsuarioCrea,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY hh:mm:ss'),
                    val.NombreTipoEstadoSistemas,
                    ''
                ]);
            });
            $('#bdgCaseteAlmacenar').html(adataset.length);

            $('#tblCaseteAlmacenar').DataTable({
                data: adataset,
                order: [],   
                columnDefs:
                    [
                        { targets: [2, 3, 4], visible: false, searchable: false },
                        { targets: 6, sType: 'date-ukLong' },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones = '';
                                //if (adataset[fila][3] == 55 || adataset[fila][3] == 52) {
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" s="' + adataset[fila][3] + '" type="checkbox" class="check">';
                                botones += '<span class="checkmark" style="margin-top:-8px; margin-left:6px;"></span>';
                                botones += '</label>';
                                //}
                                return botones;
                            }
                        },
                        {
                            targets: 8,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                return '<a  href="#/" class="btn btn-info" onclick="VerMovimientos(' + adataset[fila][2] + ')">Ver</a>';
                            }
                        }
                    ],
                columns: [
                    { title: '' },
                    { title: 'Id corte' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'GEN_idUsuarios' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: 'Movimientos' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaCaseteDespachadoparaAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=57`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            //console.log(data);
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdBiopsia,
                    val.IdTipoEstadoSistemas,
                    val.IdUsuarioCrea,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY hh:mm:ss'),
                    val.NombreTipoEstadoSistemas,
                    ''
                ]);
            });
            $('#bdgCaseteDespachado').html(adataset.length);

            $('#tblCasetedespachadoparaAlmacenar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2, 3], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 7,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="almacenarcasete" onclick="AlmacenarCaseteporID(' + adataset[fila][0] + ', 57)" class="btn btn-success">Almacenar Interconsulta</a>';
                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'Id corte' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'GEN_idUsuarios' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaLaminaAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/Grilla/ParaAlmacenar/55`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    '',
                    val.ANA_idLamina,
                    val.ANA_idBiopsia,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.ANA_nomLamina,
                    moment(val.ANA_fecLamina).format('DD/MM/YYYY hh:mm:ss'),
                    val.GEN_nombreTipo_Estados_Sistemas
                ]);
            });
            $('#bdgLaminaAlmacenar').html(adataset.length);

            $('#tblLaminaAlmacenar').DataTable({
                data: adataset,
                order: [],               
                columnDefs:
                    [
                        { targets: [2, 3], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 0,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;
                                var botones;
                                botones = '<label class="containerCheck">';
                                botones += '<input id="' + adataset[fila][1] + '" type="checkbox" class="check">';
                                botones += '<span class="checkmark" style="margin-top:-8px; margin-left:6px;"></span>';
                                botones += '</label>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: '' },
                    { title: 'Id lámina' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
        }
    });
}
function grillaLaminaDesechadasporAlmacenar() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/Grilla/ParaAlmacenar/57`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idLamina,
                    val.ANA_idBiopsia,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.ANA_nomLamina,
                    moment(val.ANA_fecLamina).format('DD/MM/YYYY hh:mm:ss'),
                    val.GEN_nombreTipo_Estados_Sistemas
                ]);
            });
            $('#bdgLaminasDespachadas').html(adataset.length);

            $('#tblLaminaDesechadaporAlmacenar').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1, 2], visible: false, searchable: false },
                        { targets: 5, sType: 'date-ukLong' },
                        {
                            targets: 6,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '"  onclick="AlmacenarLaminaporID(' + adataset[fila][0] + ', 57)" class="btn btn-success">Almacenar Interconsulta</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'Id lámina' },
                    { title: 'ANA_idBiopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                ],
                bDestroy: true
            });
        }
    });
}
function modalConfirmacion(e) {
    let id = $(e).data('id');
    var n = $(e).data('name');
    var mensaje;
    if (n == "retirarbiopsia")
        mensaje = '¿Está seguro (a) que desea retirar esta biopsia?';
    else if (n == "eliminarbiopsia")
        mensaje = '¿Está seguro (a) que desea eliminar esta biopsia?'
    else if (n == "retirarcasete")
        mensaje = '¿Está seguro (a) que desea retirar este casete?'
    else if (n == "eliminarcasete")
        mensaje = '¿Está seguro (a) que desea eliminar este casete?'
    else if (n == "almacenarcasete")
        mensaje = '¿Está seguro (a) que desea almacenar este casete?'
    else if (n == "retirarlamina")
        mensaje = '¿Está seguro (a) que desea retirar esta lámina?'
    else if (n == "eliminarlamina")
        mensaje = '¿Está seguro (a) que desea eliminar esta lámina?'
    else if (n == "almacenarmuestra")
        mensaje = '¿Está seguro (a) que desea almacenar esta muestra?';

    $("#lblConfirmacion").text(mensaje);
    $("#btnConfirmarModal").attr('data-id', id);
    $("#btnConfirmarModal").attr('data-name', n);
    $("#btnConfirmarModal").attr("onclick", "btnConfirmarModal('" + n + "', " + id + ")");
    $("#mdlConfirmacion").modal('show');
}

function btnConfirmarModal(vModal, vId) {
    if (vModal == 'retirarbiopsia') {
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/RetirarMacroAlmacenada/${vId}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado la biopsia.');
                grillaSolBiopsia();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
    else if (vModal == 'eliminarbiopsia') {
        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/EliminarSolicituddeMacro/${vId}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado la biopsia.');
                grillaSolBiopsia();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
    else if (vModal == 'retirarcasete') {
        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vId}/Retirar`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado el casete.');
                grillaSolCasete();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
    else if (vModal == 'eliminarcasete') {
        $.ajax({
            type: 'DELETE',
            url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vId}/EliminarSolicitud`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado el casete.');
                grillaSolCasete();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
    else if (vModal == 'retirarlamina') {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Laminas/RetirarLaminaAlmacenada/${vId}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha retirado la lámina.');
                grillaSolLamina();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }
    else if (vModal == 'eliminarlamina') {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Laminas/EliminarSolicituddeLamina/${vId}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha eliminado la lámina.');
                grillaSolLamina();
                $('#mdlConfirmacion').modal('hide');
            },
            error: function (jqXHR, status) {
                console.log(json.stringify(jqXHR));
            }
        });
    }

    if (vModal == 'almacenarmuestra') {
        $.ajax({
            type: 'PUT',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Almacenar/${vId}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                toastr.success('Se ha almacenado la muestra');
                grillaMuestraAlmacenar();
                $('#mdlConfirmacion').modal('hide');
            }
        });
    }
}

function grillaSolBiopsia() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?IdTipoEstadoSistema=58`,
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Numero,
                    val.OrganoBiopsia,
                    val.Almacenada,
                    val.Tumoral,
                    val.Desechada,
                    val.NombreTipoBiopsia,
                    val.LoginUsuariosPatologo,
                    '',
                    ''
                ]);
            });
            $('#bdgBiopsia').html(adataset.length);

            $('#tblSolBiopsia').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarbiopsia" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarbiopsia" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'Biopsia' },
                    { title: 'Órgano' },
                    { title: 'Alm.' },
                    { title: 'Tum.' },
                    { title: 'Des.' },
                    { title: 'Descripción.' },
                    { title: 'Patólogo' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}

function grillaSolCasete() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/Buscar?idTipoEstadoSistema=98&idTipoEstadoSistema=61`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.Nombre,
                    moment(val.FechaCorte).format('DD/MM/YYYY hh:mm:ss'),
                    val.NombreTipoEstadoSistemas,
                    val.IdTipoEstadoSistemas,
                    '',
                    ''

                ]);
            });
            $('#bdgCasete').html(adataset.length);

            $('#tblSolCasete').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: [4], visible: false, searchable: false },

                        { targets: 2, sType: 'date-ukLong' },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;

                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarcasete" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarcasete" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdCortes_Muestras' },
                    { title: 'N° corte' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: '' },
                    { title: '' }

                ],
                bDestroy: true
            });
        }
    });
}

function grillaSolLamina() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Laminas/Grilla/Solicitadas`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idLamina,
                    val.ANA_nomLamina,
                    moment(val.ANA_FecLamina).format('DD/MM/YYYY hh:mm:ss'),
                    val.GEN_nombreTipo_Estados_Sistemas,
                    val.ANA_almLamina,
                    val.ANA_desLamina,
                    '',
                    ''
                ]);
            });
            $('#bdgLamina').html(adataset.length);

            $('#tblSolLamina').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0], visible: false, searchable: false },
                        { targets: 2, sType: 'date-ukLong' },
                        {
                            targets: -2,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="retirarlamina" onclick="modalConfirmacion(this)" class="btn btn-success">Retirar</a>';
                                return botones;
                            }
                        },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                botones = '<a data-id="' + adataset[fila][0] + '" data-name="eliminarlamina" onclick="modalConfirmacion(this)" class="btn btn-danger">Eliminar solicitud</a>';
                                return botones;
                            }
                        }
                    ],
                columns: [
                    { title: 'ANA_IdLamina' },
                    { title: 'Lámina' },
                    { title: 'Fecha' },
                    { title: 'Estado' },
                    { title: 'Alm.' },
                    { title: 'Sol.' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function VerMovimientos(id) {
    modalMovimiento(id);
    $('#mdlMovimientos').modal('show');

    e.preventDefault();
}
function AlmacenarCaseteporID(id, idEstado) {
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${id}/almacenar`,
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            toastr.success('Se ha almacenado el casete', '', { timeOut: 6000 });
            if (idEstado == 57)// 'Interconsulta'
                grillaCaseteDespachadoparaAlmacenar();
        },
        error: function (jqXHR, status) {
            // console.log(json.stringify(jqXHR));
        }
    });
}
function AlmacenarLaminaporID(id, idEstado) {
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Laminas/almacenar/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            toastr.success('Se ha almacenado la Lamina', '', { timeOut: 6000 });
            if (idEstado == 57)// 'Interconsulta'
                grillaLaminaDesechadasporAlmacenar();
        },
        error: function (jqXHR, status) {
            // console.log(json.stringify(jqXHR));
        }
    });
}