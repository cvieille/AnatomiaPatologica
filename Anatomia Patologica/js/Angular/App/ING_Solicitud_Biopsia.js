﻿
var app = angular
    .module('app_ING_Solicitud_Biopsia', ['frapontillo.bootstrap-switch']);

app.controller('ING_Solicitud_Biopsia_Ctrl', function ($scope, $http) {

    //------------------------------------------------------------------------------------
    // INICIALIZA LOS COMPONENTES NECESARIOS PARA USO DE ANGULAR
    //------------------------------------------------------------------------------------
    var index = 0;
    iniciarComponentes();

    //------------------------------------------------------------------------------------
    // DA VALORES POR DEFECTO
    //------------------------------------------------------------------------------------
    function iniciarComponentes() {

        // EVITA EL POSTBACK CUANDO SE PRESIONA ENTER
        //==================================================================================================================================        
        $("input[type='text'], input[type='date'], input[type='number'], textarea").attr("onkeydown", "return (event.keyCode!=13);");

        // ELIMINA EL LA CLASE DE ERROR EXPUSTA AL GUARDAR CUANDO EL COMPONENTE GANA EL FOCUS
        //==================================================================================================================================        
        $("input[type='text'], input[type='date'], input[type='number'], select, textarea").each(function () {
            $(this).focus(function () { $(this).removeClass('has-error'); });
        });

        // ELIMINA EL LA CLASE DE ERROR EXPUSTA AL GUARDAR CUANDO EL COMPONENTE CAMBIA SU ESTADO
        //==================================================================================================================================        
        $("input[type='checkbox']").on('switchChange.bootstrapSwitch', function (event, state) {
            $(this).parent().parent().removeClass('has-error');
        });

        // SE INICIALIZACIÓN DE VARIABLES
        //================================================================================================================================== 
        $scope.infoMuestra = [];
        $scope.idBiopsia = $scope.idSolicitud = 0;
        $scope.profesional = $("#MainContent_cmb_profesional").val();
        cargarComponentes();

    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // CARGA LOS COMPONENTES CON INFORMACIÓN TRAIDA DE BASE DE DATOS
    //-------------------------------------------------------------------------------------------------------------------------------------
    function cargarComponentes() {

        // CARGA FECHA DE INGRESO
        //==================================================================================================================================        
        cargarInfoSolicitudBiopsia();

    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // LLENA INFORMACION DE LOS COMBOS DE ORIGEN, DESTINO Y DETALLES MUESTRA
    //-------------------------------------------------------------------------------------------------------------------------------------
    function llenarCombox(tipo, data) {
        $http({
            method: 'POST',
            url: getHost() + "/FormSolicitudes/frm_solicitud_Biopsia.aspx/llenarCombo",
            data: { nombreCombo: tipo, data: data }
        }).then(function (response) {

            // VALIDA EL COMBO QUE SE QUIERE LLENAR
            //=============================================================================================================================
            switch (tipo) {
                case "detalleMuestras":
                    $scope.detalleMuestras = JSON.parse(response.data.d).detalleMuestras;
                    break;
                case "origenes":
                    $scope.origenes = JSON.parse(response.data.d).origenes;
                    break;
                case "destinos":
                    $scope.destinos = JSON.parse(response.data.d).destinos;
                    break;
                case "profesionales":
                    $scope.profesionales = JSON.parse(response.data.d).profesionales;
                    break;
            }
        }, function (response) {
            alert(response.data + " " + response.status);
        });
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // CARGA LA INFORMACION DEL REGISTRO DE BIOPSIA SI ES QUE ESTA EXISTE
    //-------------------------------------------------------------------------------------------------------------------------------------
    function cargarInfoSolicitudBiopsia() {

        $http({
            method: 'POST',
            url: getHost() + "/FormSolicitudes/frm_solicitud_biopsia.aspx/getInfoSolicitudBiopsia",
            data: {}
        }).then(function (response) {

            // SE OBTIENE EL OBJ JSON DEVUELTO DEL SERVIDOR
            //=============================================================================================================================
            var infoSolicitudBiopsia = (response.data.d !== undefined) ? JSON.parse(response.data.d) : null;

            // VALIDA SI LA BIOPSIA O SOLICITUD EXITEN
            //=============================================================================================================================
            if (infoSolicitudBiopsia !== null) {

                // OBTIENE LA INFO DEL FORMULARIO DE INGRESO DE SOLICITUD DE BIOPSIA
                //=========================================================================================================================
                var infoPaciente = (infoSolicitudBiopsia.infoPaciente !== undefined) ? infoSolicitudBiopsia.infoPaciente : null;
                var infoBiopsia = (infoSolicitudBiopsia.infoBiopsia !== undefined) ? infoSolicitudBiopsia.infoBiopsia : null;
                var infoMuestras = (infoSolicitudBiopsia.infoMuestras !== undefined) ? infoSolicitudBiopsia.infoMuestras : null;

                // LOS IF VALIDAN QUE EL OBJETO SEA VALIDO Y NO ESTE VACIO
                //=========================================================================================================================
                if (infoPaciente !== null) {
                    llenarinfoPaciente(infoPaciente[0]);
                    $scope.esRutCorrecto = true;
                    $scope.inhabilitarPaciente = (infoBiopsia === null || $scope.idSolicitud !== "0");
                    $scope.activedGes = !$scope.inhabilitarPaciente;
                    $("#div_datosPaciente input[type='text'], #div_datosPaciente input[type='date'], #div_datosPaciente input[type='number'], " +
                        "#div_datosPaciente input[type='checkbox'], #div_datosPaciente select").removeAttr("data-required");
                }

                if (infoBiopsia !== null)
                    llenarInfoBiopsia(infoBiopsia[0]);

                if (infoMuestras !== null)
                    llenarInfoMuestras(infoMuestras);

            } else {

                // CARGA LA INFORMACION DE LOS INFORMACIÓN DE SOLICITUD BIOPSIA SI EXISTE, ORIGINES Y DESTINOS
                //==================================================================================================================================        
                llenarCombox("origenes", 'HCM');
                llenarCombox("destinos", 'HCM');
                llenarCombox("profesionales", 'HCM');
                $scope.activedGes = false;

            }
                

        }, function (response) {
            alert('ERROR: ' + response.data + ' ' + response.status);
        });
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // LLENA LA INFORMACION DEL PACIENTE
    //-------------------------------------------------------------------------------------------------------------------------------------
    function llenarinfoPaciente(infoPaciente) {

        // VALIDA SI INFORMACION ENTREGADA ES CORRECTA (DATOS DEL TIPO DE DOCUMENTO)
        //=================================================================================================================================
        if (infoPaciente.tipoDocumento !== undefined)
            $scope.tipoDocumento = infoPaciente.tipoDocumento;

        // VALIDA SI INFORMACION ENTREGADA ES CORRECTA (DATOS DEL NUMERO DE DOCUMENTO)
        //=================================================================================================================================
        if (infoPaciente.numeroDocumento !== undefined)
            $scope.numeroDocumento = infoPaciente.numeroDocumento;

        // VALIDA SI INFORMACION ENTREGADA ES CORRECTA (DATOS DEL DIGITO DE DOCUMENTO)
        //=================================================================================================================================
        if (infoPaciente.digitoDocumento !== undefined)
            $scope.digitoDocumento = infoPaciente.digitoDocumento;

        // VALIDA SI SE ESPECIFICA GES EN LA INFORMACIÓN TRAIDA DEL SERVIDOR
        //=================================================================================================================================
        if (infoPaciente.ges !== null) {
            $scope.activedGes = false;
            $scope.esGes = (infoPaciente.ges === 'true');
        } else
            $scope.activedGes = true;

        // SE ESTABLECE LA INFORMACION DEL PACIENTE A MOSTRAR
        //=================================================================================================================================
        $scope.nuiPaciente = { value: (infoPaciente.nuiPaciente !== "") ? parseInt(infoPaciente.nuiPaciente) : 0 };
        $scope.fechaNacimiento = { value: new Date(infoPaciente.fechaNacimiento) };
        $scope.edad = infoPaciente.edadPaciente;
        $scope.nombresPaciente = infoPaciente.nombresPaciente;
        $scope.apePatPaciente = infoPaciente.apePatPaciente;
        $scope.apeMatPaciente = infoPaciente.apeMatPaciente;
        $scope.tipoPaciente = infoPaciente.tipoPaciente;
        $scope.idSolicitud = (infoPaciente.idSolicitud !== undefined) ? infoPaciente.idSolicitud : 0;

    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // LLENA LA INFORMACION DE LA BIOPSIA
    //-------------------------------------------------------------------------------------------------------------------------------------
    function llenarInfoBiopsia(infoBiopsia) {

        llenarCombox("origenes", infoBiopsia.tipoOrigen);
        llenarCombox("destinos", infoBiopsia.tipoDestino);
        llenarCombox("profesionales", infoBiopsia.tipoOrigen);

        $scope.idBiopsia = infoBiopsia.idBiopsia;
        $scope.tipoOrigen = infoBiopsia.tipoOrigen
        $scope.origen = infoBiopsia.origen;
        $scope.tipoDestino = infoBiopsia.tipoDestino;
        $scope.destino = infoBiopsia.destino;
        $scope.profesional = infoBiopsia.profesional;
        $scope.programa = infoBiopsia.programa;
        $("#MainContent_txt_fechaIngreso").val(infoBiopsia.fechaIngreso);
        $scope.hipotesisDiagnostica = infoBiopsia.hipotesisDiagnostica;
        $scope.antRelevantes = infoBiopsia.antRelevantes;
        $scope.inhabilitarMuestras = (infoBiopsia.estado !== '42' && infoBiopsia.estado !== '49');
        
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // LLENA TABLA QUE GUARDA EL DETALLES DE LA MUESTRA
    //-------------------------------------------------------------------------------------------------------------------------------------
    function llenarInfoMuestras(infoMuestras) {
        $scope.infoMuestra = infoMuestras;
        $scope.showTablaMuestras = true;
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL COMBO DEL TIPO DE DOCUMENTO
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.cmb_TipoDocumento_change = function () {

        // REINICIA LOS COMPONENTES NECESARIOS
        //=================================================================================================================================
        $scope.numeroDocumento = $scope.digitoVerificador = "";
        $scope.esRutCorrecto = false;
        limpiarPaciente();

        // 5 = SI ES NN
        //=================================================================================================================================
        if ($scope.tipoDocumento === '5')
            $scope.esRutCorrecto = true;

    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL TXT DEL NUMERO DE DOCUMENTO
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.txt_numeroDocumento_change = function () {

        limpiarPaciente();

        // 1 = RUT
        // 4 = RUT MATERNO
        //=================================================================================================================================
        if ($scope.tipoDocumento === '1' || $scope.tipoDocumento === '4') {
            $scope.digitoVerificador = "";
            $scope.esRutCorrecto = false;
        } else {
            if ($.trim($scope.numeroDocumento) === "")
                $scope.esRutCorrecto = false;
        }

    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE BLUR(PIERDE EL FOCO) DEL TXT DEL NUMERO DE DOCUMENTO
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.txt_numeroDocumento_blur = function () {

        // 2 = DNI
        // 3 = PASAPORTE 
        //=================================================================================================================================
        if ($scope.tipoDocumento === '2' || $scope.tipoDocumento === '3') {

            // VALIDA SI EL NUMERO DE DOCUMENTO ESTA VACIO SI ES ASI EL RUT NO ES CORRECTO Y SE DESHABILITAN LOS CAMPOS
            // SI ES DISTINTO A VACIO SE HACE LA CONSULTA DEL PACIENTE EN BD Y EL RUT SE PASA COMO VALIDO
            //=============================================================================================================================
            if ($.trim($scope.numeroDocumento) === "")
                $scope.esRutCorrecto = false;
            else {
                cargarInfoPaciente();
                $scope.esRutCorrecto = true;
            }
        }
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL TXT DEL DIGITO VERIFICADOR
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.txt_digitoVerificador_change = function () {

        limpiarPaciente();

        // 1 = RUT
        // 4 = RUT MATERNO
        //=================================================================================================================================
        if ($scope.tipoDocumento === '1' || $scope.tipoDocumento === '4') {

            // VALIDA SI EL DIGITO VERIFICADOR ES CORRECTO
            //=============================================================================================================================
            $scope.activedGes = $scope.esRutCorrecto =
                esValidoDigitoVerificador($scope.numeroDocumento, $scope.digitoDocumento);

            // SI EL RUT ES CORRECTO SE VERIFICA SI ESTE EXISTE EN BD O NO PARA TRAER LA INFO DEL PACIENTE
            //=============================================================================================================================
            if ($scope.esRutCorrecto)
                cargarInfoPaciente();
        }

    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE BLUR(PIERDE EL FOCO) DEL TXT DEL FECHA NACIMIENTO, SE HACE EL CALCULCO EN EL SERVIDOR POR LA COMPARACIÓN A LA FECHA ACTUAL
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.txt_fechaNacimiento_blur = function () {
        $http({
            method: 'POST',
            url: getHost() + "/FormSolicitudes/frm_solicitud_biopsia.aspx/calculaEdad",
            data: { fecha: $("#ctl00_Cnt_Principal_txt_fechaNacimiento").val() }
        }).then(function (response) {
            $scope.edad = response.data.d;
        }, function (response) {
            alert(response.data + response.status);
        });
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // CARGA INFO DEL PACIENTE CON LOS DATOS TRAIDOS DEL SERVIDOR
    //-------------------------------------------------------------------------------------------------------------------------------------
    function cargarInfoPaciente() {

        $http({
            method: 'POST',
            url: getHost() + "/FormSolicitudes/frm_solicitud_Biopsia.aspx/getInfoPaciente",
            data: { tipoDocumento: $scope.tipoDocumento, numeroDocumento: $scope.numeroDocumento, digitoDocumento: $scope.digitoDocumento }
        }).then(function (response) {

            var infoPaciente = (response.data.d !== undefined) ? JSON.parse(response.data.d) : null;

            if (infoPaciente !== null)
                llenarinfoPaciente(infoPaciente);
            else
                $scope.esRutCorrecto = true;
        }, function (response) {
            alert("Error: " + response.data + " " + response.status);
        });

    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // LIMPIA INFORMACIÓN EL PACIENTE
    //-------------------------------------------------------------------------------------------------------------------------------------
    function limpiarPaciente() {
        $scope.edad = $scope.nombresPaciente = $scope.apePatPaciente = $scope.apeMatPaciente = "";
        $scope.fechaNacimiento = $scope.nuiPaciente = null;
        $scope.tipoPaciente = "0";
        $scope.esGes = undefined;
        $scope.activedGes = false;
        $("input[type='checkbox']").parent().parent().removeClass('has-error');
        $("#div_datosPaciente input[type='text'], #div_datosPaciente input[type='date'], #div_datosPaciente input[type='number']").removeClass('has-error');
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL COMBO DEL TIPO MUESTRA Y SE LLENA EL COMBO DE DETALLE MUESTRA
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.cmb_tipoMuestra_change = function () {
        if ($scope.tipoMuestra !== '0') {
            llenarCombox("detalleMuestras", $scope.tipoMuestra);
            $scope.detalleMuestra = '0';
        } else
            $scope.detalleMuestra = [];
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL COMBO DEL (HCM O EXTERNO) Y SE LLENA EL COMBO DE ORIGENES
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.cmb_tipoOrigen_change = function () {
        llenarCombox("origenes", $scope.tipoOrigen);
        llenarCombox("profesionales", $scope.tipoOrigen);
        $scope.origen = $scope.profesional = '0';
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CHANGE CUANDO CAMBIA EL COMBO DEL (HCM O EXTERNO) Y SE LLENA EL COMBO DE DESTINOS
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.cmb_tipoDestino_change = function () {
        llenarCombox("destinos", $scope.tipoDestino);
        $scope.destino = '0';
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // AGREGA MUESTRA EN LA TABLA
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.agregarMuestra = function () {

        if ($scope.tipoMuestra !== '0' &&
            $scope.detalleMuestra !== '0' &&
            $.trim($scope.organoDetalle).length !== 0) {

            $scope.showTablaMuestras = true;

            $scope.infoMuestra.push({
                idDetalleBiopsia: 0,
                numFrasco: ++index,
                codDetalle: $scope.detalleMuestra,
                detalleMuestra: $.grep($scope.detalleMuestras, function (detalle) {
                    return detalle.codDetalle === $scope.detalleMuestra;
                })[0].detalleMuestra,
                descripcionMuestra: $scope.organoDetalle,
                numeroMuestra: $scope.numeroMuestra
            });

        }

    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // QUITAR LA FILA DE LA TABLA DE MUESTRAS
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.quitarMuestra = function (index) {

        $scope.infoMuestra.splice(index, 1);

        if ($scope.infoMuestra.length === 0)
            $scope.showTablaMuestras = false;

        --index;

    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // VALIDA SI LOS COMPOS REQUERIDOS ESTAN LLENOS
    //-------------------------------------------------------------------------------------------------------------------------------------
    function esValidaSolicitudMuestra() {

        var esValido = true;
        var firtComponent = null;
        $("input[type='text'][data-required='true'], input[type='date'][data-required='true'], input[type='number'][data-required='true'], " +
            "input[type='checkbox'], select[data-required='true'], table[data-required='true'], textarea[data-required='true']").each(function () {

                var esComponenteValido = true;

                switch ($(this).prop('nodeName')) {
                    case 'INPUT':
                    case 'TEXTAREA':
                        if ($.trim($(this).val()) === '') {
                            esValido = esComponenteValido = false;
                            $(this).addClass((!esComponenteValido) ? 'has-error' : '');
                        }
                        break;
                    case 'SELECT':
                        if ($(this).val() === '0' || $(this).val() === 'string:0') {
                            esValido = esComponenteValido = false;
                            $(this).addClass((!esComponenteValido) ? 'has-error' : '');
                        }
                        break;
                    case 'TABLE':
                        if ($(this).children('tbody').children('tr').length === 0) {
                            esValido = esComponenteValido = false;
                            $(this).next().removeClass('alert-warning').addClass('alert-danger');
                        }
                        break;
                }

                if ($(this).attr('type') === 'checkbox' && $scope.esGes === undefined) {
                    esValido = esComponenteValido = false;
                    $(this).parent().parent().addClass('has-error')
                }

                if (firtComponent === null && !esComponenteValido)
                    firtComponent = $(this);

            });

        if (firtComponent !== null)
            $('html, body').animate({ scrollTop: firtComponent.offset().top - 100 }, 500);

        return esValido;
    }

    //-------------------------------------------------------------------------------------------------------------------------------------
    // GUARDA LA INFORMACIÓN DE LA SOLICITUD DE MUESTRA
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.guardarSolicitudMuestra = function () {

        if (esValidaSolicitudMuestra()) {
            
            var infoSolicitudMuestra =
                {
                    infoPaciente:
                    {
                        tipoDocumento: $scope.tipoDocumento,
                        numeroDocumento: $.trim($scope.numeroDocumento),
                        digitoDocumento: $scope.digitoDocumento,
                        nuiPaciente: $scope.nuiPaciente.value,
                        fechaNacimiento: $scope.fechaNacimiento.value,
                        nombresPaciente: $scope.nombresPaciente,
                        apePatPaciente: $scope.apePatPaciente,
                        apeMatPaciente: $scope.apeMatPaciente,
                        tipoPaciente: $scope.tipoPaciente,
                        ges: ($scope.esGes) ? 'SI' : 'NO'
                    },

                    infoMuestra:
                    {
                        idSolicitud: $scope.idSolicitud,
                        idBiopsia: $scope.idBiopsia,
                        origen: $scope.origen,
                        destino: $scope.destino,
                        profesional: $scope.profesional,
                        programa: $scope.programa,
                        tipoOrigen: $scope.tipoOrigen,
                        tipoDestino: $scope.tipoDestino,
                        hipotesisDiagnostica: $scope.hipotesisDiagnostica,
                        fechaIngreso: $("#ctl00_Cnt_Principal_txt_fechaIngreso").val(),
                        fechaRecepcion: $("#ctl00_Cnt_Principal_txt_fechaRecepcion").val(),
                        antRelevantes: $scope.antRelevantes
                    }
                }

            // MÉTODO AJAX PARA GUARDAR INFO EN BASE DE DATOS
            //============================================================================
            $http({
                method: "POST",
                url: getHost() + "/FormSolicitudes/frm_solicitud_Biopsia.aspx/guardarSolicitudBiopsia",
                data: { infoGeneral: JSON.stringify(infoSolicitudMuestra), infoIngresoMuestra: JSON.stringify($scope.infoMuestra) }
            }).then(function (response) {
                if (response.data.d)
                    window.location.href = getHost() + "/FormSolicitudes/frm_Solicitudes_Biopsia.aspx"
            }, function (response) {

                // MUESTRA MENSAJE DE ERROR
                //=======================================================================
                alert('Error ' + response.data + ' ' + response.status + '!');

            });
        }
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // OYENTE CLICK BOTON CANCELAR DEL MODAL DE VERIFICACIÓN
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.aceptarCancelarSolicitud = function () {
        window.location.href = getHost() + "/FormIntegracion/GST_Solicitudes_Biopsia.aspx"
    };

    //-------------------------------------------------------------------------------------------------------------------------------------
    // ABRE MODAL DE VERIFICACION DE CIERRE DEL PESTAÑA
    //-------------------------------------------------------------------------------------------------------------------------------------
    $scope.cancelarSolicitud = function () {
        $("#modal").modal('show');
    };

});