﻿var vIdBiopsia;
var vIdUsuario;
$(document).ready(function () {
    //oculta menu para esta ventana.
    $("#navMenu").hide();


    vIdBiopsia = getUrlParameter('id');
    grillaNotas();

    var sSession = getSession();
    vIdUsuario = sSession.id_usuario;

    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}/ANA_Registro_Biopsias/${vIdBiopsia}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {            
            $('#txtNumeroBiopsia').val(data.ANA_numeroRegistro_Biopsias + "-" + data.ANA_añoRegistro_Biopsias);
            $('#txtNombre').val(data.ANA_organoBiopsia);
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });

    $('#btnGuardar').click(function (e) {
        var bValido = true;
        if ($('#txtNota').val() == '' || $('#txtNota').val() == undefined || $('#txtNota').val() == null) {
            resaltaElemento($('#txtNota'));
            bValido = false;
        }
        if (bValido) {
            let json = {};        
            json.ANA_IdBiopsia = vIdBiopsia;                        
            json.ANA_detalleNota = $('#txtNota').val();           

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Notas`,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha ingresado la nota');                    
                    $('#txtNota').val('');
                    grillaNotas();
                }
            });
        }
        e.preventDefault();
    });    
});

function grillaNotas()
{
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() +  "/ANA_Notas/Grilla/ANA_Registro_Biopsias/" + vIdBiopsia,        
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {            
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idNota,
                    moment(val.ANA_fechaNota).format('DD/MM/YYYY hh:mm:ss'),                    
                    val.GEN_loginUsuarios,
                    val.ANA_detalleNota
                ]);
            });

            $('#tblNotas').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [                    
                    { targets: 0, visible: false, searchable: false }
                ],
                columns: [
                    { title: 'ANA_IdNota' },
                    { title: 'Fecha' },
                    { title: 'Creado por' },
                    { title: 'Detalle nota' },
                ],
                bDestroy: true
            });
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}