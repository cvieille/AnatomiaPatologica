﻿const vIdBiopsia = getUrlParameter('id');
const sSession = getSession();
var vIdUsuario = sSession.id_usuario;
var vCodigoPerfil = sSession.GEN_CodigoPerfil;

$(document).ready(function () {
    cargarInformacion(vIdBiopsia);

    $('#modalLaminas').on('hide.bs.modal', function (e) {
        $('#divMensaje').hide();
        $('#divMensajeExtra').hide();
        $('#hrSeparador').hide();
    });

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:checkbox', function () {
        $(this).parent().children('span').removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#btnVerMovimientos').click(function (e) {
        modalMovimiento(vIdBiopsia);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });

    $('#selTipoMuestra').change(function (e) {
        permiteCrearlamina();
    });

    $('#btnGuardarMacro').click(function (e) {
        var bValido = true;
        if ($('#selPatologo').prop('value') == 0) {
            resaltaElemento($('#selPatologo'));
            bValido = false;
        }

        if ($('#selTipoMuestra').prop('value') == 0) {
            resaltaElemento($('#selTipoMuestra'));
            bValido = false;
        }

        if ($('#selTipoMacroscopia').prop('value') == 0) {
            resaltaElemento($('#selTipoMacroscopia'));
            bValido = false;
        }
        if (bValido) {
            $('#lblConfirmacion').val('¿Esta seguro (a) de guardar esta macroscopia?');
            $("#btnConfirmarModal").attr("onclick", "btnConfirmarModal('guardarMacro', " + vIdBiopsia + ")");

            $('#mdlConfirmacion').modal('show');

        }

        e.preventDefault();
    });
    $('#btnPlantillaAc').click(function (e) {
        var sel = $('#selNomPlantilla').prop('value');
        if (sel == 0)
            resaltaElemento($('#selNomPlantilla'));
        else {
            $.ajax({
                type: 'GET',
                url: GetWebApiUrl() + '/ANA_Plantillas/' + sel + '?tipo=Macroscopia',
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    if (data != null)
                        $('#txtDescMacro').val($('#txtDescMacro').val() + ' ' + data);
                },
                error: function (jqXHR, status) {
                    console.log(json.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnSecuenciaCortes').click(function (e) {
        if ($(this).attr('disabled') == 'disabled')
            return false;
        var bValido = true;
        if ($('#selPatologo').val() == '0' || $('#selPatologo').val() == null || $('#selPatologo').val() == undefined) {
            bValido = false;
            resaltaElemento($('#selPatologo'));
            toastr.error('Solo se puede generar casete si tiene patologo definido');
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
            toastr.error('Solo se puede generar casete si la biopsia se encuentra dictada');
        }

        if ($('#selCantCortes').prop('value') == 0) {
            bValido = false;
            resaltaElemento($('#selCantCortes'));
        }
        if (bValido) {
            if ($('#tblListadoCasete').DataTable().rows().count() == 0) {
                let cantidadCortes = $('#selCantCortes').prop('value');
                $.ajax({
                    type: 'POST',
                    url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdBiopsia}`,
                    data: JSON.stringify({ rotulo: $('#txtSufijoCorte').val(), cantidad: cantidadCortes }),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function () {
                        getDataCortesApi(vIdBiopsia);
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            }
            else {
                toastr.error('Solo se pueden crear secuencias nuevas mientras no existan cortes');
            }
        }

        e.preventDefault();
    });
    $('#btnRotuloCorte').click(function (e) {

        if ($(this).attr('disabled') == 'disabled')
            return false;

        var bValido = true;
        if ($('#selPatologo').val() == '0' || $('#selPatologo').val() == null || $('#selPatologo').val() == undefined) {
            bValido = false;
            resaltaElemento($('#selPatologo'));
            toastr.error('Solo se puede generar casete si tiene patologo definido');
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
            toastr.error('Solo se puede generar casete si la biopsia se encuentra dictada');
        }

        if ($('#txtSufijoCorte').val() == '' || $('#txtSufijoCorte').val() == undefined || $('#txtSufijoCorte').val() == null) {
            bValido = false;
            resaltaElemento($('#txtSufijoCorte'));
        }

        if (bValido) {
            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdBiopsia}`,
                data: JSON.stringify({ rotulo: $('#txtSufijoCorte').val(), cantidad: 1 }),
                contentType: 'application/json',
                dataType: 'json',
                success: function () {

                    toastr.success('Se ha creado el casete ' + $('#txtSufijoCorte').val());
                    getDataCortesApi(vIdBiopsia);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });
    $('#btnSecuenciaLaminas').click(function (e) {
        if ($(this).attr('disabled') == 'disabled')
            return false;

        var bValido = true;
        if ($('#selCantLaminas').prop('value') == 0) {
            bValido = false;
            resaltaElemento($('#selCantLaminas'));
        }

        if (!$('#chkDictado').prop('checked')) {
            resaltaElemento($('#chkDictado'));
            bValido = false;
        }
        let cantidadCortes = 0;
        if (bValido) {
            let t = $('#tblListadoCasete').DataTable();
            let d = t.rows().nodes();
            let jsonPost = {};
            //revisa que exista algun checkbok seleccionado
            for (var i = 0; i < d.length; i++)
                if ($(d[i]).find('.checkCorte')[0].checked) {
                    cantidadCortes++;
                    jsonPost = {
                        idBiopsia: vIdBiopsia,
                        rotulo: null,
                        cantidad: $('#selCantLaminas').val(),
                        idCorte: $(d[i]).find('.checkCorte')[0].id
                    };
                    $.ajax({
                        type: 'POST',
                        url: `${GetWebApiUrl()}ANA_Laminas/${vIdBiopsia}`,
                        data: JSON.stringify(jsonPost),
                        contentType: 'application/json',
                        dataType: 'json',
                        success: function (data) {
                            toastr.success('Se han creado las láminas solicitadas');
                            getDataLaminasApi(data.IdBiopsia);
                        },
                        error: function (jqXHR, status) {
                            //toastr.error("No se pudo crear la secuencia de laminas");
                            console.log(JSON.stringify(jqXHR));
                        }
                    });
                }
            if (cantidadCortes == 0)
                toastr.info('Debe seleccionar al menos un casete');
        }
        e.preventDefault();
    });
    $('#btnRotuloLamina').click(function (e) {
        if ($(this).attr('disabled') == 'disabled')
            return false;

        var bValido = true;
        if ($('#txtSufijoLaminas').val() == '' || $('#txtSufijoLaminas').val() == undefined || $('#txtSufijoLaminas').val() == null) {
            bValido = false;
            resaltaElemento($('#txtSufijoLaminas'));
        }

        let vIdBiopsia = $("#txtIdBiopsia").val();
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                ANA_dictadaBiopsia = data.ANA_dictadaBiopsia;
            }
        });

        if (ANA_dictadaBiopsia != "SI") {
            resaltaElemento($('#chkDictado'));
            bValido = false;
        }

        if (bValido) {
            let json = {};
            json.rotulo = $('#txtSufijoLaminas').val();
            json.idBiopsia = vIdBiopsia;
            json.idCorte = 0;
            json.cantidad = 1;

            $.ajax({
                type: 'POST',
                url: `${GetWebApiUrl()}ANA_Laminas/${vIdBiopsia}`,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Se ha creado la lámina');
                    getDataLaminasApi(data.IdBiopsia);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });


        }
        e.preventDefault();
    });

    $('#btnGuardarDetalleMacro').click(function (e) {
        if ($('#txtDescMacro').val() == '')
            resaltaElemento($('#txtDescMacro'));
        else {
            let json = {};
            json.ANA_idBiopsia = vIdBiopsia;
            json.ANA_descMacroscopica = $('#txtDescMacro').val();

            var vMetodo;
            var vUrl;
            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vIdBiopsia}/Descripcion/Macroscopica`,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    data = data;
                    vMetodo = 'PATCH';
                    vUrl = `${GetWebApiUrl()}ANA_Descripcion_biopsia/${data[0].ANA_idDescripcion}/Macroscopia`;
                    json.ANA_idDescripcion = data[0].ANA_idDescripcion;
                },
                error: function (jqXHR, status) {
                    if (JSON.stringify(jqXHR.status) == 404) {
                        vMetodo = 'POST';
                        vUrl = `${GetWebApiUrl()}ANA_Descripcion_biopsia/Macroscopia`;
                    }
                    else
                        console.log(JSON.stringify(jqXHR));
                }
            });

            $.ajax({
                type: vMetodo,
                url: vUrl,
                data: JSON.stringify(json),
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la descripción Macroscópica');
                    else
                        toastr.success('Se ha ingresado la descripción Macroscópica');
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });

    $('#btnEnviarProcesador').click(function (e) {
        $('#btnEnviarProcesador').attr('disabled', true);
        cambiarEstadoCasete(56);

        $('#btnEnviarProcesador').removeAttr('disabled');
        e.preventDefault();
    });

    $('#btnDescal').click(function (e) {
        $('#btnDescal').attr('disabled', true);
        cambiarEstadoCasete(54);
        $('#btnDescal').removeAttr('disabled');
        e.preventDefault();
    });
    $('#btnVolverInicio').click(function (e) {
        $('#btnVolverInicio').attr('disabled', true);
        cambiarEstadoCasete(50);
        $('#btnVolverInicio').removeAttr('disabled');
        e.preventDefault();
    });
    function cambiarEstadoCasete(idEstado) {
        let sUrl;
        if (idEstado == 56)
            sUrl = GetWebApiUrl() + 'ANA_Cortes_Muestras/Enviar/ListoParaProcesador'; //Listo para Procesador
        else if (idEstado == 54)
            sUrl = GetWebApiUrl() + 'ANA_Cortes_Muestras/Enviar/Descalcificacion'; //Descalcificacion
        else if (idEstado == 50)
            sUrl = GetWebApiUrl() + 'ANA_Cortes_Muestras/Enviar/Encasetado'; //Listo para Procesador

        var t = $('#tblListadoCasete').DataTable();
        var d = t.rows().nodes();
        var idsCorte = [];

        for (var i = 0; i < d.length; i++) {
            if ($(d[i]).find('.checkCorte')[0].checked) {
                let idCasete = $(d[i]).find('.checkCorte')[0].id;
                $.ajax({
                    type: 'GET',
                    url: GetWebApiUrl() + 'ANA_Cortes_Muestras/' + idCasete,
                    contentType: 'application/json',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        let idTipoEstadoCaseteDB = data.Estado.GEN_idTipo_Estados_Sistemas;

                        if (idEstado == 56)//si esta enviando a listo para procesador
                        {
                            if (idTipoEstadoCaseteDB == 50 || idTipoEstadoCaseteDB == 54)//si esta encasetado o en descalcificacion
                                idsCorte.push(idCasete);
                            else
                                toastr.info("Casete " + data.ANA_nomCortes_Muestras + " no se puede enviar a procesador", '', { timeOut: 6000 + (i * 1500) });
                        }
                        else if (idEstado == 54)//si esta enviando a decal
                        {
                            if (idTipoEstadoCaseteDB == 50 || idTipoEstadoCaseteDB == 56)//si esta encasetado o en listo para
                                idsCorte.push(idCasete);
                            else
                                toastr.info("Casete " + data.ANA_nomCortes_Muestras + " no se puede enviar a descalcificación", '', { timeOut: 6000 + (i * 1500) });
                        }
                        else if (idEstado == 50)//si esta enviando a encasetada
                        {
                            if (idTipoEstadoCaseteDB == 54 || idTipoEstadoCaseteDB == 56)//si esta decal o en listo para
                                idsCorte.push(idCasete);
                            else
                                toastr.info("Casete " + data.ANA_nomCortes_Muestras + " no se puede enviar a Encasetado ", '', { timeOut: 6000 + (i * 1500) });
                        }
                    },
                    error: function (jqXHR, status) {
                        console.log(json.stringify(jqXHR));
                    }
                });
            }
        }

        if (idsCorte.length > 0) {
            $.ajax({
                type: 'PATCH',
                url: sUrl,
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(idsCorte),
                success: function () {
                    toastr.success('Se han enviado los casetes seleccionados');
                    getDataCortesApi(vIdBiopsia);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });

        }
        else
            toastr.info("Debe seleccionar al menos un registro valido");
    }

    $('#btnEliminarCortes').click(function (e) {
        $('#btnEliminarCortes').attr('disabled', true);

        let t = $('#tblListadoCasete').DataTable();
        let d = t.rows().nodes();
        let idCorte = 0;
        let cantCasete = 0;
        for (let i = 0; i < d.length; i++) {
            if ($(d[i]).find('.checkCorte')[0].checked) {
                cantCasete = cantCasete + 1;
                idCorte = $(d[i]).find('.checkCorte')[0].id;
                $.ajax({
                    type: 'GET',
                    url: GetWebApiUrl() + 'ANA_Cortes_Muestras/' + idCorte,
                    contentType: 'application/json',
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        let estadoCorte = data.Estado.GEN_idTipo_Estados_Sistemas;
                        if (estadoCorte == 56 || estadoCorte == 50 || estadoCorte == 54 || vCodigoPerfil == "2")//si esta encasetado o listo para procesador o es admin
                        {
                            $.ajax({
                                type: 'Delete',
                                url: GetWebApiUrl() + 'ANA_Cortes_Muestras/' + idCorte,
                                contentType: 'application/json',
                                dataType: 'json',
                                async:false,
                                success: function (data) {
                                    toastr.success('Se han anulado los registros solicitados', '', { timeOut: 6000 + (i * 1500) });
                                },
                                error: function (jqXHR, status) {
                                    console.log(JSON.stringify(jqXHR));
                                }
                            });
                        }
                        else//anular?
                            toastr.info('Casete ' + data.ANA_nomCortes_Muestras + ' no se puede anular', '', { timeOut: 6000 + (i * 1500) });
                    },
                    error: function (jqXHR, status) {
                        console.log(JSON.stringify(jqXHR));
                    }
                });
            }
        }

        if (cantCasete == 0)
            toastr.info("Debe seleccionar al menos un registro");
        else
            getDataCortesApi(vIdBiopsia);

        $('#btnEliminarCortes').removeAttr('disabled');
        e.preventDefault();
    });



    $('body').on('change', '#chkTodoLaminas', function () {
        var t = $('#tblLaminas').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.checkLamina')[0].checked = $(this).prop('checked');
    });
});

function revisarBotones(idEstado) {
    switch (parseInt(vCodigoPerfil)) {
        case 2: botonesAdministrador(); break;
        case 3: botonesPatologo(); break;
        case 4: botonesTecnologo(); break;
        case 6: botonesParamedico(idEstado); break;
        case 7: botonesAuxiliar(); break;
        case 8: botonesSecretaria(); break;
    }
}

function botonesAdministrador() {
    $('#chkDictado').removeAttr('disabled');
    //$('#chkRapida').removeAttr('disabled');
    $('#selCantCortes').removeAttr('disabled');
    $('#btnSecuenciaCortes').removeAttr('disabled');
    $('#selPatologo').removeAttr('disabled');
    $('#selTecnologo').removeAttr('disabled');

    $('#selTipoMuestra').removeAttr('disabled');

    $('#selTipoMacroscopia').removeAttr('disabled');
    $('#btnDescal').removeAttr('disabled');
    $('#btnEliminarCortes').removeAttr('disabled');
    $('#btnEnviarProcesador').removeAttr('disabled');
    $('#divTecnicas').show();
    $('#btnGuardarMacro').removeAttr('disabled');
    $('#btnInmuno').removeAttr('disabled');
    $('#txtSufijoCorte').removeAttr('disabled');
    $('#btnRotuloCorte').removeAttr('disabled');
    $('#txtSufijoLaminas').removeAttr('disabled');
    $('#btnRotuloLamina').removeAttr('disabled');
    $('#selCantLaminas').removeAttr('disabled');
    $('#btnSecuenciaLaminas').removeAttr('disabled');
    $('#btnSolicitarLamina').removeAttr('disabled');

    $('#divbtnProcesar').show();
    $('#btnVolverInicio').removeAttr('disabled');
}
function botonesPatologo() {
    $('#divTecnicas').hide();
    $('#divInmuno').hide();
}
function botonesTecnologo() {
    permiteCrearlamina();

    $('#divTecnicas').hide();
    $('#divInmuno').hide();
    $('#selCantLaminas').removeAttr('disabled');
    $('#btnParaInterconsulta').removeAttr('disabled');
    $('#btnRotuloLamina').removeAttr('disabled');
    $('#btnSecuenciaLaminas').removeAttr('disabled');
    $('#btnSolicitarLamina').removeAttr('disabled');
    $('#txtSufijoLaminas').removeAttr('disabled');
    $('#divbtnProcesar').show();
}

function botonesParamedico(idEstado) {
    /*
    $('#btnAgregarEspeciales').hide();
    $('#btnAgregarInmuno').hide();
    */
    if (idEstado == 42 || idEstado == 43) {
        //si esta en macroscopia o ingresado     
        permiteCrearlamina();
        $('#btnEliminarCortes').removeAttr('disabled');
        $('#btnGuardarDetalleMacro').show();
        $('#btnGuardarMacro').removeAttr('disabled');

        $('#btnRotuloCorte').removeAttr('disabled');
        $('#btnRotuloLamina').removeAttr('disabled');
        $('#btnSecuenciaCortes').removeAttr('disabled');
        $('#btnSecuenciaLaminas').removeAttr('disabled');
        $('#btnVolverInicio').removeAttr('disabled');
        $('#chkDictado').removeAttr('disabled');
        $('#chkTumoral').removeAttr('disabled');
        $('#lblEstadoMacro').html('Muestra en macroscopía');
        $('#selCantCortes').removeAttr('disabled');
        $('#selCantLaminas').removeAttr('disabled');
        $('#selNomPlantilla').removeAttr('disabled');
        $('#selPatologo').removeAttr('disabled');
        $('#selTipoMacroscopia').removeAttr('disabled');
        $('#selTipoMuestra').removeAttr('disabled');
        $('#txtDescMacro').removeAttr('disabled');
        $('#txtSufijoCorte').removeAttr('disabled');
        $('#txtSufijoLaminas').removeAttr('disabled');
        $('#btnGuardarMacro').removeAttr('disabled');

        $('#divTecnicas').show();
        $('#divInmuno').show();

        $('#btnPlantillaAc').show();

    }
    else {
        //¡La muestra no se encuentra en macroscopia! alert
        $('#lblEstadoMacro').html('Debe solicitar muestra');
    }
}

function botonesSecretaria() {
    $("#divTecnicas").hide();
    $("#divInmuno").hide();
    $("#btnSolicitarLamina").hide();
    $("#btnGuardarMacro").hide();
    $("#btnSolicitarMacro").hide();
    $("#divbtnCortes").hide();
}

function botonesAuxiliar() {
    $('#divTecnicas').hide();
    $('#divInmuno').hide();
    $('#btnGuardarMacro').hide();
}


function cargarCombos() {
    let url;

    for (var i = 0; i <= 40; i++) {
        $('#selCantCortes').append("<option>" + i + "</option>");
        $('#selCantLaminas').append("<option>" + i + "</option>");
    }

    if (vCodigoPerfil == 2 || vCodigoPerfil == 6) { //admin o tens
        url = `${GetWebApiUrl()}ANA_Plantillas/Combo/SI/NO/NO`;
        setCargarDataEnComboAsync(url, true, $('#selNomPlantilla'));

        url = `${GetWebApiUrl()}ANA_Tecnicas_Especiales/combo`;
        setCargarDataEnComboAsync(url, true, $('#selTecnicasEspeciales'));

        url = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica/combo`;
        setCargarDataEnComboAsync(url, true, $('#selInmunoHistoquimica'));
    }

    url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));

    url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/4`;
    setCargarDataEnComboAsync(url, true, $('#selTecnologo'));

    url = `${GetWebApiUrl()}ANA_Tipo_Biopsia/combo`;
    setCargarDataEnComboAsync(url, false, $('#selTipoMuestra'));

    url = `${GetWebApiUrl()}ANA_Tipo_Macroscopia/combo`;
    setCargarDataEnComboAsync(url, false, $('#selTipoMacroscopia'));
}

function cargarGrillas() {
    getDataCortesApi();
    getDataLaminasApi();
    getDataInmunoApi();
    getDataTecnicasApi();
    grillaDetalleMuestras();
}
//controla que tipo de biopsia puede crear laminas por rotulo.
function permiteCrearlamina() {
    if ($('#selTipoMuestra').val() != 0) {
        $('#txtSufijoLaminas').removeAttr('disabled');
        $('#btnRotuloLamina').removeAttr('disabled');
        $('#selCantLaminas').removeAttr('disabled');
        $('#btnSecuenciaLaminas').removeAttr('disabled');
    }
    else {
        $('#txtSufijoLaminas').attr('disabled', true);
        $('#btnRotuloLamina').attr('disabled', true);
        $('#selCantLaminas').attr('disabled', true);
        $('#btnSecuenciaLaminas').attr('disabled', true);
    }
}

function cargarInformacion(idBiopsia) {
    if (vCodigoPerfil == 3 || //patólogo
        vCodigoPerfil == 6 || vCodigoPerfil == 2 || vCodigoPerfil == 8 ||
        vCodigoPerfil == 4 || vCodigoPerfil == 10 || vCodigoPerfil == 7) //paramedico, admin, secre, tecnologo, supervisor, bodega
        cargarCombos();

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {

            $("#txtIdBiopsia").val(data.ANA_idBiopsia);
            $('#txtBiopsiaN').val(data.Identificacion.ANA_numeroRegistro_Biopsias + "-" + data.Identificacion.ANA_añoRegistro_Biopsias);

            $("#txtOrgano").val(data.ANA_organoBiopsia);
            $("#txtEstado").val(data.Estado.GEN_nombreTipo_Estados_Sistemas);

            if (data.Estado.GEN_idTipo_Estados_Sistemas == 44 || data.Estado.GEN_idTipo_Estados_Sistemas == 45)
                $('#divAgregar').hide();
            else if (data.Estado.GEN_idTipo_Estados_Sistemas == 43) {

                if (data.ANA_dictadaBiopsia == "SI")
                    $('#divbtnProcesar').show();

                //SI LA MUESTRA ESTA EN MACRO, TODAVIA PUEDE ENVIAR CASETE A PROCESADOR.
                $('#btnEnviarProcesador').removeAttr('disabled');
                $('#btnVolverInicio').removeAttr('disabled');
                $('#btnDescal').removeAttr('disabled');

                if (data.ANA_dictadaBiopsia == "SI")
                    $('#divbtnProcesar').show();//SI LA MUESTRA ESTA EN MACRO, TODAVIA PUEDE ENVIAR CASETE A PROCESADOR.

                $('#btnEnviarProcesador').removeAttr('disabled');
                $('#btnVolverInicio').removeAttr('disabled');
                $('#btnDescal').removeAttr('disabled');


                if (data.ANA_dictadaBiopsia == "SI") {
                    $('#divbtnProcesar').show();
                    $('#divTecnicas').show();
                    $('#divInmuno').show();
                }
                else {
                    $('#divTecnicas').hide();
                    $('#divInmuno').hide();
                }

            }

            $("#txtAlmacenado").val(data.ANA_almacenadaBiopsia);
            if (data.ANA_almacenadaBiopsia == "SI")
                $('#btnSolicitarMacro').show();


            $("#chkDictado").attr("checked", data.ANA_dictadaBiopsia == "SI" ? true : false);
            $("#chkTumoral").attr("checked", data.ANA_tumoralBiopsia == "SI" ? true : false);

            $('#selTecnologo').prop('value', data.Usuario.Tecnologo.GEN_idUsuario ?? 0);

            $('#selPatologo').prop('value', data.Usuario.Patologo.GEN_idUsuario ?? 0);

            if (data.Tipo.ANA_idTipo_Biopsia != null) {
                $('#selTipoMuestra').prop('value', data.Tipo.ANA_idTipo_Biopsia);
                $('#selTipoMuestra').change();
            }

            if (data.Tipo.ANA_idTipo_Macroscopia != null)
                $('#selTipoMacroscopia').prop('value', data.Tipo.ANA_idTipo_Macroscopia);

            $.ajax({
                type: 'GET',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${idBiopsia}/Descripcion/Macroscopica`,
                contentType: 'application/json',
                dataType: 'json',
                success: function (dataMacro) {
                    $("#txtDescMacro").val(dataMacro[0].ANA_descMacroscopica);
                }
            });


            revisarBotones(data.Estado.GEN_idTipo_Estados_Sistemas);



            $('#btnGuardarMacro').attr('data-id', idBiopsia);
            $('#btnSolicitarMacro').attr('data-id', idBiopsia);
            $('#btnGuardarMacro').attr('data-name', 'guardarMacro');

            getDataCortesApi(idBiopsia);
            getDataTecnicasApi(idBiopsia);
            getDataLaminasApi(idBiopsia);
            getDataInmunoApi(idBiopsia);

            LlenargrillaDetalleRegistroMuestras(data.Detalle);
            //     habilitarBotones(data.Acciones);
        }
    });

}
function LlenargrillaDetalleRegistroMuestras(data) {
    let adataset = [];
    $.each(data, function (key, val) {
        adataset.push([
            val.ANA_cantidadMuestras,
            val.ANA_detalleCatalogo_Muestras,
            val.ANA_descripcionBiopsia
        ]);
    });

    $('#tblMuestrasBiopsias').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: 'Muestras' },
            { title: 'Detalle' },
            { title: 'Descripción' }
        ],
        bDestroy: true
    });

    $('#bdgDetalleMuestra').html(adataset.length);
}