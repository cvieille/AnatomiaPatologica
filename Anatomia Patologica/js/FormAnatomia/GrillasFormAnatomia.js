﻿
$(document).ready(function () {

    $(document).on('change', '#chkTodoCasete', function () {

        var t = $('#tblListadoCasete').DataTable();
        var d = t.rows().nodes();
        for (var i = 0; i < d.length; i++)
            $(d[i]).find('.checkCorte')[0].checked = $(this).prop('checked');

    });

    $('#btnSolicitarMacro').click(function (e) {

        let id = $('#btnSolicitarMacro').data('id');

        $.ajax({
            type: 'PATCH',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/SolicitarMacro`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $('#btnSolicitarMacro').attr('disabled', true);
                toastr.success("Se ha solicitado macroscopia a bodega");
            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
        e.preventDefault();
    });

    $("#selTecnicasEspeciales").change(function (e) {
        if ($("#selTecnicasEspeciales").val() == "0") {
            $('#btnAgregarEspeciales').removeAttr('disabled');
            $('#btnAgregarEspeciales').hide();
        }
        else {
            $('#btnAgregarEspeciales').removeAttr('disabled');
            $('#btnAgregarEspeciales').show();
        }
    });

    $('#btnAgregarEspeciales').click(function (e) {

        if ($('#selTecnicasEspeciales').val() == "0") {
            resaltaElemento($('#selTecnicasEspeciales'));
        }
        else {
            let idTecnica = $('#selTecnicasEspeciales').prop('value');
            solicitarTecnicaOInmuno("tecnica", idTecnica);
        }
        e.preventDefault();
    });

    $("#selInmunoHistoquimica").change(function (e) {
        if ($("#selInmunoHistoquimica").val() == "0") {
            $('#btnAgregarInmuno').hide();
            $('#btnAgregarInmuno').removeAttr('disabled');
        }
        else {
            $('#btnAgregarInmuno').show();
            $('#btnAgregarInmuno').removeAttr('disabled');
        }
    });

    $('#btnAgregarInmuno').click(function (e) {

        if ($('#selInmunoHistoquimica').prop('value') == 0)
            resaltaElemento($('#selInmunoHistoquimica'));
        else {

            let idTecnica = $('#selInmunoHistoquimica').prop('value');
            solicitarTecnicaOInmuno("inmuno", idTecnica);
        }
        e.preventDefault();
    });
});

////////////////////////////////////////////
/// INICIO DE METODOS PARA GRILLA CORTES ///
////////////////////////////////////////////
function getDataCortesApi(vId) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vId}/Cortes`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (d) {
            llenarGrillaCortes(d);
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}

function MostrarModalConfirmacion(sender) {
    var mensaje = "";
    var idConfirmacion = $(sender).data('id');
    var modalConfirmacion = $(sender).data('name');


    if (modalConfirmacion == "solicitarcasete")
        mensaje = '¿Esta seguro (a) que desea solicitar este casete?';

    else if (modalConfirmacion == "eliminartecnica")
        mensaje = '¿Esta seguro (a) que desea eliminar esta técnica?';

    else if (modalConfirmacion == "eliminarinmuno")
        mensaje = '¿Esta seguro (a) que desea eliminar esta InmunoHistoquímica?';

    else if (modalConfirmacion == "caseteaInterconsulta")
        mensaje = '¿Esta seguro (a) que desea enviar este casete a interconsulta?';

    else if (modalConfirmacion == "laminaaInterconsulta")
        mensaje = '¿Esta seguro (a) que desea enviar esta lamina a interconsulta?';

    else if (modalConfirmacion == "eliminarlamina")
        mensaje = '¿Esta seguro (a) que desea eliminar esta lámina?';

    $("#lblConfirmacion").text(mensaje);
    $("#btnConfirmarModal").attr('data-id', idConfirmacion);
    $("#btnConfirmarModal").attr('data-name', modalConfirmacion);
    $("#btnConfirmarModal").attr("onclick", "btnConfirmarModal('" + modalConfirmacion + "', " + idConfirmacion + ")");
    $("#mdlConfirmacion").modal('show');
};

function btnConfirmarModal(vModal, vId) {

    if (vModal == "solicitarcasete") {
        solicitarCasete(vId);
    }
    else if (vModal == "eliminartecnica") {
        eliminarTecnica(vId);
    }
    else if (vModal == "eliminarinmuno") {
        eliminarInmuno(vId);
    }
    else if (vModal == "eliminarlamina") {
        eliminarLamina(vId);
    }
    else if (vModal == "caseteaInterconsulta") {
        caseteaInterconsulta(vId);
    }
    else if (vModal == "laminaaInterconsulta") {
        laminaaInterconsulta(vId);
    }
    else if (vModal == "guardarMacro") {
        var bValido = true;
        if ($('#selPatologo').prop('value') == 0) {
            resaltaElemento($('#selPatologo'));
            bValido = false;
        }

        if ($('#selTipoMuestra').prop('value') == 0) {
            resaltaElemento($('#selTipoMuestra'));
            bValido = false;
        }

        if ($('#selTipoMacroscopia').prop('value') == 0) {
            resaltaElemento($('#selTipoMacroscopia'));
            bValido = false;
        }
        if (bValido) {
            $('#selCantCortes').removeAttr('disabled');
            $('#selPatologo').removeAttr('disabled');
            $('#btnDescal').removeAttr('disabled');
            $('#btnEliminarCortes').removeAttr('disabled');
            $('#divTecnicas').show();
            $('#btnRotuloCorte').removeAttr('disabled');
            $('#btnVolverInicio').removeAttr('disabled');
            $('#btnEnviarProcesador').removeAttr('disabled');

            //actualizar biopsia
            $('#txtEstado').val('En Macroscopia');//'En macroscopía'
            var svalores;

            svalores = $('#selTipoMuestra').val() + "/" + $('#selTipoMacroscopia').val() + "/" + $('#selPatologo').prop('value')
                + "/" + $('#selTecnologo').prop('value') + "/";

            svalores += $('#chkDictado').prop('checked') ? 'SI/' : 'NO/';
            svalores += $('#chkTumoral').prop('checked') ? 'SI' : 'NO';

            $.ajax({
                type: 'Patch',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vId}/CREARMACROSCOPIA/${svalores}`,
                contentType: 'application/json',
                dataType: 'json',
                async: true,
                success: function (data) {
                    toastr.success('Se ha guardado la macroscopía');
                    cargarInformacion(vId);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
    }
    $("#mdlConfirmacion").modal('hide');
};

function solicitarCasete(vIdCorte) {
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdCorte}/Solicitar`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha solicitado el taco.');
            getDataCortesApi(data.ANA_idBiopsia);
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
    return false;
};

function caseteaInterconsulta(vIdCorte) {
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/${vIdCorte}/aInterconsulta`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha enviado a interconsulta.');
            getDataCortesApi(data.ANA_idBiopsia);
        }
    });
};

function laminaaInterconsulta(vIdLamina) {
    $.ajax({
        type: 'PATCH',
        url: `${GetWebApiUrl()}/ANA_Laminas/${vIdLamina}/aInterconsulta`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha enviado a interconsulta.');
            getDataLaminasApi(vIdBiopsia);
            $("#mdlConfirmacion").modal('hide');
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
    return false;
};

/////////////////////////////////////////////
//INICIO DE METODOS PARA GRILLA DE TECNICAS//
/////////////////////////////////////////////
function getDataTecnicasApi(vId) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vId}/Tecnicas`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (d) {
            llenarGrillaTecnicas(d);
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}
function eliminarTecnica(vIdCorte) {
    $.ajax({
        type: 'DELETE',
        url: GetWebApiUrl() + 'ANA_Tecnica_Biopsia/' + vIdCorte,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha eliminado la técnica');
            getDataTecnicasApi($('#txtIdBiopsia').val());
        },
        error: function (jqXHR, status) {
            console.log(JSON.stringify(jqXHR));
        }
    });
}
/////////////////////////////////////////////
///INICIO DE METODOS PARA GRILLA DE INMUNO///
/////////////////////////////////////////////
function getDataInmunoApi(vId) {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vId}/Inmunos`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (d) {
            llenarGrillaInmuno(d);
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}
function eliminarInmuno(vIdInmuno) {
    $.ajax({
        type: 'DELETE',
        url: GetWebApiUrl() + 'ANA_Inmuno_Histoquimica_Biopsia/' + vIdInmuno,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha eliminado la inmuno');
            getDataInmunoApi($('#txtIdBiopsia').val());
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}
function eliminarLamina(vIdLamina) {
    $.ajax({
        type: 'DELETE',
        url: `${GetWebApiUrl()}ANA_Laminas/${vIdLamina}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            toastr.success('Se ha eliminado la lamina seleccionada');
            getDataLaminasApi($("#txtIdBiopsia").val());
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });
}


function getDataLaminasApi(vId) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vId}/Laminas`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (d) {
            llenarGrillaLaminas(d);
        },
        error: function (jqXHR, status) {
            console.log(json.stringify(jqXHR));
        }
    });

}

function solicitarTecnicaOInmuno(tipo, idTecnica) {
    let json = [];
    var t = $('#tblListadoCasete').DataTable();
    var d = t.rows().nodes();
    for (var i = 0; i < d.length; i++)
        if ($(d[i]).find('.checkCorte')[0].checked) {
            let idCorte = $(d[i]).find('.checkCorte')[0].id;
            json.push({ "IdCortesMuestras": idCorte, "IdTecnica": idTecnica });
        }
    let url = "";
    if (tipo == "tecnica")
        url = `${GetWebApiUrl()}ANA_Tecnica_Biopsia`;
    else
        url = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica_Biopsia`;

    if (json.length > 0) {

        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify(json),
            dataType: 'json',
            success: function (data) {
                toastr.success('Se han solicitado las técnicas para los registros');
                if (tipo == "tecnica")
                    getDataTecnicasApi($('#txtIdBiopsia').val());
                else
                    getDataInmunoApi($('#txtIdBiopsia').val());

            },
            error: function (jqXHR, status) {
                console.log(JSON.stringify(jqXHR));
            }
        });
    }
    else
        toastr.info('Debe seleccionar al menos un registro');
}

//Las mismas funciones pero considerando nuevo JSON
function llenarGrillaCortes(data) {
    var btn = [];
    let adataset = [];
    if (data != null) {
        $.each(data, function (key, val) {
            adataset.push([
                '',
                val.ANA_idCortes_Muestras,
                val.ANA_nomCortes_Muestras,
                moment(val.ANA_fecCortes_Muestras).format('DD/MM/YYYY hh:mm:ss'),
                val.GEN_loginUsuariosPatologo,
                val.GEN_loginUsuariosCrea,
                val.GEN_nombreTipo_Estados_Sistemas,
                val.GEN_idTipo_Estados_Sistemas,
                val.ANA_solCortes_Muestras
            ]);

            btn.push([
                val.Acciones.Solicitar,
                val.Acciones.Interconsultar
            ]);
        });
    }

    $('#tblListadoCasete').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        headerCallback: function (thead) {
            if (vCodigoPerfil != 8) {
                if ($('#chkTodoCasete') == undefined)//guardar el estado del check al modificar la vista
                {
                    $(thead).find('th').eq(0).html('<label class="containerCheck"><input id="chkTodoCasete" type="checkbox"><span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span></label> Todos');
                }
                else {
                    if ($('#chkTodoCasete').prop('checked'))
                        $(thead).find('th').eq(0).html('<label class="containerCheck"><input id="chkTodoCasete" checked type="checkbox"><span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span></label> Todos');
                    else
                        $(thead).find('th').eq(0).html('<label class="containerCheck"><input id="chkTodoCasete" type="checkbox"><span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span></label> Todos');
                }
            }
        },
        columnDefs:
            [
                { targets: [1], visible: vCodigoPerfil == 8 || vCodigoPerfil == 7 ? false : true, searchable: false }, //id, {secretaria y auxiliar no lo ven}
                { targets: 3, sType: 'date-ukLong' },
                {
                    targets: 0,
                    data: null,
                    visible: (vCodigoPerfil == 8 ? false : true),
                    orderable: false,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var botones;

                        botones = '<label class="containerCheck">';
                        botones += '<input id="' + adataset[fila][1] + '"  type="checkbox" class="checkCorte">';
                        botones += '<span class="checkmark" style="margin-top:-9px; margin-left:6px;"></span>';
                        botones += '</label>';
                        return botones;
                    }
                },
                { targets: 7, visible: false, searchable: false },
                {
                    targets: 8,
                    searchable: false,
                    data: null,
                    visible: (vCodigoPerfil == 2 || vCodigoPerfil == 3 || vCodigoPerfil == 4 || vCodigoPerfil == 6) ? true : false, //solicitar, visible para: admin, patólogo, tecnólogo 
                    orderable: false,
                    render: function (data, type, row, meta) {

                        if (vCodigoPerfil != 2 && vCodigoPerfil != 3 && vCodigoPerfil != 4 && vCodigoPerfil != 6)//nada que hacer
                            return null;

                        let fila = meta.row;
                        if (btn[fila][0] == true)
                            return '<a class="btn btn-warning" data-toggle="tooltip"  title="Solicitar" onclick="MostrarModalConfirmacion(this);"  data-id="' + adataset[fila][1] + '" data-name="solicitarcasete"><span class="glyphicon glyphicon-transfer"></span ></a>';
                        else
                            return `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                    }
                },
                {
                    targets: 9, //interconsulta {tecnolog, paramédico, auxiliar, secretaria supervisor no lo ven} 
                    data: null,
                    visible: (vCodigoPerfil == 4 || vCodigoPerfil == 6 || vCodigoPerfil == 7 || vCodigoPerfil == 8 || vCodigoPerfil == 10) ? false : true,
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        if (btn[fila][1] == true)
                            return '<a class="btn btn-warning" data-toggle="tooltip"  title="Enviar a interconsulta"  data-id="' + adataset[fila][1] + '" data-name="caseteaInterconsulta" onclick="MostrarModalConfirmacion(this);"><span class="glyphicon glyphicon-send"></span ></a>';

                        else
                            return `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                    },
                }
            ],
        columns: [
            { title: '' },
            { title: 'ID Corte' },
            { title: 'Corte' },
            { title: 'Fecha' },
            { title: 'Patólogo' },
            { title: 'Creado por' },
            { title: 'Estado' },
            { title: 'idEstado' },
            { title: 'Solicitar' },
            { title: 'Interconsulta' }
        ],
        bDestroy: true
    });
    $('#bdgListadoCasete').html(adataset.length);
}

function llenarGrillaLaminas(data) {

    var btn = [];
    let adataset = [];
    if (data != null) {
        $.each(data, function (key, val) {
            adataset.push([
                val.ANA_idLamina,
                val.ANA_nomLamina,
                val.ANA_idBiopsia,
                moment(val.ANA_fecLamina).format('DD/MM/YYYY hh:mm:ss'),
                val.GEN_idTipo_Estados_Sistemas,
                val.GEN_nombreTipo_Estados_Sistemas,
                'Eliminar',
                'Solicitar',
                'Interconsulta'
            ]);

            btn.push([
                val.Acciones.Eliminar,
                val.Acciones.Solicitar,
                val.Acciones.Interconsultar
            ]);
        });
    }

    $('#tblLaminas').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        columnDefs:
            [
                { targets: [0, 2, 4], visible: false, searchable: false },
                { targets: 3, sType: 'date-ukLong' },
                {
                    targets: 6,
                    data: null,
                    visible: vCodigoPerfil != 4 && vCodigoPerfil != 3 && vCodigoPerfil != 2 ? false : true,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var botones;
                        if (btn[fila][0] == true)
                            botones = '<a class="btn btn-danger" data-id="' + adataset[fila][0] + '" data-name="eliminarlamina" onclick="MostrarModalConfirmacion(this)"><span class="glyphicon glyphicon-trash"></span ></a>';
                        else
                            botones = `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;

                        return botones;
                    }
                },
                {
                    targets: 7,
                    searchable: false,
                    data: null,
                    visible: (vCodigoPerfil == 2 || vCodigoPerfil == 3 || vCodigoPerfil == 4) ? true : false, //solicitar, visible para: admin, patólogo, tecnólogo 
                    orderable: false,
                    render: function (data, type, row, meta) {

                        let fila = meta.row;
                        if (btn[fila][1] == true)
                            return '<a class="btn btn-warning" data-toggle="tooltip"  title="Solicitar" onclick="MostrarModalConfirmacion(this);"  data-id="' + adataset[fila][0] + '" data-name="solicitarcasete"><span class="glyphicon glyphicon-transfer"></span ></a>';
                        else
                            return `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                    }
                },
                {
                    targets: 8, //interconsulta {tecnolog, paramédico, auxiliar, secretaria supervisor no lo ven} 
                    data: null,
                    visible: (vCodigoPerfil == 4 || vCodigoPerfil == 6 || vCodigoPerfil == 7 || vCodigoPerfil == 8 || vCodigoPerfil == 10) ? false : true,
                    searchable: false,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        if (btn[fila][2] == true)
                            return '<a class="btn btn-warning" data-toggle="tooltip"  title="Enviar a interconsulta"  data-id="' + adataset[fila][0] + '" data-name="laminaaInterconsulta" onclick="MostrarModalConfirmacion(this);"><span class="glyphicon glyphicon-send"></span ></a>';

                        else
                            return `<a class="btn btn-disabled">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </a>`;
                    },
                }
            ],
        columns: [
            { title: 'ID lámina' },
            { title: 'Lámina' },
            { title: 'idBiopsia' },
            { title: 'Fecha lámina' },
            { title: 'idTipoEstadoSistemas' },
            { title: 'Estado' },
            { title: 'Eliminar' },
            { title: 'Solicitar' },
            { title: 'Interconsulta' }
        ],
        bDestroy: true
    });
    $('#bdgLaminas').html(adataset.length);
}

function llenarGrillaTecnicas(data) {
    let creadasCount = 0;
    let adataset = [];
    if (data != null) {
        $.each(data, function (key, val) {
            adataset.push([
                moment(val.ANA_fecTecnica).format('DD/MM/YYYY hh:mm:ss'),
                val.ANA_nomCortes_Muestras,
                val.ANA_nomTecnica,
                val.ANA_idTecnicaBiopsia,
                val.ANA_idBiopsia,
                val.GEN_nombreTipo_Estados_Sistemas,
                val.GEN_idTipo_Estados_Sistemas,
                ''
            ]);
        });
    }
    for (i = 0; i < adataset.length; i++) {
        if (adataset[i][6] == 46)
            creadasCount++;
    }

    $('#tblListadoTecEspeciales').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        columnDefs:
            [
                { targets: [3, 4], visible: false, searchable: false },
                { targets: 0, sType: 'date-ukLong' },
                {
                    targets: 7,
                    data: null,
                    orderable: false,
                    visible: (vCodigoPerfil == 2 || vCodigoPerfil == 3 || (vCodigoPerfil == 6) ? true : false),
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var btn = "";
                        //var botones = '<a class="btn btn-danger disabled"><span class="glyphicon glyphicon-trash"></span ></a>';
                        if (adataset[fila][6] == 58)//solicitada      
                        {
                            btn = '<a class="btn btn-danger" data-id="' + adataset[fila][3] + '" data-name="eliminartecnica" onclick="MostrarModalConfirmacion(this);"><span class="glyphicon glyphicon-trash"></span ></a>';

                        }

                        return btn;
                    }
                },
                {
                    //columna de GEN_idTipo_Estados_Sistemas
                    targets: 6,
                    visible: false,
                    searchable: false
                },
            ],
        columns: [
            { title: 'Fecha técnica' },
            { title: 'Nº Corte' },
            { title: 'Técnica' },
            { title: 'ANA_IdTecnicaBiopsia' },
            { title: 'ANA_IdBiopsia' },
            { title: 'Estado' },
            { title: 'IdEstado' },
            { title: 'Acción' }
        ],
        bDestroy: true
    });
    $('#bdgListadoTecnicas').html(adataset.length);
    $('#bdgListadoTecnicasCreadas').html(creadasCount);
}

function llenarGrillaInmuno(data) {
    let creadasCount = 0;
    let adataset = [];
    if (data != null) {
        $.each(data, function (key, val) {
            adataset.push([
                val.ANA_idInmuno_Histoquimica_Biopsia,
                moment(val.ANA_fecInmuno_Histoquimica_Biopsia).format('DD/MM/YYYY hh:mm:ss'),
                val.ANA_nomCortes_Muestras,
                val.ANA_idBiopsia,
                val.ANA_nomInmunoHistoquimica,
                val.GEN_nombreTipo_Estados_Sistemas,
                val.GEN_idTipo_Estados_Sistemas
            ]);
        });
    }
    for (i = 0; i < adataset.length; i++) {
        if (adataset[i][6] == 46)
            creadasCount++;
    }
    $('#tblListadoInmuno').addClass('nowrap').DataTable({
        data: adataset,
        order: [],
        columnDefs:
            [
                { targets: [0, 3, 6], visible: false, searchable: false },
                { targets: 1, sType: 'date-ukLong' },
                {
                    targets: -1,
                    data: null,
                    orderable: false,
                    visible: (vCodigoPerfil == 2 || vCodigoPerfil == 3 || vCodigoPerfil == 6 ? true : false),
                    render: function (data, type, row, meta) {
                        let fila = meta.row;
                        var btn = "";
                        if (adataset[fila][6] == 58)//solicitada
                            btn = '<a class="btn btn-danger" data-id="' + adataset[fila][0] + '" data-name="eliminarinmuno" onclick="MostrarModalConfirmacion(this);"><span class="glyphicon glyphicon-trash"></span ></a>';

                        return btn;
                    }
                },

            ],
        columns: [
            { title: 'ANA_IdInmuno_Histoquimica_Biopsia' },
            { title: 'Fecha inmuno' },
            { title: 'N° corte' },
            { title: 'ANA_IdBiopsia' },
            { title: 'Inmunohistoquímicas' },
            { title: 'Estado' },
            { title: 'IdEstado' },
            { title: 'Acción' }
        ],
        bDestroy: true
    });

    $('#bdgListadoInmuno').html(adataset.length);
    $('#bdgListadoInmunoCreadas').html(creadasCount);
}