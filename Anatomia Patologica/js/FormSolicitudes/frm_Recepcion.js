﻿
var vID;
var vIdPaciente;
var vCodigoPerfil;

$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    vID = getUrlParameter('id');

    var sSession = getSession();
    vCodigoPerfil = sSession.GEN_CodigoPerfil;

    cargarCombosRecepcion();

    $('#txtRecepcion').val(moment().format('YYYY-MM-DD'));

    $('#selTipoMuestra').change(function (e) {
        if ($(this).val() != undefined && $(this).val() != 0)
            comboDetalleCatalogo($(this).val());
    });

    $('#btnAgregar').click(function (e) {

        var bValido = true;

        if ($('#selTipoMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoMuestra'));
        }
        if ($('#selDetalleMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDetalleMuestra'));
        }
        if ($('#txtOrgano').val() == '') {
            bValido = false;
            resaltaElemento($('#txtOrgano'));
        }
        if ($('#selMuestrasPorFrasco').val() == '0') {
            bValido = false;
            resaltaElemento($('#selMuestrasPorFrasco'));
        }
        if (bValido) {

            var attr = $('#btnAgregar').attr('disabled');
            if (!(typeof attr !== typeof undefined && attr !== false)) {
                var t = $('#tblDetalle').DataTable();
                var d = t.rows().data().toArray();
                t.row.add([
                    0,
                    $('#selDetalleMuestra').val(),
                    $('#selDetalleMuestra>option:selected').html(),
                    $('#txtOrgano').val(),
                    $('#selMuestrasPorFrasco').val()
                ]).draw();
            }
        }
    });

    if (vID != 0) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (dataBio) {
                $('#txtId').val(vID);
                $('#txtNRegistro').val(dataBio.Identificacion.ANA_numeroRegistro_Biopsias + "-" + dataBio.Identificacion.ANA_añoRegistro_Biopsias);
                $('#txtRecepcion').val(moment(dataBio.ANA_fec_RecepcionRegistro_Biopsias).format('YYYY-MM-DD'));


                vIdPaciente = dataBio.Paciente.Identificacion.GEN_idPaciente;
                $('#selDocumento').val(dataBio.Paciente.Identificacion.GEN_idIdentificacion);
                $('#txtRut').val(dataBio.Paciente.Identificacion.GEN_numero_documentoPaciente);
                if (dataBio.Paciente.Identificacion.GEN_idIdentificacion == 1) {
                    $('#txtDigito').val(dataBio.Paciente.Identificacion.GEN_digitoPaciente);
                    $('#txtDigito').blur();
                }
                else
                    $('#txtRut').change();

                $('#selPaciente').val(dataBio.GEN_idModalidad_Fonasa);
                $('#selGES').val(dataBio.ANA_gesRegistro_Biopsias);
                $('#txtEstado').val(dataBio.Estado.GEN_nombreTipo_Estados_Sistemas);

                $('#selTipoMuestra').val(dataBio.ANA_idCatalogo_Muestras);
                comboDetalleCatalogo(dataBio.ANA_idCatalogo_Muestras);
                $('#selDetalleMuestra').val(dataBio.ANA_idDetalleCatalogo_Muestras);

                $('#txtOrgano').val(dataBio.ANA_organoBiopsia);

                comboServicio($('#selServicioOrigen'), dataBio.Origen.GEN_dependenciaServicio);
                $('#selServicioOrigen').val(dataBio.Origen.GEN_idServicioOrigen);
                $('#selTipoOrigen').val(dataBio.Origen.GEN_dependenciaServicio);

                $('#selTipoDestino').val(dataBio.Destino.GEN_dependenciaServicio);
                comboServicio($('#selServicioDestino'), dataBio.Destino.GEN_dependenciaServicio);
                $('#selServicioDestino').val(dataBio.Destino.GEN_idServicioDestino);


                if (!$.isEmptyObject(dataBio.Programa.GEN_idPrograma))
                    $('#selPrograma').val(dataBio.Programa.GEN_idPrograma);
                else
                    $('#selPrograma').val(0);

                $('#selSolicitado').val(dataBio.Medico.ANA_idMedSolicitaBiopsia);

                $('#selPatologo').val(valCampo(dataBio.Usuario.Patologo.GEN_idUsuario));
                $('#selTecnologo').val(valCampo(dataBio.Usuario.Tecnologo.GEN_idUsuario));
                $('#selTipoBiopsia').val(valCampo(dataBio.Tipo.ANA_idTipo_Biopsia));
                $('#selTipoMacroscopia').val(valCampo(dataBio.Tipo.ANA_idTipo_Macroscopia));
                $('#txtAntecedentesClinicos').val(dataBio.ANA_antecedentes_ClínicosRegistro_Biopsias);

                if (dataBio.ANA_dictadaBiopsia == 'SI')
                    $('#chkDictado').attr('checked', true);
                if (dataBio.ANA_tumoralBiopsia == 'SI')
                    $('#chkTumoral').attr('checked', true);

                $('#btnMovimientos').removeAttr('disabled');
            }
        });
    }
    else {
        jsonBiopsia = {};
        $('#txtRecepcion').removeAttr('disabled');

        $('#txtId').val('0');
        $('#txtNRegistro').val('0');
        if (vCodigoPerfil == 6 || vCodigoPerfil == 8) {
            $('#selDocumento').removeAttr('disabled');
            $('#txtRut').removeAttr('disabled');
            $('#txtDigito').removeAttr('disabled');
        }
    }

    if (vCodigoPerfil == 6 || vCodigoPerfil == 8) {
        //SECRETARIA O PARAMEDICO
        $('#selPaciente').removeAttr('disabled');
        $('#selGES').removeAttr('disabled');
        $('#selTipoMuestra').removeAttr('disabled');
        $('#selDetalleMuestra').removeAttr('disabled');
        $('#txtOrgano').removeAttr('disabled');
        $('#selMuestrasPorFrasco').removeAttr('disabled');

        $('#selTipoOrigen').removeAttr('disabled');
        $('#selServicioOrigen').removeAttr('disabled');
        $('#selTipoDestino').removeAttr('disabled');
        $('#selServicioDestino').removeAttr('disabled');
        $('#selPrograma').removeAttr('disabled');
        $('#selSolicitado').removeAttr('disabled');

        $('#btnAgregar').removeAttr('disabled');
        $('#btnGuardar').show();
    }

    grillaDetalle(vID);

    $('#selDocumento').change(function (e) {
        if ($('#selDocumento').val() == 1) {
            $('#txtDigito').show();
            $('#txtguion').show();
            $("#txtRut").attr('maxlength', '8');

        }
        else {
            $('#txtDigito').hide();
            $('#txtguion').hide();
            $("#txtRut").attr('maxlength', '15');
        }
        $("#txtRut").val("");
        $("#txtDigito").val("");
        $("#txtNombre").val("");
        $("#txtApellidoP").val("");
        $("#txtApellidoM").val("");
        $("#txtEdad").val("");
        $("#txtNacimiento").val("");
        $("#txtNUI").val("");
        $("#txtNacionalidad").val("");
    });

    $('#txtRut').blur(function (e) {
        if ($('#selDocumento').val() != 1)
            cargarPaciente();
    });
    $('#txtDigito').blur(function (e) {
        if ($('#selDocumento').val() == 1)
            cargarPaciente();
    });

    $('#selTipoOrigen').change(function (e) {
        comboServicio($('#selServicioOrigen'), $('#selTipoOrigen>option:selected').html());
    });
    $('#selTipoDestino').change(function (e) {
        comboServicio($('#selServicioDestino'), $('#selTipoDestino>option:selected').html());
    });

    $('#btnGuardar').click(function (e) {

        var bValido = true;

        if ($('#selDocumento').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDocumento'));
        }
        if ($('#txtOrgano').val() == '') {
            bValido = false;
            resaltaElemento($('#txtOrgano'));
        }
        if (vIdPaciente == 0 || vIdPaciente == 'undefined' || vIdPaciente == null) {
            bValido = false;
            resaltaElemento($('#txtRut'));
        }

        if ($('#selPaciente').val() == '0') {
            bValido = false;
            resaltaElemento($('#selPaciente'));
        }
        if ($('#selTipoMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoMuestra'));
        }

        if ($('#selServicioOrigen').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioOrigen'));
        }
        if ($('#selServicioDestino').val() == '0') {
            bValido = false;
            resaltaElemento($('#selServicioDestino'));
        }

        //Secretaria pidió que no sea obligatorio.
        //if ($('#selPrograma').val() == '0' || $('#selPrograma').val() == null) {
        //    bValido = false;
        //    resaltaElemento($('#selPrograma'));

        if ($('#selSolicitado').val() == '0') {
            bValido = false;
            resaltaElemento($('#selSolicitado'));
        }

        if ($('#selDetalleMuestra').val() == '0') {
            bValido = false;
            resaltaElemento($('#selDetalleMuestra'));
        }

        if (bValido) {

            let jsonBiopsia = {};

            jsonBiopsia.FechaRecepcion = $('#txtRecepcion').val();
            jsonBiopsia.IdPaciente = vIdPaciente;
            jsonBiopsia.Organo = $('#txtOrgano').val();
            jsonBiopsia.Ges = $('#selGES').val();
            jsonBiopsia.IdServicioOrigen = $('#selServicioOrigen').val();
            jsonBiopsia.IdMedicoSolicita = $('#selSolicitado').val();
            jsonBiopsia.IdServicioDestino = $('#selServicioDestino').val();
            jsonBiopsia.IdPrograma = ($('#selPrograma').val() == '0' || $('#selPrograma').val() == undefined) ? null : $('#selPrograma').val();
            jsonBiopsia.IdMedicoSolicita = $('#selSolicitado').val();
            jsonBiopsia.IdCatalogoMuestra = $('#selTipoMuestra').val();
            jsonBiopsia.IdDetalleCatalogoMuestra = $('#selDetalleMuestra').val();
            jsonBiopsia.IdModalidadFonasa = $('#selPaciente').val();
            jsonBiopsia.AntecedentesClinicos = ($.trim($('#txtAntecedentesClinicos').val()) != "") ? $.trim($('#txtAntecedentesClinicos').val()) : null;

            var vMetodo;
            var vUrl;

            if (vID == 0) {
                vMetodo = 'POST';
                vUrl = `${GetWebApiUrl()}ANA_Registro_Biopsias`;
                jsonBiopsia.Numero = CargarConf('n_biopsia');
                jsonBiopsia.Año = CargarConf('a_biopsia');
            }
            else {

                vMetodo = 'PUT';
                vUrl = `${GetWebApiUrl()}ANA_Registro_Biopsias/${vID}`;
                jsonBiopsia.IdBiopsia = parseInt(vID);
                jsonBiopsia.IdUsuarioPatologo = ($('#selPatologo').val() != "0") ? $('#selPatologo').val() : null;
                jsonBiopsia.IdUsuarioTecnologo = ($('#selTecnologo').val() != "0") ? $('#selTecnologo').val() : null;
                jsonBiopsia.IdTipoBiopsia = ($('#selTipoBiopsia').val() != "0") ? $('#selTipoBiopsia').val() : null;
                jsonBiopsia.IdTipoMacroscopia = ($('#selTipoMacroscopia').val() != "0") ? $('#selTipoMacroscopia').val() : null;
                let numeroBiopsiaPut = $('#txtNRegistro').val().split("-");
                jsonBiopsia.Numero = numeroBiopsiaPut[0];
                jsonBiopsia.Año = numeroBiopsiaPut[1];
            }

            //la tabla de muestras
            var t = $('#tblDetalle').DataTable();
            var d = t.rows().data().toArray();

            if (d.length == 0) {
                resaltaElemento($('#selTipoMuestra'));
                resaltaElemento($('#selDetalleMuestra'));
                resaltaElemento($('#txtOrgano'));
                return false;
            }

            jsonBiopsia.DetalleCatalogo = [];

            for (var p = 0; p < d.length; p++) {
                let json = {};
                json.IdDetalleCatalogoMuestra = d[p][1];
                json.Descripcion = d[p][3];
                json.CantidadMuestras = d[p][4];

                jsonBiopsia.DetalleCatalogo.push(json);
            }
            console.log(jsonBiopsia);

            $.ajax({
                type: vMetodo,
                url: vUrl,
                data: JSON.stringify(jsonBiopsia),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {

                    if (vMetodo == 'POST') {
                        GuardarConf('n_biopsia', CargarConf('n_biopsia') + 1);

                        toastr.success('Se ha ingresado solicitud de biopsia', '', {
                            timeOut: 100, extenderTimeOut: 100, onShown: function () { window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`); }
                        });
                        vID = data.ANA_idBiopsia;

                        $('#btnGuardar').attr('disabled', true);
                        $('#btnCancelar').attr('disabled', true);

                    }
                    else if (vMetodo == 'PUT') {
                        toastr.success('Se ha actualizado la solicitud de biopsia', '', {
                            timeOut: 100, extenderTimeOut: 100, onShown: function () { window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`); }
                        });
                        $('#btnGuardar').attr('disabled', true);
                        $('#btnCancelar').attr('disabled', true);
                    }
                    //                    CrearDetalleRegistro(vID);
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }

        e.preventDefault();
    });

    $('#btnCancelar').click(function (e) {
        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
        e.preventDefault();
    });

    $('#btnMovimientos').click(function (e) {
        modalMovimiento(vID);
        $('#mdlMovimientos').modal('show');
        e.preventDefault();
    });
});

function cargarPaciente() {

    if ($('#selDocumento').val() == 1) {
        $("#txtRut").attr('maxlength', '8');
        $('#txtDigito').html(DigitoVerificador($('#txtRut').val()));
    }
    else {
        $("#txtRut").attr('maxlength', '10');

    }

    if ($('#txtRut').val().length >= 6) {

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${$('#selDocumento').val()}/${$('#txtRut').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            success: function (dataPac) {
                if (!$.isEmptyObject(dataPac[0])) {

                    vIdPaciente = dataPac[0].GEN_idPaciente;
                    $('#txtNombre').val(dataPac[0].GEN_nombrePaciente);
                    $('#txtApellidoP').val(dataPac[0].GEN_ape_paternoPaciente);
                    $('#txtApellidoM').val(dataPac[0].GEN_ape_maternoPaciente);
                    $('#txtNacimiento').val(moment(dataPac[0].GEN_fec_nacimientoPaciente).format('YYYY-MM-DD'));
                    $('#txtNUI').val(dataPac[0].GEN_nuiPaciente);
                    $('#txtEdad').val(CalcularEdad(moment(dataPac[0].GEN_fec_nacimientoPaciente).format('YYYY-MM-DD')));

                    if (!$.isEmptyObject(dataPac.GEN_Nacionalidad))
                        $('#txtNacionalidad').val(dataPac.GEN_Nacionalidad.GEN_descripcionNacionalidad);

                } else {
                    vIdPaciente = 0;
                    $('#txtNombre').val('');
                    $('#txtApellidoP').val('');
                    $('#txtApellidoM').val('');
                    $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));
                    $('#txtEdad').val('');
                    $('#txtNacionalidad').val('');
                }
            }
        });
    }
}

function linkQuitar(e) {
    let id = $(e).data("id");
    var t = $('#tblDetalle').DataTable();
    t.row(id).remove();
    var d = t.rows().data().toArray();
    t.clear();
    t.rows.add(d).draw();
}

function grillaDetalle(id) {

    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}/DetalleMuestras`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idDetalleBiopsia,
                    val.ANA_idDetalleCatalogo_Muestras,
                    val.ANA_detalleCatalogo_Muestras,
                    val.ANA_descripcionBiopsia,
                    val.ANA_cantidadMuestras,
                    ''
                ]);
            });
            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                        {
                            targets: -1,
                            data: null,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                let fila = meta.row;
                                var botones;
                                if (vCodigoPerfil == 6 || vCodigoPerfil == 8)
                                    botones = '<a class="btn btn-danger" data-id="' + fila + '" onclick="linkQuitar(this)"><span class="glyphicon glyphicon-remove"></span></a>';
                                else
                                    botones = '<a class="btn btn-danger" disabled><span class="glyphicon glyphicon-remove"></span></a>';

                                return botones;
                            }
                        },
                    ],
                columns: [
                    { title: 'ANA_idDetalleBiopsia' },
                    { title: 'ANA_idDetalleCatalogo_Muestras' },
                    { title: 'Detalle muestra' },
                    { title: 'Descripción' },
                    { title: 'N° muestras' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}
function cargarCombosRecepcion() {
    comboMedicos();
    comboProgramasAsync();
    comboIdentificacion();

    comboModalidad();
    comboCatalogo();

    comboServicio($('#selServicioOrigen'), 'HCM');
    comboServicio($('#selServicioDestino'), 'HCM');

    let url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/3`;
    setCargarDataEnComboAsync(url, true, $('#selPatologo'));

    url = `${GetWebApiUrl()}GEN_Usuarios/Perfil/4`;
    setCargarDataEnComboAsync(url, true, $('#selTecnologo'));

    url = `${GetWebApiUrl()}ANA_Tipo_Biopsia/combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoBiopsia'));

    url = `${GetWebApiUrl()}ANA_Tipo_Macroscopia/combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoMacroscopia'));
}

function comboMedicos() {
    $('#selSolicitado').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Profesional/Medico`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selSolicitado').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selSolicitado').append("<option value='" + val.GEN_idProfesional + "'>" + val.GEN_nombrePersonas + "</option>");
            });
        }
    });
}

function comboProgramasAsync() {
    let url = `${GetWebApiUrl()}GEN_Programa/combo`;
    setCargarDataEnComboAsync(url, true, $('#selPrograma'));
}

function comboServicio(cmbServicio, dependencia) {
    //debe ser sincrono pq es una cascada
    let url = `${GetWebApiUrl()}GEN_Servicio/combo/${dependencia}`;
    setCargarDataEnComboAsync(url, false, cmbServicio);
}

function comboIdentificacion() {
    $('#selDocumento').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                $('#selDocumento').append("<option value='" + val.GEN_idIdentificacion + "'>" + val.GEN_nombreIdentificacion + "</option>");
            });
        }
    });
}

function comboModalidad() {
    let url = `${GetWebApiUrl()}GEN_Modalidad_Fonasa/Combo`;
    setCargarDataEnComboAsync(url, false, $('#selPaciente'));
}

function comboCatalogo() {
    let url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/Combo`;
    setCargarDataEnComboAsync(url, true, $('#selTipoMuestra'));
}

function comboDetalleCatalogo(idCatalogo) {
    if (idCatalogo > 0) {
        let url = `${GetWebApiUrl()}ANA_Catalogo_Muestras/${idCatalogo}/Detalle/combo`;
        setCargarDataEnComboAsync(url, false, $('#selDetalleMuestra'));
    }
}