﻿$(document).ready(function () {
    getBiopsiasApi();
    getCortespi();
});

function getCortespi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Cortes_Muestras/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getCortesPorEstado(data);
        }
    });
}

function getBiopsiasApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getBiopsiasPorEstado(data);
        }
    });
}

function getCortesApi() {
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/SituacionActual`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            getBiopsiasPorEstado(data);
        }
    });
}

function getBiopsiasPorEstado(data) {
    let html;
    let cabecera = `<tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblResumenBiopsias').append(cabecera);

    let estados = [42, 43, 47, 46, 45];
    estados.forEach(function (key, i) {
        let registros = data.filter(d => d.IdTipoEstadoSistema == key);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].Estado}</td>
                    <td>${registros.length}</td>
                    <td>
                       <a id="aCerrarInterconsultor_${i}" 
                            onclick="getDetalleBiopsiaApi(0, ${key})" 
                            class="btn btn-info">Ver</a></td>
                <tr>`;
            $('#tblResumenBiopsias').append(html);
        }
    });

}

function getDetalleBiopsiaApi(id, idEstado) {
    let parametros = "";
    let urlApi;
    if (idEstado > 0) {
        parametros = `idTipoEstadoSistema=${idEstado}`;
        urlApi = `${GetWebApiUrl()}ANA_Registro_Biopsias/Buscar?${parametros}`;
    }
    else {
        urlApi = `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}`;
    }

    $.ajax({
        type: 'GET',
        url: urlApi,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdTipoEstadoSistemas,
                    val.Numero,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.Patologo.Login ?? '',
                    val.OrganoBiopsia,
                    val.NombreTipoBiopsia,
                    val.NombreTipoEstadoSistemas
                ]);
            });
            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                    ],
                columns: [
                    { title: 'Id biopsia' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'N°' },
                    { title: 'Recepción' },
                    { title: 'Patologo' },
                    { title: 'Órgano' },
                    { title: 'Tipo Biopsia' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}

let registrox = [];
function getCortesPorEstado(data) {
    let html;
    let cabecera = `<tr>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Detalle</th>
                    <tr>`;
    $('#tblResumenCortes').append(cabecera);
    let estados = [50, 56, 53, 60, 54, 55, 59, 57, 61, 51];
    estados.forEach(function (key, i) {
        let registros = data.filter(d => d.IdTipoEstadoSistema == key);
        registrox.push(registros);
        if (registros.length > 0) {
            html = `<tr>
                    <td>${registros[0].Estado}</td>
                    <td>${registros.length}</td>
                    <td><a id="aCerrarInterconsultor_${i}" onclick="getDetalleCortesDatos(${i})" class="btn btn-info">Ver</a></td>
                <tr>`;
            
            $('#tblResumenCortes').append(html);
        }
    });
}

function getDetalleCortesApi(idEstado) {
    let parametros = "";
    let urlApi;
    if (idEstado > 0) {
        parametros = `idTipoEstadoSistema=${idEstado}`;
        urlApi = `${GetWebApiUrl()}/ANA_Cortes_Muestras/Buscar?${parametros}`;
    }
    $.ajax({
        type: 'GET',
        url: urlApi,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.Id,
                    val.IdTipoEstadoSistemas,
                    val.Numero,
                    moment(val.FechaRecepcion).format('DD-MM-YYYY'),
                    val.OrganoBiopsia,
                    val.NombreTipoBiopsia,
                    val.NombreTipoEstadoSistemas
                ]);
            });

            $('#tblDetalle').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                    [
                        { targets: [0, 1], visible: false, searchable: false },
                    ],
                columns: [
                    { title: 'Id Corte' },
                    { title: 'GEN_idTipo_Estados_Sistemas' },
                    { title: 'N°' },
                    { title: 'Recepción' },
                    { title: 'Órgano' },
                    { title: 'Tipo Biopsia' },
                    { title: 'Estado' }
                ],
                bDestroy: true
            });
            $("#mdlDetalle").modal("show");
        }
    });
}

function getDetalleCortesDatos(i) {
    let adataset = [];
    let data = registrox[i];
    $.each(data, function (key, val) {
        adataset.push([
            val.Id,
            val.IdBiopsia,
            val.Estado,
        ]);
    });

    $('#tblDetalle').DataTable({
        data: adataset,
        order: [],
        columns: [
            { title: 'Id Corte' },
            { title: 'Id Biopsia' },
            { title: 'Estado' },
        ],
        bDestroy: true
    });
    $("#mdlDetalle").modal("show");
}