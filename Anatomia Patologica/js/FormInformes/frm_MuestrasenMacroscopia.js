﻿$(document).ready(function () {
    grillaMuestras();
});

function grillaMuestras()
{
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/buscar?idTipoEstadoSistema=43`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            // console.log(data);
            if (!$.isEmptyObject(data)) {
                let adataset = [];
                $.each(data, function (key, val) {
                    let tc = [];
                    for (let i = 0; i < val.Tecnicas.length; i++) {
                    tc.push(' ' + val.Tecnicas[i].Nombre.substring(0,4))
                    }
                    adataset.push([
                        val.Numero,
                        val.NombreTipoBiopsia,
                        val.OrganoBiopsia,
                        val.Cortes.length,
                        val.NombreTipoMacroscopia,
                        `${val.Patologo.Nombre} ${val.Patologo.ApellidoPaterno}  ${val.Patologo.ApellidoMaterno}`,                        
                        tc,
                        val.Descacificaciones
                    ]);
                });

                $('#tblMuestrasenMacroscopia').DataTable({
                    data: adataset,
                    order: [],
                    columns: [
                        { title: 'N° biopsia' },
                        { title: 'Tipo Biopsia' },
                        { title: 'Órgano' },
                        { title: 'Cortes' },
                        { title: 'Tipo Macroscopia' },
                        { title: 'Patólogo' },
                        { title: 'Técnicas' },
                        { title: 'Descalc.' }
                    ],
                    bDestroy: true
                });
            }
        }
    });
}
