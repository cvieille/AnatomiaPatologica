﻿$(document).ready(function () {
    $('#txtDesde').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));
    $('#txtHasta').attr("max", moment().format('YYYY-MM-DD'));
    $('#txtDesde').attr("max", moment().format('YYYY-MM-DD'));

    grillaRecepcionadas();

    $('#btnBuscar').click(function (e) {
        grillaRecepcionadas();
        e.preventDefault();
    });
  
});

function grillaRecepcionadas()
{
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/RECEPCIONADAS/${$('#txtDesde').val()}/${$('#txtHasta').val()}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idBiopsia,
                    val.ANA_NumeroRegistro_Biopsias,
                    val.GEN_nombrePaciente,
                    (moment(val.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY/MM/DD hh:mm:ss').format('DD-MM-YYYY')),
                    val.GEN_dependenciaServicioOrigen,
                    val.GEN_nombreServicioOrigen,
                    val.ANA_organoBiopsia,
                    val.ANA_fecDespBiopsia == undefined ? '' : (moment(val.ANA_fecDespBiopsia, 'YYYY/MM/DD hh:mm:ss').format('DD-MM-YYYY')),
                    val.GEN_PersonaRecibe
                ]);
            });

            if (adataset.length == 0)
                $('#btnExportar').attr('disabled', true);
            else
                $('#btnExportar').removeAttr('disabled');

            $('#tblRecepcionadas').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [
                    { targets: [3, 7], sType: 'date-ukShort' },
                    { targets: 0, visible: false, searchable: false },
                ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'N° biopsia' },
                    { title: 'Nombre paciente' },
                    { title: 'Fecha recepción' },
                    { title: 'Dependencia' },
                    { title: 'Origen' },
                    { title: 'Organo' },
                    { title: 'Fecha despacho' },
                    { title: 'Recibe' }
                ],
                bDestroy: true
            });
        }
    });
}