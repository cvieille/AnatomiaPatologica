﻿var vIdBiopsia;
$(document).ready(function () {

    var sSession = getSession();
    var vIdUsuario = sSession.id_usuario;

    $('#txtDesde').val(moment().add(-1, 'month').format('YYYY-MM-DD'));
    $('#txtHasta').val(moment().format('YYYY-MM-DD'));

    $('#btnBuscar').click(function (e) {
        grillaNeoplasia();
        e.preventDefault();
    });

    $('#btnCodificacion').click(function (e) {
        $.ajax({
            type: 'POST',
            url: `${ObtenerHost()}/FormInformes/frm_BiopsiasconNeoplasia.aspx/verCodificacion`,
            data: JSON.stringify({ idBiopsia: vIdBiopsia }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
            }
        });
        e.preventDefault();
    });
    $('#btnInforme').click(function (e) {
        $.ajax({
            type: 'POST',
            url: `${ObtenerHost()}/FormInformes/frm_BiopsiasconNeoplasia.aspx/verInforme`,
            data: JSON.stringify({ idBiopsia: vIdBiopsia, idUsuario: vIdUsuario }),
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                window.open(data.d);
            }
        });
        e.preventDefault();
    });
    grillaNeoplasia();
});

function detalleArchivo(e)
{
    let archivo = $(e).data('id');
    let url = archivo.replace('/GIAP/dcto/adj/', '');
    window.open(ObtenerHost() + '/Documento.aspx?nombre=' + url + 'idBiopsia=' + data.Archivo.IdBiopsia );
}

function detalleNeo(e)
{
    let id = $(e).data('id');    
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Registro_Biopsias/${id}?tipo=2`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var o = '';
            for (var i = 0; i < data[0].ANA_organoBiopsia.length; i++)
                o += data[0].ANA_organoBiopsia[i] + '<br/>';
            $('#spanFechaRecepcion').html(moment(data.ANA_fec_RecepcionRegistro_Biopsias).format('YYYY-MM-DD'));
            $('#spanRutPaciente').html(data.Paciente.GEN_numero_documentoPaciente);
            $('#spanNombrePaciente').html(data[0].GEN_Nombre_PersonasPaciente);
            $('#spanOrgano').html(o);
            $('#spanPatologo').html(data[0].GEN_nombrePersonasPatologo);
            $('#spanTecnologo').html(data[0].GEN_nombrePersonasTecnologo);
            $('#spanMedSolicitante').html(data[0].GEN_nombrePersonasMedico);
            $('#spanServOrigen').html(data[0].GEN_nombreServicioOrigen);
            $('#spanServDestino').html(data[0].GEN_nombreServicioDestino);
        }
    });

    $.ajax({
        type: 'POST',
        url: `${ObtenerHost()}/FormInformes/frm_BiopsiasconNeoplasia.aspx/adjuntosBiopsia`,
        data: JSON.stringify({ idBiopsia: id }),
        contentType: 'application/json',
        dataType: 'json',        
        success: function (data) {
            var json = JSON.parse(data.d);
            let adataset = [];
            //console.log(json);
            $.each(json, function (key, val) {
                adataset.push([
                    val.ANA_FecArchivos_Biopsias,
                    val.ANA_NomArchivos_Biopsias,
                    val.ruta_archivo
                ]);
            });
            $('#tblArchivo').DataTable({
                data: adataset,
                order: [],
                dom: 't',
                columnDefs:
                [
                    { targets: [2], visible: false, searchable: false },
                    { targets: 0, sType: 'date-ukLong' },
                    {
                        targets: 1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones = `<a data-id="${adataset[fila][2]}" href="#/" onclick="detalleArchivo(this)">${adataset[fila][1]}</a>`;
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'Fecha' },
                    { title: 'Archivo' },
                    { title: ''}
                ],
                bDestroy: true
            });
        }
    });
    $('#mdlDetalle').modal('show');
}

//grilla para informe de neoplasia
function grillaNeoplasia() {
    let url = `${GetWebApiUrl()}ANA_Registro_Biopsias/INFORME/NEOPLASIA/${moment($('#txtDesde').val()).format('YYYY-MM-DD')}/${moment($('#txtHasta').val()).format('YYYY-MM-DD')}`;

    $.ajax({
        type: 'GET',
        url: url,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];

            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idBiopsia,
                    val.ANA_NumeroRegistro_Biopsias,
                    val.GEN_nombrePaciente,
                    val.ANA_organoBiopsia,
                    val.ANA_fec_RecepcionRegistro_Biopsias == undefined ? '' : (moment(val.ANA_fec_RecepcionRegistro_Biopsias, 'YYYY/MM/DD').format('DD-MM-YYYY')),
                    val.ANA_fecDespBiopsia == undefined ? '' : (moment(val.ANA_fecDespBiopsia, 'YYYY/MM/DD').format('DD-MM-YYYY')),
                    val.GEN_dependenciaServicioOrigen,
                    val.GEN_nombreServicioOrigen,
                    val.GEN_nombreServicioDestino,
                    ''
                ]);
            });

            $('#tblNeoplasia').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [
                    { targets: [0], visible: false, searchable: false },
                    { targets: [4, 5], sType: 'date-ukShort' },
                    {
                        targets: 1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones = `<a data-id="${adataset[fila][0]}" href="#/" onclick="detalleNeo(this)">${adataset[fila][1]}</a>`;
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'ANA_IdBiopsia' },
                    { title: 'Nº biopsia' },
                    { title: 'Paciente' },
                    { title: 'Órgano' },
                    { title: 'Fecha recepción' },
                    { title: 'Fecha despacho' },
                    { title: 'Dependencia' },
                    { title: 'Origen' },
                    { title: 'Destino' },
                    { title: 'Recibe' },
                ],
                bDestroy: true
            });
        }
    });
}