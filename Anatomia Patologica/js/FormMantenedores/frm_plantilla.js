﻿$(document).ready(function () {
    var vIdPlantilla = getUrlParameter('id');

    var sSession = getSession();
    var vIdUsuario = sSession.id_usuario;
    var vCreacionPlantilla;

    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Plantillas/' + vIdPlantilla,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            if (!$.isEmptyObject(data)) {
                
                $('#txtNombrePlantilla').val(data.ANA_nombreplantilla);
                vCreacionPlantilla = data.ANA_fec_creacionPlantilla;

                if (data.ANA_desc_ant_clinicosPlantilla == '')
                    $('#aAnteClinicos').first().addClass('_bordeError1px');
                else
                    $('#txtAntecedentesClinicos').val(data.ANA_desc_ant_clinicosPlantilla);

                if (data.ANA_desc_macroscopicaPlantilla == '')
                    $('#aDescMacro').first().addClass('_bordeError1px');
                else
                    $('#txtDescMacro').val(data.ANA_desc_macroscopicaPlantilla);

                if (data.ANA_desc_microscopicaPlantilla == '')
                    $('#aExamenMicro').first().addClass('_bordeError1px');
                else
                    $('#txtExamenMicro').val(data.ANA_desc_microscopicaPlantilla);

                if (data.ANA_desc_diagnosticoPlantilla == '')
                    $('#aDiag').first().addClass('_bordeError1px');
                else
                    $('#txtDiag').val(data.ANA_desc_diagnosticoPlantilla);

                if (data.ANA_estu_inmunoPlantilla == '')
                    $('#aInmuno').first().addClass('_bordeError1px');
                else
                    $('#txtInmuno').val(data.ANA_estu_inmunoPlantilla);

                if (data.ANA_desc_notaPlantilla == '')
                    $('#aNotaAdicional').first().addClass('_bordeError1px');
                else
                    $('#txtNotaAdicional').val(data.ANA_desc_notaPlantilla);
            }
        }
    });

    $('#btnGuardar').click(function (e) {
        var bValido = true;
        if ($('#txtNombrePlantilla').val() == '' || $('#txtNombrePlantilla').val() == undefined || $('#txtNombrePlantilla').val() == null) {
            bValido = false;
            resaltaElemento($('#txtNombrePlantilla'));
        }

        if (bValido) {

            let json = {};
            
            json.ANA_nombreplantilla = $('#txtNombrePlantilla').val();
            
            if ($('#txtAntecedentesClinicos').val()!='')
                json.ANA_desc_ant_clinicosPlantilla = $('#txtAntecedentesClinicos').val();
            
            if ($('#txtDescMacro').val()!='')
                json.ANA_desc_macroscopicaPlantilla = $('#txtDescMacro').val();
            
            if ($('#txtExamenMicro').val()!='')                
                json.ANA_desc_microscopicaPlantilla = $('#txtExamenMicro').val();
            
            if ($('#txtDiag').val()!='')
                json.ANA_desc_diagnosticoPlantilla = $('#txtDiag').val();
            
            if ($('#txtInmuno').val()!='')
                json.ANA_estu_inmunoPlantilla = $('#txtInmuno').val();
            
            if ($('#txtNotaAdicional').val()!='')
                json.ANA_desc_notaPlantilla = $('#txtNotaAdicional').val();
            
            json.GEN_IdUsuarios = vIdUsuario;
            json.ANA_estadoPlantillas = 'Activo';
           

            var vMetodo;
            var vUrl = `${GetWebApiUrl()}ANA_Plantillas/${vIdPlantilla}`
            if (vIdPlantilla == 0) {
                vMetodo = 'POST';
                vUrl = `${GetWebApiUrl()}ANA_Plantillas`
            }
            else {
                vMetodo = 'PUT';
                json.ANA_fec_creacionPlantilla = vCreacionPlantilla;
                json.ANA_idPlantilla = vIdPlantilla;
            }

            $.ajax({
                type: vMetodo,
                url: vUrl,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la plantilla <br/> <u>Click</u> en este mensaje para volver', '',
                            {
                                onclick: function () { window.location.replace('frm_ListaPlantillas.aspx'); },
                                onHidden: function () { window.location.replace('frm_ListaPlantillas.aspx'); }
                            });
                    else
                        toastr.success('Se ha creado la plantilla <br/> <u>Click</u> en este mensaje para volver', '',
                            {
                                onclick: function () { window.location.replace('frm_ListaPlantillas.aspx'); },
                                onHidden: function () { window.location.replace('frm_ListaPlantillas.aspx'); }
                            });

                    $('#btnGuardar').attr('disabled', true);
                    $('#btnCancelar').attr('disabled', true);
                }
            });
        }
        e.preventDefault();
    });
    $('#btnCancelar').click(function (e) {
        window.location.replace('frm_ListaPlantillas.aspx');
        e.preventDefault();
    });
});