﻿$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    tablaProgramas();

    $('#btnNuevoRegistro').click(function (e) {
        modalEditar(0);
        e.preventDefault();
    });

    $('#btnGuardarPrograma').click(function (e) {
        var bValido = true;
        if ($('#txtNombrePrograma').val() == '' || $('#txtNombrePrograma').val() == null) {
            resaltaElemento($('#txtNombrePrograma'));
            bValido = false;
        }
        if ($('#selEstadoPrograma').val() == '0') {
            resaltaElemento($('#selEstadoPrograma'));
            bValido = false;
        }

        if (bValido) {
            var m = 'POST';
            var u = `${GetWebApiUrl()}GEN_Programa`;
            let json = {};
            json.GEN_nomPrograma = $('#txtNombrePrograma').val();
            json.GEN_estadoPrograma = $('#selEstadoPrograma').val();
            if ($('#txtIdPrograma').val() != '0') {
                m = 'PUT';
                u = `${GetWebApiUrl()}GEN_Programa/${$('#txtIdPrograma').val()}`;
                json.GEN_idPrograma = $('#txtIdPrograma').val();
            }

            $.ajax({
                type: m,
                url: u,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado el programa');
                    else
                        toastr.success('Se ha creado el programa');
                    tablaProgramas();
                    $('#mdlEditarPrograma').modal('hide');
                }
            });
        }
        
        e.preventDefault();
    });
});

function modalEditar(id)
{
    if (id != 0) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Programa/${id}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                $('#txtIdPrograma').val(data.GEN_idPrograma);
                $('#txtNombrePrograma').val(data.GEN_nomPrograma);
                $('#selEstadoPrograma').val(data.GEN_estadoPrograma);
                $('#selEstadoPrograma').removeAttr('disabled');
            }
        });
        $('#selEstadoPrograma').removeAttr('disabled');
    }
    else {
        $('#txtIdPrograma').val('0');
        $('#txtNombrePrograma').val('');
        $('#selEstadoPrograma').attr('disabled', true);
        $('#selEstadoPrograma').val('Activo');
    }
    $('#txtNombrePrograma').removeClass('_bordeError');
    $('#selEstadoPrograma').removeClass('_bordeError');
    $('#mdlEditarPrograma').modal('show');
}

function tablaProgramas()
{
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Programa`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.GEN_idPrograma,
                    val.GEN_nomPrograma,
                    val.GEN_estadoPrograma,
                    ''
                ]);
            });
            $('#tblProgramas').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones;
                            botones = '<a onclick="modalEditar(' + adataset[fila][0] + ')" class="btn btn-info">Editar</a>';
                            return botones;
                        }
                    }
                ],
                columns: [
                    { title: 'ID' },
                    { title: 'Descripción' },
                    { title: 'Estado' },
                    { title: 'Editar' },
                ],
                bDestroy: true
            });
        }
    });
}