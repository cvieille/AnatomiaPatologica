var vIdInmuno;
$(document).ready(function () {      
    grillaInmuno();
    $('#btnNuevoRegistro').click(function (e) {
        vIdInmuno = 0;
        $('#txtIdInmuno').val('0');
        $('#txtNombreInmuno').val('');
        $('#selEstadoInmuno').prop('value', '0');

        $('#selEstadoInmuno').removeClass('_bordeError');
        $('#txtNombreInmuno').removeClass('_bordeError');

        $('#mdlInmuno').modal('show');
        e.preventDefault();
    });

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('body').on('click', '#btnGuardarTecnica', function (e) {
        var bValido = true;

        if ($('#selEstadoInmuno').prop('value') == '0') {
            resaltaElemento($('#selEstadoInmuno'));
            bValido = false;
        }
        if ($('#txtNombreInmuno').val() == '' || $('#txtNombreInmuno').val() == undefined || $('#txtNombreInmuno').val() == null) {
            resaltaElemento($('#txtNombreInmuno'));
            bValido = false;
        }
        if (bValido)
        {
            var json = {};
            json.ANA_NomInmunoHistoquimica = $('#txtNombreInmuno').val();
            json.ANA_EstInmunoHistoquimica = $('#selEstadoInmuno').val();

            var vMetodo;
            var vUrl;
            
            if (vIdInmuno == 0) {
                vMetodo = 'POST';
                vUrl = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica`
            }
            else {
                vMetodo = 'PUT';
                vUrl = `${GetWebApiUrl()}ANA_Inmuno_Histoquimica/${vIdInmuno}`
                json.ANA_idInmunoHistoquimica = vIdInmuno;
            }
            $.ajax({
                type: vMetodo,
                url: vUrl,
                contentType: 'application/json',
                data: JSON.stringify(json),
                dataType: 'json',
                async: false,
                success: function (data) {
                    if ($.isEmptyObject(data))
                        toastr.success('Se ha actualizado la Inmuno Histoquimica');
                    else
                        toastr.success('Se ha creado la Inmuno Histoquimica');
                    grillaInmuno();
                    $('#mdlInmuno').modal('hide');
                }
            });
        }
        e.preventDefault();
    });

});

function lnkEditarInmuno(e) {
    var id = $(e).data("id");
    vIdInmuno = id;
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}ANA_Inmuno_Histoquimica/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#txtIdInmuno').val(data.ANA_idInmunoHistoquimica);
            $('#txtNombreInmuno').val(data.ANA_NomInmunoHistoquimica);
            $('#selEstadoInmuno').val(data.ANA_EstInmunoHistoquimica.trim());

            $('#selEstadoInmuno').removeClass('_bordeError');
            $('#txtNombreInmuno').removeClass('_bordeError');
        }
    });
    
    $('#mdlInmuno').modal('show');
}

function grillaInmuno() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + '/ANA_Inmuno_Histoquimica',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idInmunoHistoquimica,
                    val.ANA_nomInmunoHistoquimica,
                    val.ANA_estInmunoHistoquimica,
                    ''
                ]);
            });

            $('#tblInmunos').DataTable({
                data: adataset,
                order: [],
                stateSave: true,
                iStateDuration: 60,
                columnDefs: [
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var fila = meta.row;
                            var botones = '<input type="button" value="Editar Inmuno" class="btn btn-info" data-id="' + adataset[fila][0] + '" onclick="lnkEditarInmuno(this)" />';
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'ID Inmuno' },
                    { title: 'Nombre Inmuno' },
                    { title: 'Estado' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}