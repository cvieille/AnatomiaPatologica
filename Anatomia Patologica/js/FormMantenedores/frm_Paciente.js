﻿var idPaciente;
$(document).ready(function () {

    $('body').on('change', 'select._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    comboPaciente();
    comboSexo();
    comboPais();

    $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));

    $('#selPais').change(function (e) {
        comboRegion($(this).val(), 0);
        comboProvincia(0, 0);
        comboCiudad(0, 0);
    });
    $('#selRegion').change(function (e) {
        comboProvincia($(this).val(), 0);
        comboCiudad(0, 0);
    });
    $('#selProvincia').change(function (e) {
        comboCiudad($(this).val(), 0);
    });
    $('#txtNacimiento').on('change', function (e) {
        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/CalcularEdad?GEN_fec_nacimientoPaciente=${$('#txtNacimiento').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                $('#txtEdad').val(data.edad);
            }
        });
    });

    $('#selTipoDocumento').change(function (e) {
        cargarPaciente();
        if ($(this).val() == '2' || $(this).val() == '3')
            $('#txtRut').attr('maxlength', 10);
        else {
            $('#txtRut').attr('maxlength', 8).val($('#txtRut').val().substring(0, 8));
        }
    });

    $('#txtRut').blur(function (e) {
        cargarPaciente();
    });

    $('#btnGuardar').click(function (e) {
        var bValido = true;

        $('#selEstado').removeClass('_bordeError');
        $('#txtRut').removeClass('_bordeError');
        $('#txtNombre').removeClass('_bordeError');
        $('#txtApellidoP').removeClass('_bordeError');
        $('#txtDireccion').removeClass('_bordeError');
        $('#txtNumero').removeClass('_bordeError');
        $('#txtTel').removeClass('_bordeError');
        $('#selSexo').removeClass('_bordeError');
        $('#selPais').removeClass('_bordeError');
        $('#selRegion').removeClass('_bordeError');
        $('#selProvincia').removeClass('_bordeError');
        $('#selCiudad').removeClass('_bordeError');

        if ($('#selTipoDocumento').val() == '0') {
            bValido = false;
            resaltaElemento($('#selTipoDocumento'));
        }

        if ($('#txtRut').val().length < 6) {
            bValido = false;
            resaltaElemento($('#txtRut'));
        }
        if ($('#selEstado')[0].hasAttribute('disabled')) {
            if ($('#selEstado').val() == '0') {
                bValido = false;
                resaltaElemento($('#selEstado'));
            }
        }
        if ($('#txtNombre').val() == '' || $('#txtNombre').val() == null || $('#txtNombre').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtNombre'));
        }
        if ($('#txtApellidoP').val() == '' || $('#txtApellidoP').val() == null || $('#txtApellidoP').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtApellidoP'));
        }
        if ($('#txtDireccion').val() == '' || $('#txtDireccion').val() == null || $('#txtDireccion').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtDireccion'));
        }
        if ($('#txtNumero').val() == '' || $('#txtNumero').val() == null || $('#txtNumero').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtNumero'));
        }
        if ($('#txtTel').val() == '' || $('#txtTel').val() == null || $('#txtTel').val() == undefined) {
            bValido = false;
            resaltaElemento($('#txtTel'));
        }
        if ($('#selSexo').val() == '0') {
            bValido = false;
            resaltaElemento($('#selSexo'));
        }
        var fechaHoy = moment().format('YYYY-MM-DD');
        var fechaNac = moment($('#txtNacimiento').val()).format('YYYY-MM-DD');
        if (fechaHoy < fechaNac) {
            bValido = false;
            resaltaElemento($('#txtNacimiento'));
        }
        if ($('#selPais').val() == '0') {
            bValido = false;
            resaltaElemento($('#selPais'));
        }
        if ($('#selRegion option').length > 1 && $('#selRegion').val() == '0') {
            bValido = false;
            resaltaElemento($('#selRegion'));
        }
        if ($('#selProvincia option').length > 1 && $('#selProvincia').val() == '0') {
            bValido = false;
            resaltaElemento($('#selProvincia'));
        }
        if ($('#selCiudad option').length > 1 && $('#selCiudad').val() == '0') {
            bValido = false;
            resaltaElemento($('#selCiudad'));
        }

        if (bValido) {
            let json = {};

            json.GEN_idPaciente = idPaciente;
            json.GEN_numero_documentoPaciente = $('#txtRut').val();
            json.GEN_digitoPaciente = $('#txtDigito').html();
            json.GEN_nombrePaciente = $('#txtNombre').val();
            json.GEN_ape_paternoPaciente = $('#txtApellidoP').val();
            json.GEN_ape_maternoPaciente = $('#txtApellidoM').val() == '' ? null : $('#txtApellidoM').val();
            json.GEN_dir_callePaciente = $('#txtDireccion').val();
            json.GEN_dir_numeroPaciente = $('#txtNumero').val();
            json.GEN_dir_ruralidadPaciente = 'NO';
            json.GEN_idPais = $('#selPais').val();
            json.GEN_idRegion = $('#selRegion').val();
            json.GEN_idProvincia = $('#selProvincia').val();

            json.GEN_idCiudad = $('#selCiudad').val();
            json.GEN_telefonoPaciente = $('#txtTel').val();
            json.GEN_idSexo = $('#selSexo').val();
            json.GEN_fec_nacimientoPaciente = $('#txtNacimiento').val();
            json.GEN_otros_fonosPaciente = $('#txtOtrosNumeros').val() == '' ? null : $('#txtOtrosNumeros').val();
            json.GEN_nuiPaciente = $('#txtNui').val();
            json.GEN_idIdentificacion = $('#selTipoDocumento').val();
            json.GEN_estadoPaciente = $('#selEstado').val();

            if (idPaciente == 0) {
                $.ajax({
                    type: 'POST',
                    url: `${GetWebApiUrl()}GEN_Paciente`,
                    data: JSON.stringify(json),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (data) {
                        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
                    },
                    error: function (jqXHR, status) {
                        console.log(json.stringify(jqXHR));
                    }
                });
            }
            else {
                $.ajax({
                    type: 'PUT',
                    url: `${GetWebApiUrl()}GEN_Paciente/${idPaciente}`,
                    data: JSON.stringify(json),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function (data) {
                        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
                    },
                    error: function (jqXHR, status) {
                        console.log(json.stringify(jqXHR));
                    }
                });
            }
        }
        e.preventDefault();
    })

    $(document).keypress(function (e) {
        if (e.which == '13') {
            e.preventDefault();
        }
    });

    $('#btnVolver').click(function (e) {
        window.location.replace(`${ObtenerHost()}/FormPrincipales/frm_principal.aspx`);
        e.preventDefault();
    });
});

function cargarPaciente() {
    if ($('#txtRut').val().length > 6 && (/^[0-9]*$/.test($('#txtRut').val()) || $('#selTipoDocumento').val() == '2' || $('#selTipoDocumento').val() == '3')) {
        if ($('#selTipoDocumento').val() == '2' || $('#selTipoDocumento').val() == '3')
            $('#txtDigito').html('-');
        else
            $('#txtDigito').html(DigitoVerificador($('#txtRut').val()));

        $.ajax({
            type: 'GET',
            url: `${GetWebApiUrl()}GEN_Paciente/BuscarporDocumento/${$('#selTipoDocumento').val()}/${$('#txtRut').val()}`,
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (!$.isEmptyObject(data[0])) {
                    idPaciente = data[0].GEN_idPaciente;
                    $('#txtNui').val(data[0].GEN_nuiPaciente);
                    $('#selEstado').val(data[0].GEN_estadoPaciente);
                    $('#txtNombre').val(data[0].GEN_nombrePaciente);
                    $('#txtApellidoP').val(data[0].GEN_ape_paternoPaciente);
                    $('#txtApellidoM').val(data[0].GEN_ape_maternoPaciente);
                    $('#txtDireccion').val(data[0].GEN_dir_callePaciente);
                    $('#txtNumero').val(data[0].GEN_dir_numeroPaciente);
                    $('#txtTel').val(data[0].GEN_telefonoPaciente);
                    $('#txtOtrosNumeros').val(data[0].GEN_otros_fonosPaciente);
                    $('#selSexo').val(data[0].GEN_idSexo);
                    $('#txtNacimiento').val(moment(data[0].GEN_fec_nacimientoPaciente).format('YYYY-MM-DD'));
                    $('#txtNacimiento').change();


                    if (!$.isEmptyObject(data[0].GEN_idPais)) {
                        $('#selPais').val(data[0].GEN_idPais);
                        comboRegion(data[0].GEN_idPais);

                        if (!$.isEmptyObject(data[0].GEN_idRegion)) {
                            $('#selRegion').val(data[0].GEN_idRegion);
                            comboProvincia(data[0].GEN_idRegion);

                            if (!$.isEmptyObject(data[0].GEN_idProvincia)) {
                                $('#selProvincia').val(data[0].GEN_idProvincia);
                                comboCiudad(data[0].GEN_idProvincia);

                                if (!$.isEmptyObject(data[0].GEN_idCiudad))
                                    $('#selCiudad').val(data[0].GEN_idCiudad);
                                else
                                    $('#selCiudad').val(0);
                            }
                            else
                                $('#selProvincia').val(0);
                        }
                        else
                            $('#selRegion').val(0);
                    }
                    else
                        $('#selPais').val(0);

                    toastr.success('Se ha cargado un paciente existente');

                    habilitarCampos();
                }
                else {
                    idPaciente = 0;

                    $('#txtNui').val('');
                    $('#txtNombre').val('');
                    $('#txtApellidoP').val('');
                    $('#txtApellidoM').val('');
                    $('#txtDireccion').val('Sin Información');
                    $('#txtNumero').val('0');
                    $('#txtTel').val('0');
                    $('#txtOtrosNumeros').val('');
                    $('#selSexo').val(1);
                    $('#txtNacimiento').val(moment().format('YYYY-MM-DD'));


                    toastr.success('Se está creando un paciente nuevo');

                    habilitarCampos();
                    $('#selEstado').val('Activo');
                    $('#selPais').val('1').change();
                    $('#selRegion').val(14).change();
                    $('#selProvincia').val(45).change();
                    $('#selCiudad').val(284);
                    $('#txtNacimiento').change();
                }
            }
        });
    }
    else
        $('#txtDigito').html('-');
}

function habilitarCampos() {
    $('#txtNombre').removeAttr('disabled');
    $('#txtApellidoP').removeAttr('disabled');
    $('#txtApellidoM').removeAttr('disabled');
    $('#txtDireccion').removeAttr('disabled');
    $('#txtNumero').removeAttr('disabled');
    $('#txtTel').removeAttr('disabled');
    $('#txtOtrosNumeros').removeAttr('disabled');
    $('#selSexo').removeAttr('disabled');
    $('#txtNacimiento').removeAttr('disabled');
    $('#selPais').removeAttr('disabled');
    $('#selRegion').removeAttr('disabled');
    $('#selProvincia').removeAttr('disabled');
    $('#selCiudad').removeAttr('disabled');

    $('#btnGuardar').removeAttr('disabled');
}

function inhabilitarCampos() {
    $('#selEstado').attr('disabled', true);
    $('#txtNombre').attr('disabled', true);
    $('#txtApellidoP').attr('disabled', true);
    $('#txtApellidoM').attr('disabled', true);
    $('#txtDireccion').attr('disabled', true);
    $('#txtNumero').attr('disabled', true);
    $('#txtTel').attr('disabled', true);
    $('#txtOtrosNumeros').attr('disabled', true);
    $('#selSexo').attr('disabled', true);
    $('#txtNacimiento').attr('disabled', true);
    $('#selPais').attr('disabled', true);
    $('#selRegion').attr('disabled', true);
    $('#selProvincia').attr('disabled', true);
    $('#selCiudad').attr('disabled', true);

    $('#btnGuardar').attr('disabled', true);
}

function comboPaciente() {
    $('#selTipoDocumento').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Identificacion/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (key, val) {
                $('#selTipoDocumento').append("<option value='" + val.GEN_idIdentificacion + "'>" + val.GEN_nombreIdentificacion + "</option>");
            });
        }
    });
}

function comboSexo() {
    let url = `${GetWebApiUrl()}GEN_Sexo/Combo`;
    setCargarDataEnComboAsync(url, false, $('#selSexo'));
}

function comboPais() {
    $('#selPais').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Pais/Combo`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selPais').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selPais').append("<option value='" + val.GEN_idPais + "'>" + val.GEN_nombrePais + "</option>");
            });
        }
    });
}

function comboRegion(idPais, idSel) {
    $('#selRegion').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Region/GEN_idPais/${idPais}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selRegion').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selRegion').append("<option value='" + val.GEN_idRegion + "'>" + val.GEN_nombreRegion + "</option>");
            });
            $('#selRegion').val(idSel);
        }
    });
}

function comboProvincia(idRegion, idSel) {
    $('#selProvincia').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Provincia/GEN_idRegion/${idRegion}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selProvincia').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selProvincia').append("<option value='" + val.GEN_idProvincia + "'>" + val.GEN_nombreProvincia + "</option>");
            });
            $('#selProvincia').val(idSel);
        }
    });
}

function comboCiudad(idProvincia, idSel) {
    $('#selCiudad').empty();
    $.ajax({
        type: 'GET',
        url: `${GetWebApiUrl()}GEN_Ciudad/GEN_idProvincia/${idProvincia}`,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            $('#selCiudad').append("<option value='0'>-Seleccione-</option>");
            $.each(data, function (key, val) {
                $('#selCiudad').append("<option value='" + val.GEN_idCiudad + "'>" + val.GEN_nombreCiudad + "</option>");
            });
            $('#selCiudad').val(idSel);
        }
    });
}