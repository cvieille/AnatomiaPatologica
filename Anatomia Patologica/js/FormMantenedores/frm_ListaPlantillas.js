﻿$(document).ready(function () {

    grillaPlantillas();

    $('#btnNuevoRegistro').click(function (e) {
        let url = 'frm_plantilla.aspx?id=0';
        location.href = url;
        e.preventDefault();
    });    
});

function lnkInactivarPlantilla(e)
{
    let id = $(e).data("id");
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Plantillas/Anular/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha inactivado la plantilla');
            grillaPlantillas();
        }
    });
}

function lnkActivarPlantilla(e) {
    let id = $(e).data("id");
    //como se vaya a llamar el controlador
    $.ajax({
        type: 'PUT',
        url: `${GetWebApiUrl()}ANA_Plantillas/Activar/${id}`,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            toastr.success('Se ha activado la plantilla');
            grillaPlantillas();
        }
    });
}

function lnkVerPlantilla(e)
{
    let id = $(e).data("id");
    let url = `frm_plantilla.aspx?id=${id}`;
    location.href = url;
}

function grillaPlantillas()
{
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + '/ANA_Plantillas',
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            let adataset = [];
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idPlantilla,
                    val.ANA_nombreplantilla,
                    moment(val.ANA_fec_creacionPlantilla).format('DD/MM/YYYY hh:mm:ss'),                    
                    val.ANA_estadoPlantillas,
                    '<input type="button" value="Ver" id="btnVerPlantilla" class="btn btn-info" data-id="' + val.ANA_idPlantilla + '" onclick="lnkVerPlantilla(this)" />',
                    ''
                ]);
            });

            $('#tblPlantillas').DataTable({
                data: adataset,
                order: [],
                stateSave: true,
                iStateDuration: 60,
                columnDefs: [
                    { targets: 2, sType: 'date-ukLong' },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones;
                            if (adataset[fila][3] == 'Activo') 
                                botones = '<input type="button" value="Inactivar" class="btn btn-danger" data-id="' + adataset[fila][0] + '" onclick="lnkInactivarPlantilla(this)" />';
                            else
                                botones = '<input type="button" value="Activar" class="btn btn-success" data-id="' + adataset[fila][0] + '" onclick="lnkActivarPlantilla(this)" />'
                            return botones;
                        }
                    },
                ],
                columns: [
                    { title: 'N° plantilla' },
                    { title: 'Nombre' },
                    { title: 'Fecha de creación' },                    
                    { title: 'Estado' },
                    { title: '' },
                    { title: '' }
                ],
                bDestroy: true
            });
        }
    });
}