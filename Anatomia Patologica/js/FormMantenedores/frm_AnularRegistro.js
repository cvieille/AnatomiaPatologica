﻿var idBiopsia;
var idUsuario;
$(document).ready(function () {
    var vSession = getSession();
    idUsuario = vSession.id_usuario;
    
    $('body').on('change', 'input:text._bordeError', function () {
        $(this).removeClass('_bordeError');
    });
    $('body').on('change', 'textarea._bordeError', function () {
        $(this).removeClass('_bordeError');
    });

    $('#btnBuscarBiopsia').click(function (e) {
        
        if (($('#txtNBiopsia').val() != '' && $('#txtNBiopsia').val() != null) && ($('#txtAñoBiopsia').val() != '' && $('#txtAñoBiopsia').val() != null)) {
            
            tabladeSeleccion();            
            idBiopsia = 0;
            $('#txtPaciente').val('');
            $('#txtMotivo').attr('disabled', true);
            $('#txtMotivo').val('');
            $('#btnAnular').attr('disabled', true);

            $('#txtNBiopsia').removeClass('_bordeError');
            $('#txtAñoBiopsia').removeClass('_bordeError');
        }
        else {
            resaltaElemento($('#txtNBiopsia'));
            resaltaElemento($('#txtAñoBiopsia'));
        }
        e.preventDefault();
    });

    $('#btnAnular').click(function (e) {

        if ($('#txtMotivo').val() == '' || $('#txtMotivo').val() == null)
            resaltaElemento($('#txtMotivo'));
        else
        {
            let json = {};
            json.ANA_motivoAnulaBiopsia = $('#txtMotivo').val();
            $.ajax({                
                type: 'PUT',
                url: `${GetWebApiUrl()}ANA_Registro_Biopsias/Anular/${idBiopsia}`,
                data: JSON.stringify(json),                
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                success: function (data) {                    
                    toastr.success('Se ha anulado la biopsia seleccionada');

                    var t = $('#tblBiopsia').DataTable();
                    t.clear();
                    t.draw(false);

                    idBiopsia = 0;
                    $('#txtPaciente').val('');
                    $('#txtMotivo').attr('disabled', true);
                    $('#txtMotivo').val('');
                    $('#btnAnular').attr('disabled', true);

                    $('#txtNBiopsia').val('')
                    $('#txtAñoBiopsia').val('')
                },
                error: function (jqXHR, status) {
                    console.log(JSON.stringify(jqXHR));
                }
            });
        }
        e.preventDefault();
    });
});

function seleccionar(id, iestado, n) {
    idBiopsia = id;
    $('#txtPaciente').val(n);
    $('#txtMotivo').removeAttr('disabled');
    $('#btnAnular').removeAttr('disabled');
}

function tabladeSeleccion() {
    $.ajax({
        type: 'GET',
        url: GetWebApiUrl() + 'ANA_Registro_Biopsias/Numero/' + $('#txtNBiopsia').val() + '/' + $('#txtAñoBiopsia').val(),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (data) {
            let adataset = [];
            //console.log(data);
            $.each(data, function (key, val) {
                adataset.push([
                    val.ANA_idBiopsia,
                    val.GEN_idTipo_Estados_Sistemas,
                    val.ANA_numeroRegistro_Biopsias + '-' + val.ANA_añoRegistro_Biopsias,
                    val.GEN_Tipo_Estados_Sistemas.GEN_nombreTipo_Estados_Sistemas,
                    val.GEN_Paciente.GEN_nombrePaciente + ' ' + val.GEN_Paciente.GEN_ape_paternoPaciente + ' ' + val.GEN_Paciente.GEN_ape_maternoPaciente,
                    ''
                ]);
            });

            if (data.length == 0) 
                $('#alertEstado').hide();
            else
                $('#alertEstado').show();
            $('#tblBiopsia').DataTable({
                data: adataset,
                order: [],
                columnDefs:
                [
                    {   targets: 1, visible: false, searchable: false   },
                    {
                        targets: -1,
                        data: null,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let fila = meta.row;
                            var botones;
                            if (adataset[fila][1] == 42)
                                botones = `<a onclick="seleccionar(${adataset[fila][0]},${adataset[fila][1]},'${adataset[fila][4]}')" class="btn btn-success">Seleccionar</a>`
                            else 
                                botones = `<a class="btn btn-success disabled">Seleccionar</a>`
                            return botones;
                        }
                    }
                ],
                columns: [
                    { title: 'ID' },
                    { title: 'idEstado' },
                    { title: 'N° biopsia' },
                    { title: 'Estado' },
                    { title: 'Paciente' },
                    { title: '' }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (aData[1] != '42') //si es caso GES, pinta de color la fila
                        $('td', nRow).css('background-color', 'rgb(240, 240, 240)');
                },
                bDestroy: true
            });
        }
    });
}