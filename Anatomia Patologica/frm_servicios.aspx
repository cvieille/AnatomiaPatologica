﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_servicios.aspx.vb" Inherits="Anatomia_Patologica.WebForm15" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <h3>Registro de Servicios:</h3>
    <table border="0" align="center">
        <tr>
            <td>Id:</td>
            <td>
            <asp:TextBox ID="txt_idServicio" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Origen:</td>
            <td>
                <asp:DropDownList ID="cmb_origen" runat="server" Width="304px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>
            <asp:TextBox ID="txt_nombre" runat="server" Width="305px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Clasificación:</td>
            <td>
                <asp:DropDownList ID="cmb_clasificacion" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Cód. Winsig:</td>
            <td>
                <asp:TextBox ID="txt_cod_winsig" runat="server" Width="305px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Estado:</td>
            <td>
                <asp:DropDownList ID="cmb_estado" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" CssClass="BtnA-Grande" />
            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" CssClass="BtnR-Grande" />
                <br />
            <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False">
            </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
