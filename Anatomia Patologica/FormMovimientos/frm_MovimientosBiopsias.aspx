<%@ Page Title="Movimientos Biopsia" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_MovimientosBiopsias.aspx.vb" Inherits="Anatomia_Patologica.frm_MovimientosBiopsias" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormMovimientos/frm_MovimientosBiopsias.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Biopsias para despachar</h2>
        </div>
        <div class="panel-body">
            <table id="tblBiopsiasDespachar" class="table table-bordered table-hover table-responsive"></table>          
            <div class="row alert alert-info">
                <div class="col-md-4">
                    <label>Entregar informe a:</label>
                    <input type="text" id="txtDesEntregar" class="form-control"/>
                </div>
                <div class="col-md-2">
                    <label>Fecha de entrega:</label>
                        <input type="date" id="txtDesFechaEntrega" required class="form-control"/>
                </div>
                <div class="col-md-3" style="margin-top:5px;">
                    <br />
                    <button id="btnDesDespachar" data-name="informe" class="btn btn-success">Despachar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Biopsias para interconsulta</h2>
        </div>
        <div class="panel-body">
            <table id="tblBiopsiasInterconsulta" class="table table-bordered table-hover table-responsive"></table>          
            <div class="row alert alert-info">
                <div class="col-md-4">
                    <label>Entregar informe a:</label>
                    <input type="text" id="txtIntEntregar" class="form-control"/>
                </div>
                <div class="col-md-2">
                    <label>Fecha de entrega:</label>
                        <input type="date" id="txtIntFechaEntrega" required class="form-control"/>
                </div>
                <div class="col-md-3" style="margin-top:5px;">
                    <br />
                    <button id="btnIntDespachar" data-name="biopsia" class="btn btn-success">Despachar</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="mdlConfirmacion" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header _header-warning">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="display: contents;">
                        &nbsp<label id="lblConfirmacion"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="btnConfirmarModal" class="btn btn-success">Confirmar</a>
                    <button class="btn btn-warning" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>    
</asp:Content>