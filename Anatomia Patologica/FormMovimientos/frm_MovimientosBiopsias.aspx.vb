﻿Imports System.Web.Services
Imports Newtonsoft.Json

Partial Public Class frm_MovimientosBiopsias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
    End Sub

    <WebMethod>
    Public Shared Function updatesCasosValidados()
        ejecuta_sql("update ANA_Registro_Biopsias 
	                set GEN_idTipo_Estados_Sistemas=45
	                where ANA_EstadoRegistro_Biopsias='Validado y Autorizado para Publicar'")

        ejecuta_sql("	update ANA_Registro_Biopsias 
	                set ANA_despachadaBiopsia = 'NO'
	                WHERE ANA_despachadaBiopsia is null or ANA_despachadaBiopsia = ''")

        Return True
    End Function
    <WebMethod>
    Public Shared Function despacharInterconsulta(s, idUsuario, sFecha, sUsuario)
        Dim l = JsonConvert.DeserializeObject(Of Integer())(s)
        For i = 0 To l.Count() - 1
            ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
            '==========================================================
            Dim rm As New Registro_Biopsia
            rm.Registro_Biopsia(l(i))
            rm.Set_ANA_fecDespBiopsia(CDate(sFecha).ToString("yyyy-MM-dd"))
            'rm.Set_Estado("Despachado a Interconsulta")
            rm.Set_ANA_NomRecibeBiopsia(sUsuario)
            rm.Set_GEN_IdUsuarioDespacha(idUsuario)
            rm.Set_ANA_InterconsultaBiopsia("SI")
            'rm.Set_ModificaRegistroBiopsia()

            '*** CREA MOVIMIENTO REALIZADO
            '==========================================================
            Dim mov As New movimientos
            mov.Set_GEN_IdUsuarios(idUsuario)
            mov.Set_ANA_IdBiopsia(l(i))
            mov.Set_GEN_idTipo_Movimientos_Sistemas(105)
            mov.Set_ANA_DetalleMovimiento("Se despacha muestras a: " & sUsuario & ", con fecha: " & CDate(sFecha).ToString("yyyy-MM-dd"))
            mov.Set_CrearNuevoMoviento()

            Dim c As New Cortes
            c.Set_ANA_IdBiopsia(l(i))
            c.Set_ANA_FecInterCortes_Muestras(CDate(sFecha).ToString("yyyy-MM-dd"))
            c.Set_GEN_idTipo_Estados_Sistemas(57)
            c.Set_GEN_IdUsuarioDespCortes_Muestras(idUsuario)
            c.Set_ANA_InterconsCortes_Muestras(sUsuario)
            c.Set_Update_Cortes_Inter()

            Dim lam As New Laminas
            lam.Set_ANA_FecInterLamina(CDate(sFecha).ToString("yyyy-MM-dd"))
            lam.Set_ANA_IdDespLamina(idUsuario)
            lam.Set_Interconsultista(sUsuario)
            lam.Set_ANA_IdBiopsia(l(i))
            lam.Set_GEN_idTipo_Estados_Sistemas(57)
            lam.Set_Update_Laminas_Inter()
        Next
        Return True
    End Function
End Class