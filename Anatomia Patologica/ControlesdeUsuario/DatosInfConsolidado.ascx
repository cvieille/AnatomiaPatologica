﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatosInfConsolidado.ascx.vb" Inherits="Anatomia_Patologica.DatosInfConsolidado" %>

<%--<link rel="Stylesheet" href="../seriousface/style.css" />--%>
<div class="DivFullCentrado">
    <asp:Label ID="LabelTITULO" runat="server" Text="Informe Consolidado" Font-Size="XX-Large"></asp:Label>
    <br />
    <br />
    <table style="margin: 0 auto; border-style: ridge; width: 40%; background-color: White;">
        <tr>
            <td style="background-color: #507CD1" align="center">
                <asp:Label ID="Label3" runat="server" Text="Desde:" ForeColor="White">
                </asp:Label>

            </td>
            <td style="background-color: #507CD1" align="center">
                <asp:Label ID="Label4" runat="server" Text="Hasta:" ForeColor="White">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:TextBox ID="txt_fec_inicio" runat="server" type="date" required="required">
                </asp:TextBox>

                <br />
            </td>
            <td align="center">
                <asp:TextBox ID="txt_fec_final" runat="server" type="date" required="required">
                </asp:TextBox>

                <br />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="btn btn-success" />
    <asp:Button ID="Btn_Excel" runat="server" Text="Exportar a Excel" CssClass="btn btn-success" />

    <p>
        <asp:Label ID="Lbl_error" runat="server" Text="Revise el rango de fechas a consultar ....."
            CssClass="alert alert-danger" Visible="False"></asp:Label>
    </p>

</div>
<br />
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                Espere un momento .....
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Label ID="LabelMensaje" runat="server" Text="No hay Biopsias en este período" ForeColor="Red" Visible="false"></asp:Label>
        <asp:GridView ID="gdv_Totales" runat="server" AutoGenerateColumns="False"
            CssClass="table table-bordered table-hover table-responsive"
            DataSourceID="dts_ConsolidadoGeneral">
            <RowStyle BorderStyle="None" />
            <Columns>
                <asp:BoundField DataField="GEN_TipoPaciente" HeaderText="Tipo Paciente"
                    SortExpression="GEN_TipoPaciente" />
                <asp:BoundField DataField="001" HeaderText="08-01-001" ReadOnly="True" SortExpression="001" />
                <asp:BoundField DataField="002" HeaderText="08-01-002" ReadOnly="True" SortExpression="002" />
                <asp:BoundField DataField="003" HeaderText="08-01-003" ReadOnly="True" SortExpression="003" />
                <asp:BoundField DataField="004" HeaderText="08-01-004" ReadOnly="True" SortExpression="004" />
                <asp:BoundField DataField="005" HeaderText="08-01-005" ReadOnly="True" SortExpression="005" />
                <asp:BoundField DataField="006" HeaderText="08-01-006" ReadOnly="True" SortExpression="006" />
                <asp:BoundField DataField="007" HeaderText="08-01-007" ReadOnly="True" SortExpression="007" />
                <asp:BoundField DataField="008" HeaderText="08-01-008" ReadOnly="True" SortExpression="008" />
            </Columns>
            <HeaderStyle Font-Size="Small" />
        </asp:GridView>
        <asp:SqlDataSource ID="dts_ConsolidadoGeneral" runat="server" ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
        <br />
        <asp:GridView ID="gdv_Detalle" runat="server" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="dts_BiopsiaConsolidado" ForeColor="#333333">
            <RowStyle BackColor="#EFF3FB" BorderStyle="None" />
            <Columns>
                <asp:BoundField DataField="n_biopsia" HeaderText="Nº Biopsia" SortExpression="n_biopsia" ReadOnly="True" />
                <asp:BoundField DataField="GEN_TipoPaciente" HeaderText="Tipo Paciente" SortExpression="GEN_TipoPaciente" />
                <asp:BoundField DataField="ANA_Cod001Codificacion_Biopsia" HeaderText="08-01-001" SortExpression="ANA_Cod001Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod002Codificacion_Biopsia" HeaderText="08-01-002" SortExpression="ANA_Cod002Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod003Codificacion_Biopsia" HeaderText="08-01-003" SortExpression="ANA_Cod003Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod004Codificacion_Biopsia" HeaderText="08-01-004" SortExpression="ANA_Cod004Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod005Codificacion_Biopsia" HeaderText="08-01-005" SortExpression="ANA_Cod005Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod006Codificacion_Biopsia" HeaderText="08-01-006" SortExpression="ANA_Cod006Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod007Codificacion_Biopsia" HeaderText="08-01-007" SortExpression="ANA_Cod007Codificacion_Biopsia" />
                <asp:BoundField DataField="ANA_Cod008Codificacion_Biopsia" HeaderText="08-01-008" SortExpression="ANA_Cod008Codificacion_Biopsia" />
                <asp:BoundField DataField="cant_cortes" HeaderText="Cortes" SortExpression="cant_cortes" />
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="dts_BiopsiaConsolidado" runat="server"
            ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="Btn_Buscar" />
    </Triggers>
</asp:UpdatePanel>
