﻿Public Class InmunoHistoquimicas
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("GEN_CodigoPerfil") = 3 Then
            'SI ES PATOLOGO OCULTA COLUMNA DE BOTONES DE ACCION.
            OcultarColumna(gdv_inmunoHistoquimica, False, 8)
        End If
        OcultarColumna(gdv_inmunoHistoquimica, False, 6)
        OcultarColumna(gdv_inmunoHistoquimica, False, 7)
    End Sub
    Protected Sub cmd_EntregarInmuno_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim ANA_IdInmunoBiopsia As Integer = TryCast(sender, Button).CommandArgument

        Dim nih As New NegocioInmuno
        nih.InmunoHistoquimicaBiopsia(ANA_IdInmunoBiopsia)
        Dim estadoInmuno As Integer = nih.Get_GEN_idTipo_Estados_Sistemas()


        Dim negcortes As New NegocioCortes
        negcortes.Cortes(nih.Get_ANA_IdCortes_Muestras())
        Dim estadoCorte As Integer = negcortes.Get_GEN_idTipo_Estados_Sistemas()

        If estadoCorte = 60 Then
            'CREA MOVIMIENTO REALIZADO
            '====================================================================================================
            Dim mov As New movimientos

            mov.Set_GEN_IdUsuarios(Session("id_usuario"))
            mov.Set_ANA_IdBiopsia(nih.Get_ANA_IdBiopsia())
            mov.Set_GEN_idTipo_Movimientos_Sistemas(201)
            mov.Set_ANA_DetalleMovimiento("Envia a Inmunohistoquímica " & nih.Get_ANA_NombreInmuno() & " de corte: " & negcortes.Get_ANA_NomCortes_Muestras() & " a Patologo")
            mov.Set_CrearNuevoMoviento()

            nih.Set_EntregarInmunoyCreaLamina(Session("id_usuario"), nih.Get_ANA_IdInmunoBiopsia)
            wuc_alert.showModal(Alert.ACCION_REALIZADA, "¡Se ha entregado esta Inmuno!")
        Else
            wuc_alert.showModal(Alert.INFORMACION, "¡Debe solicitar el corte para poder realizar esta técnica!")
        End If

        Dim theDiv As HtmlGenericControl = New HtmlGenericControl("InmunoHistoquimicas")
        theDiv.Attributes.Add("class", "tab-pane fade in active")

    End Sub

    Protected Sub cmd_exportarInmunoHistoquimica_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_exportarInmunoHistoquimica.Click
        gdv_inmunoHistoquimica.Columns(6).Visible = False
        gdv_inmunoHistoquimica.Columns(9).Visible = False
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        Me.gdv_inmunoHistoquimica.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.gdv_inmunoHistoquimica)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Inmunohistoquímicas Pendientes</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=InmunohistoquímicasPendientes.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub

    Protected Sub gdv_inmunoHistoquimica_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If gdv_inmunoHistoquimica.Rows.Count > 0 Then
                pnl_Inmuno.Visible = True
            End If

            CType(Me.Parent.FindControl("lbl_totalInmuno"), Label).Text = gdv_inmunoHistoquimica.Rows.Count
            If gdv_inmunoHistoquimica.Rows.Count > 0 Then
                CType(Me.Parent.FindControl("lbl_totalInmuno"), Label).BackColor = System.Drawing.ColorTranslator.FromHtml("#5cb85c")
            End If
            If e.Row.Cells(6).Text = "SI" Then
                'si el casete esta solicitado, se bloquea boton.
                CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Enabled = False
                CType(e.Row.FindControl("cmd_EntregarInmuno"), Button).Visible = False
            ElseIf e.Row.Cells(6).Text = "NO" Or e.Row.Cells(6).Text = "" Then
                If e.Row.Cells(7).Text = "60" Then
                    'inmuno solicitada y casete en tincion y montaje
                    CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Visible = False
                Else
                    CType(e.Row.FindControl("cmd_EntregarInmuno"), Button).Visible = False
                End If
                'CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Enabled = False
                'CType(e.Row.FindControl("cmd_EntregarInmuno"), Button).Visible = False
            End If
        Catch ex As Exception
            wuc_alert.showModal(Alert._ERROR, ex.Message)
        End Try

    End Sub

    Protected Sub gdv_inmunoHistoquimica_PreRender(sender As Object, e As EventArgs)
        If gdv_inmunoHistoquimica.Rows.Count > 0 Then
            If (Not IsNothing(gdv_inmunoHistoquimica.HeaderRow)) Then
                gdv_inmunoHistoquimica.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If (Not IsNothing(gdv_inmunoHistoquimica.FooterRow)) Then
                gdv_inmunoHistoquimica.FooterRow.TableSection = TableRowSection.TableFooter
            End If
        End If
    End Sub

    Protected Sub cmd_SolicitarCasete_Click(sender As Object, e As EventArgs)
        '*** INSTANCIA CLASE DE NEGOCIOS DE CORTES
        '==========================================================
        Dim ANA_IdInmunoBiopsia As Integer = TryCast(sender, Button).CommandArgument
        Dim nih As New NegocioInmuno
        nih.InmunoHistoquimicaBiopsia(ANA_IdInmunoBiopsia)


        Dim Nc As New NegocioCortes
        Nc.Set_ANA_IdBiopsia(nih.Get_ANA_IdBiopsia())
        Nc.Set_ANA_IdCortes_Muestras(nih.Get_ANA_IdCortes_Muestras())
        Nc.Set_GEN_IdUsuarios(Session("id_usuario"))

        wuc_alert.showModal(Alert.INFORMACION, Nc.Set_SolicitaCaseteAlmacenado(Session("GEN_CodigoPerfil")))

    End Sub
    Protected Sub cmd_VerCaso_Click(sender As Object, e As EventArgs)
        Dim idcaso = CType(sender, Button).CommandArgument
        Response.Redirect("../FormAnatomia/frm_Cortes.aspx?id=" & idcaso)
    End Sub
End Class