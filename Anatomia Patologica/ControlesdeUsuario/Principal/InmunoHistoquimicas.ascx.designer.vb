﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class InmunoHistoquimicas
    
    '''<summary>
    '''Control pnl_Inmuno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnl_Inmuno As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control cmd_exportarInmunoHistoquimica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_exportarInmunoHistoquimica As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control wuc_modal_cargando.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents wuc_modal_cargando As Global.Anatomia_Patologica.Modal_Cargando
    
    '''<summary>
    '''Control gdv_inmunoHistoquimica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gdv_inmunoHistoquimica As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control wuc_alert.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents wuc_alert As Global.Anatomia_Patologica.Alert
End Class
