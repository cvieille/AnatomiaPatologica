﻿Public Class TecnicasPendientes
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler wuc_alert.cmd_alertaAceptar.Click, AddressOf cmd_alertaAceptar_Click
    End Sub

    Protected Sub cmd_EntregarTecnica_Click(ByVal sender As Object, ByVal e As EventArgs)
        ViewState("ANA_IdTecnica") = CType(sender, Button).CommandArgument
        wuc_alert.showModal(Alert.CONFIRMACION, "¿Estás seguro que quieres entregar esta lámina?")
    End Sub


    Protected Sub cmd_alertaAceptar_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim ANA_IdTecnica As Integer = CInt(ViewState("ANA_IdTecnica"))
        Dim nt As New NegocioTecnica
        nt.TecnicaBiopsia(ANA_IdTecnica)
        Dim nc As New NegocioCortes
        nc.Cortes(nt.Get_ANA_IdCortes_Muestras)
        If nt.Get_ANA_EstadoTecnica = Trim("Solicitada") And nc.Get_GEN_idTipo_Estados_Sistemas() = 60 Then
            'SI EL TACO ESTA EN TINCION Y LA TECNICA SOLICITADA.

            ' CREA MOVIMIENTO REALIZADO
            '====================================================================================================
            Dim mov As New movimientos

            mov.Set_GEN_IdUsuarios(Session("id_usuario"))
            mov.Set_ANA_IdBiopsia(nt.Get_ANA_IdBiopsia())
            mov.Set_GEN_idTipo_Movimientos_Sistemas(90)
            mov.Set_ANA_DetalleMovimiento("Envia a Técnica Especial " & nt.Get_ANA_NombreTecnica & " de corte: " & nc.Get_ANA_NomCortes_Muestras() & " a Patologo")
            mov.Set_CrearNuevoMoviento()

            nt.Set_EntregarTecnicayCreaLamina(Session("id_usuario"), nt.Get_Ana_Idtecnicabiopsia)

            CType(Me.Parent.Page, frm_principal).IniciaTecnicasPendientes()
            wuc_alert.showModal(Alert.ACCION_REALIZADA, "Lámina entregada correctamente.")
        Else
            wuc_alert.showModal(Alert.ADVERTENCIA, "Debe solicitar el corte para poder realizar esta técnica.")
        End If

        ViewState.Remove("ANA_IdTecnica")

    End Sub

    Protected Sub cmd_ExcelTecnica_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_ExcelTecnica.Click

        gdv_tecnicas.Columns(6).Visible = False
        gdv_tecnicas.Columns(10).Visible = False
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        Me.gdv_tecnicas.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.gdv_tecnicas)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.Write("<h1>Técnicas Especiales Pendientes</h1>")
        Response.Write(Date.Today)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=TécnicasEspecialesPendientes.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()

    End Sub

    Protected Sub gdv_tecnicas_DataBound(sender As Object, e As GridViewRowEventArgs)
        OcultaColumnasGrillaTecnicas()
        CType(Me.Parent.FindControl("lbl_totalTecnica"), Label).Text = gdv_tecnicas.Rows.Count
        If gdv_tecnicas.Rows.Count > 0 Then
            CType(Me.Parent.FindControl("lbl_totalTecnica"), Label).BackColor = System.Drawing.ColorTranslator.FromHtml("#5cb85c")
        End If

        If e.Row.Cells(6).Text = "SI" Then
            'si ya fue solicitado el taco bloquea boton
            CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Enabled = False
            CType(e.Row.FindControl("cmd_EntregarTecnica"), Button).Visible = False
        ElseIf e.Row.Cells(6).Text = "NO" Or e.Row.Cells(6).Text = " " Then
            'no esta solicitado el taco.
            If e.Row.Cells(7).Text = "60" Then
                'tecnica solicitada y casete en tincion y montaje
                'puede entregar
                CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Visible = False
            Else
                If e.Row.Cells(8).Text = "NO" Then
                    'si esta almacenada puede solicitar
                    CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Enabled = False
                End If
                CType(e.Row.FindControl("cmd_EntregarTecnica"), Button).Visible = False
            End If
            'CType(e.Row.FindControl("cmd_SolicitarCasete"), Button).Enabled = False
            'CType(e.Row.FindControl("cmd_EntregarInmuno"), Button).Visible = False
        End If
    End Sub

    Protected Sub cmd_SolicitarCasete_Click(sender As Object, e As EventArgs)
        '*** INSTANCIA CLASE DE NEGOCIOS DE CORTES
        '==========================================================
        Dim ANA_IdTecnicaBiopsia As Integer = TryCast(sender, Button).CommandArgument
        Dim ntb As New NegocioTecnica
        ntb.TecnicaBiopsia(ANA_IdTecnicaBiopsia)


        Dim Nc As New NegocioCortes
        Nc.Set_ANA_IdBiopsia(ntb.Get_ANA_IdBiopsia())
        Nc.Set_ANA_IdCortes_Muestras(ntb.Get_ANA_IdCortes_Muestras())
        Nc.Set_GEN_IdUsuarios(Session("id_usuario"))

        wuc_alert.showModal(Alert.INFORMACION, Nc.Set_SolicitaCaseteAlmacenado(Session("GEN_CodigoPerfil")))
        CType(Me.Parent.Page, frm_principal).IniciaTecnicasPendientes()
        OcultaColumnasGrillaTecnicas()
    End Sub
    Private Sub OcultaColumnasGrillaTecnicas()
        If Session("GEN_CodigoPerfil") = 3 Then
            'SI ES PATOLOGO OCULTA  DE BOTONES DE ACCION.
            OcultarColumna(gdv_tecnicas, False, 9)
        End If
        OcultarColumna(gdv_tecnicas, False, 6)
        OcultarColumna(gdv_tecnicas, False, 7)
        OcultarColumna(gdv_tecnicas, False, 8)
    End Sub

    Protected Sub cmd_VerCaso_Click(sender As Object, e As EventArgs)
        Dim idcaso = CType(sender, Button).CommandArgument
        Response.Redirect("../FormAnatomia/frm_Cortes.aspx?id=" & idcaso)
    End Sub
End Class