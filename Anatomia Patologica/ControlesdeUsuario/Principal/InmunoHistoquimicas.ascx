﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="InmunoHistoquimicas.ascx.vb" Inherits="Anatomia_Patologica.InmunoHistoquimicas" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>

<!-------------------- Panel Principal de Inmuno-->
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnl_Inmuno" Visible="false">
            <div class="text-right" style="margin-bottom: 10px;">
                <asp:Button ID="cmd_exportarInmunoHistoquimica" runat="server" Text="Exportar" CssClass="btn btn-success" />
            </div>
        </asp:Panel>
        <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="cmd_exportarInmunoHistoquimica" />
    </Triggers>
</asp:UpdatePanel>

<asp:GridView ID="gdv_inmunoHistoquimica" runat="server"
    AutoGenerateColumns="False" DataKeyNames="GEN_loginUsuarios"
    DataSourceID="dts_InmunoHistoquimica"
    CssClass="table table-bordered table-hover table-responsive"
    OnRowDataBound="gdv_inmunoHistoquimica_RowDataBound" OnPreRender="gdv_inmunoHistoquimica_PreRender">
    <Columns>
        <asp:BoundField DataField="ANA_FecInmuno_Histoquimica_Biopsia" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Fecha"
            SortExpression="ANA_FecInmuno_Histoquimica_Biopsia" />
        <asp:BoundField DataField="ANA_NomCortes_Muestras" HeaderText="Nº Corte" SortExpression="Nº Corte" />
        <asp:BoundField DataField="ANA_NomInmunoHistoquimica" HeaderText="Inmunohistoquímicas" SortExpression="ANA_NomInmunoHistoquimica" />
        <asp:BoundField DataField="GEN_loginUsuarios" HeaderText="Sol. por:" ReadOnly="True" SortExpression="GEN_loginUsuarios" />
        <asp:BoundField DataField="ANA_NombreTecnologo" HeaderText="Tecnólogo" SortExpression="ANA_NombreTecnologo" />
        <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado Casete" SortExpression="GEN_nombreTipo_Estados_Sistemas" />
        <asp:BoundField DataField="ANA_SolCortes_Muestras" HeaderText="Solicitado" SortExpression="ANA_SolCortes_Muestras" />
        <asp:BoundField DataField="GEN_idTipo_Estados_Sistemas" HeaderText="Estado Corte" SortExpression="GEN_idTipo_Estados_Sistemas" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="cmd_SolicitarCasete" runat="server" Text="Solicitar Casete" CssClass="btn btn-warning"
                    OnClientClick="return confirm('¿Esta seguro que desea solicitar este casete ?');"
                    CommandArgument='<%# Eval("ANA_IdInmuno_Histoquimica_Biopsia") %>' OnClick="cmd_SolicitarCasete_Click" />
                <asp:Button ID="cmd_EntregarInmuno" runat="server" Text="Entregar" CssClass="btn btn-success"
                    CommandArgument='<%# Eval("ANA_IdInmuno_Histoquimica_Biopsia") %>' OnClick="cmd_EntregarInmuno_Click"
                    OnClientClick="return confirm('¿Esta seguro que desea entregar esta tecnica?');" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="cmd_VerInmuno" runat="server" Text="Ver" CssClass="btn btn-info"
                  CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_VerCaso_Click" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<wuc:Alert ID="wuc_alert" runat="server" />

