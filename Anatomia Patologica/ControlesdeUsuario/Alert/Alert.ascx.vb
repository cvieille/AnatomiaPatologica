﻿Public Class Alert
    Inherits System.Web.UI.UserControl

    Public Shared _ERROR As String = "ERROR"
    Public Shared CONFIRMACION As String = "CONFIRMACIÓN"
    Public Shared ADVERTENCIA As String = "ADVERTENCIA"
    Public Shared ACCION_REALIZADA As String = "ACCIÓN REALIZADA"
    Public Shared INFORMACION As String = "INFORMACIÓN"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#Region "ALERTAS"

    '---------------------------------------------------------------------------------------------------------
    ' DA ESTILOS E IMPRIME EL MENSAJE SEGUN LO QUE SE QUIERA HACER
    '---------------------------------------------------------------------------------------------------------
    Public Sub showModal(ByVal tipo As String, ByVal mensaje As String)

        ' DA LOS ESTILOS SEGÚN SEA EL TIPO
        '===================================================================
        Select Case tipo
            Case "ERROR"
                pnl_alertaEstilo.CssClass = "panel panel-danger"
                pnl_mensaje.CssClass = "alert alert-danger"
                cmd_alertaAceptar.Visible = False
            Case "CONFIRMACIÓN"
                pnl_alertaEstilo.CssClass = "panel panel-warning"
                pnl_mensaje.CssClass = "alert alert-warning"
                cmd_alertaAceptar.Visible = True
            Case "ADVERTENCIA"
                pnl_alertaEstilo.CssClass = "panel panel-warning"
                pnl_mensaje.CssClass = "alert alert-warning"
                cmd_alertaAceptar.Visible = False
            Case "ACCIÓN REALIZADA"
                pnl_alertaEstilo.CssClass = "panel panel-success"
                pnl_mensaje.CssClass = "alert alert-success"
                cmd_alertaAceptar.Visible = False
            Case "INFORMACIÓN"
                pnl_alertaEstilo.CssClass = "panel panel-info"
                pnl_mensaje.CssClass = "alert alert-info"
                cmd_alertaAceptar.Visible = False
        End Select

        ' SE ESPECIFICA EL TÍTULO DEL MODAL
        '====================================================================
        lbl_alerta_titulo.Text = tipo
        ' SE ESPECIFICA EL MENSAJE DEL MODAL
        '=====================================================================
        lbl_alertaMensaje.Text = mensaje
        ' SE VISUALIZA EL MODAL
        '=====================================================================
        modal_alerta.Show()

    End Sub

#End Region

End Class