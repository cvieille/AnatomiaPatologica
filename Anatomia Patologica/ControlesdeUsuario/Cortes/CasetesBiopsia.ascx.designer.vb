﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class casetesbiopsia
    
    '''<summary>
    '''Control gdv_cortes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gdv_cortes As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control cmd_select_all.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_select_all As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_select_all0.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_select_all0 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_enviar_procesador.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_enviar_procesador As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_EliminarCortes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_EliminarCortes As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_especiales.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_especiales As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_inmuno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_inmuno As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_descal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_descal As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_volver_inicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_volver_inicio As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_imprimir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_imprimir As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_solicitar_macro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_solicitar_macro As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_solicitar_casete.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_solicitar_casete As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control cmd_para_interconsulta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_para_interconsulta As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control pnl_tecnica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnl_tecnica As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control cmb_tecnicas_especiales.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_tecnicas_especiales As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cmd_agregar_especiales.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_agregar_especiales As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control pnl_inmuno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnl_inmuno As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control cmb_inmuno_histoquimica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_inmuno_histoquimica As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cmd_agregar_inmuno.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmd_agregar_inmuno As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control lbl_mensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbl_mensaje As Global.System.Web.UI.WebControls.Label
End Class
