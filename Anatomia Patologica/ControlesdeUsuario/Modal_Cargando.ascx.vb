﻿Public Class Modal_Cargando
    Inherits System.Web.UI.UserControl

    Private mostrar As Boolean = False
    Public Property esVisible() As Boolean
        Get
            Return mostrar
        End Get
        Set(ByVal value As Boolean)
            mostrar = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If esVisible Then
            upd_Cargando.Attributes("style") = "display:inline-block;"
        End If
    End Sub

End Class