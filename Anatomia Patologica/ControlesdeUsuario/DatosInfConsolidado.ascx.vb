﻿Public Partial Class DatosInfConsolidado
    Inherits System.Web.UI.UserControl

    '-------------------------------------------------------
    'VARIABLES GLOBALES  
    '-------------------------------------------------------
    Dim tot_tipopac, tot_tipopaci, tot_biopsia, tot_codcinco, tot_codseis, tot_codsiete, tot_codocho As Integer
    Dim Biopsia As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If IsPostBack = False Then
                ' *** DEFINE RANGO DE FECHAS
                '==========================================================
                Me.txt_fec_final.Text = Today.Date
                Me.txt_fec_inicio.Text = Today.Date

                ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
                '==========================================================
                Dim con As New Consultas

                dts_ConsolidadoGeneral.SelectCommand = con.Ds_con_InfConsolidado(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
                gdv_Totales.DataBind()

                dts_BiopsiaConsolidado.SelectCommand = con.Ds_con_InfDetalladoConsolidado(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
                gdv_Detalle.DataBind()

            End If
        Catch ex As Exception
            LabelMensaje.Visible = True
            LabelMensaje.Text = ex.Message
        End Try

    End Sub

    '-------------------------------------------------------
    'FUNCION QUE EXPORTA LOS DATOS A UN EXCEL  
    '-------------------------------------------------------
    Protected Sub cmd_exportar_excel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Excel.Click

        If gdv_Detalle.Rows.Count = 0 Then Exit Sub

        ' *** DEFINE RANGO DE FECHAS 
        '==========================================================
        Dim fec_inicio As String = Me.txt_fec_inicio.Text
        Dim fec_final As String = Me.txt_fec_final.Text

        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As IO.StringWriter = New IO.StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm

        gdv_Detalle.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(gdv_Totales)

        'SE ELIMINA LA COLUMNA DEL HIPERLINK
        If Me.gdv_Detalle.Rows.Count <> 0 Then
            For Each row As GridViewRow In gdv_Detalle.Rows
                row.Cells(1).Text = row.Cells(8).Text
            Next
        End If
        form.Controls.Add(gdv_Detalle)

        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True

        Response.Write("<h1>Informe Consolidado</h1>")
        Response.Write("Desde:" & fec_inicio & " - Hasta:" & fec_final)
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=informeconsolidado-" & fec_inicio & "-" & fec_final & ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()

    End Sub

    ''-------------------------------------------------------
    ''FUNCION QUE CALCULA LOS CODIGOS 08-01-007 Y 08-01-008  
    ''-------------------------------------------------------
    'Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewDetalle.RowDataBound
    '    On Error Resume Next
    '    'EN CASO QUE NO SEA LA PRIMERA FILA <HEADER>
    '    If (e.Row.Cells(5).Text <> "08-01-007") Then

    '        'CONTAR TECNICAS ESPECIALES ES CODIGO 08-01-005 - 
    '        'TECNICAS ESPECIALES MENOS NIVEL Y DESGASTE Y SUMAR UNA TECNICA CUANDO LA BIOPSIA ESTUVO EN ESTADO EN DESCALCIFICACION
    '        Dim neg As New Negocio
    '        Dim suma_tecnica As Integer = neg.Cantidad_Tecnicas(e.Row.Cells(7).Text)
    '        suma_tecnica = suma_tecnica + neg.Cantidad_Descalcificaciones(e.Row.Cells(7).Text)

    '        e.Row.Cells(2).Text = suma_tecnica
    '        If (e.Row.Cells(2).Text = "") Then
    '            e.Row.Cells(2).Text = "0"
    '        End If

    '        'CONTAR RAPIDAS ES CODIGO 08-01-006
    '        e.Row.Cells(3).Text = neg.Cantidad_Rapidas(e.Row.Cells(7).Text)
    '        If (e.Row.Cells(3).Text = "") Then
    '            e.Row.Cells(3).Text = "0"
    '        End If

    '        'CONTAR TACOS
    '        Dim N As Integer = Int(e.Row.Cells(4).Text)
    '        Dim D As Double, U As Integer

    '        If N < 10 Then
    '            'SI EL NUMERO DE CORTES ES MENOR QUE 10 ES CODIGO 08-01-008
    '            e.Row.Cells(5).Text = "0"
    '            e.Row.Cells(6).Text = "1"
    '        Else
    '            'SI EL NUMERO DE CORTES ES UNA DECENA SE CALCULA LOS CODIGOS 08-01-007 Y 08-01-008
    '            D = N / 10
    '            e.Row.Cells(5).Text = Int(D)
    '            U = N Mod 10
    '            If U <> "0" Then
    '                e.Row.Cells(6).Text = "1"
    '            Else
    '                e.Row.Cells(6).Text = "0"
    '            End If
    '        End If

    '        'SE CALCULAN LOS CONTADORES TOTALES
    '        tot_biopsia = tot_biopsia + 1
    '        If e.Row.Cells(0).Text = "Institucional" Then
    '            tot_tipopaci = tot_tipopaci + 1
    '        Else
    '            tot_tipopac = tot_tipopac + 1
    '        End If
    '        If e.Row.Cells(2).Text <> "0" Then
    '            tot_codcinco = tot_codcinco + Int(e.Row.Cells(2).Text)
    '        End If
    '        If e.Row.Cells(3).Text <> "0" Then
    '            tot_codseis = tot_codseis + Int(e.Row.Cells(3).Text)
    '        End If
    '        If e.Row.Cells(5).Text <> "0" Then
    '            tot_codsiete = tot_codsiete + Int(e.Row.Cells(5).Text)
    '        End If
    '        If e.Row.Cells(6).Text <> "0" Then
    '            tot_codocho = tot_codocho + Int(e.Row.Cells(6).Text)
    '        End If

    '        'SE AGREGAN LOS TOTALES AL GRIDVIEWTOTALES
    '        If Biopsia = tot_biopsia Then
    '            Dim dt As New DataTable()

    '            dt.Columns.Add("Total Biopsias", GetType(Integer))
    '            dt.Columns.Add("Total Pac Inst", GetType(Integer))
    '            dt.Columns.Add("Total Pac Priv", GetType(Integer))
    '            dt.Columns.Add("Total 08-01-005", GetType(Integer))
    '            dt.Columns.Add("Total 08-01-006", GetType(Integer))
    '            dt.Columns.Add("Total 08-01-007", GetType(Integer))
    '            dt.Columns.Add("Total 08-01-008", GetType(Integer))

    '            Dim Row1 As DataRow
    '            Row1 = dt.NewRow()

    '            Row1("Total Biopsias") = CStr(tot_biopsia)
    '            Row1("Total Pac Inst") = CStr(tot_tipopaci)
    '            Row1("Total Pac Priv") = CStr(tot_tipopac)
    '            Row1("Total 08-01-005") = CStr(tot_codcinco)
    '            Row1("Total 08-01-006") = CStr(tot_codseis)
    '            Row1("Total 08-01-007") = CStr(tot_codsiete)
    '            Row1("Total 08-01-008") = CStr(tot_codocho)

    '            dt.Rows.Add(Row1)
    '            Me.GridViewTotales.DataSource = dt
    '            Me.GridViewTotales.DataBind()
    '        End If

    '    Else 'ES LA PRIMERA FILA CON EL ENCABEZADO - SE INICIALIZAN LOS CONTADORES TOTALES EN CERO
    '        tot_biopsia = 0
    '        tot_tipopaci = 0
    '        tot_tipopac = 0
    '        tot_codcinco = 0
    '        tot_codseis = 0
    '        tot_codsiete = 0
    '        tot_codocho = 0

    '    End If
    '    e.Row.Cells(7).Visible = False
    '    e.Row.Cells(8).Visible = False
    'End Sub

    '--------------------------------------------------------------------------------
    'FUNCION QUE BUSCA EL TOTAL DE BIOPSIAS EN EL PERIODO BUSCADO
    '--------------------------------------------------------------------------------
    Protected Sub Btn_Buscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Buscar.Click

        If Not (IsDate(Me.txt_fec_inicio.Text) And IsDate(Me.txt_fec_final.Text)) Then
            Lbl_error.Visible = True
            Exit Sub
        Else
            Lbl_error.Visible = False
        End If

        ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
        '==========================================================
        Dim con As New Consultas

        dts_ConsolidadoGeneral.SelectCommand = con.Ds_con_InfConsolidado(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
        gdv_Totales.DataBind()

        dts_BiopsiaConsolidado.SelectCommand = con.Ds_con_InfDetalladoConsolidado(Me.txt_fec_inicio.Text, Me.txt_fec_final.Text)
        gdv_Detalle.DataBind()

    End Sub

    
End Class