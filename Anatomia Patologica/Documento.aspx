﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Documento.aspx.vb" Inherits="Anatomia_Patologica.Documento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p><b>No se pudo encontrar el archivo seleccionado, redirigiendo al login...</b></p>
            <p><b>Haga click <a href="frm_login.aspx">AQUÍ </a>si la página no se redirige automáticamente</b></p>
        </div>
    </form>

    <script src="<%= ResolveClientUrl("~/js/jquery-1.11.3.js") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            setTimeout(function () {
                window.location.href = "frm_login.aspx";
            }, 3000);
            //window.location = "frm_login.aspx";
        });
    </script>
</body>
</html>
