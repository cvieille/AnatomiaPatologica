﻿Partial Public Class frm_recibe_solicitud
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub cmd_seleccionar_todo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo.Click

        For Each row As GridViewRow In GridView1.Rows
            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
            elcheckbox.Checked = True
        Next

    End Sub

    Protected Sub cmd_seleccionar_nada_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_nada.Click

        For Each row As GridViewRow In GridView1.Rows
            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
            elcheckbox.Checked = False
        Next

    End Sub

    Protected Sub cmd_imprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_imprimir.Click

        Dim contador As Integer
        Dim los_id As String

        los_id = ""
        For Each row As GridViewRow In GridView1.Rows
            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
            If elcheckbox.Checked Then
                contador = 1
                Dim tuFila As GridViewRow = row
                Try
                    If los_id = "" Then
                        los_id = row.Cells(1).Text
                    Else
                        los_id = los_id & "," & row.Cells(1).Text
                    End If
                Catch ex As Exception
                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message
                End Try
                contador = contador + 1
            End If
        Next
        Response.Write("<script>window.open('frm_impr_lista_solicitud.aspx?los_id=" & los_id & " ','Titulo','height=300', 'width=300')</script>")

    End Sub

    Protected Sub cmd_recibir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_recibir.Click
       
        Try

            Dim consulta, id_soli As String
            
            For Each row As GridViewRow In GridView1.Rows
                id_soli = row.Cells(1).Text
                Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

                If elcheckbox.Checked Then

                    consulta = "UPDATE solicitud_biopsia SET estado = 'Recepcionada', ANA_fec_RecepcionRegistro_Biopsias = '" & Date.Today & "' "
                    consulta = consulta & "WHERE id_soli = " & id_soli & " "
                    ejecuta_sql(consulta)
                End If
            Next

            Response.Redirect("frm_principal.aspx")

        Catch ex As Exception
            Me.lbl_mensaje.Visible = True
            Me.lbl_mensaje.Text = ex.Message
        End Try

    End Sub

    Protected Sub cmd_ver_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_ver.Click
        Dim id_soli As String
        For Each row As GridViewRow In GridView1.Rows
            id_soli = row.Cells(1).Text
            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
            If elcheckbox.Checked Then
                Response.Redirect("~/FormSolicitudes/frm_recepcion.aspx?id=" & id_soli)
            End If
        Next
    End Sub
End Class