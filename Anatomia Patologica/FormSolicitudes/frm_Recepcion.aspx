﻿<%@ Page Language="vb" Title="Registro de Muestras" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_Recepcion.aspx.vb" Inherits="Anatomia_Patologica.WebForm7" %>

<%@ Register Src="~/ControlesdeUsuario/ModalMovimientos.ascx" TagName="Movimientos" TagPrefix="wuc" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        jQuery.ajax({ url: `${'<%= ResolveClientUrl("~/js/FormSolicitudes/frm_Recepcion.js") %>'}?${new Date().getTime()}` });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Cnt_Principal" runat="server">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="text-center">Registro de muestras</h2>
        </div>
        <div class="panel-body">
            <div style="margin-bottom: 10px;">
                <div class="row" id="divPestañas">
                    <div class="panel with-nav-tabs panel-info">
                        <div class="panel-heading clearfix">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#divTabTecnicasEspeciales" data-toggle="tab">
                                        <label style="color: black">Recepción</label>
                                    </a>
                                </li>
                                <li>
                                    <a href="#divTabMacroscopia" data-toggle="tab">
                                        <label style="color: black">Macroscopia</label>
                                    </a>
                                </li>
                                <li>
                                    <a href="#divTabInterconsultas" data-toggle="tab">
                                        <label style="color: black">Interconsultas&nbsp;<span id="bdgInterconsultas" class="badge">0</span></label>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="divTabTecnicasEspeciales">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Id:</label>
                                                    <input id="txtId" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de registro:</label>
                                                    <input id="txtNRegistro" type="text" class="form-control" disabled />
                                                </div>                                                
                                                <div class="col-md-2">
                                                    <label>Fecha de recepción:</label>
                                                    <input type="date" id="txtRecepcion" required class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>&nbsp;</label>
                                                    <button id="btnMovimientos" class="btn btn-info" disabled="disabled" style="width: 100%;">Ver movimientos</button>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Tipo de documento:</label>
                                                    <select id="selDocumento" class="form-control" disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Nº de documento:</label>
                                                    <div class="input-group">
                                                        <input id="txtRut" type="text" maxlength="8" class="form-control" disabled>
                                                        <span id="txtguion" class="input-group-addon">-</span>
                                                        <input id="txtDigito" type="text" maxlength="1" class="form-control" disabled style="width:40%;" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Nombre:</label>
                                                    <input id="txtNombre" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Apellido paterno:</label>
                                                    <input id="txtApellidoP" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Apellido materno:</label>
                                                    <input id="txtApellidoM" type="text" class="form-control" disabled />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Fecha de nacimiento:</label>
                                                    <input type="date" id="txtNacimiento" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Edad:</label>
                                                    <input id="txtEdad" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Nacionalidad:</label>
                                                    <input id="txtNacionalidad" type="text" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de ubicación interna:</label>
                                                    <input id="txtNUI" type="text" class="form-control" disabled />
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row ">
                                                <div class="col-md-2">
                                                    <label>Tipo de paciente:</label>
                                                    <select id="selPaciente" class="form-control" disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>GES:</label>
                                                    <select id="selGES" class="form-control" disabled>
                                                        <option value="SI">SI</option>
                                                        <option selected value="NO">NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Estado Muestra:</label>
                                                    <input id="txtEstado" type="text" class="form-control" disabled />
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Tipo de muestra:</label>
                                                    <select id="selTipoMuestra" class="form-control" disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Detalle de muestra:</label>
                                                    <select id="selDetalleMuestra" class="form-control" disabled>
                                                        <option value="0">-Seleccione-</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Órgano:</label>
                                                    <input id="txtOrgano" type="text" maxlength="200" class="form-control" disabled />
                                                </div>
                                                <div class="col-md-2">
                                                    <label>N° de muestras por frasco:</label>
                                                    <select id="selMuestrasPorFrasco" class="form-control" disabled>
                                                        <option value="0">-Seleccione-</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value=">15">>15</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <br />
                                                    <a id="btnAgregar" disabled class="btn btn-success" style="margin-top: 5px;"><span class="glyphicon glyphicon-plus"></span></a>
                                                </div>
                                            </div>
                                            <hr />
                                            <table id="tblDetalle" class="table table-bordered  table-hover table-responsive"></table>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Derivado de:</label>
                                                    <div class="form-inline">
                                                        <select id="selTipoOrigen" class="form-control" disabled>
                                                            <option value="HCM">HCM</option>
                                                            <option value="Externo">Externo</option>
                                                        </select>
                                                        <select id="selServicioOrigen" class="form-control" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Lugar de destino:</label>
                                                    <div class="form-inline">
                                                        <select id="selTipoDestino" class="form-control" disabled>
                                                            <option value="HCM">HCM</option>
                                                            <option value="Externo">Externo</option>
                                                        </select>
                                                        <select id="selServicioDestino" class="form-control" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Programa:</label>
                                                    <select id="selPrograma" class="form-control" disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Solicitado por:</label>
                                                    <select id="selSolicitado" class="form-control" disabled>
                                                    </select>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Antecedentes Clinicos:</label>
                                                    <textarea id="txtAntecedentesClinicos" rows="4" maxlength="300" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button id="btnGuardar" class="btn btn-primary" style="display: none;">Guardar</button>
                                            <button id="btnCancelar" class="btn btn-warning">Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="divTabMacroscopia">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Patólogo:</label>
                                                    <select id="selPatologo" disabled class="form-control"></select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Tecnólogo:</label>
                                                    <select id="selTecnologo" disabled class="form-control"></select>
                                                </div>
                                                <div class="col-md-3">
                                                    <br />
                                                    <label class="containerCheck">
                                                        Dictado
                                                        <input id="chkDictado" type="checkbox" disabled />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="containerCheck">
                                                        Tumoral
                                                    <input id="chkTumoral" type="checkbox" disabled />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Tipo de Biopsia:</label>
                                                    <select id="selTipoBiopsia" disabled class="form-control"></select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Corte:</label>
                                                    <select id="selTipoMacroscopia" disabled class="form-control"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="divTabInterconsultas">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 col-md-offset-10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <wuc:Movimientos ID="wucmdlMovimientos" runat="server" />
</asp:Content>