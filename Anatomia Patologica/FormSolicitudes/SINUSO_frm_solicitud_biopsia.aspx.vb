﻿Imports System.Data

Partial Public Class frm_solicitud_biopsia

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then
            ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
            '==========================================================
            Dim com As New Combobox
            com.Get_Profesionales_combobox(Me.cmb_medico_solicita, "HCM")
            com.Get_ProgramasHCM_combobox(Me.cmb_programa)
            com.Get_ServiciosHCM_combobox(Me.cmb_servicio_sol, Me.cmb_tipo_origen.Text)
            com.Get_ServiciosHCM_combobox(Me.cmb_servicio_dest, Me.cmb_tipo_destino.Text)
            com.CatalogoBiopsias_combobox(Me.cmb_catalogo)
            com.DetalleCatalogoBiopsias_combobox(Me.cmb_detalle_catalogo, Me.cmb_catalogo.Text)

            Session("dt_registro_muestra") = Nothing

            Me.txt_fecha_ingreso.Text = Date.Today
            Me.txt_fecha_recepcion.Text = Date.Today

            Me.txt_biopsia_a.Enabled = False
            Me.txt_biopsia_n.Enabled = False
            Me.txt_nombre.Enabled = False
            Me.txt_ap_paterno.Enabled = False
            Me.txt_ap_materno.Enabled = False
            Me.txt_ficha.Enabled = False

            Dim ANA_IdBiopsiaBuscada As String = Request.QueryString("id")
            Dim id_solicitud As String = Request.QueryString("id_soli")

            Me.txt_id.Text = Request.QueryString("id")
            Me.txt_id_soli.Text = Request.QueryString("id_soli")

            Try
                If Me.txt_id.Text <> 0 Then

                    Me.txt_rut.Enabled = False
                    Me.txt_digito.Enabled = False

                    Dim rm As New Registro_Biopsia
                    rm.Registro_Biopsia(ANA_IdBiopsiaBuscada)

                    Me.txt_biopsia_n.Text = rm.Get_ANA_NumBiopsia()
                    Me.txt_biopsia_a.Text = rm.Get_ANA_AñoBiopsia()
                    'Me.txt_rut.Text = rm.Get_GEN_numero_documentoPaciente

                    ' *** INSTANCIA EL CONSTRUCTOR DE PACIENTES
                    '==========================================================
                    Dim p As New Pacientes

                    p.Pacientes(rm.Get_GEN_Id_Paciente)

                    Me.txt_nombre.Text = p.Get_GEN_Nombre()
                    Me.txt_ap_paterno.Text = p.Get_GEN_ApePaterno()
                    Me.txt_ap_materno.Text = p.Get_GEN_ApeMaterno()
                    Me.txt_nacionalidad.Text = p.Get_GEN_NacionalidadPaciente()
                    Me.txt_ficha.Text = p.Get_GEN_NuiPaciente()
                    Me.txt_rut.Text = p.Get_GEN_numero_documentoPaciente()
                    Me.txt_digito.Text = p.Get_GEN_digitoPaciente()
                    Me.txt_id_pac.Text = p.Get_GEN_IdPaciente()
                    Me.txt_nacimiento.Text = p.Get_GEN_FecNacimientoPaciente()

                    Me.cmb_tipo_paciente.Text = rm.Get_PAB_idModalidad()

                    Me.txt_fecha_ingreso.Text = rm.Get_ANA_fec_IngresoRegistro_Biopsias()
                    Me.txt_fecha_recepcion.Text = rm.Get_ANA_fec_RecepcionRegistro_Biopsias()

                    Me.cmb_catalogo.Text = rm.Get_ANA_IdCatalogoBiopsia()
                    Me.cmb_catalogo.DataBind()

                    com.DetalleCatalogoBiopsias_combobox(Me.cmb_detalle_catalogo, Me.cmb_catalogo.Text)
                    Me.cmb_detalle_catalogo.SelectedValue = rm.Get_ANA_IdDetalleCatalogoBiopsia()

                    Me.txt_organo.Text = rm.Get_ANA_OrganoBiopsia()

                    Me.cmb_muestras_frasco.Text = rm.Get_ANA_NMuestrasFrasco()

                    Me.cmb_ges.Text = rm.Get_ANA_GesRegistro_Biopsias()
                    Me.cmb_tipo_origen.Text = rm.Get_ANA_TipoOrigen()

                    com.Get_ServiciosHCM_combobox(Me.cmb_servicio_sol, Me.cmb_tipo_origen.Text)

                    Me.cmb_servicio_sol.Text = rm.Get_GEN_IdServicioOrigen()

                    Me.cmb_programa.DataBind()
                    Me.cmb_programa.Text = rm.Get_GEN_IdPrograma()

                    Me.cmb_tipo_destino.Text = rm.Get_ANA_TipoDestino()

                    com.Get_ServiciosHCM_combobox(Me.cmb_servicio_dest, Me.cmb_tipo_destino.Text)
                    Me.cmb_servicio_dest.Text = rm.Get_GEN_IdServicioDestino()
                    Me.txt_estado.Text = rm.Get_ANA_EstadoRegistro_Biopsias()

                    Me.cmb_medico_solicita.Text = rm.Get_ANA_IdMedSolicitaBiopsia()

                    CargaDetalleaTable(Me.txt_id.Text)

                End If
            Catch ex As Exception
                Me.lbl_mensaje.Visible = True
                Me.lbl_mensaje.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub cmd_guardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_guardar.Click

        If ValidarDatosIngreso() <> "Error" Then

            Try

                Dim consulta As String = "", ANA_OrganoBiopsia As String = Me.txt_organo.Text, cod_servicio_sol As String = Me.cmb_servicio_sol.SelectedValue
                Dim cod_medico As String = Me.cmb_medico_solicita.SelectedValue, cod_servicio_dest As String = Me.cmb_servicio_dest.SelectedValue
                Dim GEN_IdPrograma As String = Me.cmb_programa.SelectedValue
                Dim texto_cuadro_organo As String = "", id_biopsia As Long = Me.txt_id.Text

                ' *** INSTANCIA EL CONSTRUCTOR DE REGISTRO BIOPSIA POR ID REGISTRO BIOPSIA
                '==========================================================
                Dim rm As New Registro_Biopsia
                rm.Set_ANA_IdBiopsia(Me.txt_id.Text)
                rm.Registro_Biopsia(Me.txt_id.Text)


                rm.Set_ANA_TipoDestino(Me.cmb_tipo_destino.Text)
                rm.Set_ANA_EstadoRegistro_Biopsias(Me.txt_estado.Text)
                rm.Set_ANA_fec_IngresoRegistro_Biopsias(Me.txt_fecha_ingreso.Text)
                rm.Set_ANA_fec_RecepcionRegistro_Biopsias(Me.txt_fecha_recepcion.Text)
                rm.Set_ANA_IdBiopsia(Me.txt_id.Text)
                rm.Set_ANA_IdCatalogoBiopsia(Me.cmb_catalogo.Text)
                rm.Set_ANA_IdDetalleCatalogoBiopsia(Me.cmb_detalle_catalogo.Text)
                rm.Set_ANA_IdMedSolicitaBiopsia(cod_medico)

                rm.Set_GEN_IdServicioOrigen(cod_servicio_sol)
                rm.Set_GEN_IdServicioDestino(cod_servicio_dest)
                rm.Set_ANA_EstadoRegistro_Biopsias(Me.txt_estado.Text)
                rm.Set_ANA_TipoOrigen(Me.cmb_tipo_origen.Text)

                rm.Set_GEN_idPaciente(Me.txt_id_pac.Text)
                rm.Set_GEN_IdPrograma(GEN_IdPrograma)

                rm.Set_PAB_idModalidad(Me.cmb_tipo_paciente.Text)
                rm.Set_ANA_GesRegistro_Biopsias(Me.cmb_ges.Text)

                Dim mov As New movimientos

                If Me.txt_id.Text <> 0 Then

                    rm.Set_ANA_OrganoBiopsia(Me.txt_organo.Text)
                    rm.Set_ModificaRegistroBiopsia()

                    '*** CREA MOVIMIENTO REALIZADO
                    '==========================================================

                    mov.Set_ANA_IdBiopsia(Me.txt_id.Text)
                    mov.Set_ANA_DetalleMovimiento("Se modifica Registro de Biopsia")
                    mov.Set_GEN_idTipo_Movimientos_Sistemas(107)
                    mov.Set_GEN_IdUsuarios(Session("id_usuario"))
                    mov.Set_CrearNuevoMoviento()

                    recorre_ingresa_detalle_muestra()
                Else
                    rm.Set_ANA_OrganoBiopsia(Me.txt_organo.Text)
                    rm.Set_GEN_IdUsuarios(Session("id_usuario"))
                    mov.Set_GEN_idTipo_Movimientos_Sistemas(57)
                    id_biopsia = rm.Set_Consulta_CrearRegistroBiopsia()

                    Me.txt_id.Text = id_biopsia
                    recorre_ingresa_detalle_muestra()



                End If

                mov.Set_GEN_IdUsuarios(Session("id_usuario"))
                mov.Set_CrearNuevoMoviento()
                Response.Redirect("frm_principal.aspx")

            Catch ex As Exception

                Me.lbl_mensaje.Visible = True
                Me.lbl_mensaje.Text = ex.Message

            End Try

        End If

        Exit Sub

    End Sub

    Protected Sub txt_rut_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txt_rut.TextChanged

        ' *** INSTANCIA EL CONSTRUCTOR DE PACIENTES
        '==========================================================
        Dim p As New Pacientes
        If IsNumeric(Me.txt_rut.Text) Then
            Me.txt_digito.Text = RutDigito(Me.txt_rut.Text)
            p.CargarDatosdesdeOrden(Me.txt_rut.Text, Me.txt_digito.Text)
        End If

        p.Pacientes(Me.txt_rut.Text)

        Me.txt_nombre.Text = p.Get_GEN_Nombre()
        Me.txt_ap_paterno.Text = p.Get_GEN_ApePaterno()
        Me.txt_ap_materno.Text = p.Get_GEN_ApeMaterno()
        Me.txt_nacionalidad.Text = p.Get_GEN_NacionalidadPaciente()
        Me.txt_ficha.Text = p.Get_GEN_NuiPaciente()
        Me.txt_rut.Text = p.Get_GEN_numero_documentoPaciente()
        Me.txt_digito.Text = p.Get_GEN_digitoPaciente()
        Me.txt_id_pac.Text = p.Get_GEN_IdPaciente()
        Me.txt_nacimiento.Text = p.Get_GEN_FecNacimientoPaciente()

    End Sub

    Protected Sub cmd_cancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_cancelar.Click
        Response.Redirect("frm_principal.aspx")
    End Sub

    Protected Sub btn_frasco_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_frasco.Click
        If Me.cmb_catalogo.SelectedValue = 0 Or cmb_detalle_catalogo.SelectedValue = 0 Then
            Me.lbl_mensaje.Text = "Debe seleccionar el catalogo y detalle de catalogo"
            Me.lbl_mensaje.Visible = True
        Else
            PoblarDataTable(Me.cmb_detalle_catalogo.Text, Me.cmb_detalle_catalogo.SelectedItem.Text, Me.txt_organo.Text, Me.cmb_muestras_frasco.Text)

            Me.lbl_mensaje.Visible = False
            Me.txt_organo.Focus()

        End If

    End Sub

    Public Function CreaDataTableDetalle() As DataTable

        Dim dt As New DataTable()

        dt.Columns.Add("Nº Frasco", GetType(String))
        dt.Columns.Add("Código Detalle", GetType(String))
        dt.Columns.Add("Detalle Muestra", GetType(String))
        dt.Columns.Add("Descripción", GetType(String))
        dt.Columns.Add("N° Muestras", GetType(String))

        Return dt

    End Function

    Protected Sub btn_frasco0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_frasco0.Click
        Dim rm As New Registro_Biopsia
        rm.Set_ANA_IdBiopsia(Me.txt_id.Text)

        'GridView1.DataBind()
        gdv_DetalleMuestra.DataBind()
        Session("dt_registro_muestra") = Nothing

    End Sub

    Private Function ValidarDatosIngreso()
        Dim mensaje As String = ""
        'VERIFICA CAMPOS INGRESADOS
        If Me.cmb_medico_solicita.SelectedValue = 0 Or cmb_detalle_catalogo.SelectedValue = 0 Or Me.txt_rut.Text = "" Or Me.txt_organo.Text = "" Or cmb_servicio_dest.SelectedValue = 0 Or cmb_servicio_sol.SelectedValue = 0 Then
            Me.lbl_mensaje.Visible = True
            Me.lbl_mensaje.Text = "Revise la información Ingresada"
            mensaje = "Error"
        End If

        'VERIFICA EN CASO QUE SEA UN INGRESO SIN SOLICITUD
        Dim flag As Byte = 0
        If Me.txt_id.Text = 0 And Me.txt_id_soli.Text = 0 Then
            If Me.gdv_DetalleMuestra.Rows.Count = 0 Then
                Me.lbl_mensaje.Visible = True
                Me.lbl_mensaje.Text = "Falta Ingresar la Muestra"
                mensaje = "Error"
            Else
                flag = 1 'PASO EL PRIMER FILTRO
            End If
        ElseIf Me.txt_id.Text > 0 Then
            flag = 1
        End If

        If flag = 0 Then
            Me.lbl_mensaje.Visible = True
            Me.lbl_mensaje.Text = "Falta Seleccionar la Muestra"
            mensaje = "Error"
        End If
        Return mensaje

    End Function

    Private Sub recorre_ingresa_detalle_muestra()

        Dim nrm As New NegocioRegistroMuestra
        nrm.Set_ANA_IdBiopsia(Me.txt_id.Text)
        nrm.Set_Consulta_EliminarDetalleMuestra()

        Dim frascos As String, cod_detalle As String, descripcion As String, muestras As String
        Dim texto_cuadro_organo As String = ""

        For Each row As GridViewRow In gdv_DetalleMuestra.Rows
            frascos = row.Cells(0).Text
            cod_detalle = row.Cells(1).Text
            descripcion = Page.Server.HtmlDecode(row.Cells(3).Text)

            If texto_cuadro_organo = "" Then
                texto_cuadro_organo = descripcion
            Else
                texto_cuadro_organo = texto_cuadro_organo & "; " & descripcion
            End If

            muestras = row.Cells(4).Text

            nrm.Set_Consulta_IngresaDetalleMuestras(frascos, cod_detalle, descripcion, muestras)

        Next
        Me.txt_organo.Text = texto_cuadro_organo

    End Sub

    Protected Sub cmb_tipo_origen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_tipo_origen.SelectedIndexChanged
        ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
        '==========================================================
        Dim cmb As New Combobox
        cmb.Get_Profesionales_combobox(Me.cmb_medico_solicita, Me.cmb_tipo_origen.Text)
        cmb.Get_ServiciosHCM_combobox(Me.cmb_servicio_sol, Me.cmb_tipo_origen.Text)

    End Sub

    Protected Sub cmb_tipo_destino_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_tipo_destino.SelectedIndexChanged
        ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
        '==========================================================
        Dim cmb As New Combobox
        cmb.Get_ServiciosHCM_combobox(Me.cmb_servicio_dest, Me.cmb_tipo_destino.Text)
    End Sub

    Protected Sub cmb_catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_catalogo.SelectedIndexChanged
        ' *** INSTANCIA EL CLASE COMBOBOX Y RELLENA COMBO SEGUN LO Q SE NECESITA
        '==========================================================
        Dim com As New Combobox
        com.DetalleCatalogoBiopsias_combobox(Me.cmb_detalle_catalogo, Me.cmb_catalogo.Text)
    End Sub

    Private Sub PoblarDataTable(ByVal ANA_IdDetalleCatalogo_Muestras As String, ByVal DetMuestra As String, ByVal Descripcion As String, ByVal NumMuestras As String)
        If Session("dt_registro_muestra") Is Nothing Then

            Dim dt As DataTable = CreaDataTableDetalle()
            Dim Row1 As DataRow
            Row1 = dt.NewRow()

            Row1("Nº Frasco") = 1
            Row1("Código Detalle") = ANA_IdDetalleCatalogo_Muestras
            Row1("Detalle Muestra") = DetMuestra
            Row1("Descripción") = Descripcion
            Row1("N° Muestras") = NumMuestras

            dt.Rows.Add(Row1)
            gdv_DetalleMuestra.Visible = True
            gdv_DetalleMuestra.DataSource = dt
            gdv_DetalleMuestra.DataBind()
            Session("dt_registro_muestra") = dt

        Else

            Dim dt As DataTable = TryCast(Session("dt_registro_muestra"), DataTable)
            Dim Row1 As DataRow
            Row1 = dt.NewRow()

            Row1("Nº Frasco") = dt.Rows.Count + 1
            Row1("Código Detalle") = ANA_IdDetalleCatalogo_Muestras
            Row1("Detalle Muestra") = DetMuestra
            Row1("Descripción") = Descripcion
            Row1("N° Muestras") = NumMuestras

            dt.Rows.Add(Row1)
            gdv_DetalleMuestra.DataSource = dt
            gdv_DetalleMuestra.DataBind()
            Session("dt_registro_muestra") = dt

        End If
    End Sub

    Private Sub CargaDetalleaTable(ByVal IdBiopsia As Integer)
        Dim nrm As New NegocioRegistroMuestra
        nrm.Set_ANA_IdBiopsia(Me.txt_id.Text)

        Dim dt As New DataTable
        dt = consulta_sql_datatable(nrm.Get_Cargar_Detalle_Muestra)

        Dim filas As Data.DataRow

        For i = 0 To dt.Rows.Count - 1
            filas = dt.Rows(i)
            PoblarDataTable(filas.Item("ANA_IdDetalleCatalogo_Muestras"), filas.Item("ANA_DetalleCatalogo_Muestras"), filas.Item("ANA_DescripcionBiopsia"), filas.Item("ANA_CantidadMuestras"))
        Next

    End Sub

End Class