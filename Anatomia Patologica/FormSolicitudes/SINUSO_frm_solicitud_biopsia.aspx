<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_solicitud_biopsia.aspx.vb" Inherits="Anatomia_Patologica.frm_solicitud_biopsia" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
 <div class="Titulo">Solicitud de Estudio Histopatol�gico y Citol�gico</div>
    <div style="margin:0 auto; text-align: 0 center">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table>
                    <tr class="cambia_tr">
                        <th style="text-align:left">N� de Registro:</th>
                        <td>
                            <div style="float:left; vertical-align:middle;">
                                <asp:TextBox ID="txt_biopsia_n" runat="server" Width="51px">
                                </asp:TextBox>
                                <asp:Label ID="Label1" runat="server" Text="  /  "></asp:Label>
                                <asp:TextBox ID="txt_biopsia_a" runat="server" Width="51px">
                                </asp:TextBox>                                    
                            </div>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Fecha de Ingreso:</th>
                        <td>
                            <asp:TextBox ID="txt_fecha_ingreso" runat="server" MaxLength="10" ReadOnly="True" Width="120px" Enabled="False">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Fecha de Muestra:</th>
                        <td>
                            <asp:TextBox ID="txt_fecha_recepcion" runat="server" MaxLength="10" Width="120px">
                            </asp:TextBox>
                            <asp:CalendarExtender ID="txt_fecha_recepcion_CalendarExtender0" runat="server" Enabled="True" TargetControlID="txt_fecha_recepcion">
                            </asp:CalendarExtender>
                            <asp:CalendarExtender ID="txt_fecha_recepcion_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_fecha_recepcion">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">N� de Documento:</th>
                        <td>
                            <asp:TextBox ID="txt_rut" runat="server" AutoPostBack="True" MaxLength="10" TabIndex="1" Width="120px">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_digito" runat="server" Enabled="False" ReadOnly="True" Width="17px">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_id" runat="server" Width="37px" Visible="False">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_id_pac" runat="server" Visible="False" Width="37px">
                            </asp:TextBox>
                            <asp:TextBox ID="txt_id_soli" runat="server" Width="37px" Visible="False">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="validador1" runat="server" ControlToValidate="txt_rut" ErrorMessage="Falta Informaci�n">
                                Falta Informaci�n
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">N� de Ubicaci�n Int.:</th>
                        <td>
                            <div>
                                <asp:TextBox ID="txt_ficha" runat="server" MaxLength="10" TabIndex="2" Width="120px">
                                </asp:TextBox>
                                <asp:Label ID="Label2" runat="server" Text="Fecha de Nacimiento:" Font-Bold="true" ></asp:Label>                                    
                                <asp:TextBox ID="txt_nacimiento" runat="server" MaxLength="10" TabIndex="2" Width="120px" Enabled="False">
                                </asp:TextBox>                                    
                            </div>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Nombre:</th>
                        <td>
                            <asp:TextBox ID="txt_nombre" runat="server" MaxLength="100" Width="400px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Ap. Paterno:</th>
                        <td>
                            <asp:TextBox ID="txt_ap_paterno" runat="server" MaxLength="100" Width="400px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Ap. Materno:</th>
                        <td>
                            <asp:TextBox ID="txt_ap_materno" runat="server" MaxLength="100" Width="400px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Nacionalidad:</th>
                        <td style="text-align:left">
                            <asp:TextBox ID="txt_nacionalidad" runat="server" Enabled="False" MaxLength="100" Width="400px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Tipo de Paciente:</th>
                        <td>
                            <asp:DropDownList ID="cmb_tipo_paciente" style="font-size: medium"  Width="400px" runat="server">
                            <asp:ListItem>Institucional</asp:ListItem>
                            <asp:ListItem>Privado</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Tipo de Muestra:</th>
                        <td>
                            <asp:DropDownList ID="cmb_catalogo" runat="server" AutoPostBack="True" style="font-size: medium" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Detalle de Muestra:</th>
                        <td>
                            <asp:DropDownList ID="cmb_detalle_catalogo" runat="server" Width="400px" style="font-size: medium">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Organo:</th>
                        <td>
                            <asp:TextBox ID="txt_organo" runat="server" MaxLength="100" Width="400px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">N� de Muestras por Frasco:</th>
                        <td>
                            <asp:DropDownList ID="cmb_muestras_frasco" runat="server" Width="200px">
                            <asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>>15</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btn_frasco" runat="server" Text="Nueva Muestra" CssClass="BtnV-Grande" />
                            <asp:Button ID="btn_frasco0" runat="server" CssClass="BtnR-Grande" Text="Eliminar Muestras" />
                            <asp:ConfirmButtonExtender ID="btn_frasco0_ConfirmButtonExtender" 
                                runat="server" ConfirmText="�Esta seguro que desea eliminar las muestras de forma definitiva?" 
                                Enabled="True" TargetControlID="btn_frasco0">
                            </asp:ConfirmButtonExtender>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <td>&nbsp;</td>
                        <td>
                            <asp:GridView ID="gdv_DetalleMuestra" runat="server" CellPadding="4" 
                                ForeColor="#333333" GridLines="None" Visible="False" Width="632px" >
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>                        
                    </td>
                </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">GES:</th>
                        <td style="text-align:left">
                            <asp:DropDownList ID="cmb_ges" runat="server" Width="400px">
                            <asp:ListItem>SI</asp:ListItem><asp:ListItem Selected="True">NO</asp:ListItem></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Lugar de Origen:</th>
                        <td style="text-align:left">
                            <asp:DropDownList ID="cmb_tipo_origen" runat="server" AutoPostBack="True" Width="100px">
                                <asp:ListItem>HCM</asp:ListItem><asp:ListItem>Externo</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="cmb_servicio_sol" runat="server" DataTextField="nombre" 
                                DataValueField="IdSERVICIO" Width="300px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Programa:</th>
                        <td style="text-align:left">
                            <asp:DropDownList ID="cmb_programa" runat="server" Width="400px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Lugar de Destino:</th>
                        <td style="text-align:left">
                            <asp:DropDownList ID="cmb_tipo_destino" runat="server" AutoPostBack="True" Width="100px">
                                <asp:ListItem>HCM</asp:ListItem><asp:ListItem>Externo</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="cmb_servicio_dest" runat="server" DataTextField="nombre" 
                                DataValueField="IdSERVICIO" Width="300px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="cambia_tr">
                        <th style="text-align:left">Solicitado por:</th>
                        <td align="left" style="text-align: left">
                            <asp:DropDownList ID="cmb_medico_solicita" runat="server" Width="400px" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align:left">Hip�tesis Diagn�stica:</th>
                        <td style="text-align:left">
                            <asp:TextBox ID="TextBox1" runat="server" Height="116px" TextMode="MultiLine" 
                                Width="402px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align:left">Antecedentes Cl�nicos Relevantes:</th>
                        <td style="text-align:left">
                            <asp:TextBox ID="TextBox2" runat="server" Height="116px" TextMode="MultiLine" 
                                Width="402px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left">
                            &nbsp;</td>
                        <td style="text-align:left">
                            <asp:TextBox ID="txt_estado" runat="server" ReadOnly="True" Visible="False" 
                                Width="400px">Ingresado</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left">&nbsp;</td>
                        <td style="text-align:left">
                            <asp:Button ID="cmd_guardar" runat="server" Text="Guardar" CssClass="BtnA-Grande" />
                            <asp:Button ID="cmd_cancelar" runat="server" Text="Cancelar" CssClass="BtnR-Grande" />
                            <br />
                            <asp:RequiredFieldValidator ID="validador3" runat="server" ControlToValidate="txt_organo" ErrorMessage="Debe Indicar el Organo">
                                Debe Indicar el Organo
                            </asp:RequiredFieldValidator>
                            <asp:Label ID="lbl_mensaje" runat="server" CssClass="LblMsjAdvertencia" Text="Label" Visible="False">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True">
                </asp:ToolkitScriptManager>                
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
</asp:Content>