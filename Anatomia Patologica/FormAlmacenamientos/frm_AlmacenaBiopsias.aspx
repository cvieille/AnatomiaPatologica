﻿<%@ Page Title="Almacenamiento de Biopsias" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_AlmacenaBiopsias.aspx.vb" Inherits="Anatomia_Patologica.frm_AlmacenaBiopsias" %>

<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_biopsias").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_biopsias").find("tr:first"))).dataTable();
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">

    <!-- Panel Principal -->
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h2 class="text-center">Muestras para Almacenar</h2>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="text-right" style="margin-bottom: 10px;">
                        <asp:Button ID="cmd_exportarBiopsia" runat="server" Text="Exportar" CssClass="btn btn-success" />
                    </div>
                    <asp:GridView ID="gdv_biopsias" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dts_ListaBiopsias" CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:BoundField DataField="n_biopsia" HeaderText="Nº" SortExpression="n_biopsia" />
                            <asp:BoundField DataField="ANA_OrganoBiopsia" HeaderText="Organo" SortExpression="ANA_OrganoBiopsia" />
                            <asp:BoundField DataField="ANA_TumoralBiopsia" HeaderText="Tum." SortExpression="ANA_TumoralBiopsia" />
                            <asp:BoundField DataField="ANA_IdBiopsia" HeaderText="Id Biopsia" SortExpression="ANA_IdBiopsia" ReadOnly="True" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="cmd_AlmacenarMacro" runat="server" Text="Almacenar" CssClass="btn btn-success"
                                        CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_AlmacenarMacro_Click"
                                        OnClientClick="return confirm('¿Esta seguro que desea almacenar esta Muestra?');" />
                                    <asp:Button ID="cmd_VerBiopsia" runat="server" Text="Ver" CssClass="btn btn-info"
                                        CommandArgument='<%# Eval("ANA_IdBiopsia") %>' OnClick="cmd_VerBiopsia_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>             
                    <br />
                    <table class="DivFullCentrado" style="display: none;">
                        <tr>                           
                            <td>
                                <asp:Button ID="cmd_desechar" runat="server" Text="Desechar" CssClass="btn btn-warning" Visible="false" />
                            </td>                       
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="cmd_exportarBiopsia" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
         
    <br />
    <!----- PANEL POPUP COMO MENSAJE DE ALERTA ----->
    <wuc:Alert ID="wuc_alert" runat="server" />
    <br />
    <asp:SqlDataSource ID="dts_ListaBiopsias" runat="server"
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
