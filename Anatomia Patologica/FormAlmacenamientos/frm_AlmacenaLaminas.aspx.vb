﻿Partial Public Class frm_AlmacenaLaminas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
        If IsPostBack = False Then
            ' *** INSTANCIA EL CLASE PERFIL
            '==========================================================
            Dim per As New Perfil
            If Not (Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Supervisor") Or Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Administrador del Sistema")) Then
                Me.cmd_desechar.Enabled = False
            End If

            Me.filtros_grilla()
        End If
    End Sub

    'Protected Sub cmd_almacenar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_almacenar.Click
    '    Dim cantidad As Integer = 0
    '    Try
    '        For Each row As GridViewRow In gdv_Laminas.Rows

    '            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
    '            If elcheckbox.Checked Then
    '                Dim NL As New NegocioLaminas
    '                NL.Laminas(row.Cells(4).Text)
    '                If NL.Get_ANA_Almacenada() = "NO" Then
    '                    cantidad = cantidad + 1
    '                    row.Cells(3).Text = "Almacenada"
    '                    ' *** METODO QUE ALMACENA LAMINAS
    '                    '==========================================================
    '                    NL.Set_GEN_IdUsuarios(Session("id_usuario"))
    '                    NL.Set_AlmacenarLaminas()
    '                    '==========================================================

    '                ElseIf NL.Get_ANA_Almacenada() = "SI" And elcheckbox.Checked Then
    '                    wuc_alert.showModal(Alert.ADVERTENCIA, "¡Existen muestras que ya se encontraban almacenadas!")
    '                End If
    '            End If

    '        Next
    '        If cantidad > 0 Then
    '            wuc_alert.showModal(Alert.ACCION_REALIZADA, "¡Se han almacenado " & cantidad & " lamina(s)")
    '        End If
    '        Me.filtros_grilla()
    '    Catch ex As Exception
    '        wuc_alert.showModal(Alert._ERROR, ex.Message & " " & ex.StackTrace)
    '    End Try
    'End Sub

    Protected Sub cmd_desechar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_desechar.Click

        For Each row As GridViewRow In gdv_Laminas.Rows

            Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

            Dim NL As New NegocioLaminas
            NL.Laminas(row.Cells(7).Text)

            If NL.Get_ANA_Almacenada() = "SI" And elcheckbox.Checked Then

                Try
                    row.Cells(3).Text = "Desechada"
                    row.Cells(4).Text = "NO"
                    row.Cells(5).Text = "SI"
                    row.Cells(6).Text = "NO"
                    ' *** METODO QUE DESECHA LAMINAS
                    '==========================================================
                    NL.Set_GEN_IdUsuarios(Session("id_usuario"))
                    NL.Set_DesecharLaminas()
                    '==========================================================

                Catch ex As Exception
                    Me.lbl_mensaje.Visible = True
                    Me.lbl_mensaje.Text = ex.Message
                End Try

            ElseIf NL.Get_ANA_Almacenada() = "NO" And elcheckbox.Checked Then
                Me.lbl_mensaje.Visible = True
                Me.lbl_mensaje.Text = "¡Solo se pueden desechar las muestras que se encuentren almacenadas!"
            End If
        Next

    End Sub

    Private Sub filtros_grilla()
        ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
        '==========================================================
        Dim con As New Consultas
        dts_laminas.SelectCommand = con.GetLaminasparaAlmacenar()
        dts_laminas.DataBind()
        gdv_Laminas.DataBind()

    End Sub

    Protected Sub cmd_imprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_imprimir.Click

        Try
            Session("dt_Impr_MovLamina") = Nothing

            Dim dt As New DataTable()

            dt.Columns.Add("Lamina", GetType(String))
            dt.Columns.Add("Fecha", GetType(String))
            dt.Columns.Add("Estado", GetType(String))
            dt.Columns.Add("Alm.", GetType(String))
            dt.Columns.Add("Des.", GetType(String))
            dt.Columns.Add("Sol.", GetType(String))

            Dim Row1 As DataRow

            For Each row As GridViewRow In gdv_Laminas.Rows
                Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)

                If elcheckbox.Checked Then
                    Row1 = dt.NewRow()
                    Row1("Lamina") = row.Cells(1).Text
                    Row1("Fecha") = row.Cells(2).Text
                    Row1("Estado") = row.Cells(3).Text
                    Row1("Alm.") = row.Cells(4).Text
                    Row1("Des.") = row.Cells(5).Text
                    Row1("Sol.") = row.Cells(6).Text
                    dt.Rows.Add(Row1)
                End If
            Next

            Session("dt_Impr_MovLamina") = dt
            Dim url As String = "../FormImpresion/frm_SeleccionLaminas.aspx"
            Dim s As String = "window.open('" & url + "', 'popup_window', 'width=600,height=500,left=200,top=100,resizable=yes');"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", s, True)

        Catch ex As Exception
            wuc_alert.showModal(Alert._ERROR, ex.Message & " " & ex.StackTrace)
        End Try

    End Sub

    Protected Sub cmd_seleccionar_todo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_Laminas, "chkSeleccion", True)
    End Sub

    Protected Sub cmd_seleccionar_todo0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo0.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_Laminas, "chkSeleccion", False)
    End Sub


End Class