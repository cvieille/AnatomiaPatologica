<%@ Page Title="Almacena Casete" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/MasterCrAnatomia.Master" CodeBehind="frm_AlmacenaCasete.aspx.vb" Inherits="Anatomia_Patologica.frm_AlmacenaCasete" %>
<%@ Register Src="~/ControlesdeUsuario/Alert/Alert.ascx" TagName="Alert" TagPrefix="wuc" %>
<%@ Register Src="~/ControlesdeUsuario/Modal_Cargando.ascx" TagName="Modal_Cargando" TagPrefix="wuc" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function iniciarComponentes() {
            $("#ctl00_Cnt_Principal_gdv_casete").prepend($("<thead></thead>").append($("#ctl00_Cnt_Principal_gdv_casete").find("tr:first"))).dataTable();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cnt_Principal" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server">
                <ProgressTemplate>
                    <wuc:Modal_Cargando ID="wuc_modal_cargando" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <!-- Panel Principal Casete para Almacenar-->
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="text-center">Casete para Almacenar</h2>
                </div>
                <div class="panel-body">                    
                    <asp:GridView ID="gdv_casete" runat="server" AutoGenerateColumns="False"
                        DataSourceID="sql_ds_cortes" CssClass="table table-bordered table-hover table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccion" runat="server" CssClass="controlSeleccion" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="ANA_IdBiopsia"
                                DataNavigateUrlFormatString="~/FormAnatomia/frm_cortes.aspx?id={0}"
                                DataTextField="ANA_NomCortes_Muestras" HeaderText="N� Corte" Text="N� Corte" />
                            <asp:BoundField DataField="ANA_FecCortes_Muestras" DataFormatString="{0:dd-MM-yyyy}"
                                HeaderText="Fecha" ReadOnly="True" SortExpression="ANA_FecCortes_Muestras" />                            
                            <asp:BoundField DataField="GEN_nombreTipo_Estados_Sistemas" HeaderText="Estado" ReadOnly="True"
                                SortExpression="GEN_nombreTipo_Estados_Sistemas" />                            
                            <asp:BoundField DataField="ANA_SolporCortes_Muestras" HeaderText="Solicitado Por"
                                SortExpression="ANA_SolporCortes_Muestras" />
                            <asp:BoundField DataField="ANA_IdCortes_Muestras" HeaderText="ANA_IdCortes_Muestras" />                            

                        </Columns>
                    </asp:GridView>
                    <div style="text-align: center">
                        <asp:Button ID="cmd_seleccionar_todo" runat="server" CssClass="btn btn-default"
                            Text="Seleccionar Todo" />
                        <asp:Button ID="cmd_seleccionar_todo0" runat="server"
                            CssClass="btn btn-default" Text="Quitar Selecci�n" />
                        <div class="alert alert-info" style="margin: 0px auto; text-align: center; padding: 5px">
                            <asp:Button ID="cmd_almacenar" runat="server" CssClass="btn btn-success"
                                OnClientClick="return confirm('�Esta seguro que desea almacenar este(os) casete(s)?');" Text="Almacenar" />
                            <asp:Button ID="cmd_desechar" runat="server" CssClass="btn btn-danger"
                                OnClientClick="return confirm('�Esta seguro que desea desechar este(os) casete (s)?');" Text="Desechar" />
                            <asp:Button ID="cmd_imprimir" runat="server" CssClass="btn btn-warning" Text="Imprimir Selecci�n" />
                        </div>
                    </div>
                </div>
            </div>
            <p style="text-align: center">
                <asp:Label ID="lbl_mensaje" runat="server" Text="Label" Visible="False" CssClass="alert alert-info">
                </asp:Label>
            </p>
            <br />

            <wuc:Alert ID="wuc_alert" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="sql_ds_cortes" runat="server"
        ConnectionString="<%$ ConnectionStrings:anatomia_patologicaConnectionString %>"></asp:SqlDataSource>
    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(iniciarComponentes);
    </script>
</asp:Content>
