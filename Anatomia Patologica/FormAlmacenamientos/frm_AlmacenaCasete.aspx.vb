﻿Partial Public Class frm_AlmacenaCasete
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("ID_USUARIO")) Then
            Response.Redirect("../frm_login.aspx")
        End If
        If IsPostBack = False Then
            ' *** METODO HACE LA CARGA DE DATOS SEGUN CHECKBOX
            '==========================================================
            CargarGrilla()

            ' *** INSTANCIA EL CLASE PERFIL
            '==========================================================
            Dim per As New Perfil
            If Not (Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Supervisor") Or Session("GEN_CodigoPerfil") = per.devuelve_perfil_por_tipo("Administrador del Sistema")) Then
                Me.cmd_desechar.Enabled = False
            End If
        End If
    End Sub
    Private Sub CargarGrilla()

        ' *** INSTANCIA CLASE CONSULTAS. TRAE LOS SQL QUE VAN A POBLAR GRILLA
        '==========================================================
        Dim con As New Consultas
        sql_ds_cortes.SelectCommand = con.GetCaseteparaAlmacenar()
        gdv_casete.DataBind()

        OcultarColumna(gdv_casete, False, 5)

    End Sub

    Protected Sub cmd_imprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_imprimir.Click
        Try
            Session("dt_Impr_casete") = Nothing

            Dim dt As New DataTable()

            dt.Columns.Add("Nº Corte", GetType(String))
            dt.Columns.Add("Fecha", GetType(String))
            dt.Columns.Add("Patologo", GetType(String))
            dt.Columns.Add("Estado", GetType(String))
            dt.Columns.Add("Alm.", GetType(String))
            dt.Columns.Add("Sol.", GetType(String))
            dt.Columns.Add("Des.", GetType(String))
            dt.Columns.Add("Solicitado Por", GetType(String))

            Dim Row1 As DataRow

            For Each row As GridViewRow In gdv_casete.Rows
                Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
                Dim c As New Cortes
                c.Cortes(row.Cells(10).Text)
                If elcheckbox.Checked Then
                    Row1 = dt.NewRow()
                    Row1("Nº Corte") = c.Get_ANA_NomCortes_Muestras()
                    Row1("Fecha") = c.Get_ANA_FecCortes_Muestras()
                    Row1("Patologo") = row.Cells(3).Text
                    Row1("Estado") = c.Get_ANA_EstCortes_Muestras()
                    Row1("Alm.") = c.Get_ANA_Almacenada()
                    Row1("Sol.") = c.Get_ANA_Solicitada
                    Row1("Des.") = c.Get_ANA_Desechada
                    Row1("Solicitado Por") = c.Get_ANA_SolporCortes_Muestras()

                    dt.Rows.Add(Row1)

                End If
            Next

            Session("dt_Impr_MovCasete") = dt

            Dim url As String = "../FormImpresion/frm_SeleccionCasete.aspx"
            Dim s As String = "window.open('" & url + "', 'popup_window', 'width=600,height=500,left=200,top=100,resizable=yes');"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "script", s, True)
            UpdatePanel1.Update()


        Catch ex As Exception
            wuc_alert.showModal(Alert._ERROR, ex.Message & " " & ex.StackTrace)
        End Try

    End Sub

    Protected Sub cmd_seleccionar_todo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_casete, "chkSeleccion", True)
    End Sub

    Protected Sub cmd_seleccionar_todo0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_seleccionar_todo0.Click
        ' *** LLAMA A METODO QUE RECORRE GRILLA Y SELECCIONA O QUITA SELECCION SEGUN VALOR "ACTIVO"
        '==========================================================
        selecciona_todo_grilla(gdv_casete, "chkSeleccion", False)
    End Sub

    'Private Sub estados_corte(ByVal accion As String)
    '    Dim glosa As String = ""

    '    For Each row As GridViewRow In gdv_casete.Rows

    '        Dim elcheckbox As CheckBox = CType(row.FindControl("chkSeleccion"), CheckBox)
    '        Try

    '            If elcheckbox.Checked Then
    '                '*** INSTANCIA CLASE Y CARGA ID DE CORTE
    '                '==========================================================
    '                Dim NC As New NegocioCortes
    '                NC.Cortes(row.Cells(5).Text)
    '                NC.Set_GEN_IdUsuarios(Session("id_usuario"))
    '                If accion = "Almacenar" Then
    '                    '*** ALMACENA CASETE
    '                    '==========================================================
    '                    If NC.Get_GEN_idTipo_Estados_Sistemas() = 55 Then
    '                        'ESTA PARA ALMACENAR
    '                        NC.Set_AlmacenarCasete()
    '                        wuc_alert.showModal(Alert.ACCION_REALIZADA, "¡Se han almacenado las muestras seleccionadas!")
    '                    ElseIf NC.Get_GEN_idTipo_Estados_Sistemas() = 52 Then
    '                        'YA ESTABA ALMACENADA
    '                        wuc_alert.showModal(Alert.INFORMACION, "¡Existen muestras que ya se encontraban almacenadas!")
    '                    End If

    '                ElseIf accion = "Desechar" Then
    '                    '*** DESECHA CASETE
    '                    '==========================================================
    '                    If Trim(NC.Get_ANA_Desechada) = "NO" And Trim(NC.Get_ANA_Almacenada()) = "SI" And elcheckbox.Checked Then
    '                        NC.Set_DesecharCasete()


    '                    ElseIf Trim(NC.Get_ANA_Desechada) = "SI" And elcheckbox.Checked Then
    '                        Me.lbl_mensaje.Visible = True
    '                        Me.lbl_mensaje.Text = "¡Solo se pueden desechar las muestras que se encuentren almacenadas!"

    '                    ElseIf Trim(NC.Get_ANA_Almacenada()) = "NO" And elcheckbox.Checked Then
    '                        wuc_alert.showModal(Alert._ERROR, "¡Solo se pueden desechar las muestras que se encuentren almacenadas!")
    '                    End If
    '                End If

    '                ' *** METODO HACE LA CARGA DE DATOS SEGUN CHECKBOX
    '                '==========================================================
    '                CargarGrilla()
    '                CargarGrilla()
    '            End If
    '        Catch ex As Exception
    '            wuc_alert.showModal(Alert._ERROR, ex.Message & " " & ex.StackTrace)
    '        End Try

    '    Next
    '    CargarGrilla()
    'End Sub

    Private Sub grilla_casete_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdv_casete.PageIndexChanging
        ' *** METODO HACE LA CARGA DE DATOS SEGUN CHECKBOX
        '==========================================================
        CargarGrilla()
    End Sub
End Class